# Unfinished project/nepabaigtas projektas
---
Tvarkingiausia versija(kuriai reikia daug refactoring'o) yra [4.0.0 A1](https://gitlab.com/Rls.LT/cSharp_GM/tree/4.0.0_A1), visa kita tik bandymas perrašyti kodą. 

# Paleidimas ant windows
---
Kadangi visi failai yra git'e, tai parsisiuntus turėtų iškart veikti. 

.sql failo nebeturiu.

# Fremework
---
[SampSharp](http://sampsharp.timpotze.nl/)

# License
---
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

P.S. Nei vienas failuose rastas prisijugnimas prie duomenų bazių neveiks, net netikrinkit :)))