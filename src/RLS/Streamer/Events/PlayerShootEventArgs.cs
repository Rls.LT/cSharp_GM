﻿using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.World;

namespace RLS.Streamer.Events
{
    public class PlayerShootEventArgs : PlayerEventArgs
    {
        public PlayerShootEventArgs(BasePlayer player, Weapon weapon, Vector3 position)
            : base(player)
        {
            Weapon = weapon;
            Position = position;
        }

        public bool PreventDamage { get; set; }

        public Weapon Weapon { get; }

        public Vector3 Position { get; }
    }
}
