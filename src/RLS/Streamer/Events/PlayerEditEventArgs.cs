﻿using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.World;

namespace RLS.Streamer.Events
{
    public class PlayerEditEventArgs : PositionEventArgs
    {
        public PlayerEditEventArgs(BasePlayer player, EditObjectResponse response,
            Vector3 position, Vector3 rotation) : base(position)
        {
            Player = player;
            Response = response;
            Rotation = rotation;
        }

        public BasePlayer Player { get; }

        public EditObjectResponse Response { get; set; }

        public Vector3 Rotation { get; }
    }
}
