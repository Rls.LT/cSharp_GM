﻿using SampSharp.GameMode;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.World;

namespace RLS.Streamer.Events
{
    public class PlayerSelectEventArgs : PlayerEventArgs
    {
        public PlayerSelectEventArgs(BasePlayer player, int modelid, Vector3 position)
            : base(player)
        {
            ModelId = modelid;
            Position = position;
        }

        public int ModelId { get; }

        public Vector3 Position { get; }
    }
}
