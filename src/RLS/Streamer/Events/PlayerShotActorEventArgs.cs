﻿using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.World;

namespace RLS.Streamer.Events
{
    public class PlayerShotActorEventArgs : PlayerEventArgs
    {
        public PlayerShotActorEventArgs(BasePlayer player, Weapon weapon, float amount, BodyPart bodyPart)
            : base(player)
        {
            Weapon = weapon;
            Amount = amount;
            BodyPart = bodyPart;
        }

        public Weapon Weapon { get; }

        public float Amount { get; }

        public BodyPart BodyPart { get; }
    }
}
