﻿using SampSharp.GameMode;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.Pools;
using SampSharp.GameMode.World;
using System;
using System.Linq;

namespace RLS.Streamer
{
    public partial class DynamicPickup : IdentifiedPool<DynamicPickup>
    {
        public Vector3 Position { get; private set; }

        public DynamicPickup(int modelid, int type, Vector3 position, int worldid = -1, int interiorid = -1,
            BasePlayer player = null, float streamdistance = 100.0f, DynamicArea area = null, int priority = 0)
        {
            Position = position;
            Id = plugin.CreateDynamicPickup(modelid, type, position.X, position.Y, position.Z, worldid,
                interiorid, player?.Id ?? -1, streamdistance, area?.Id ?? -1, priority);
        }

        public DynamicPickup(int modelid, int type, Vector3 position, float streamdistance, int[] worlds = null,
            int[] interiors = null, BasePlayer[] players = null, DynamicArea[] areas = null, int priority = 0)
        {
            Position = position;
            if (worlds == null) worlds = new[] { -1 };
            if (interiors == null) interiors = new[] { -1 };
            var pl = players?.Select(p => p.Id).ToArray() ?? new[] { -1 };
            var ar = areas?.Select(a => a.Id).ToArray() ?? new[] { -1 };

            Id = plugin.CreateDynamicPickupEx(modelid, type, position.X, position.Y, position.Z, streamdistance,
                worlds, interiors, pl, ar, priority, worlds.Length, interiors.Length, pl.Length, ar.Length);
        }

        public bool IsValid => plugin.IsValidDynamicPickup(Id);
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            plugin.DestroyDynamicPickup(Id);
        }

        public event EventHandler<PlayerEventArgs> PickedUp;

        public virtual void OnPickedUp(PlayerEventArgs e)
        {
            PickedUp?.Invoke(this, e);
        }
    }
}
