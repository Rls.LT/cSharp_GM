﻿using SampSharp.GameMode;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.Pools;
using SampSharp.GameMode.World;
using System;
using System.Linq;

namespace RLS.Streamer
{
    partial class DynamicCheckpoint : IdentifiedPool<DynamicCheckpoint>
    {
        public DynamicCheckpoint(Vector3 position, float size = 1.0f, int worldid = -1, int interiorid = -1,
            BasePlayer player = null, float streamdistance = 100.0f, DynamicArea area = null, int priority = 0)
        {
            this.position = position;
            Id = plugin.CreateDynamicCP(position.X, position.Y, position.Z, size, worldid, interiorid,
                player?.Id ?? -1, streamdistance, area?.Id ?? -1, priority);
        }

        public DynamicCheckpoint(Vector3 position, float size, float streamdistance, int[] worlds = null,
            int[] interiors = null, BasePlayer[] players = null, DynamicArea[] areas = null, int priority = 0)
        {
            if (worlds == null) worlds = new[] { -1 };
            if (interiors == null) interiors = new[] { -1 };
            var pl = players?.Select(p => p.Id).ToArray() ?? new[] { -1 };
            var ar = areas?.Select(a => a.Id).ToArray() ?? new[] { -1 };
            this.position = position;
            Id = plugin.CreateDynamicCPEx(position.X, position.Y, position.Z, size, streamdistance, worlds, interiors,
                pl, ar, priority, worlds.Length, interiors.Length, pl.Length, ar.Length);
        }
        public bool IsValid => plugin.IsValidDynamicCP(Id);

        private Vector3 position = Vector3.Zero;
        public Vector3 Position => position;

        public void ToggleForPlayer(BasePlayer player, bool toggle)
        {
            AssertNotDisposed();

            if (player == null)
                throw new ArgumentNullException(nameof(player));

            plugin.TogglePlayerDynamicCP(player.Id, Id, toggle);
        }
        public bool IsPlayerInside(BasePlayer player)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            return plugin.IsPlayerInDynamicCP(player.Id, Id);
        }
        public static void ToggleAllForPlayer(BasePlayer player, bool toggle)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            plugin.TogglePlayerAllDynamicCPs(player.Id, toggle);
        }

        public static DynamicCheckpoint GetPlayerVisibleDynamicCheckpoint(BasePlayer player)
        {
            var id = plugin.GetPlayerVisibleDynamicCP(player.Id);

            return id < 0 ? null : FindOrCreate(id);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            plugin.DestroyDynamicCP(Id);
        }

        public event EventHandler<PlayerEventArgs> Enter;

        public event EventHandler<PlayerEventArgs> Leave;

        public virtual void OnEnter(PlayerEventArgs e)
        {
            Enter?.Invoke(this, e);
        }

        public virtual void OnLeave(PlayerEventArgs e)
        {
            Leave?.Invoke(this, e);
        }
    }
}
