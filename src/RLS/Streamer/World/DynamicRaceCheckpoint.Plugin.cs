﻿using SampSharp.GameMode.API.NativeObjects;

namespace RLS.Streamer
{
    partial class DynamicRaceCheckpoint
    {
        protected static readonly DynamicRaceCheckpointPlugin plugin;

        static DynamicRaceCheckpoint()
        {
            plugin = NativeObjectProxyFactory.CreateInstance<DynamicRaceCheckpointPlugin>();
        }

        public class DynamicRaceCheckpointPlugin
        {
            [NativeMethod]
            public virtual int CreateDynamicRaceCP(int type, float x, float y, float z, float nextx, float nexty,
                float nextz, float size, int worldid, int interiorid, int playerid, float streamdistance, int areaid,
                int priority)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod(14, 15, 16, 17)]
            public virtual int CreateDynamicRaceCPEx(int type, float x, float y, float z, float nextx, float nexty,
                float nextz, float size, float streamdistance, int[] worlds, int[] interiors, int[] players, int[] areas,
                int priority, int maxworlds, int maxinteriors, int maxplayers, int maxareas)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual int DestroyDynamicRaceCP(int checkpointid)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual bool IsValidDynamicRaceCP(int checkpointid)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual int TogglePlayerDynamicRaceCP(int playerid, int checkpointid, bool toggle)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual int TogglePlayerAllDynamicRaceCPs(int playerid, bool toggle)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual bool IsPlayerInDynamicRaceCP(int playerid, int checkpointid)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual int GetPlayerVisibleDynamicRaceCP(int playerid)
            {
                throw new NativeNotImplementedException();
            }
        }
    }
}
