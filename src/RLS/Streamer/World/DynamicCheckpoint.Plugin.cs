﻿using SampSharp.GameMode.API.NativeObjects;

namespace RLS.Streamer
{
    public partial class DynamicCheckpoint
    {
        protected static readonly DynamicCheckpointPlugin plugin;

        static DynamicCheckpoint()
        {
            plugin = NativeObjectProxyFactory.CreateInstance<DynamicCheckpointPlugin>();
        }
        public class DynamicCheckpointPlugin
        {
            [NativeMethod]
            public virtual int CreateDynamicCP(float x, float y, float z, float size, int worldid, int interiorid,
                int playerid, float streamdistance, int areaid, int priority)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod(10, 11, 12, 13)]
            public virtual int CreateDynamicCPEx(float x, float y, float z, float size, float streamdistance,
                int[] worlds, int[] interiors, int[] players, int[] areas, int priority, int maxworlds, int maxinteriors,
                int maxplayers, int maxareas)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual int DestroyDynamicCP(int checkpointid)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual bool IsValidDynamicCP(int checkpointid)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual int TogglePlayerDynamicCP(int playerid, int checkpointid, bool toggle)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual int TogglePlayerAllDynamicCPs(int playerid, bool toggle)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual bool IsPlayerInDynamicCP(int playerid, int checkpointid)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual int GetPlayerVisibleDynamicCP(int playerid)
            {
                throw new NativeNotImplementedException();
            }
        }
    }
}
