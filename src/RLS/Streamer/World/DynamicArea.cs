﻿using SampSharp.GameMode;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.Pools;
using SampSharp.GameMode.World;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RLS.Streamer
{
    public partial class DynamicArea : IdentifiedPool<DynamicArea>
    {
        public bool IsValid => plugin.IsValidDynamicArea(Id);

        public void AttachTo(BasePlayer player, Vector3 offset = default(Vector3))
        {
            AssertNotDisposed();

            if (player == null)
                throw new ArgumentNullException(nameof(player));

            plugin.AttachDynamicAreaToPlayer(Id, player.Id, offset.X, offset.Y, offset.Z);
        }

        public void AttachTo(BaseVehicle vehicle, Vector3 offset = default(Vector3))
        {
            AssertNotDisposed();

            if (vehicle == null)
                throw new ArgumentNullException(nameof(vehicle));

            plugin.AttachDynamicAreaToVehicle(Id, vehicle.Id, offset.X, offset.Y, offset.Z);
        }

        public bool IsInArea(BasePlayer player, bool recheck = false)
        {
            AssertNotDisposed();

            if (player == null)
                throw new ArgumentNullException(nameof(player));

            return plugin.IsPlayerInDynamicArea(player.Id, Id, recheck);
        }

        public bool IsInArea(IWorldObject obj)
        {
            AssertNotDisposed();

            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            return plugin.IsPointInDynamicArea(Id, obj.Position.X, obj.Position.Y, obj.Position.Z);
        }

        public bool IsLineInArea(Vector3 from, Vector3 to)
        {
            AssertNotDisposed();

            return plugin.IsLineInDynamicArea(Id, from.X, from.Y, from.Z, to.X, to.Y, to.Z);
        }

        public static bool IsLineInAnyArea(Vector3 from, Vector3 to)
        {
            return plugin.IsLineInAnyDynamicArea(from.X, from.Y, from.Z, to.X, to.Y, to.Z);
        }

        public IEnumerable<Vector3> GetPoints()
        {
            AssertNotDisposed();

            var pointCount = GetPointsCount();
            plugin.GetDynamicPolygonPoints(Id, out float[] points, pointCount * 2);

            if (points == null) yield break;

            for (var i = 0; i < points.Length - 1; i += 2)
            {
                yield return new Vector3(points[i], points[i + 1]);
            }
        }

        public int GetPointsCount()
        {
            AssertNotDisposed();

            return plugin.GetDynamicPolygonNumberPoints(Id);
        }

        public static IEnumerable<DynamicArea> GetAreasForPoint(Vector3 point)
        {
            plugin.GetDynamicAreasForPoint(point.X, point.Y, point.Z, out int[] areas, GetAreasForPointCount(point));

            if (areas == null) yield break;

            foreach (var areaid in areas)
            {
                var area = Find(areaid);

                if (area != null)
                    yield return area;
            }
        }

        public static int GetAreasForPointCount(Vector3 point)
        {
            return plugin.GetNumberDynamicAreasForPoint(point.X, point.Y, point.Z);
        }

        public static IEnumerable<DynamicArea> GetAreasForLine(Vector3 from, Vector3 to)
        {
            plugin.GetDynamicAreasForLine(from.X, from.Y, from.Z, to.X, to.Y, to.Z, out int[] areas, GeAreasForLineCount(from, to));

            if (areas == null) yield break;

            foreach (var areaid in areas)
            {
                var area = Find(areaid);

                if (area != null)
                    yield return area;
            }
        }

        public static int GeAreasForLineCount(Vector3 from, Vector3 to)
        {
            return plugin.GetNumberDynamicAreasForLine(from.X, from.Y, from.Z, to.X, to.Y, to.Z);
        }

        public bool IsInArea(Vector3 point)
        {
            AssertNotDisposed();

            return plugin.IsPointInDynamicArea(Id, point.X, point.Y, point.Z);
        }

        public static bool IsInAnyArea(Vector3 point)
        {
            return plugin.IsPointInAnyDynamicArea(point.X, point.Y, point.Z);
        }

        public bool IsAnyPlayerInArea(bool recheck = false)
        {
            AssertNotDisposed();

            return plugin.IsAnyPlayerInDynamicArea(Id, recheck);
        }

        public static bool IsPlayerInAnyArea(BasePlayer player, bool recheck = false)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            return plugin.IsPlayerInAnyDynamicArea(player.Id, recheck);
        }

        public static bool IsAnyPlayerInAnyArea(bool recheck = false)
        {
            return plugin.IsAnyPlayerInAnyDynamicArea(recheck);
        }

        public static int GetAreaCountForPlayer(BasePlayer player)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            return plugin.GetPlayerNumberDynamicAreas(player.Id);
        }

        public static IEnumerable<DynamicArea> GetAreasForPlayer(BasePlayer player)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            plugin.GetPlayerDynamicAreas(player.Id, out int[] areas, GetAreaCountForPlayer(player));

            return areas?.Select(FindOrCreate);
        }

        public void ToggleForPlayer(BasePlayer player, bool toggle)
        {
            AssertNotDisposed();

            if (player == null)
                throw new ArgumentNullException(nameof(player));

            plugin.TogglePlayerDynamicArea(player.Id, Id, toggle);
        }

        public static void ToggleAllForPlayer(BasePlayer player, bool toggle)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            plugin.TogglePlayerAllDynamicAreas(player.Id, toggle);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            plugin.DestroyDynamicArea(Id);
        }

        public event EventHandler<PlayerEventArgs> Enter;

        public event EventHandler<PlayerEventArgs> Leave;

        public virtual void OnEnter(PlayerEventArgs e)
        {
            Enter?.Invoke(this, e);
        }

        public virtual void OnLeave(PlayerEventArgs e)
        {
            Leave?.Invoke(this, e);
        }

        #region Factories

        private static DynamicArea CreateArea<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3, int[] worlds, int[] interiors, BasePlayer[] players,
            int priority, Func<T1, T2, T3, int[], int[], int[], int, int, int, int, int> func)
        {
            if (worlds == null) worlds = new[] { -1 };
            if (interiors == null) interiors = new[] { -1 };
            var pl = players?.Select(p => p.Id).ToArray() ?? new[] { -1 };
            return
                FindOrCreate(func(arg1, arg2, arg3, worlds, interiors, pl, worlds.Length, interiors.Length, pl.Length, priority));
        }

        private static DynamicArea CreateArea<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, int[] worlds, int[] interiors, BasePlayer[] players,
            int priority, Func<T1, T2, T3, T4, int[], int[], int[], int, int, int, int, int> func)
        {
            if (worlds == null) worlds = new[] { -1 };
            if (interiors == null) interiors = new[] { -1 };
            var pl = players?.Select(p => p.Id).ToArray() ?? new[] { -1 };
            return
                FindOrCreate(func(arg1, arg2, arg3, arg4, worlds, interiors, pl, worlds.Length, interiors.Length,
                    pl.Length, priority));
        }

        private static DynamicArea CreateArea<T1, T2, T3, T4, T5, T6>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, int[] worlds,
            int[] interiors, BasePlayer[] players, int priority, Func<T1, T2, T3, T4, T5, T6, int[], int[], int[], int, int, int, int, int> func)
        {
            if (worlds == null) worlds = new[] { -1 };
            if (interiors == null) interiors = new[] { -1 };
            var pl = players?.Select(p => p.Id).ToArray() ?? new[] { -1 };
            return
                FindOrCreate(func(arg1, arg2, arg3, arg4, arg5, arg6, worlds, interiors, pl, worlds.Length, interiors.Length,
                    pl.Length, priority));
        }

        public static DynamicArea CreateCircle(float x, float y, float size, int worldid = -1, int interiorid = -1,
            BasePlayer player = null, int priority = 0)
        {
            return
                FindOrCreate(plugin.CreateDynamicCircle(x, y, size, worldid, interiorid, player?.Id ?? -1, priority));
        }

        public static DynamicArea CreateCircleEx(float x, float y, float size, int[] worlds = null,
            int[] interiors = null, BasePlayer[] players = null, int priority = 0)
        {
            return CreateArea(x, y, size, worlds, interiors, players, priority, plugin.CreateDynamicCircleEx);
        }

        public static DynamicArea CreateCube(float minx, float miny, float minz, float maxx, float maxy, float maxz,
            int worldid = -1, int interiorid = -1, BasePlayer player = null, int priority = 0)
        {
            return
                FindOrCreate(plugin.CreateDynamicCube(minx, miny, minz, maxx, maxy, maxz, worldid, interiorid, player?.Id ?? -1, priority));
        }


        public static DynamicArea CreateCube(Vector3 min, Vector3 max, int worldid = -1, int interiorid = -1,
            BasePlayer player = null, int priority = 0)
        {
            return
                FindOrCreate(plugin.CreateDynamicCube(min.X, min.Y, min.Z, max.X, max.Y, max.Z, worldid, interiorid, player?.Id ?? -1, priority));
        }

        public static DynamicArea CreateCubeEx(float minx, float miny, float minz, float maxx, float maxy, float maxz,
            int[] worlds = null, int[] interiors = null, BasePlayer[] players = null, int priority = 0)
        {
            return CreateArea(minx, miny, minz, maxx, maxy, maxz, worlds, interiors, players, priority, plugin.CreateDynamicCubeEx);
        }

        public static DynamicArea CreateCubeEx(Vector3 min, Vector3 max, int[] worlds = null, int[] interiors = null,
            BasePlayer[] players = null, int priority = 0)
        {
            return CreateArea(min.X, min.Y, min.Z, max.X, max.Y, max.Z, worlds, interiors, players, priority, plugin.CreateDynamicCubeEx);
        }

        public static DynamicArea CreatePolygon(float[] points, float minz = float.NegativeInfinity,
            float maxz = float.PositiveInfinity, int worlid = -1, int interiorid = -1, BasePlayer player = null, int priority = 0)
        {
            return
                FindOrCreate(plugin.CreateDynamicPolygon(points, minz, maxz, points.Length, worlid, interiorid,
                    player?.Id ?? -1, priority));
        }

        public static DynamicArea CreatePolygon(Vector3[] points, float minz, float maxz, int worlid = -1,
            int interiorid = -1, BasePlayer player = null, int priority = 0)
        {
            return
                FindOrCreate(plugin.CreateDynamicPolygon(points.SelectMany(p => new[] { p.X, p.Y }).ToArray(),
                    minz, maxz, points.Length * 2, worlid, interiorid, player?.Id ?? -1, priority));
        }

        public static DynamicArea CreatePolygon(Vector3[] points, int worlid = -1, int interiorid = -1,
            BasePlayer player = null, int priority = 0)
        {
            return
                FindOrCreate(plugin.CreateDynamicPolygon(points.SelectMany(p => new[] { p.X, p.Y }).ToArray(),
                    points.Min(p => p.Z), points.Max(p => p.Z), points.Length * 2, worlid, interiorid,
                    player?.Id ?? -1, priority));
        }

        public static DynamicArea CreatePolygonEx(float[] points, float minz = float.NegativeInfinity,
            float maxz = float.PositiveInfinity, int[] worlds = null, int[] interiors = null,
            BasePlayer[] players = null, int priority = 0)
        {
            return CreateArea(points, minz, maxz, points.Length, worlds, interiors, players, priority, plugin.CreateDynamicPolygonEx);
        }

        public static DynamicArea CreatePolygonEx(Vector3[] points, float minz = float.NegativeInfinity,
            float maxz = float.PositiveInfinity, int[] worlds = null, int[] interiors = null,
            BasePlayer[] players = null, int priority = 0)
        {
            return CreateArea(points.SelectMany(p => new[] { p.X, p.Y }).ToArray(), minz, maxz,
                points.Length * 2, worlds, interiors, players, priority, plugin.CreateDynamicPolygonEx);
        }

        public static DynamicArea CreatePolygonEx(Vector3[] points, int[] worlds = null, int[] interiors = null,
            BasePlayer[] players = null, int priority = 0)
        {
            return CreateArea(points.SelectMany(p => new[] { p.X, p.Y }).ToArray(), points.Min(p => p.Z),
                points.Max(p => p.Z), points.Length * 2, worlds, interiors, players, priority, plugin.CreateDynamicPolygonEx);
        }

        public static DynamicArea CreateRectangle(float minx, float miny, float maxx, float maxy, int worldid = -1,
            int interiorid = -1, BasePlayer player = null, int priority = 0)
        {
            return FindOrCreate(plugin.CreateDynamicRectangle(minx, miny, maxx, maxy, worldid, interiorid,
                player?.Id ?? -1, priority));
        }

        public static DynamicArea CreateRectangleEx(float minx, float miny, float maxx, float maxy, int[] worlds = null,
            int[] interiors = null, BasePlayer[] players = null, int priority = 0)
        {
            return CreateArea(minx, miny, maxx, maxy, worlds, interiors, players, priority, plugin.CreateDynamicRectangleEx);
        }

        public static DynamicArea CreateSphere(Vector3 pos, float size, int worldid = -1, int interiorid = -1,
            BasePlayer player = null, int priority = 0)
        {
            return FindOrCreate(plugin.CreateDynamicSphere(pos.X, pos.Y, pos.Z, size, worldid, interiorid, player?.Id ?? -1, priority));
        }

        public static DynamicArea CreateSphereEx(Vector3 pos, float size, int[] worlds = null, int[] interiors = null,
            BasePlayer[] players = null, int priority = 0)
        {
            return CreateArea(pos.X, pos.Y, pos.Z, size, worlds, interiors, players, priority, plugin.CreateDynamicSphereEx);
        }

        #endregion
    }
}
