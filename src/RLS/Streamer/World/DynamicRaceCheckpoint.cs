﻿using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.Pools;
using SampSharp.GameMode.World;
using System;
using System.Linq;

namespace RLS.Streamer
{
    public partial class DynamicRaceCheckpoint : IdentifiedPool<DynamicRaceCheckpoint>
    {
        public DynamicRaceCheckpoint(CheckpointType type, Vector3 position, Vector3 nextPosition,
            float size = 3.0f, int worldid = -1, int interiorid = -1, BasePlayer player = null,
            float streamdistance = 100.0f, DynamicArea area = null, int priority = 0)
        {
            Id = plugin.CreateDynamicRaceCP((int)type, position.X, position.Y, position.Z, nextPosition.X,
                nextPosition.Y, nextPosition.Z, size, worldid, interiorid, player?.Id ?? -1,
                streamdistance, area?.Id ?? -1, priority);
        }

        public DynamicRaceCheckpoint(CheckpointType type, Vector3 position, Vector3 nextPosition,
            float size, float streamdistance, int[] worlds = null, int[] interiors = null,
            BasePlayer[] players = null, DynamicArea[] areas = null, int priority = 0)
        {
            if (worlds == null) worlds = new[] { -1 };
            if (interiors == null) interiors = new[] { -1 };
            var pl = players?.Select(p => p.Id).ToArray() ?? new[] { -1 };
            var ar = areas?.Select(a => a.Id).ToArray() ?? new[] { -1 };

            Id = plugin.CreateDynamicRaceCPEx((int)type, position.X, position.Y, position.Z, nextPosition.X,
                nextPosition.Y, nextPosition.Z, size, streamdistance, worlds, interiors, pl, ar, priority, worlds.Length,
                interiors.Length, pl.Length, ar.Length);
        }

        public bool IsValid => plugin.IsValidDynamicRaceCP(Id);

        public void ToggleForPlayer(BasePlayer player, bool toggle)
        {
            AssertNotDisposed();

            if (player == null)
                throw new ArgumentNullException(nameof(player));

            plugin.TogglePlayerDynamicRaceCP(player.Id, Id, toggle);
        }

        public bool IsPlayerInCheckpoint(BasePlayer player)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            return plugin.IsPlayerInDynamicRaceCP(player.Id, Id);
        }

        public static void ToggleAllForPlayer(BasePlayer player, bool toggle)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            plugin.TogglePlayerAllDynamicRaceCPs(player.Id, toggle);
        }

        public static DynamicRaceCheckpoint GetPlayerVisibleDynamicCheckpoint(BasePlayer player)
        {
            var id = plugin.GetPlayerVisibleDynamicRaceCP(player.Id);

            return id < 0 ? null : FindOrCreate(id);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            plugin.DestroyDynamicRaceCP(Id);
        }

        public event EventHandler<PlayerEventArgs> Enter;

        public event EventHandler<PlayerEventArgs> Leave;

        public virtual void OnEnter(PlayerEventArgs e)
        {
            Enter?.Invoke(this, e);
        }

        public virtual void OnLeave(PlayerEventArgs e)
        {
            Leave?.Invoke(this, e);
        }
    }
}
