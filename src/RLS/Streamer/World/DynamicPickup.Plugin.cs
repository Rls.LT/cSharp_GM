﻿using SampSharp.GameMode.API.NativeObjects;
namespace RLS.Streamer
{
    partial class DynamicPickup
    {
        protected static readonly DynamicPickupPlugin plugin;

        static DynamicPickup()
        {
            plugin = NativeObjectProxyFactory.CreateInstance<DynamicPickupPlugin>();
        }

        public class DynamicPickupPlugin
        {
            [NativeMethod]
            public virtual int CreateDynamicPickup(int modelid, int type, float x, float y, float z, int worldid,
                int interiorid, int playerid, float streamdistance, int areaid, int priority)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod(11, 12, 13, 14)]
            public virtual int CreateDynamicPickupEx(int modelid, int type, float x, float y, float z,
                float streamdistance, int[] worlds, int[] interiors, int[] players, int[] areas, int priority,
                int maxworlds, int maxinteriors, int maxplayers, int maxareas)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual int DestroyDynamicPickup(int pickupid)
            {
                throw new NativeNotImplementedException();
            }

            [NativeMethod]
            public virtual bool IsValidDynamicPickup(int pickupid)
            {
                throw new NativeNotImplementedException();
            }
        }
    }
}
