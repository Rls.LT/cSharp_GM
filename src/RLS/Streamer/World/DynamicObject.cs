﻿using RLS.Streamer.Events;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Pools;
using SampSharp.GameMode.SAMP;
using System;

namespace RLS.Streamer
{
    partial class DynamicObject : IdentifiedPool<DynamicObject>
    {
        public DynamicObject(int model, Vector3 position, Vector3 rotation = new Vector3(), int worldid = -1, 
            int interiorid = -1, Player player = null, float streamdistance = 200f, float drawdistance = 0f, 
            DynamicArea area = null, int priority = 0)
        {
            Id = plugin.CreateDynamicObject(model, position.X, position.Y, position.Z, rotation.X, rotation.Y, rotation.Z, worldid,
                interiorid, player?.Id ?? -1, streamdistance, drawdistance, area?.Id ?? -1, priority);
        }
        public Vector3 Rotation
        {
            get
            {
                plugin.GetDynamicObjectRot(Id, out float x, out float y, out float z);
                return new Vector3(x, y, z);
            }
            set => plugin.SetDynamicObjectRot(Id, value.X, value.Y, value.Z);
        }
        public Vector3 Position
        {
            get
            {
                plugin.GetDynamicObjectPos(Id, out float x, out float y, out float z);
                return new Vector3(x, y, z);
            }
            set => plugin.SetDynamicObjectPos(Id, value.X, value.Y, value.Z);
        }
        public void Move(Vector3 newPosition, float speed)
        {
            Vector3 currentRot = Rotation;
            plugin.MoveDynamicObject(Id, newPosition.X, newPosition.Y, newPosition.Z, speed, currentRot.X, currentRot.Y, currentRot.Z);
        }
        public void Move(Vector3 newPosition, float speed, Vector3 rotation)
        {
            plugin.MoveDynamicObject(Id, newPosition.X, newPosition.Y, newPosition.Z, speed, rotation.X, rotation.Y, rotation.Z);
        }
        public bool IsMoving => plugin.IsDynamicObjectMoving(Id);
        public bool IsValid => plugin.IsValidDynamicObject(Id);
        //material
        /// <summary>
        /// Replace the texture of an object with the texture from another model in the game.
        /// </summary>
        /// <param name="materialIndex">The material index on the object to change (0 to 15)</param>
        /// <param name="fromModel">The modelid on which the replacement texture is located. Use 0 for alpha. Use -1 to change the material color without altering the texture.</param>
        /// <param name="txdName">The name of the txd file which contains the replacement texture (use "none" if not required)</param>
        /// <param name="textureName">The name of the texture to use as the replacement (use "none" if not required)</param>
        /// <param name="materialcolor">The object color to set, as an integer or hex in ARGB color format. Using 0 keeps the existing material color</param>
        /// <returns>DynamicObject</returns>
        public DynamicObject SetMaterial(int materialIndex, int fromModel, string txdName, string textureName, Color materialcolor)
        {
            plugin.SetDynamicObjectMaterial(Id, materialIndex, fromModel, txdName, textureName, materialcolor);
            return this;
        }
        //
        protected override void Dispose(bool disposing)
        {
            plugin.DestroyDynamicObject(Id);
            base.Dispose(disposing);
        }
        #region events
        public event EventHandler<EventArgs> Moved;
        public event EventHandler<PlayerSelectEventArgs> Selected;
        public event EventHandler<PlayerEditEventArgs> Edited;
        public event EventHandler<PlayerShootEventArgs> Shot;
        public virtual void OnShot(PlayerShootEventArgs e)
        {
            Shot?.Invoke(this, e);
        }
        public virtual void OnMove(EventArgs e)
        {
            Moved?.Invoke(this, e);
        }
        public virtual void OnEdit(PlayerEditEventArgs e)
        {
            Edited?.Invoke(this, e);
        }
        public virtual void OnSelect (PlayerSelectEventArgs e)
        {
            Selected?.Invoke(this, e);
        }
        #endregion
    }
}
