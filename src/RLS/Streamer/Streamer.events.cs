﻿using RLS.Streamer.Events;
using SampSharp.GameMode.Events;
using System;

namespace RLS.Streamer
{
    partial class Streamer
    {
        protected virtual void OnDynamicObjectMoved(DynamicObject @object, EventArgs e)
        {
            DynamicObjectMoved?.Invoke(@object, e);
            @object.OnMove(e);
        }

        protected virtual void OnPlayerEditDynamicObject(DynamicObject @object, PlayerEditEventArgs e)
        {
            PlayerEditDynamicObject?.Invoke(@object, e);
            @object.OnEdit(e);
        }

        protected virtual void OnPlayerSelectDynamicObject(DynamicObject @object, PlayerSelectEventArgs e)
        {
            PlayerSelectDynamicObject?.Invoke(@object, e);
            @object.OnSelect(e);
        }

        protected virtual void OnPlayerShootDynamicObject(DynamicObject @object, PlayerShootEventArgs e)
        {
            PlayerShootDynamicObject?.Invoke(@object, e);
            @object.OnShot(e);
        }

        protected virtual void OnPlayerPickUpDynamicPickup(DynamicPickup pickup, PlayerEventArgs e)
        {
            PlayerPickUpDynamicPickup?.Invoke(pickup, e);
            pickup.OnPickedUp(e);
        }

        protected virtual void OnPlayerEnterDynamicCheckpoint(DynamicCheckpoint checkpoint, PlayerEventArgs e)
        {
            PlayerEnterDynamicCheckpoint?.Invoke(checkpoint, e);
            checkpoint.OnEnter(e);
        }

        protected virtual void OnPlayerLeaveDynamicCheckpoint(DynamicCheckpoint checkpoint, PlayerEventArgs e)
        {
            PlayerLeaveDynamicCheckpoint?.Invoke(checkpoint, e);
            checkpoint.OnLeave(e);
        }

        protected virtual void OnPlayerEnterDynamicRaceCheckpoint(DynamicRaceCheckpoint checkpoint, PlayerEventArgs e)
        {
            PlayerEnterDynamicRaceCheckpoint?.Invoke(checkpoint, e);
            checkpoint.OnEnter(e);
        }

        protected virtual void OnPlayerLeaveDynamicRaceCheckpoint(DynamicRaceCheckpoint checkpoint, PlayerEventArgs e)
        {
            PlayerLeaveDynamicRaceCheckpoint?.Invoke(checkpoint, e);
            checkpoint.OnLeave(e);
        }

        protected virtual void OnPlayerEnterDynamicArea(DynamicArea area, PlayerEventArgs e)
        {
            PlayerEnterDynamicArea?.Invoke(area, e);
            area.OnEnter(e);
        }

        protected virtual void OnPlayerLeaveDynamicArea(DynamicArea area, PlayerEventArgs e)
        {
            PlayerLeaveDynamicArea?.Invoke(area, e);
            area.OnLeave(e);
        }

        public event EventHandler<EventArgs> DynamicObjectMoved;

        public event EventHandler<PlayerEditEventArgs> PlayerEditDynamicObject;

        public event EventHandler<PlayerSelectEventArgs> PlayerSelectDynamicObject;

        public event EventHandler<PlayerShootEventArgs> PlayerShootDynamicObject;

        public event EventHandler<PlayerEventArgs> PlayerPickUpDynamicPickup;

        public event EventHandler<PlayerEventArgs> PlayerEnterDynamicCheckpoint;

        public event EventHandler<PlayerEventArgs> PlayerLeaveDynamicCheckpoint;

        public event EventHandler<PlayerEventArgs> PlayerEnterDynamicRaceCheckpoint;
    
        public event EventHandler<PlayerEventArgs> PlayerLeaveDynamicRaceCheckpoint;

        public event EventHandler<PlayerEventArgs> PlayerEnterDynamicArea;

        public event EventHandler<PlayerEventArgs> PlayerLeaveDynamicArea;

    }
}
