﻿using RLS.Core.Vehicles;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.Commands
{
    class VehicleControl
    {
        [Command("sukurtitr")]
        public static void VehicleCommand(World.Players.Player player, VehicleModelType model = VehicleModelType.PetrolTrailer)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper || player.IsTester))
                return;
            if (model == VehicleModelType.PetrolTrailer)
                return;
            Vehicle vehicle = new Vehicle(model, player.Position + new Vector3(0, 0, 1f), player.Rotation.Z, 0, 0);
            if (vehicle != null)
                vehicle.PutPlayerIn(player);
            player.SendClientMessage($"Sukūrei {model}!");
            vehicle.Fuel = 10f;
        }
        [Command("fuel")]
        public static void SoundCommand(World.Players.Player player)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper))
                return;
            if (player.InAnyVehicle)
                player.Vehicle.Fuel = 2f;
        }
        [Command("sound")]
        public static void SoundCommand(World.Players.Player player, int id = 0)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper))
                return;
            player.PlaySound(id);
        }
    }
}
