﻿using RLS.World.Players;

namespace RLS.Commands
{
    class Messages
    {
        public static bool CanAccess(Player player, bool can)
        {
            if (can) return true;
            else
            {
                NoAccess(player);
                return false;
            }
        }
        public static void NoAccess(Player player)
        {
            player.Error("Tu neturi prieigos prie šios komandos.");
        }
    }
}
