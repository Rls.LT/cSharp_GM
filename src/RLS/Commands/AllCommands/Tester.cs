﻿using RLS.Definitions.Commands;
using RLS.World.Players;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.Commands.AllCommands
{
    class Tester
    {
        [Command("testkomandos")]
        public static void TesterCommands(Player player)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper || player.IsTester))
                return;
            TablistDialog commandsDialog = new TablistDialog("Testuotojo komandos", new[] { "Komanda", "Paaiškinimas" }, "Uždaryti")
            {
                new Command("testkomandos", "`Tester` lygio komandos").ToDialogRow(),
                new Command("manoxp", "Pakeisti savo patirties lygį").ToDialogRow(),
                new Command("sukurtitr", "Sukurti transporto priemonę").ToDialogRow(),
            };
            commandsDialog.Show(player);
        }
    }
}
