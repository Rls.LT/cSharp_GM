﻿using RLS.Definitions.Commands;
using RLS.World.Jobs;
using RLS.World.Players;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP.Commands;
using System;
namespace RLS.Commands.AllCommands
{
    class ManagerCommands
    {
        [Command("dkomandos")]
        public static void ManagerCommandList(Player player)
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            TablistDialog commandsDialog = new TablistDialog("Direktoriaus komandos", new[] { "Komanda", "Paaiškinimas" }, "Uždaryti")
            {
                new Command("dkomandos", "Direktoriaus komandos").ToDialogRow(),
                new Command("pakviesti", "Pakviesti žaidėją į darbą").ToDialogRow(),
                new Command("pakvietimai", "Žaidėjams išsiųsti pakvietimai į darbą").ToDialogRow(),
                new Command("darbuotojai", "Firmos darbuotojai ir jų valdymas").ToDialogRow(),
                new Command("dfondas", "Darbo fondo valdymas").ToDialogRow(),
                new Command("rangai", "Rangų valdymas").ToDialogRow()
            };
            commandsDialog.Show(player);
        }
    }
}
