﻿using RLS.Definitions;
using RLS.Definitions.Commands;
using RLS.World.Players;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.Commands.AllCommands
{
    class Developer
    {
        [Command("devkomandos")]
        public static void DeveloperCommands(Player player)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper))
                return;
            TablistDialog commandsDialog = new TablistDialog("Developer komandos", new[] { "Komanda", "Paaiškinimas" }, "Uždaryti")
            {
                new Command("devkomandos", "`Developer` lygio komandos").ToDialogRow(),
                new Command("exit", "Perkrauti modifikaciją").ToDialogRow(),
                new Command("testkomandos", "`Tester` lygio komandos").ToDialogRow(),
                new Command("nustatytixp", "Nustatyti patirties lygį").ToDialogRow(),
                new Command("nustatytihp", "Nustatyti gyvybės taškus").ToDialogRow(),
                new Command("manoxp", "Pakeisti savo patirties lygį").ToDialogRow(),
                new Command("direktorius", "Uždėti/nuimti direktorių").ToDialogRow(),
                new Command("testuotojas", "Paskirti žaidėją testuotoju").ToDialogRow(),
                new Command("sound", "PlaySound testavimui").ToDialogRow(),
                new Command("sukurtitr", "Sukurti transporto priemonę").ToDialogRow(),
                new Command("pinigai", "Pakeisti pinigų reikšmę").ToDialogRow(),
                new Command("fuel", "Papildyti dabrtinę transporto priemonę 2l kuro").ToDialogRow(),
            };
            commandsDialog.Show(player);
        }
        [Command("exit")]
        public static void Exit(Player player)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper))
                return;
            Player.SendClientMessageToAll(Colors.Lime, "_______________________________");
            Player.SendClientMessageToAll(Colors.Lime, "[~]");
            Player.SendClientMessageToAll(Colors.Lime, "[~] Serveris persikrauna...");
            Player.SendClientMessageToAll(Colors.Lime, "[~]");
            Player.SendClientMessageToAll(Colors.Lime, "_______________________________");
            GM.Exit();
        }
    }
}
