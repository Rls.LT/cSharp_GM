﻿using RLS.Definitions.Commands;
using RLS.World.Players;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.Commands.AllCommands
{
    class PlayerCommands
    {
        [Command("komandos")]
        public static void TesterCommands(Player player)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper || player.IsTester))
                return;
            TablistDialog commandsDialog = new TablistDialog("Žaidėjo komandos", new[] { "Komanda", "Paaiškinimas" }, "Uždaryti")
            {
                new Command("komandos", "Žaidėjo komandos").ToDialogRow(),
                new Command("info", "Informacija apie žaidėją").ToDialogRow(),
                new Command("darbas", "Darbo komandos").ToDialogRow(),
                new Command("nustatymai", "Registracijos nustatymai").ToDialogRow(),
                new Command("kviesti", "Iškviesti tarnybą").ToDialogRow(),
                new Command("namas", "Namo valdymas").ToDialogRow(),
                new Command("verslas", "Verslo valdymas").ToDialogRow(),
            };
            if (player.IsManager)
                commandsDialog.Add(new Command("dkomandos", "Direktoriaus komandos").ToDialogRow());
            if(player.IsTester)
                commandsDialog.Add(new Command("testkomandos", "Testuotojo komandos").ToDialogRow());
            if (player.IsDeveloper)
                commandsDialog.Add(new Command("devkomandos", "Kūrėjo komandos").ToDialogRow());
            commandsDialog.Show(player);
        }
    }
}
