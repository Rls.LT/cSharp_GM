﻿using RLS.Definitions;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.Commands.PlayerControl
{
    class Money
    {
        [Command("pinigai", UsageMessage = "Pinigų nustatymas")]
        public static void SetExperience(World.Players.Player player, World.Players.Player otherPlayer = null, int size = 0)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper))
                return;
            if (otherPlayer == null)
            {
                player.SendClientMessage(Colors.CommandText, "Pinigų nustatymas: /pinigai [žaidėjas] [skaičius]");
                return;
            }
            if (size < 1)
            {
                player.Error("Pinigų skaičius turi būti natūralusis.");
                return;
            }
            RichText text = new RichText(Color.Green, Color.Green.Lighten(0.2f));
            otherPlayer.SendClientMessage(Color.Green, text.SetText($"* Tavo pinigai pakeisti į @[{size}@]."));
            player.SendClientMessage(Color.Green, text.SetText($"* Tavo pinigai pakeisti iš @[{otherPlayer.Money}@] į @[{size}@]."));
            otherPlayer.GiveMoney(-otherPlayer.Money+ size);
        }
    }
}
