﻿using RLS.Definitions;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.Commands.PlayerControl
{
    class Health
    {
        [Command("nustatytihp", UsageMessage = "Gyvybės nustatymas")]
        public static void SetExperience(World.Players.Player player, World.Players.Player otherPlayer = null, float hp = 100f)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper))
                return;
            if (otherPlayer == null)
            {
                player.SendClientMessage(Colors.CommandText, "Gyvybės nustatymas: /nustatytihp [žaidėjas] [skaičius]");
                return;
            }
            otherPlayer.Health = hp;
            player.SuccessMessage($"Nustatei $s{otherPlayer.Name}$m gyvybę iš $s{otherPlayer.Health}$m į $s{hp}$m.");
            otherPlayer.SuccessMessage($"Tavo gyvybę $s{player.Name}$m nustatė į $s{hp}$m.");
        }
    }
}
