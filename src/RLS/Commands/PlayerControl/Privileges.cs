﻿using RLS.Definitions;
using SampSharp.GameMode.SAMP.Commands;
namespace RLS.Commands.PlayerControl
{
    class Privileges
    {
        [Command("testuotojas")]
        public static void SetExperience(World.Players.Player player, World.Players.Player otherPlayer = null)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper))
                return;
            if (otherPlayer == null)
            {
                player.SendClientMessage(Colors.CommandText, "Paskyrimas testuotoju: /testuotojas [žaidėjas]");
                return;
            }
            if (otherPlayer.IsTester)
            {
                otherPlayer.SetTesterState = false;
                otherPlayer.Error($"$s{player.Name}$m tave nušalinuo nuo testuotojų.");
            }
            else
            {
                otherPlayer.SetTesterState = true;
                otherPlayer.SuccessMessage($"$s{player.Name}$m tave paskyrė $stestuotoju$m.");
            }
        }
    }
}
