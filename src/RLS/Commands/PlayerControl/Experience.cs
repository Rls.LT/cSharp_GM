﻿using RLS.Definitions;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.Commands.PlayerControl
{
    class Experience
    {
        [Command("nustatytixp", UsageMessage = "Patirties nustatymas")]
        public static void SetExperience(World.Players.Player player, World.Players.Player otherPlayer = null, int size = 0)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper))
                return;
            if (otherPlayer == null)
            {
                player.SendClientMessage(Colors.CommandText, "Patirties nustatymas: /nustatytixp [žaidėjas] [patirtis]");
                return;
            }
            if (size < 1)
            {
                player.Error("Patirtis turi būti natūralusis skaičius.");
                return;
            }
            RichText text = new RichText(Color.Green, Color.Green.Lighten(0.2f));
            otherPlayer.SendClientMessage(Color.Green, text.SetText($"* Tavo patirtis pakeista į @[{size}@]."));
            player.SendClientMessage(Color.Green, text.SetText($"* {otherPlayer.Name} patirtis pakeista iš @[{otherPlayer.Experience}@] į @[{size}@]."));
            otherPlayer.Experience = size;
        }
        [Command("manoxp", UsageMessage = "Patirties nustatymas")]
        public static void SetMyExperience(World.Players.Player player, int size = 0)
        {
            if (!Messages.CanAccess(player, player.IsDeveloper || player.IsTester))
                return;
            if (size < 1)
            {
                player.Error("Patirtis turi būti natūralusis skaičius.");
                return;
            }
            RichText text = new RichText(Color.Green, Color.Green.Lighten(0.2f));
            player.SendClientMessage(Color.Green, text.SetText($"* Tavo patirtis pakeista iš @[{player.Experience}@] į @[{size}@]."));
            player.Experience = size;
        }
    }
}
