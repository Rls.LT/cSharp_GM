﻿using System;
using SampSharp.GameMode.Controllers;
using SampSharp.GameMode;
using RLS.World.Jobs;
using SampSharp.GameMode.Events;
using RLS.World.Players;
using RLS.World.Jobs.Taxi;
using RLS.World.Jobs.Police;
using RLS.World.Jobs.Medics;

namespace RLS.Controllers
{
    class JobsController : IEventListener, IController
    {
        private static GM gameMode;

        public void RegisterEvents(BaseMode gameMode)
        {
            JobsController.gameMode = gameMode as GM;
            gameMode.Initialized += CreateAllWorks;
            gameMode.PlayerEnterVehicle += OnPlayerEnterWorkVehicle;
            gameMode.Exited += OnGameModeExit;

        }

        private void OnGameModeExit(object sender, EventArgs e)
        {
            foreach (Job job in Job.All)
                job.SaveData();
        }

        private void OnPlayerEnterWorkVehicle(object sender, EnterVehicleEventArgs e)
        {
            Player player = sender as Player;
        }

        private void CreateAllWorks(object sender, EventArgs e)
        {
            if (!Database.MySql.Connection.Connected)
                return;
            Job.Add(new Policeman());
            Job.Add(new Medic());
            Job.Add(new TaxiDriver());
            //after all jobs created
            Job.SaveInvites();
        }

        public static void PlayerJoinedJob(Job job, Player player)
        {
            gameMode.CallPlayerJoinJob(player, job);
        }
    }
}
