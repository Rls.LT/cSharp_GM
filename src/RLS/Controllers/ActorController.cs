﻿using SampSharp.GameMode;
using RLS.World;
namespace RLS.Controllers
{
    class ActorController : SampSharp.GameMode.Controllers.ActorController
    {
        public override void RegisterTypes()
        {
            Actor.Register<Actor>();
        }
        public override void RegisterEvents(BaseMode gameMode)
        {
            base.RegisterEvents(gameMode);
        }
    }
}
