﻿using SampSharp.GameMode.Controllers;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;

namespace RLS.Controllers
{
    class PlayerController : BasePlayerController
    {
        public override void RegisterTypes()
        {
            Player.Register<Player>();
        }
        public override void RegisterEvents(BaseMode gameMode)
        {
            base.RegisterEvents(gameMode);
            gameMode.Exited += OnGameModeExit;
        }

        private void OnGameModeExit(object sender, System.EventArgs e)
        {
            foreach (Player player in Player.All)
                player.SaveData(DisconnectReason.Kicked);
        }
    }
}
