﻿using SampSharp.GameMode.Controllers;
using System;
using SampSharp.GameMode;

namespace RLS.Controllers
{
    class DatabaseController : IEventListener, IController
    {
        private GM mode;
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.Initialized += OnGameModeInit;
            gameMode.Exited += OnGameModeExit;
            mode = (GM)gameMode;
        }

        private void OnGameModeExit(object sender, EventArgs e)
        {
            Database.MySql.Connection.CloseConnection();
        }

        private void OnGameModeInit(object sender, EventArgs e)
        {
            new Database.MySql()
                .Initialize("localhost", "server", "server", "pyUkvkE7zCsIjqTy")
                .PrintLogsToConsole()
                .Log(Database.Logger.LogType.All)
                .SSLMode(MySql.Data.MySqlClient.MySqlSslMode.None)
                .OpenConnection();
            if (!Database.MySql.Connection.Connected)
                mode.DisposeGM();
        }
    }
}
