﻿using System;
using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;
using RLS.World.Players;
using SampSharp.GameMode.Events;
using RLS.World.Players.Definitions;

namespace RLS.Controllers.Account
{
    class Session : IEventListener, IController
    {
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.PlayerConnected += OnPlayerConnect;
            gameMode.PlayerDisconnected += OnPlayerDisconnect;
            gameMode.PlayerRequestSpawn += OnPlayerRequestSpawn;
            gameMode.PlayerRequestClass += OnPlayerRequestClass;
            gameMode.PlayerCommandText += OnPlayerEnterCommand;
        }

        private void OnPlayerRequestClass(object sender, RequestClassEventArgs e)
        {
            Player player = sender as Player;
            if (player.GetConnectionState() == ConnectionStates.NONE)
            {
                Functions.ClearClientScreen(player);
                new LoginRegister(player);
            }
        }

        private void OnPlayerConnect(object sender, EventArgs e)
        {
            Player player = sender as Player;
            player.SetConnectionState(ConnectionStates.NONE);
            player.Color = player.DefaultColor;
        }

        private void OnPlayerDisconnect(object sender, DisconnectEventArgs e)
        {
            Player player = sender as Player;
            player.SaveData(e.Reason);
        }

        private void OnPlayerRequestSpawn(object sender, RequestSpawnEventArgs e)
        {
            Player player = sender as Player;
            if (player.GetConnectionState() != ConnectionStates.LOADED)
                player.Kick("tried to spawn while not loaded.");
        }

        private void OnPlayerEnterCommand(object sender, CommandTextEventArgs e)
        {
            Player player = sender as Player;
            if (player.GetConnectionState() != ConnectionStates.LOADED)
            {
                player.Kick("tried to use commands while not logged.");
                return;
            }
            if (!e.Success)
                player.SendClientMessage(Definitions.Colors.ErrorText, "* Šios komandos nėra arba ji tau neprieinama.");
            e.Success = true;
        }
    }
}
