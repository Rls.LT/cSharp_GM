﻿using RLS.Definitions;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using RLS.World.Players.Definitions;
using RLS.World.Players;
using RLS.World.Players.Stories;

namespace RLS.Controllers.Account
{
    class FullRegistration
    {
        private Player player;

        public FullRegistration(Player player)
        {
            this.player = player;
            CheckWhatMissing();
        }

        private void CheckWhatMissing()
        {
            if (player.GetGender() == Gender.NONE)
            {
                ShowGenderSelect();
                return;
            }
            if (player.Email == null || player.Email == "")
            {
                ShowMailSetDialog();
                return;
            }
            if (!player.Story.Selected)
            {
                StartHistorySelection();
                return;
            }
            player.OnLoaded();
        }

        private void ShowGenderSelect()
        {
            ListDialog dialog = new ListDialog("Lyties pasirinkimas", "Pasirinkti");
            dialog.AddItem("Vaikinas");
            dialog.AddItem("Mergina");
            dialog.Response += (sender, args) => {
                if (args.DialogButton == DialogButton.Right)
                {
                    CheckWhatMissing();
                    return;
                }
                player.SetGender((Gender)args.ListItem + 1);
                player.SuccessMessage(new RichText(Colors.White,Colors.Yellow,$"Tavo lytis: $w{player.GetGender().GetString()}$m.").ToString());
                CheckWhatMissing();
            };
            dialog.Show(player);
        }
        
        private void ShowMailSetDialog(string error="")
        {
            InputDialog inputDialog = new InputDialog("El. paštas", new RichText(Colors.Dialog, Colors.Yellow, error).ToString() + "Kad užbaigtum registraciją reikalingas el. pašto adresa.\n\n" +
                    "Jei pamirši slaptažodį, į jį bus siunčiamas naujas.\n\n", false, "Nustatyti");
            inputDialog.Response += (sender, args) => {
                if (args.DialogButton == DialogButton.Right)
                {
                    CheckWhatMissing();
                    return;
                }
                if (!Functions.IsMailValid(args.InputText))
                {
                    ShowMailSetDialog("$eEl. paštas neteisingas.$m\n");
                    return;
                }
                player.Email = args.InputText;
                player.SuccessMessage($"El. paštas nustatytas į: {Colors.Hex.Yellow}{player.Email}");
                CheckWhatMissing();
            };
            inputDialog.Show(player);
        }

        private void StartHistorySelection()
        {
            player.Spawn(Config.historySelection_Pos);
            player.StorySelection = Story.GetById();

            player.SetCameraLookAt(Config.historySelection_CameraLookAt, CameraCut.Cut);
            player.CameraPosition = Config.historySelection_CameraPos;
            player.SpecialState = SpecialState.SELECTING_HISTORY;
            player.ToggleControllable(false);

            player.ShowTutorial("Istorija " + player.StorySelection.Title,
                player.StorySelection.Line + "\n\nIstorijas keisti su ~r~H~w~ ir ~r~N~w~, pasirinkimui: ~r~ENTER~W~.");
            player.Skin = player.StorySelection.Skin(player.GetGender());
            return;
        }
    }
}
