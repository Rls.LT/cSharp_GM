﻿using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;
using RLS.World.Players;
using RLS.Definitions;
using SampSharp.GameMode.Definitions;
using RLS.World.Players.Stories;

namespace RLS.Controllers.Account
{
    class StorySelection : IEventListener, IController
    {
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.PlayerKeyStateChanged += OnPlayerKeyStateChange;
            gameMode.Initialized += OnGameModeInit;
        }

        private void OnGameModeInit(object sender, System.EventArgs e)
        {
            Story.RegisterStory(new Tourist());
            Story.RegisterStory(new Patient());
            Story.RegisterStory(new Prisoner());
        }

        private void OnPlayerKeyStateChange(object sender, SampSharp.GameMode.Events.KeyStateChangedEventArgs e)
        {
            Player player = sender as Player;
            if (player.SpecialState == SpecialState.SELECTING_HISTORY)
            {
                if (e.NewKeys == Keys.CtrlBack || e.NewKeys == Keys.No)
                {
                    if(e.NewKeys == Keys.CtrlBack) player.StorySelection = player.StorySelection.Next;
                    else player.StorySelection = player.StorySelection.Prev;
                    player.ShowTutorial("Istorija " + player.StorySelection.Title,
                        player.StorySelection.Line + "\n\nIstorijas keisti su ~r~H~w~ ir ~r~N~w~, pasirinkimui: ~r~ENTER~W~.");
                    player.Skin = player.StorySelection.Skin(player.GetGender());
                }
                else if (e.OldKeys == Keys.SecondaryAttack)
                {
                    player.Clothes = player.Skin;
                    player.SetStory(player.StorySelection);
                    player.StorySelection = null;
                    player.SuccessMessage($"Pasirinkai istoriją: {Colors.Hex.Yellow}{player.Story.Title}{Colors.Hex.White}.");
                    player.HideTutorial();
                    player.SpecialState = SpecialState.NONE;
                    player.SpawnLocation = player.Story.SpawnPoint;
                    new FullRegistration(player);
                }
            }
        }
    }
}
