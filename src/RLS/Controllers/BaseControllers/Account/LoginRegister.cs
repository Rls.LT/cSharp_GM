﻿using RLS.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.Definitions;
using RLS.World.Players.Definitions;
using RLS.World.Players;
using System;
using Database;

namespace RLS.Controllers.Account
{
    class LoginRegister
    {
        private Player player;
        public LoginRegister(Player player)
        {
            this.player = player;
            player.SetConnectionState(ConnectionStates.CONNECTING);
            CheckIfExist();
        }

        private void CheckIfExist()
        {
            Database.MySql.Connection.RunThreaded(player,
                new Query(Configs.Database.players).Select("*").Where(new Field("name", player.Name)), OnAccountChecked);
        }

        private void OnAccountChecked(object sender, Result result)
        {
            if (player.IsDeleted)
            {
                Console.WriteLine($"[Race condition] Detected for player: {player.Name}");
                return;
            }
            if (result.NumRows == 0)
            {
                Register();
            }
            else
            {
                player.cache.AddMysqlResult(result);
                Login();
            }
        }
        private string JoinText(Player player, string accFound, string add="", bool replace = false)
        {
            RichText text = new RichText(Colors.Dialog, Colors.Blue);
            if (replace)
                return text.SetText($"{add}").ToString();
            return text.SetText($"{add}Sveiki atvykę į $s{GM.ServerName}$m.\n\n" +
                    $"Šiuo metu žaidžia: $s{(World.Players.Player.PoolSize + 1)}$m žaidėjai.\n" +
                        $"$s{player.Name}$m registracija {accFound}.\n").ToString();
        }
        private void Register(string text="")
        {
            if (player.GetConnectionState() != ConnectionStates.CONNECTING) return;
            text = JoinText(player, "$enerasta$m", text, text.Length>5) +
                        "Norint žaisti serveryje reikia užsiregistruoti, prašome įvesti slaptažodį, kurį naudosite.\n\n" +
                        "Keli patarimai slaptažodžui:\n" +
                        "* Slaptažodis turėtų būti bent 5 simbolių ilgio;\n" +
                        "* Slaptažodyje turėtų būti bent 1 didžioji, 1 mažoji raidė ir skaičius;\n" +
                        "* Slaptažodis neturėtų būti susijęs su vardu;\n" +
                        "* Slaptažodis turėtų būti žinomas tik tau.\n\n" +
                        "Taigi tavo slaptažodis yra:";
            Dialog dialog = new InputDialog("Registracija", text, true, "Registruotis", "Atšaukti");
            dialog.Response += (sender, args) =>
            {
                if (args.DialogButton == DialogButton.Right)
                { player.Kick("disconnected without register"); return; }
                if (args.InputText.Length < 5)
                    Register($"$eSlaptažodis per trumpas.$m\nJį turėtų sudaryti bent 5 simboliai.\n");
                else if (!Functions.ContainsNumber(args.InputText))
                    Register($"{Colors.Hex.Red}Slaptažodyje turėtų būti bent 1 skaičius.{Colors.Hex.Dialog}");
                else if (!Functions.ContainsLower(args.InputText) || !Functions.ContainsUpper(args.InputText))
                    Register($"$eSlaptažodyje turėtų buti bent 1 didžioji ir 1 mažoji raidė.$m\n");
                else
                {
                    Database.MySql.Connection.RunVoid(new Query(Configs.Database.players).Insert(
                        new Fields()
                            .Add("name", player.Name)
                            //.Add("password", Hash.Sha256(args.InputText))
                            /// TODO: update
                            .Add("registered", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                        ));
                    player.SuccessMessage("Registracija sukurta sėkmingai. Norėdamas tęsti prisijunk.");
                    CheckIfExist();
                }

            };
            dialog.Show(player);
        }

        private void Login(string text = "")
        {
            if (player.GetConnectionState() != ConnectionStates.CONNECTING) return;
            text = JoinText(player, "$srasta$m", text, text.Length > 5) +
                        "Norint tęsti žaidimą reikia įvesti slaptažodį:";
            if (player.cache.IsNull)
            {
                player.Kick(500, "nepavyko užkrauti žaidėjo informacijos. Pabandyk prisijungti iš naujo.");
                return;
            }
            InputDialog dialog = new InputDialog("Prisijungimas", text, true, "Prisijungti", "Atšaukti");
            dialog.Response += (sender, args) =>
            {
                if (args.DialogButton == DialogButton.Right)
                {
                    player.Kick("disconnected without login");
                    return;
                }
                /*if (player.cache.FindFieldByName("password").String.Equals(Hash.Sha256(args.InputText)))
                {
                    player.SetConnectionState(ConnectionStates.LOGGED);
                    player.SuccessMessage("Slaptažodis teisingas.");
                    player.LoadFromCache();
                    player.JoinedServerOn = Definitions.DateTime.CurrentTimestamp;
                    FullRegistration reg = new FullRegistration(player);
                }
                else
                {
                    Login("$eSlaptažodis neteisingas.$m\n");
                    player.failedLogins++;
                }*/
            };
            dialog.Show(player);
        }
    }
}
