﻿using RLS.World.Players;
using SampSharp.GameMode;

namespace RLS.Controllers.BaseControllers
{
    class CommandController : SampSharp.GameMode.Controllers.CommandController
    {
        public override void RegisterServices(BaseMode gameMode, GameModeServiceContainer serviceContainer)
        {
            // Register our own commands manager service instead of the default.
            CommandsManager = new Definitions.Commands.RlsCommandManager(gameMode);
            serviceContainer.AddService(CommandsManager);

            // Register commands in game mode.
            CommandsManager.RegisterCommands(gameMode.GetType());
            gameMode.PlayerCommandText += OnPlayerCommandText;
        }

        private void OnPlayerCommandText(object sender, SampSharp.GameMode.Events.CommandTextEventArgs e)
        {
            System.Console.WriteLine($"[Command] [{(sender as Player).Name}] " + e.Text);
        }
    }
}
