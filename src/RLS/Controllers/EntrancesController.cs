﻿using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;
using SampSharp.GameMode.Events;
using RLS.World.Players;
using RLS.World.Entrances;
using RLS.Events.Args;

namespace RLS.Controllers
{
    class EntrancesController : IEventListener, IController
    {
        GM gameMode = null;
        public void RegisterEvents(BaseMode gameMode)
        {
            this.gameMode = gameMode as GM;
            gameMode.PlayerKeyStateChanged += EntranceKeyPressed;
        }

        private void EntranceKeyPressed(object sender, KeyStateChangedEventArgs e)
        {
            if (e.OldKeys != SampSharp.GameMode.Definitions.Keys.SecondaryAttack) return;
            Player player = sender as Player;
            Entrance entrance = Entrance.PlayerCloseEnter(player);
            if (entrance != null)
            {
                player.LastEntranceUsed = entrance;
                EntranceArgs args = new EntranceArgs(entrance, player);
                entrance.UseEnter(player, args);
                if (args.Cancel) return;
                player.OnUseEntrance(args);
                gameMode.OnPlayerUseEntrance(args);
                return;
            }
            entrance = Entrance.PlayerCloseExit(player);
            if (entrance != null)
            {
                player.LastEntranceUsed = entrance;
                EntranceArgs args = new EntranceArgs(entrance, player, false);
                entrance.UseExit(player, args);
                if (args.Cancel) return;
                player.OnUseEntrance(args);
                gameMode.OnPlayerUseEntrance(args);
                return;
            }
        }
    }
}
