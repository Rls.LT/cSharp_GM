﻿using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;
using RLS.Events.Args;
using RLS.Core.Tutorials;
using SampSharp.GameMode.Events;
using RLS.World.Players;
using RLS.Core.Tutorials.Scripts;
using RLS.World.Entrances;

namespace RLS.Controllers
{
    class TutorialsController : IEventListener, IController
    {
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.PlayerDisconnected += OnPlayerDisconnect;
            (gameMode as GM).PlayerUseEntrance += OnPlayerUseEntrance;
            BaseTutorial.ActionEnded += OnActionEnded;
            
        }

        private void OnPlayerDisconnect(object sender, DisconnectEventArgs e)
        {
            if (sender is Player player)
                player.currentTutorial?.Dispose();
        }

        private void OnActionEnded(object sender, MissionArgs e)
        {
            if (e.Mission.CurrentStep >= e.Mission.StepCount)
                e.Player.OnPassedTutorial(e.Mission as BaseTutorial);
        }

        private void OnPlayerUseEntrance(object sender, EntranceArgs e)
        {  
            if (!e.entered && !e.player.LastEntranceUsed.Group.HasFlag(EntranceGroups.House))
                e.player.StartTutorial(typeof(Core.Tutorials.Scripts.Entrance));
            if (!e.entered && e.player.LastEntranceUsed.Group.HasFlag(EntranceGroups.Hospital))
                e.player.StartTutorial(typeof(Hospital));
        }
    }
}
