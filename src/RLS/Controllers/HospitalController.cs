﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;
using SampSharp.GameMode.SAMP;

namespace RLS.Controllers
{
    class HospitalController : IEventListener, IController
    {
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.PlayerDied += OnPlayerDied;
            gameMode.PlayerSpawned += OnPlayerSpawned;
            gameMode.PlayerRequestClass += OnPlayerRequestClass;
        }

        private void OnPlayerRequestClass(object sender, SampSharp.GameMode.Events.RequestClassEventArgs e)
        {
            Player player = sender as Player;
            if (player.SpawnInHospital)
            {
                player.SetSpawnInfo(Player.NoTeam, player.Skin, GetPlayerWardPos(), 0f);
                player.Spawn(GetPlayerWardPos());
            }

        }

        private Vector3 GetPlayerWardPos()
        {
            return new Vector3(-2723.9844, 743.6948, 1381.7000);
        }

        private void HospitalTimerUpdate(Player player, Timer timer)
        {
            if (!player.IsConnected)
                timer.Dispose();
            else
            {
                player.InHospitalFor--;
                player.ShowUILineNamed("hospital", Colors.Red, null, "Ligoninėje", "" + player.InHospitalFor, 1100);
                //check if player is in hospital radius
                if (!player.IsInRangeOfPoint(100f, GetPlayerWardPos()))
                    player.Position = GetPlayerWardPos();
            }
            if (player.InHospitalFor < 1)
            {
                timer.Dispose();
                TimeInHospitalEnded(player);
            }
        }

        private void TimeInHospitalEnded(Player player)
        {
            player.InHospitalFor = 0;
        }

        private void OnPlayerSpawned(object sender, SampSharp.GameMode.Events.SpawnEventArgs e)
        {
            Player player = sender as Player;
            if (player.SpawnInHospital)
            {
                player.InHospitalFor = Config.InHospitalAfterDeath;
                Timer timer = new Timer(1000, true);
                timer.Tick += (_sender, arg) =>
                {
                    HospitalTimerUpdate(player, timer);
                };
                player.SpawnInHospital = false;
            }
        }

        private void OnPlayerDied(object sender, SampSharp.GameMode.Events.DeathEventArgs e)
        {
            Player player = sender as Player;
            player.SpawnInHospital = true;
            player.SetSpawnInfo(Player.NoTeam, player.Skin, GetPlayerWardPos(), 0f);
        }
    }
}
