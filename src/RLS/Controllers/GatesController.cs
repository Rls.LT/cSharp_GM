﻿using RLS.Definitions;
using RLS.World;
using RLS.World.Players;
using RLS.World.Players.Definitions;
using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;

namespace RLS.Controllers
{
    class GatesController : IEventListener, IController
    {
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.PlayerKeyStateChanged += OnPlayerKeyStateChanges;
        }

        private void OnPlayerKeyStateChanges(object sender, SampSharp.GameMode.Events.KeyStateChangedEventArgs e)
        {
            Player player = (sender as Player);
            if (e.OldKeys != player.Settings.GetKey(KeyId.Gates).GtaKey(player)) return;
            Gates gate = Gates.FindClose(player.Position, 6f);
            if (gate == null) return;
            if (gate.IsClosed)
                gate.Open(player);
            else
                gate.Close(player);
        }
    }
}
