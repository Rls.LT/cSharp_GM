﻿using System;
using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;
using RLS.World.Maps;
using RLS.World.Players;
using RLS.World.Maps.JobMaps;
using RLS.World.Maps.Missions;

namespace RLS.Controllers
{
    class MapsController : IEventListener, IController
    {
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.Initialized += OnGameModeInit;
            gameMode.PlayerConnected += OnPlayerConnected;
            gameMode.PlayerEditAttachedObject += OnEditAttachedObject;
        }

        private void OnEditAttachedObject(object sender, SampSharp.GameMode.Events.EditAttachedObjectEventArgs e)
        {
            Console.WriteLine($"SetAttachedObject({e.Index}, {e.ModelId}, {e.Bone}, " +
                $"new Vector3({e.Offset.X}, {e.Offset.Y}, {e.Offset.Z}), new Vector3({e.Rotation.X}, {e.Rotation.Y}, {e.Rotation.Z}), " +
                $"new Vector3({e.Scale.X}, {e.Scale.Y}, {e.Scale.Z}), Colors.Red, Colors.Red);");
        }

        private void OnPlayerConnected(object sender, EventArgs e)
        {
            Player player = sender as Player;
            if (player.PVars.Exists("mapRemoved"))
            {
                Console.WriteLine("cancel deletion");
                return;
            }
            player.PVars["mapRemoved"] = "removed";
            Map.RemoveObjectsForPlayer(sender as Player);
        }

        private void OnGameModeInit(object sender, EventArgs e)
        {
            new Hospital();
            new Taxi();
            new Tourist();
            new Airport();
            new PetrolStations();
            Map.CreateMaps();
        }
    }
}
