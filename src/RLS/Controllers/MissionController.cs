﻿using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;
using RLS.World.Players;
using SampSharp.GameMode.Events;
using RLS.Events.Args;
using RLS.Missions;

namespace RLS.Controllers
{
    class MissionController : IEventListener, IController
    {
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.PlayerDisconnected += OnPlayerDisconnect;
            Player.FullyLoaded += OnPlayerLoaded;
            Mission.ActionEnded += OnActionEnded;
        }

        private void OnPlayerLoaded(object sender, PlayerEventArgs e)
        {
            if (e.Player is Player player)
                player.StartStoryMission("s1");
            
        }

        private void OnPlayerDisconnect(object sender, DisconnectEventArgs e)
        {
            if(sender is Player player)
                player.currentMission?.Dispose();
        }

        private void OnActionEnded(object sender, MissionArgs e)
        {
            if (e.Mission.CurrentStep >= e.Mission.StepCount)
                e.Player.PassedMission(e.Mission);
        }
    }
}
