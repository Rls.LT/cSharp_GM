﻿using SampSharp.GameMode.Controllers;
using SampSharp.GameMode;
using RLS.World.Property;

namespace RLS.Controllers
{
    class HouseController : IEventListener, IController
    {
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.Initialized += OnGameModeInit;
            gameMode.Exited += OnGameModeExit;
        }

        private void OnGameModeExit(object sender, System.EventArgs e)
        {
            foreach (House house in House.All)
            {
                house.SaveData();
            }
        }

        private void OnGameModeInit(object sender, System.EventArgs e)
        {
            if (!Database.MySql.Connection.Connected)
                return;
            new House(50000, new Vector3(-2638.6377, 623.2087, 14.4531), 270f, Interior.Worst);
            new House(50000, new Vector3(-1604.9692, 737.8581, 11.7133), 270f, Interior.Worst);
        }
    }
}
