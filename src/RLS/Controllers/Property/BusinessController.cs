﻿using SampSharp.GameMode.Controllers;
using SampSharp.GameMode;
using RLS.World.Property;
using RLS.World.Players;
using RLS.Definitions;

namespace RLS.Controllers
{
    class BusinessController : IEventListener, IController
    {
        public void RegisterEvents(BaseMode gameMode)
        {
            gameMode.Initialized += OnGameModeInit;
            gameMode.Exited += OnGameModeExit;
            gameMode.PlayerKeyStateChanged += PlayerKeyPressed;
        }

        private void PlayerKeyPressed(object sender, SampSharp.GameMode.Events.KeyStateChangedEventArgs e)
        {
            Player player = sender as Player;
            if (player.SpecialState != SpecialState.EditingObject) return;
            foreach(Business business in Business.All)
            {
                if (business.Editor!=null && business.Editor.Equals(player))
                {
                    business.OnEdit(player, e.OldKeys);
                    break;
                }
            }
        }

        private void OnGameModeExit(object sender, System.EventArgs e)
        {
            foreach (Business business in Business.All)
            {
                business.SaveData();
            }
        }

        private void OnGameModeInit(object sender, System.EventArgs e)
        {
            if (!Database.MySql.Connection.Connected)
                return;
            new Business("test business", new Vector3(-1621.0, 724.0, 14.45), 500000);
            new Business("test business2", new Vector3(-1601.0, 724.0, 14.45), 500000);
            new PetrolStation("test", new Vector3(-1675.5853, 431.5362, 7.1797), 50001, 135f)
                .AddPumpPosition(new Vector3(-1672.1328, 423.5000, 6.3828))
                .AddPumpPosition(new Vector3(-1676.5156, 419.1172, 6.3828))
                .AddPumpPosition(new Vector3(-1681.8281, 413.7813, 6.3828))
                .AddPumpPosition(new Vector3(-1685.9688, 409.6406, 6.3828))
                .AddPumpPosition(new Vector3(-1679.3594, 403.0547, 6.3828))
                .AddPumpPosition(new Vector3(-1675.2188, 407.1953, 6.3828))
                .AddPumpPosition(new Vector3(-1669.9063, 412.5313, 6.3828))
                .AddPumpPosition(new Vector3(-1665.5234, 416.9141, 6.3828)).LoadPumps();
            new CarShop("TestAutos", new Vector3(-1968.3455, 296.1346, 35.1719), 50000)
                .AddModel(411)
                .AddModel(562);
        }
    }
}
