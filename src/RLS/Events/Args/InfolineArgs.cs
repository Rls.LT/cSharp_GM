﻿using RLS.Display;
using RLS.Display.Builder;
using RLS.World.Players;
using System;

namespace RLS.Events.Args
{
    public class InfolineArgs : EventArgs
    {
        public bool Shown;
        public InfoLine UsedUI;
        public Player player;
        public InfolineArgs(InfoLine usedUI, Player player, bool shown = true)
        {
            this.player = player;
            UsedUI = usedUI;
            Shown = shown;
        }
    }
}
