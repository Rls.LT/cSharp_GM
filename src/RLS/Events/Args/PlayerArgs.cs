﻿using RLS.World.Players;
using System;
namespace RLS.Events.Args
{
    public class PlayerArgs : EventArgs
    {
        public Player player;
        public PlayerArgs(Player player)
        {
            this.player = player;
        }
    }
}
