﻿using RLS.World.Entrances;
using RLS.World.Players;
using System;

namespace RLS.Events.Args
{
    public class EntranceArgs : EventArgs
    {
        /// <summary>
        /// Entered or left building
        /// </summary>
        public bool entered;
        /// <summary>
        /// Entrance entity used
        /// </summary>
        public Entrance entranceUsed;
        /// <summary>
        /// Cancel action?
        /// </summary>
        public bool Cancel = false;
        /// <summary>
        /// Player instance
        /// </summary>
        public Player player;
        public EntranceArgs(Entrance entranceUsed, Player player, bool entered = true)
        {
            this.player = player;
            this.entranceUsed = entranceUsed;
            this.entered = entered;
        }
    }
}
