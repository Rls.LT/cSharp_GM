﻿using RLS.World;
using RLS.World.Players;
using System;

namespace RLS.Events.Args
{
    public class GatesArgs : EventArgs
    {
        public bool opening;
        public Gates gateUsed;
        public bool Cancel = false;
        public Player player;
        public GatesArgs(Gates gateUsed, Player player, bool opening = true)
        {
            this.player = player;
            this.gateUsed = gateUsed;
            this.opening = opening;
        }
    }
}
