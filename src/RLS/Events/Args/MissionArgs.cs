﻿using RLS.Missions;
using RLS.World.Players;
using System;

namespace RLS.Events.Args
{
    public class MissionArgs : EventArgs
    {
        public Player Player { get; private set; }
        public Mission Mission { get; private set; }
        public MissionArgs(Mission mission, Player player)
        {
            Player = player;
            Mission = mission;
        }
    }
}
