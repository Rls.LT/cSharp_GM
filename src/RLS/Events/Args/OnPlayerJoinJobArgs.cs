﻿using System;

namespace RLS.Events.Args
{
    public class OnPlayerJoinJobArgs : EventArgs
    {
        public World.Players.Player player;
        public World.Jobs.Job job;
        public OnPlayerJoinJobArgs(World.Players.Player player, World.Jobs.Job job)
        {
            this.player = player;
            this.job = job;
        }
    }
}
