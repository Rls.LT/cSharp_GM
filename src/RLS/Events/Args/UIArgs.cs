﻿using RLS.Display.Builder;
using RLS.World.Players;
using System;

namespace RLS.Events.Args
{
    public class UIArgs : EventArgs
    {
        public UI UsedUI;
        public Player player;
        public UIArgs(UI usedUI, Player player)
        {
            this.player = player;
            UsedUI = usedUI;
        }
    }
}
