﻿using RLS.Core.Vehicles;
using System;
namespace RLS.Events.VehicleEvents
{
    public class VehicleEventArgs : EventArgs
    {
        public Vehicle Vehicle { get; set; }
        public VehicleEventArgs(Vehicle veh)
        {
            Vehicle = veh;
        }
    }
}
