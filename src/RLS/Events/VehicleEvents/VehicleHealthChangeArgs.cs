﻿using RLS.Core.Vehicles;
using System;

namespace RLS.Events.VehicleEvents
{
    public class VehicleHealthChangeArgs : EventArgs
    {
        public float OldHealth { get; set; }
        public float NewHealth { get; set; }
        public Vehicle Vehicle { get; set; }
        public bool CancelChange { get; set; } = false;

        public VehicleHealthChangeArgs(float oldHealth, float newHealth, Vehicle vehicle)
        {
            OldHealth = oldHealth;
            NewHealth = newHealth;
            Vehicle = vehicle;
        }
    }
}
