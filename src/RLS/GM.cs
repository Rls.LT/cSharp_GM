﻿using System;
using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;
using RLS.Controllers;
using RLS.World.Players;
using RLS.World.Jobs;
using RLS.Events.Args;
using SampSharp.GameMode.SAMP;

namespace RLS
{
    public class GM : BaseMode
    {
        public static string ServerName = "RLS.LT";
        private static string version = "4.0.0 RC1";
        public static ControllerCollection AllControllers;
        private static GM instance;
        public GM() : base()
        {
            instance = this;
        }
        protected override void OnInitialized(EventArgs e)
        {
            SetGameModeText(ServerName + " " + Version);
            Console.WriteLine("\n----------------------------------");
            Console.WriteLine(" "+ ServerName + " in c# by d0Nt");
            Console.WriteLine(" Copyright 2012 - 2018");
            Console.WriteLine("----------------------------------\n");
            DisableInteriorEnterExits();
            EnableStuntBonusForAll(false);
            ManualVehicleEngineAndLights();
            ShowPlayerMarkers(SampSharp.GameMode.Definitions.PlayerMarkersMode.Global);
            base.OnInitialized(e);
        }
        public static string Version{ get => version; }

        protected override void LoadControllers(ControllerCollection controllers)
        {
            base.LoadControllers(controllers);
            controllers.Add(new DatabaseController());
            //player
            controllers.Override(new PlayerController());
            controllers.Override(new Controllers.BaseControllers.CommandController());
            controllers.Add(new Controllers.Account.Session());
            controllers.Add(new Controllers.Account.StorySelection());
            //vehicle
            controllers.Override(new Core.Vehicles.Controllers.VehicleController());
            //actors
            controllers.Override(new Controllers.ActorController());
            //other
            controllers.Add(new JobsController());
            controllers.Add(new MapsController());
            controllers.Add(new EntrancesController());
            controllers.Add(new GatesController());
            controllers.Add(new BusinessController());
            controllers.Add(new HouseController());
            controllers.Add(new HospitalController());
            //missins & tuts
            controllers.Add(new MissionController());
            controllers.Add(new TutorialsController());
            AllControllers = controllers;
        }
        #region custom events
        //gamemode exit
        public static new event EventHandler Exited;
        public void DisposeGM() => base.Exit();
        public static new void Exit()
        {
            Exited?.Invoke(null, new EventArgs());
            new Timer(5000, false).Tick += (sender, e) =>
            {
                instance?.DisposeGM();
            };
        }
        //entrances
        public event EventHandler<EntranceArgs> PlayerUseEntrance;

        public void OnPlayerUseEntrance(EntranceArgs e) => 
            PlayerUseEntrance?.Invoke(this, e);
        //job
        public event EventHandler<OnPlayerJoinJobArgs> PlayerJoinJob;
        public delegate void OnPlayerJoinJob(OnPlayerJoinJobArgs JoinJob);
        public void CallPlayerJoinJob(Player player, Job job) => 
            PlayerJoinJob?.Invoke(this, new OnPlayerJoinJobArgs(player, job));
        #endregion
    }
}
