﻿namespace RLS.Configs
{
    class Database
    {
        public static string players = "player";
        public static string jobs = "job";
        public static string job_warnings = "job_warning";
        public static string job_invites = "job_invite";
        public static string business = "business";
        public static string houses = "house";
        public static string vehicles = "vehicle";
    }
}
