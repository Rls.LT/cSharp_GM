﻿using System.Linq;

namespace RLS
{
    abstract partial class Functions
    {
        public static string IntFormat(int number, char spliter='.')
        {
            string stringFormat = "" + number;
            if (stringFormat.Length < 4) return stringFormat;
            string returnString = "";
            while (stringFormat.Length > 3)
            {
                returnString = stringFormat.Substring(stringFormat.Length - 3) + ((returnString.Length>1)?""+spliter:"") + returnString;
                stringFormat = stringFormat.Remove(stringFormat.Length - 3);
            }
            return stringFormat + spliter + returnString;
        }
        public static void ClearClientScreen(object client)
        {
            for (int x = 0; x < 11; x++)
                (client as World.Players.Player).SendClientMessage(" ");
        }
        #region input checkers
        public static bool ContainsNumber(string text)
        {
            foreach (char ch in text)
                if (char.IsNumber(ch)) return true;
            return false;
        }
        public static bool ContainsUpper(string text)
        {
            foreach (char ch in text)
                if (char.IsUpper(ch)) return true;
            return false;
        }
        public static bool ContainsLower(string text)
        {
            foreach (char ch in text)
                if (char.IsLower(ch)) return true;
            return false;
        }
        public static bool IsMailValid(string mail)
        {
            if (!mail.Contains("@")) return false;
            if (mail.Split('@')[1].Length < 6) return false;
            if (mail.Split('@')[0].Length < 4) return false;
            if (!Config.ValidMailEndings.Contains(mail.Split('@')[1])) return false;
            return true;
        }
        #endregion
    }
}
