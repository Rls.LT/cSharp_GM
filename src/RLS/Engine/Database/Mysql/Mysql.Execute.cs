﻿using MySql.Data.MySqlClient;

namespace Database
{
    partial class MySql
    {
        public void RunVoid(Query query)
        {
            if (connection == null) return;
            using (MySqlCommand cmd = new MySqlCommand(query.Format(), connection))
            {
                try
                {
                    affectedRows = (uint)cmd.ExecuteNonQuery();
                }
                catch (MySqlException ex)
                {
                    LogParse(ex);
                }
            }
        }
        public Result Run(Query query)
        {
            Result result = new Result();
            if (connection == null) return result;
            using (MySqlCommand cmd = new MySqlCommand(query.Format(), connection))
            {
                try
                {
                    return ReadResult(cmd);
                }
                catch (MySqlException e)
                {
                    LogParse(e);
                }
            }
            return result;
        }
        private Result ReadResult(MySqlCommand cmd)
        {
            Result result = new Result();
            using (MySqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                            result.Add(new Field(reader.GetName(i), reader.GetValue(i).ToString()));
                        result.EndRow();
                    }
                }
            }
            return result;
        }

    }
}
