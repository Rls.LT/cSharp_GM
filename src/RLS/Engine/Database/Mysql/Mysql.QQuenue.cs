﻿using System;

namespace Database
{
    partial class MySql
    {
        private class QQuenee
        {
            private Action<Result> cb1 = null;
            private Action<object, Result> cb2 = null;
            public Query query = null;
            private object sender = null;
            public QQuenee(object sender, Query query, Action<Result> callback = null)
            {
                this.query = query;
                cb1 = callback;
            }
            public QQuenee(object sender, Query query, Action<object, Result> callback)
            {
                this.query = query;
                cb2 = callback;
                this.sender = sender;
            }
            public bool CallbackExist() => !(cb1 == null && cb2 == null);
            public void ExecuteCallback(Result result)
            {
                if (!CallbackExist()) return;
                if (cb1 == null)
                {
                    if (sender == null)
                        new Logger.Logger().Log($"Callback sender is null. Query: {query.Format()}", Logger.LogType.Warning);
                    cb2(sender, result);
                }
                else
                    cb1(result);
            }
        }

    }
}
