﻿using MySql.Data.MySqlClient;
using System;

namespace Database
{
    public class Field
    {
        private string fieldName;
        private object fieldValue;
        private string comparator = "=";
        private bool escape = true;

        public Field(string fieldName, object fieldValue)
        {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
        }

        public Field(string fieldName, object fieldValue, bool escape)
        {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            this.escape = escape;
        }

        public Field(string fieldName, object fieldValue, string comparator)
        {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            this.comparator = comparator;
        }
        public string GetField() => MySqlHelper.EscapeString(fieldName);
        public string GetValue() => Query.ConvertValueForQuery((escape ? MySqlHelper.EscapeString("" + fieldValue) : "" + fieldValue));
        public override string ToString() => GetField() + comparator + GetValue();
        public string String => "" + fieldValue;
        public int Int()
        {
            int.TryParse(String, out int intval);
            return intval;
        }
        public void Int(out int output) => int.TryParse(String, out output);
        public float Float()
        {
            float.TryParse(String, out float floatval);
            return floatval;
        }
        public void Float(out float output) => float.TryParse(String, out output);
        public bool Bool()
        {
            bool.TryParse(String, out bool boolval);
            return boolval;
        }
        public void Bool(out bool output) => bool.TryParse(String, out output);
        public DateTime DateTime() => System.DateTime.Parse(String);
    }

}
