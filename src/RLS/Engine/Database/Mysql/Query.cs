﻿using System;

namespace Database
{
    class Query
    {
        private string command = null;
        private string table = null;
        private string where = null;
        private string orderBy = null;
        private string limit = null;
        private string offset = null;
        public Query() { }
        public Query(string table) => this.table = table;
        public Query Table(string table)
        {
            this.table = table;
            return this;
        }
        public Query Select(string select)
        {
            command = "SELECT " + select + " FROM";
            return this;
        }
        public Query Update(Field update)
        {
            command = "UPDATE " + table + " SET " + update.ToString();
            return this;
        }
        public Query Update(Fields update)
        {
            command = "UPDATE " + table + " SET ";
            for (int i = 0; i < update.Size; i++)
            {
                command += update.GetAt(i).ToString();
                if (update.Size - 1 > i)
                    command += ",";
            }
            return this;
        }
        public Query Delete()
        {
            command = "DELETE FROM";
            return this;
        }
        public Query Insert(Fields toInsert, bool returnId = false)
        {
            command = "INSERT INTO " + table + " (" + toInsert.GetColumns() + ") VALUES (" + toInsert.GetValues() + ");" + (returnId ? "SELECT LAST_INSERT_ID();" : "");
            return this;
        }
        public Query Replace(Fields toInsert)
        {
            command = "REPLACE INTO " + table + " (" + toInsert.GetColumns() + ") VALUES (" + toInsert.GetValues() + ")";
            return this;
        }
        public Query Where(Fields whereFields)
        {
            where = null;
            for (int i = 0; i < whereFields.Size; i++)
            {
                where += whereFields.GetAt(i).ToString() + " ";
                if (whereFields.Size - 1 > i)
                    where += "AND ";
            }
            return this;
        }
        public Query Where(Field where)
        {
            this.where = " WHERE " + where.ToString();
            return this;
        }
        public Query OrderBy(string field, bool asc = true)
        {
            orderBy = " ORDER BY " + field + (asc ? " ASC" : " DESC");
            return this;
        }
        public Query Limit(int limit)
        {
            this.limit = " LIMIT " + limit;
            return this;
        }
        public Query Offset(int offset)
        {
            this.offset = " OFFSET " + offset;
            return this;
        }
        public string Format()
        {
            string query = "";
            if (command.Contains("UPDATE")) query = command + where + limit + offset;
            else if (command.Contains("INSERT") || command.Contains("REPLACE")) query = command;
            else query = command + " " + table + where + orderBy + limit + offset;
            new Logger.Logger().Log(query, Logger.LogType.Query);
            return query;
        }
        public static string ConvertValueForQuery(string value)
        {
            if (!Double.TryParse(value, out var test) && !int.TryParse(value, out var test2)) value = "'" + value + "'";
            return value;
        }
    }

}
