﻿using System;

namespace Database.Logger.MessageTypes
{
    class Warning : IMessage
    {
        public string Format(string text)
        {
            return $"[{DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")}][Mysql][Warning] {text}";
        }
    }
}
