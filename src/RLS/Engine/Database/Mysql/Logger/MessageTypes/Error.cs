﻿using System;

namespace Database.Logger.MessageTypes
{
    class Error : IMessage
    {
        public string Format(string text)
        {
            return $"[{DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")}][Mysql][Error] {text}";
        }
    }
}
