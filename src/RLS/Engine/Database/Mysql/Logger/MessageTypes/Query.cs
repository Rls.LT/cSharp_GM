﻿using System;

namespace Database.Logger.MessageTypes
{
    class Query : IMessage
    {
        public string Format(string text)
        {
            return $"[{DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")}][Mysql][Query] {text}";
        }
    }
}
