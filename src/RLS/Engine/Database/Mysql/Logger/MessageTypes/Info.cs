﻿using System;

namespace Database.Logger.MessageTypes
{
    class Info : IMessage
    {
        public string Format(string text)
        {
            return $"[{DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")}][Mysql][Info] {text}";
        }
    }
}
