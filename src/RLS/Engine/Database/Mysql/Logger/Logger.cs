﻿using Database.Logger.MessageTypes;
using System;
using System.IO;

namespace Database.Logger
{
    class Logger
    {
        private bool print = false;
        private LogType LogThese = LogType.Warning | LogType.Query | LogType.Error;
        
        /// <summary>
        /// Print log message
        /// </summary>
        /// <param name="message">Message to print</param>
        /// <param name="type">Log type</param>
        public void Log(string message, LogType type = LogType.Error)
        {
            if (!LogThese.HasFlag(type) && LogThese != LogType.All) return;
            string text = FormatMessage(message, type);
            if (print)
                Console.WriteLine(text);
            LogToFile(text);
        }

        private void LogToFile(string text)
        {
            using (StreamWriter w = File.AppendText("mysql_log.log"))
            {
                w.WriteLine(text);
            }
        }
        /// <summary>
        /// Print logs to console
        /// </summary>
        /// <param name="print">Print or not</param>
        public void PrintToConsole(bool print)
        {
            this.print = print;
        }

        /// <summary>
        /// Set which logs should be printed/logged
        /// </summary>
        /// <param name="types"></param>
        public void LogWithTypes(LogType types)
        {
            LogThese = types;
        }

        /// <summary>
        /// Format message before print
        /// </summary>
        /// <param name="text"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private string FormatMessage(string text, LogType type)
        {
            IMessage errorFormater = null;
            switch (type)
            {
                case LogType.Error:
                    errorFormater = new Error();
                    break;
                case LogType.Warning:
                    errorFormater = new Warning();
                    break;
                case LogType.Query:
                    errorFormater = new MessageTypes.Query();
                    break;
                case LogType.Info:
                    errorFormater = new Info();
                    break;
            }
            if (errorFormater == null)
                 return new Error().Format("Failed to execute errorFormat function");
            return errorFormater.Format(text);
        }
    }
}
