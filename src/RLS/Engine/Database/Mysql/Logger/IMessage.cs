﻿namespace Database.Logger
{
    interface IMessage
    {
        string Format(string text);
    }
}
