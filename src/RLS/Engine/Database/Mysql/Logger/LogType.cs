﻿using System;

namespace Database.Logger
{
    [Flags]
    enum LogType
    {
        /// <summary>Errors</summary>
        Error = 1,
        /// <summary>Warnings</summary>
        Warning = 2,
        /// <summary>Queries</summary>
        Query = 4,
        /// <summary>Info</summary>
        Info = 8,
        /// <summary>Everything</summary>
        All = 16
    }
}
