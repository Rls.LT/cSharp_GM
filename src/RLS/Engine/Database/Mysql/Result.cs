﻿using System;
using System.Collections.Generic;

namespace Database
{
    public class Result
    {
        private int addToRow = 0;
        List<Fields> rows = new List<Fields>();
        public bool ValidRow(int id) => NumRows > id;
        public int InsertedRow { get => GetRow().GetAt(0).Int(); }
        public Result() { }
        public void Add(Field column, int toRow = -1)
        {
            if (toRow == -1) toRow = addToRow;
            if (rows.Count == 0 || addToRow >= rows.Count)
                rows.Add(new Fields());
            rows[addToRow].Add(column);
        }

        public void EndRow()
        {
            addToRow++;
        }
        public int FieldCount(int rowID)
        {
            if (!ValidRow(rowID)) return 0;
            return rows[addToRow].Size;
        }
        public Fields GetRow(int rowID = 0)
        {
            if (!ValidRow(rowID))
            {
                Console.WriteLine($"[{GetType()}]Not valid row.");
                return new Fields();
            }
            return rows[rowID];
        }
        public int NumRows
        {
            get => rows.Count;
        }
        public void ToConsole()
        {
            for (int i = 0; i < NumRows; i++)
            {
                Console.WriteLine("rowid " + i);
                foreach (Field x in GetRow(i))
                {
                    Console.WriteLine(x.GetField() + " = " + x.GetValue());
                }
                Console.WriteLine("_______________________________________");
            }
        }
    }

}
