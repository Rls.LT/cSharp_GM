﻿using MySql.Data.MySqlClient;

namespace Database
{
    partial class MySql
    {
        private void LogParse(MySqlException ex)
        {
            switch (ex.Number)
            {
                default:
                    logger.Log($"[{ex.Number}] {ex.Message}", Logger.LogType.Error);
                    break;
            }
        }
    }
}
