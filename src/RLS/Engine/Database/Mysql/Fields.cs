﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Database
{
    public class Fields : IEnumerator, IEnumerable
    {
        private List<Field> fields = new List<Field>();
        public int Size { get => fields.Count; }
        public Fields Add(Field field)
        {
            fields.Add(field);
            return this;
        }
        public Fields Add(string name, object value)
        {
            fields.Add(new Field(name, value));
            return this;
        }
        public string GetColumns()
        {
            string str = null;
            int i = 1;
            foreach (Field item in fields)
            {
                str += item.GetField();
                if (i < fields.Count) str += ",";
                i++;
            }
            return str;
        }
        public Field GetAt(int id)
        {
            if (id >= Size) return new Field(null, null);
            else return fields[id];
        }

        internal Field[] ToArray()
        {
            return fields.ToArray();
        }

        public string GetValues()
        {
            string str = null;
            int i = 1;
            foreach (Field item in fields)
            {
                str += item.GetValue();
                if (i < fields.Count) str += ",";
                i++;
            }
            return str;
        }
        public void Clear()
        {
            fields.Clear();
        }
        public Field GetFieldByName(string name)
        {
            foreach (Field res in fields)
            {
                if (res.GetField() == name)
                    return res;
            }
            return new Field("notfound", "notfound");
        }
        #region enumerator
        private int position = -1;
        public object Current
        {
            get
            {
                try
                {
                    return fields[position];
                }

                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public IEnumerator GetEnumerator() => this;

        public bool MoveNext()
        {
            position++;
            return (position < fields.Count);
        }

        public void Reset() => position = 0;
        #endregion
    }

}
