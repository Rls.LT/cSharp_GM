﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace Database
{
    /// <summary>
    /// Mysql handler for c#
    /// </summary>
    partial class MySql
    {
        /// <summary>
        /// Active connections list
        /// </summary>
        public static List<MySql> Connections = new List<MySql>();
        /// <summary>
        /// First connection in list
        /// </summary>
        public static MySql Connection {
            get
            {
                if (Connections.Count < 1) return new MySql();
                return Connections[0];
            }
        }
        private uint affectedRows;
        private MySqlConnection connection = null;
        private string host;
        private string user;
        private string database;
        private string password;
        private string charset = null;
        private MySqlSslMode ssl = MySqlSslMode.None;
        private uint port = 3306;
        private Logger.Logger logger;

        /// <summary>
        /// Initialize connection data
        /// </summary>
        /// <param name="host">Host to connect to</param>
        /// <param name="user">Database user</param>
        /// <param name="password">Database user password</param>
        /// <param name="database">Database to select</param>
        /// <param name="port">Server port number. Default: 3306</param>
        /// <returns></returns>
        public MySql Initialize(string host, string user, string database, string password, uint port = 3306)
        {
            this.host       = host;
            this.user       = user;
            this.database   = database;
            this.password   = password;
            this.port       = port;

            logger = new Logger.Logger();
            return this;
        }

        /// <summary>
        /// Check if client connected to server
        /// </summary>
        public bool Connected => connection != null;

        /// <summary>
        /// Change logging preferences
        /// </summary>
        /// <param name="types">Types to print</param>
        /// <returns></returns>
        public MySql Log(Logger.LogType types)
        {
            logger.LogWithTypes(types);
            return this;
        }

        /// <summary>
        /// Print logs to console
        /// </summary>
        /// <returns></returns>
        public MySql PrintLogsToConsole()
        {
            logger.PrintToConsole(true);
            return this;
        }

        /// <summary>
        /// Sets character set for connection
        /// </summary>
        /// <param name="charset">Example: utf8</param>
        /// <returns></returns>
        public MySql Charset(string charset)
        {
            this.charset = charset;
            return this;
        }

        /// <summary>
        /// Change ssl mode for connection
        /// </summary>
        /// <param name="sslMode"></param>
        /// <returns></returns>
        public MySql SSLMode(MySqlSslMode sslMode)
        {
            ssl = sslMode;
            return this;
        }

        /// <summary>
        /// Formats connection string with already defined variables
        /// </summary>
        /// <returns></returns>
        private string ConnectionString()
        {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder
            {
                Server      = host,
                UserID      = user,
                Database    = database,
                Password    = password,
                Port        = port,
                SslMode     = ssl
            };
            if (charset != null)
                builder.CharacterSet = this.charset;
            return builder.ToString();
        }

        /// <summary>
        /// Open connection with specified data
        /// </summary>
        public void OpenConnection()
        {
            logger.Log($"Logging started.", Logger.LogType.Info);
            connection = new MySqlConnection(ConnectionString());
            try
            {
                connection.Open();
                Connections.Add(this);
                logger.Log($"Connected to {host}@{database}.", Logger.LogType.Info);
            }
            catch (MySqlException ex)
            {
                connection = null;
                LogParse(ex);
            }
            catch(System.Exception ex)
            {
                connection = null;
                logger.Log($"C# error: {ex.Message}");
            }
        }
        /// <summary>
        /// Close opened mysql connection
        /// </summary>
        /// <returns></returns>
        public bool CloseConnection()
        {
            if (!Connected) return false;
            try
            {
                connection.Close();
                Connections.Remove(this);
                logger.Log($"Connection closed to {host}@{database}.", Logger.LogType.Warning);
            }
            catch (MySqlException ex)
            {
                LogParse(ex);
            }
            return true;
        }
    }
}
