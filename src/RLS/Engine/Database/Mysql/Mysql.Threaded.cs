﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Database
{
    partial class MySql
    {
        private Thread thread = null;
        private List<QQuenee> queries = new List<QQuenee>();
        private void Start()
        {
            if (thread != null)
                return;
            thread = new Thread(() =>
            {
                StartExecution();
                thread = null;
            });
            thread.Start();
        }
        private void StartExecution()
        {
            using (MySqlConnection con = new MySqlConnection(ConnectionString()))
            {
                try
                {
                    con.Open();
                    while (queries.Count > 0)
                    {
                        using (MySqlCommand cmd = new MySqlCommand(queries[0].query.Format(), con))
                        {
                            try
                            {
                                if (cmd == null)
                                    throw new Exception("Mysql Cmd null");
                                if (!queries[0].CallbackExist())
                                    cmd.ExecuteNonQuery();
                                else
                                {
                                    Result res = ReadResult(cmd);
                                    queries[0].ExecuteCallback(res);
                                }
                            }
                            catch (MySqlException e)
                            {
                                LogParse(e);
                            }
                            queries.RemoveAt(0);
                        }
                    }
                    con.Close();
                }
                catch (Exception e)
                {
                    new Logger.Logger().Log($"c# Error: {e.Message}", Logger.LogType.Error);
                }
            }
        }
        public void RunThreadedVoid(Query query)
        {
            if (connection == null) return;
            queries.Add(new QQuenee(null, query));
            Start();
        }
        public void RunThreaded(Query query, Action<Result> callback)
        {
            if (connection == null) return;
            queries.Add(new QQuenee(null, query, callback));
            Start();
        }
        public void RunThreaded(object sender, Query query, Action<object, Result> callback)
        {
            if (connection == null) return;
            queries.Add(new QQuenee(sender, query, callback));
            Start();
        }

    }
}
