﻿using System;

namespace Database
{
    public class Cache
    {
        private Result result = null;
        public Cache() { }
        public Cache(Result res) { result = res; }
        public void AddMysqlResult(Result res) => result = res;
        public Result GetMysqlResult() => result;
        public bool IsNull { get => result == null; }
        public Field FindFieldByName(string name)
        {
            if (IsNull)
            {
                new Logger.Logger().Log("[Cache] Cache result is null.", Logger.LogType.Error);
                return new Field("unset", "unset");
            }
            if (result.NumRows != 1)
            {
                new Logger.Logger().Log("[Cache] Bad cache content. Found {result.NumRows} rows.", Logger.LogType.Error);
                return new Field("unset", "unset");
            }
            return result.GetRow(0).GetFieldByName(name);
        }
    }

}
