﻿namespace RLS.Definitions
{
    enum BusinessType
    {
        Default,
        PetrolStation,
        CarShop
    }
    static class BusinessTypeMethods
    {
        public static string GetName(this BusinessType s1)
        {
            switch (s1)
            {
                case BusinessType.PetrolStation:
                    return "Degalinė ";
                case BusinessType.CarShop:
                    return "Automobilių parduotuvė ";
            }
            return "Tipas nežinomas ";
        }
    }
}
