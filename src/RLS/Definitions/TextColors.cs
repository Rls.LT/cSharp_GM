﻿using SampSharp.GameMode.SAMP;

namespace RLS.Definitions
{
    class Colors
    {
        public static string C(string text,string color, string defaultColor = "{a9c4e4}")
        {
            return color + text + defaultColor;
        }

        //client message color config
        public static readonly Color Dialog                 = 0xa9c4e4FF;
        public static readonly Color Red                    = 0xFF0000FF;
        public static readonly Color Green                  = 0x1aa31aFF;
        public static readonly Color Blue                   = 0x149BFFFF;
        public static readonly Color Yellow                 = 0xffff00FF;
        public static readonly Color Orange                 = 0xFF4606FF;
        public static readonly Color White                  = 0xFFFFFFFF;
        public static readonly Color Lime                   = 0xd4e51dFF;
        //specific
        public static readonly Color JobText                = 0xFED700FF;
        public static readonly Color CommandText            = 0x00C8F8FF;
        public static readonly Color ErrorText              = 0xFF0000FF;

        public static string GetHex(Color color, bool withBrackets = true)
        {
            if(withBrackets)
                return "{"+color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2")+"}";
            else
                return color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
        }
        public class Hex
        {
            public static readonly string JobText = GetHex(Colors.JobText);
            public static readonly string Dialog = GetHex(Colors.Dialog);
            public static readonly string White = GetHex(Colors.White);
            public static readonly string Red = GetHex(Colors.Red);
            public static readonly string Green = GetHex(Colors.Green);
            public static readonly string Lime = GetHex(Colors.Lime);
            public static readonly string Blue = GetHex(Colors.Blue);
            public static readonly string Yellow = GetHex(Colors.Yellow);
            public static readonly string Orange = GetHex(Colors.Orange);
        }
    }
}
