﻿using SampSharp.GameMode.SAMP;
namespace RLS.Definitions
{
    public class RichText
    {
        private Color main;
        private Color secondary;
        private string text;

        public RichText(Color main, Color secondary, string text)
        {
            this.main = main;
            this.secondary = secondary;
            this.text = text;
        }
        public RichText()
        {
            this.main = Colors.White;
            this.secondary = Colors.White;
            this.text = "";
        }
        public RichText(Color main, Color secondary)
        {
            this.main = main;
            this.secondary = secondary;
            this.text = "";
        }
        public RichText(Color main, string text)
        {
            this.main = main;
            this.secondary = main.Lighten(0.4f);
            this.text = text;
        }
        public RichText SetMainColor(Color main)
        {
            this.main = main;
            return this;
        }
        public Color GetMainColor() => main;
        public RichText SetSecondaryColor(Color secondary)
        {
            this.secondary = secondary;
            return this;
        }
        public string GetText() => text;
        public RichText SetText(string text)
        {
            this.text = text;
            return this;
        }
        public override string ToString()
        {
            text = text.Replace("@[", "" + Colors.GetHex(secondary));//old
            text = text.Replace("@]", "" + Colors.GetHex(main));//old
            //
            text = text.Replace("$s", "" + Colors.GetHex(secondary));
            text = text.Replace("$w", "" + Colors.Yellow);
            text = text.Replace("$m", "" + Colors.GetHex(main));
            text = text.Replace("$e", "" + Colors.Hex.Red);
            text = text.Replace("$y", "" + Colors.Hex.Yellow);
            text = text.Replace("$g", "" + Colors.Hex.Green);
            return Colors.GetHex(main) + text;
        }
    }
}
