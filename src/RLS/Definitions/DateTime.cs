﻿using System;

namespace RLS.Definitions
{
    public enum ReadableFormat
    {
        Full,
        Short
    }
    public class DateTime
    {
        private System.DateTime dateTime = System.DateTime.Now;
        private IFormatProvider culture = new System.Globalization.CultureInfo("lt-LT", true);
        public System.DateTime SystemDate { get => dateTime; }
        public static string ReadableSeconds(int seconds, ReadableFormat format = ReadableFormat.Full)
        {
            string time = "";
            TimeSpan t = TimeSpan.FromSeconds(seconds);
            if (format == ReadableFormat.Full)
            {
                time = Math.Ceiling(t.TotalHours) + ":" + t.ToString(@"mm\:ss");
            }
            else if(format == ReadableFormat.Short)
            {
                time = "";
                if(t.Hours>0)
                    time += Math.Ceiling(t.TotalHours) + "h ";
                if (t.Minutes > 0)
                    time += t.Minutes + "min ";
                if (t.Seconds > 0)
                    time += t.Seconds + "s";
            }
            return time;
        }
        public static System.DateTime Now
        {
            get => System.DateTime.Now;
        }
        public static int CurrentTimestamp
        {
            get => new DateTime(Now).Timestamp;
        }
        public static System.DateTime FromTimestamp(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
        public int Timestamp
        {
            get => (int)(dateTime.Subtract(new System.DateTime(1970, 1, 1)).TotalSeconds);
        }
        public DateTime(System.DateTime dateTime)
        {
            this.dateTime = dateTime;
        }
        public DateTime(string dateTime)
        {
            this.dateTime = System.DateTime.Parse(dateTime, culture, System.Globalization.DateTimeStyles.AssumeLocal);
        }
        public override string ToString()
        {
            return dateTime.ToString(culture);
        }
        
    }
}
