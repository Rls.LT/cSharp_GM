﻿namespace RLS.Definitions.Commands
{
    public class Command
    {
        public string command;
        public string description;

        public Command(string command, string description)
        {
            this.command = command;
            this.description = description;
        }

        public string[] ToDialogRow()
        {
            return new string[] { Colors.C("/"+command, "{07b1f4}"), description };
        }
    }
}
