﻿using SampSharp.GameMode;
using SampSharp.GameMode.SAMP.Commands;
using SampSharp.GameMode.SAMP.Commands.PermissionCheckers;
using System.Reflection;

namespace RLS.Definitions.Commands
{
    class RlsCommandManager : CommandsManager
    {
        public RlsCommandManager(BaseMode gameMode) : base(gameMode)
        {
        }
        protected override ICommand CreateCommand(CommandPath[] commandPaths, string displayName, bool ignoreCase,
        IPermissionChecker[] permissionCheckers, MethodInfo method, string usageMessage)
        {
            // Create an instance of your own command type.
            return new RlsCommand(commandPaths, displayName, ignoreCase, permissionCheckers, method, usageMessage);
        }
    }
}
