﻿using SampSharp.GameMode.SAMP.Commands;
using System;
using SampSharp.GameMode.SAMP.Commands.PermissionCheckers;
using System.Reflection;
using SampSharp.GameMode.World;
using SampSharp.GameMode.SAMP;
using System.Linq;

namespace RLS.Definitions.Commands
{
    class RlsCommand : DefaultCommand
    {
        public RlsCommand(CommandPath[] names, string displayName, bool ignoreCase, IPermissionChecker[] permissionCheckers, MethodInfo method, string usageMessage) : base(names, displayName, ignoreCase, permissionCheckers, method, usageMessage)
        {
        }
        protected override bool SendPermissionDeniedMessage(IPermissionChecker permissionChecker, BasePlayer player)
        {
            // Override SendPermissionDeniedMessage to send permission denied messages in the way you prefer.

            if (permissionChecker == null) throw new ArgumentNullException(nameof(permissionChecker));
            if (player == null) throw new ArgumentNullException(nameof(player));

            if (permissionChecker.Message == null)
                return false;

            // Send permission denied message in red instead of white.
            player.SendClientMessage(Color.Red, "[Klaida] "+permissionChecker.Message);
            return true;
        }

        protected override bool SendUsageMessage(BasePlayer player)
        {
            if (UsageMessage == null)
            {
                player.SendClientMessage(Colors.CommandText, $"* Naudojimas: " + "{59C4C5}" + $"{this.CustomMessage()}");
                return true;
            }

            player.SendClientMessage(Colors.CommandText, $"* {UsageMessage}: " + "{59C4C5}" + $"{this.CustomMessage()}");
            return true;
        }
        private string ParamName(string name)
        {
            switch (name.ToLower())
            {
                case "player":
                    return "žaidėjas";
                case "otherplayer":
                    return "žaidėjas";
                case "_player":
                    return "žaidėjas";
                case "model":
                    return "modelis";
                case "skinid":
                    return "skin id";
                case "size":
                    return "dydis";
                case "reason":
                    return "priežastis";
                default:
                    return name;
            }
        }
        public string CustomMessage()
        {
            var name = "/" + DisplayName;

            if (Parameters.Any())
                return name + " " +
                       string.Join(" ", Parameters.Select(p => p.IsOptional ? $"<{ParamName(p.Name)}>" : $"[{ParamName(p.Name)}]"));
            return name;
        }

    }
}
