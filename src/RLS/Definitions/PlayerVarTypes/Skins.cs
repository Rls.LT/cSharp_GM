﻿
using System;

namespace RLS.Definitions
{
    /// <summary>All skins from gta sa</summary>
    /// <remarks>See http://wiki.sa-mp.com/wiki/Skins:All. </remarks>
    public enum Skins
    {
        /// <summary>Carl "CJ" Johnson (Main Character)</summary>
        CJ,
        /// <summary>The Truth</summary>
        MALE_TRUTH,
        /// <summary>Mace</summary>
        MALE_MACCER,
        /// <summary>Andre</summary>
        MALE_ANDRE,
        /// <summary>Barry "Big Bear" Thorne [Thin]</summary>
        MALE_BBTHIN,
        /// <summary>Barry "Big Bear" Thorne [Big]</summary>
        MALE_BIG_BEAR,
        /// <summary>Emmet</summary>
        MALE_EMMET,
        /// <summary>Taxi Driver/Train Driver</summary>
        MALE_PRISONER,
        /// <summary>Janitor</summary>
        MALE_JANITOR,
        /// <summary>Normal Ped</summary>
        FEMALE_BFORI,
        /// <summary>Old Woman</summary>
        FEMALE_BFOST,
        /// <summary>Casino croupier</summary>
        FEMALE_VBFYCRP,
        /// <summary>Rich Woman</summary>
        FEMALE_BFYRI,
        /// <summary>Street Girl</summary>
        FEMALE_BFYST,
        /// <summary>Normal Ped</summary>
        MALE_BMORI,
        /// <summary>Mr.Whittaker (RS Haul Owner)</summary>
        MALE_BMOST,
        /// <summary>Airport Ground Worker</summary>
        MALE_BMYAP,
        /// <summary>Businessman</summary>
        MALE_BMYBU,
        /// <summary>Beach Visitor</summary>
        MALE_BMYBE,
        /// <summary>DJ</summary>
        MALE_BMYDJ,
        /// <summary>Rich Guy (Madd Dogg's Manager)</summary>
        MALE_BMYRI,
        /// <summary>Normal Ped</summary>
        MALE_BMYCR,
        /// <summary>Normal Ped</summary>
        MALE_BMYST,
        /// <summary>BMXer</summary>
        MALE_WMYBMX,
        /// <summary>Madd Dogg Bodyguard</summary>
        MALE_WBDYG1,
        /// <summary>Madd Dogg Bodyguard</summary>
        MALE_WBDYG2,
        /// <summary>Backpacker</summary>
        MALE_WMYBP,
        /// <summary>Construction Worker</summary>
        MALE_WMYCON,
        /// <summary>Drug Dealer</summary>
        MALE_BMYDRUG,
        /// <summary>Drug Dealer</summary>
        MALE_WMYDRUG,
        /// <summary>Drug Dealer</summary>
        MALE_HMYDRUG,
        /// <summary>Farm-Town inhabitant</summary>
        FEMALE_DWFOLC,
        /// <summary>Farm-Town inhabitant</summary>
        MALE_DWMOLC1,
        /// <summary>Farm-Town inhabitant</summary>
        MALE_DWMOLC2,
        /// <summary>Farm-Town inhabitant</summary>
        MALE_DWMYCL1,
        /// <summary>Gardener</summary>
        MALE_HMOGAR,
        /// <summary>Golfer</summary>
        MALE_WMYGOL1,
        /// <summary>Golfer</summary>
        MALE_WMYGOL2,
        /// <summary>Normal Ped</summary>
        MALE_HFORI,
        /// <summary>Normal Ped</summary>
        MALE_HFOST,
        /// <summary>Normal Ped</summary>
        MALE_HFYRI,
        /// <summary>Normal Ped</summary>
        MALE_HFYST,
        /// <summary>Jethro</summary>
        MALE_JETHRO,
        /// <summary>Normal Ped</summary>
        MALE_HMORI,
        /// <summary>Normal Ped</summary>
        MALE_HMOST,
        /// <summary>Beach Visitor</summary>
        MALE_HMYBE,
        /// <summary>Normal Ped</summary>
        MALE_HMYRI,
        /// <summary>Normal Ped</summary>
        MALE_HMYCR,
        /// <summary>Normal Ped</summary>
        MALE_HMYST,
        /// <summary>Snakehead (Da Nang)</summary>
        MALE_OMOKUNG,
        /// <summary>Mechanic</summary>
        MALE_WMYMECH,
        /// <summary>Mountain Biker</summary>
        MALE_BMYMOUN,
        /// <summary>Mountain Biker</summary>
        MALE_WMYMOUN,
        /// <summary>Unknown</summary>
        FEMALE_OFORI,
        /// <summary>Normal Ped</summary>
        FEMALE_OFOST,
        /// <summary>Normal Ped</summary>
        FEMALE_OFYRI,
        /// <summary>Normal Ped</summary>
        FEMALE_OFYST,
        /// <summary>Oriental Ped</summary>
        MALE_OMORI,
        /// <summary>Oriental Ped</summary>
        MALE_OMOST,
        /// <summary>Normal Ped</summary>
        MALE_OMYRI,
        /// <summary>Normal Ped</summary>
        MALE_OMYST,
        /// <summary>Pilot</summary>
        MALE_WMYPLT,
        /// <summary>Colonel Fuhrberger</summary>
        MALE_WMOPJ,
        /// <summary>Prostitute</summary>
        FEMALE_BFYPRO,
        /// <summary>Prostitute</summary>
        FEMALE_HFYPRO,
        /// <summary>Kendl Johnson</summary>
        FEMALE_KENDL,
        /// <summary>Pool Player</summary>
        MALE_BMYPOL1,
        /// <summary>Pool Player</summary>
        MALE_BMYPOL2,
        /// <summary>Priest/Preacher</summary>
        MALE_WMOPREA,
        /// <summary>Normal Ped</summary>
        MALE_SBFYST,
        /// <summary>Scientist</summary>
        MALE_WMOSCI,
        /// <summary>Security Guard</summary>
        MALE_WMYSGRD,
        /// <summary>Hippy</summary>
        MALE_SWMYHP1,
        /// <summary>Hippy</summary>
        MALE_SWMYHP2,
        /// <summary>Not used/Unknown</summary>
        MALE_NA,
        /// <summary>Prostitute</summary>
        FEMALE_SWFOPRO,
        /// <summary>Stewardess</summary>
        FEMALE_WFYSTEW,
        /// <summary>Homeless</summary>
        FEMALE_SWMOTR1,
        /// <summary>Homeless</summary>
        MALE_WMOTR1,
        /// <summary>Homeless</summary>
        MALE_BMOTR1,
        /// <summary>Boxer</summary>
        MALE_VBMYBOX,
        /// <summary>Boxer</summary>
        MALE_VWMYBOX,
        /// <summary>Black Elvis</summary>
        MALE_VHMYELV,
        /// <summary>White Elvis</summary>
        MALE_VBMYELV,
        /// <summary>Blue Elvis</summary>
        MALE_VIMYELV,
        /// <summary>Prostitute</summary>
        FEMALE_VWFYPRO,
        /// <summary>Ryder with robbery mask</summary>
        MALE_RYDER3,
        /// <summary>Stripper</summary>
        FEMALE_VWFYST1,
        /// <summary>Normal Ped</summary>
        FEMALE_WFORI,
        /// <summary>Normal Ped</summary>
        FEMALE_WFOST,
        /// <summary>Jogger</summary>
        FEMALE_WFYJG,
        /// <summary>Rich Woman</summary>
        FEMALE_WFYRI,
        /// <summary>Rollerskater</summary>
        FEMALE_WFYRO,
        /// <summary>Normal Ped</summary>
        FEMALE_WFYST,
        /// <summary>Normal Ped</summary>
        MALE_WMORI,
        /// <summary>Normal Ped, Works at or owns Dillimore Gas Station</summary>
        MALE_WMOST,
        /// <summary>Jogger</summary>
        MALE_WMYJG,
        /// <summary>Lifeguard</summary>
        MALE_WMYLG,
        /// <summary>Normal Ped</summary>
        MALE_WMYRI,
        /// <summary>Rollerskater</summary>
        MALE_WMYRO,
        /// <summary>Biker</summary>
        MALE_WMYCR,
        /// <summary>Normal Ped</summary>
        MALE_WMYST,
        /// <summary>Balla</summary>
        MALE_BALLAS1,
        /// <summary>Balla</summary>
        MALE_BALLAS2,
        /// <summary>Balla</summary>
        MALE_BALLAS3,
        /// <summary>Grove Street Families</summary>
        MALE_FAM1,
        /// <summary>Grove Street Families</summary>
        MALE_FAM2,
        /// <summary>Grove Street Families</summary>
        MALE_FAM3,
        /// <summary>Los Santos Vagos</summary>
        MALE_LSV1,
        /// <summary>Los Santos Vagos</summary>
        MALE_LSV2,
        /// <summary>Los Santos Vagos</summary>
        MALE_LSV3,
        /// <summary>The Russian Mafia</summary>
        MALE_MAFFA,
        /// <summary>The Russian Mafia</summary>
        MALE_MAFFB,
        /// <summary>The Russian Mafia</summary>
        MALE_MAFBOSS,
        /// <summary>Varios Los Aztecas</summary>
        MALE_VLA1,
        /// <summary>Varios Los Aztecas</summary>
        MALE_VLA2,
        /// <summary>Varios Los Aztecas</summary>
        MALE_VLA3,
        /// <summary>Triad</summary>
        MALE_TRIADA,
        /// <summary>Triad</summary>
        MALE_TRIADB,
        /// <summary>Johhny Sindacco</summary>
        MALE_SINDACO,
        /// <summary>Triad Boss</summary>
        MALE_TRIBOSS,
        /// <summary>Da Nang Boy</summary>
        MALE_DNB1,
        /// <summary>Da Nang Boy</summary>
        MALE_DNB2,
        /// <summary>Da Nang Boy</summary>
        MALE_DNB3,
        /// <summary>The Mafia</summary>
        MALE_VMAFF1,
        /// <summary>The Mafia</summary>
        MALE_VMAFF2,
        /// <summary>The Mafia</summary>
        MALE_VMAFF3,
        /// <summary>The Mafia</summary>
        MALE_VMAFF4,
        /// <summary>Farm Inhabitant</summary>
        MALE_DNMYLC,
        /// <summary>Farm Inhabitant</summary>
        FEMALE_DNFOLC1,
        /// <summary>Farm Inhabitant</summary>
        FEMALE_DNFOLC2,
        /// <summary>Farm Inhabitant</summary>
        FEMALE_DNFYLC,
        /// <summary>Farm Inhabitant</summary>
        MALE_DNMOLC1,
        /// <summary>Farm Inhabitant</summary>
        MALE_DNMOLC2,
        /// <summary>Homeless</summary>
        MALE_SBMOTR2,
        /// <summary>Homeless</summary>
        MALE_SWMOTR2,
        /// <summary>Normal Ped</summary>
        MALE_SBMYTR3,
        /// <summary>Homeless</summary>
        MALE_SWMOTR3,
        /// <summary>Beach Visitor</summary>
        FEMALE_WFYBE,
        /// <summary>Beach Visitor</summary>
        FEMALE_BFYBE,
        /// <summary>Beach Visotor</summary>
        FEMALE_HFYBE,
        /// <summary>Businesswoman</summary>
        FEMALE_SOFYBU,
        /// <summary>Taxi Driver</summary>
        MALE_SBMYST,
        /// <summary>Crack Maker</summary>
        MALE_SBMYCR,
        /// <summary>Crack Maker</summary>
        MALE_BMYCG,
        /// <summary>Crack Maker</summary>
        FEMALE_WFYCRK,
        /// <summary>Crack Maker</summary>
        MALE_HMYCM,
        /// <summary>Businessman</summary>
        MALE_WMYBU,
        /// <summary>Businesswoman</summary>
        FEMALE_BFYBU,
        /// <summary>Big Smoke Armored</summary>
        MALE_SMOKEV,
        /// <summary>Businesswoman</summary>
        FEMALE_WFYBU,
        /// <summary>Normal Ped</summary>
        FEMALE_DWFYLC1,
        /// <summary>Prostitute</summary>
        FEMALE_WFYPRO,
        /// <summary>Construction Worker</summary>
        MALE_WMYCONB,
        /// <summary>Beach Visitor</summary>
        MALE_WMYBE,
        /// <summary>Well Stacked Pizza Worker</summary>
        MALE_WMYPIZZ,
        /// <summary>Barber</summary>
        MALE_BMOBAR,
        /// <summary>Hillbilly</summary>
        FEMALE_CWFYHB,
        /// <summary>Farmer</summary>
        MALE_CWMOFR,
        /// <summary>Hillbilly</summary>
        MALE_CWMOHB1,
        /// <summary>Hillbilly</summary>
        MALE_CWMOHB2,
        /// <summary>Farmer</summary>
        MALE_CWMYFR,
        /// <summary>Hillbilly</summary>
        MALE_CWMYHB1,
        /// <summary>Black Bouncer</summary>
        MALE_BMYBOUN,
        /// <summary>White Bouncer</summary>
        MALE_WMYBOUN,
        /// <summary>White MIB agent</summary>
        MALE_WMOMIB,
        /// <summary>Black MIB agent</summary>
        MALE_BMYMIB,
        /// <summary>Cluckin' Bell Worker</summary>
        MALE_WMYBELL,
        /// <summary>Hotdog/Chilli Dog Vendor</summary>
        MALE_BMOCHIL,
        /// <summary>Normal Ped</summary>
        FEMALE_SOFYRI,
        /// <summary>Normal Ped</summary>
        MALE_SOMYST,
        /// <summary>Blackjack Dealer</summary>
        MALE_VWMYBJD,
        /// <summary>Casino croupier</summary>
        FEMALE_VWFYCRP,
        /// <summary>San Fierro Rifa</summary>
        MALE_SFR1,
        /// <summary>San Fierro Rifa</summary>
        MALE_SFR2,
        /// <summary>San Fierro Rifa</summary>
        MALE_SFR3,
        /// <summary>Barber</summary>
        MALE_BMYBAR,
        /// <summary>Barber</summary>
        MALE_WMYBAR,
        /// <summary>Whore</summary>
        FEMALE_WFYSEX,
        /// <summary>Ammunation Salesman</summary>
        MALE_WMYAMMO,
        /// <summary>Tattoo Artist</summary>
        MALE_BMYTATT,
        /// <summary>Punk</summary>
        MALE_VWMYCR,
        /// <summary>Cab Driver</summary>
        MALE_VBMOCD,
        /// <summary>Normal Ped</summary>
        MALE_VBMYCR,
        /// <summary>Normal Ped</summary>
        MALE_VHMYCR,
        /// <summary>Normal Ped</summary>
        MALE_SBMYRI,
        /// <summary>Normal Ped</summary>
        MALE_SOMYRI,
        /// <summary>Businessman</summary>
        MALE_SOMYBU,
        /// <summary>Normal Ped</summary>
        MALE_SWMYST,
        /// <summary>Valet</summary>
        MALE_WMYVA,
        /// <summary>Barbara Schternvart</summary>
        FEMALE_COPGRL3,
        /// <summary>Helena Wankstein</summary>
        FEMALE_GUNGRL3,
        /// <summary>Michelle Cannes</summary>
        FEMALE_MECGRL3,
        /// <summary>Katie Zhang</summary>
        FEMALE_NURGRL3,
        /// <summary>Millie Perkins</summary>
        FEMALE_CROGRL3,
        /// <summary>Denise Robinson</summary>
        FEMALE_GANGRL3,
        /// <summary>Farm-Town inhabitant</summary>
        FEMALE_CWFOFR,
        /// <summary>Hillbilly</summary>
        FEMALE_CWFOHB,
        /// <summary>Farm-Town inhabitant</summary>
        FEMALE_CWFYFR1,
        /// <summary>Farm-Town inhabitant</summary>
        FEMALE_CWFYFR2,
        /// <summary>Hillbilly</summary>
        MALE_CWMYHB2,
        /// <summary>Farmer</summary>
        FEMALE_DWFYLC2,
        /// <summary>Farmer</summary>
        MALE_DWMYLC2,
        /// <summary>Karate Teacher</summary>
        MALE_OMYKARA,
        /// <summary>Karate Teacher</summary>
        MALE_WMYKARA,
        /// <summary>Burger Shot Cashier</summary>
        FEMALE_WFYBURG,
        /// <summary>Cab Driver</summary>
        MALE_VWMYCD,
        /// <summary>Prostitute</summary>
        FEMALE_VHFYPRO,
        /// <summary>Su Xi Mu (Suzie)</summary>
        MALE_SUZIE,
        /// <summary>Oriental Noodle stand vendor</summary>
        MALE_OMONOOD,
        /// <summary>Oriental Boating School Instructor</summary>
        MALE_OMOBOAT,
        /// <summary>Clothes shop staff</summary>
        FEMALE_WFYCLOT,
        /// <summary>Homeless</summary>
        MALE_VWMOTR1,
        /// <summary>Weird old man</summary>
        MALE_VWMOTR2,
        /// <summary>Waitress (Maria Latore)</summary>
        FEMALE_VWFYWAI,
        /// <summary>Normal Ped</summary>
        FEMALE_SBFORI,
        /// <summary>Normal Ped</summary>
        FEMALE_SWFYRI,
        /// <summary>Clothes shop staff</summary>
        MALE_WMYCLOT,
        /// <summary>Normal Ped</summary>
        FEMALE_SBFOST,
        /// <summary>Rich Woman</summary>
        FEMALE_SBFYRI,
        /// <summary>Cab Driver</summary>
        MALE_SBMOCD,
        /// <summary>Normal Ped</summary>
        MALE_SBMORI,
        /// <summary>Normal Ped</summary>
        MALE_SBMOST,
        /// <summary>Normal Ped</summary>
        MALE_SHMYCR,
        /// <summary>Normal Ped</summary>
        FEMALE_SOFORI,
        /// <summary>Normal Ped</summary>
        FEMALE_SOFOST,
        /// <summary>Normal Ped</summary>
        FEMALE_SOFYST,
        /// <summary>Oriental Businessman</summary>
        MALE_SOMOBU,
        /// <summary>Oriental Ped</summary>
        MALE_SOMORI,
        /// <summary>Oriental Ped</summary>
        MALE_SOMOST,
        /// <summary>Homeless</summary>
        MALE_SWMOTR5,
        /// <summary>Normal Ped</summary>
        FEMALE_SWFORI,
        /// <summary>Normal Ped</summary>
        FEMALE_SWFOST,
        /// <summary>Normal Ped</summary>
        FEMALE_SWFYST,
        /// <summary>Cab Driver</summary>
        MALE_SWMOCD,
        /// <summary>Normal Ped</summary>
        MALE_SWMORI,
        /// <summary>Normal Ped</summary>
        MALE_SWMOST,
        /// <summary>Prostitute</summary>
        FEMALE_SHFYPRO,
        /// <summary>Prostitute</summary>
        FEMALE_SBFYPRO,
        /// <summary>Homeless</summary>
        FEMALE_SWMOTR4,
        /// <summary>The D.A</summary>
        MALE_SWMYRI,
        /// <summary>Afro-American</summary>
        MALE_SMYST,
        /// <summary>Mexican</summary>
        MALE_SMYST2,
        /// <summary>Prostitute</summary>
        MALE_SFYPRO,
        /// <summary>Stripper</summary>
        MALE_VBFYST2,
        /// <summary>Prostitute</summary>
        FEMALE_VBFYPRO,
        /// <summary>Stripper</summary>
        FEMALE_VHFYST3,
        /// <summary>Biker</summary>
        MALE_BIKERA,
        /// <summary>Biker</summary>
        MALE_BIKERB,
        /// <summary>Pimp</summary>
        MALE_BMYPIMP,
        /// <summary>Normal Ped</summary>
        MALE_SWMYCR,
        /// <summary>Lifeguard</summary>
        FEMALE_WFYLG,
        /// <summary>Naked Valet</summary>
        MALE_WMYVA2,
        /// <summary>Bus Driver</summary>
        MALE_BMOSEC,
        /// <summary>Biker Drug Dealer</summary>
        MALE_BIKDRUG,
        /// <summary>Chauffeur (Limo Driver)</summary>
        MALE_WMYCH,
        /// <summary>Stripper</summary>
        FEMALE_SBFYSTR,
        /// <summary>Stripper</summary>
        FEMALE_SWFYSTR,
        /// <summary>Heckler</summary>
        MALE_HECK1,
        /// <summary>Heckler</summary>
        MALE_HECK2,
        /// <summary>Construction Worker</summary>
        MALE_BMYCON,
        /// <summary>Cab Driver</summary>
        MALE_WMYCD1,
        /// <summary>Cab Driver</summary>
        MALE_BMOCD,
        /// <summary>Normal Ped</summary>
        MALE_VWFYWA2,
        /// <summary>Clown</summary>
        MALE_WMOICE,
        /// <summary>Officer Frank Tenpenny (Corrupt Cop)</summary>
        MALE_TENPEN,
        /// <summary>Officer Eddie Pulaski (Corrupt Cop)</summary>
        MALE_PULASKI,
        /// <summary>Officer Jimmy Hernandez</summary>
        MALE_HERN,
        /// <summary>Dwaine/Dwayne</summary>
        MALE_DWAYNE,
        /// <summary>Melvin "Big Smoke" Harris (Mission)</summary>
        MALE_SMOKE,
        /// <summary>Sean 'Sweet' Johnson</summary>
        MALE_SWEET,
        /// <summary>Lance 'Ryder' Wilson</summary>
        MALE_RYDER,
        /// <summary>Mafia Boss</summary>
        MALE_FORELLI,
        /// <summary>T-Bone Mendez</summary>
        MALE_TBONE,
        /// <summary>Paramedic</summary>
        MALE_LAEMT1,
        /// <summary>Paramedic</summary>
        MALE_LVEMT1,
        /// <summary>Paramedic</summary>
        MALE_SFEMT1,
        /// <summary>Firefighter</summary>
        MALE_LAFD1,
        /// <summary>Firefighter</summary>
        MALE_LVFD1,
        /// <summary>Firefighter</summary>
        MALE_SFFD1,
        /// <summary>Los Santos Police Officer</summary>
        MALE_LAPD1,
        /// <summary>San Fierro Police Officer</summary>
        MALE_SFPD1,
        /// <summary>Las Venturas Police Officer</summary>
        MALE_LVPD1,
        /// <summary>Country Sheriff</summary>
        MALE_CSHER,
        /// <summary>LSPD Motorbike Cop</summary>
        MALE_LAPDM1,
        /// <summary>S.W.A.T Special Forces</summary>
        MALE_SWAT,
        /// <summary>Federal Agent</summary>
        MALE_FBI,
        /// <summary>San Andreas Army</summary>
        MALE_ARMY,
        /// <summary>Desert Sheriff</summary>
        MALE_DSHER,
        /// <summary>Zero</summary>
        MALE_ZERO,
        /// <summary>Ken Rosenberg</summary>
        MALE_ROSE,
        /// <summary>Kent Paul</summary>
        MALE_PAUL,
        /// <summary>Cesar Vialpando</summary>
        MALE_CESAR,
        /// <summary>Jeffery "OG Loc" Martin/Cross</summary>
        MALE_OGLOC,
        /// <summary>Wu Zi Mu (Woozie)</summary>
        MALE_WUZIMU,
        /// <summary>Michael Toreno</summary>
        MALE_TORINO,
        /// <summary>Jizzy B.</summary>
        MALE_JIZZY,
        /// <summary>Madd Dogg</summary>
        MALE_MADDOGG,
        /// <summary>Catalina</summary>
        MALE_CAT,
        /// <summary>Claude Speed</summary>
        MALE_CLAUDE,
        /// <summary>Los Santos Police Officer (Without gun holster)</summary>
        MALE_LAPDNA,
        /// <summary>San Fierro Police Officer (Without gun holster)</summary>
        MALE_SFPDNA,
        /// <summary>San Fierro Police Officer (Without gun holster)</summary>
        MALE_LVPDNA,
        /// <summary>San Fierro Police Officer (Without gun holster)</summary>
        MALE_LAPDPC,
        /// <summary>Los Santos Police Officer (Without uniform)</summary>
        MALE_LAPDPD,
        /// <summary>Las Venturas Police Officer (Without uniform)</summary>
        MALE_LVPDPC,
        /// <summary>Los Santos Police Officer</summary>
        FEMALE_WFYCLPD,
        /// <summary>San Fierro Police Officer</summary>
        FEMALE_VBFYCPD,
        /// <summary>San Fierro Paramedic</summary>
        FEMALE_WFYCLEM,
        /// <summary>Las Venturas Police Officer</summary>
        FEMALE_WFYCLLV,
        /// <summary>Country Sheriff (Without hat)</summary>
        MALE_CSHERNA,
        /// <summary>Desert Sheriff (Without hat)</summary>
        MALE_DSHERNA,
    }
    static class SkinsMethods
    {
        public static int Int(Skins sk)
        {
            return (int)sk;
        }
    }
}
