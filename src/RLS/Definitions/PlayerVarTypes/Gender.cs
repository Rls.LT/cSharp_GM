﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLS.Definitions
{
    public enum Gender
    {
        NONE,
        MALE,
        FEMALE
    }
    static class GenderMethods
    {
        public static String GetString(this Gender s1)
        {
            switch (s1)
            {
                case Gender.MALE:
                    return "Vaikinas";
                case Gender.FEMALE:
                    return "Mergina";
                default:
                    return "Nenustatyta";
            }
        }
    }
}
