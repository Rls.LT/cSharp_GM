﻿using System.Collections.Generic;

namespace RLS.Definitions
{
    class Pooling<T>
    {
        private List<T> list = new List<T>();

        public T[] All { get => list.ToArray(); }

        public bool Contains(T item)
        {
            return list.Contains(item);
        }
        public virtual void Add(T item)
        {
            list.Add(item);
        }
        public void Remove(T item)
        {
            list.Remove(item);
        }
    }
}
