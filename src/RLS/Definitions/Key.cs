﻿using RLS.World.Players;
using SampSharp.GameMode.Definitions;


namespace RLS.Definitions
{
    public enum Key
    {
        Y,
        N,
        H,
        SPACE,
        NUM_2,
        NUM_4,
        NUM_6,
        NUM_8
    }
    static class KeyMethods
    {
        public static Keys GtaKey(this Key s1, Player player)
        {
            switch (s1)
            {
                case Key.N:
                    return Keys.No;
                case Key.SPACE:
                    {
                        if (player.InAnyVehicle)
                            return Keys.Handbrake;
                        else
                            return Keys.Sprint;
                    }
                case Key.H:
                    {
                        if (player.InAnyVehicle)
                            return Keys.Crouch;
                        else
                            return Keys.CtrlBack;
                    }
                case Key.Y:
                    return Keys.Yes;
                case Key.NUM_2:
                    return Keys.AnalogDown;
                case Key.NUM_4:
                    return Keys.AnalogLeft;
                case Key.NUM_6:
                    return Keys.AnalogRight;
                case Key.NUM_8:
                    return Keys.AnalogUp;
                default:
                    return Keys.Yes;
            }
        }
    }
}
