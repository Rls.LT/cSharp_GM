﻿namespace RLS.Definitions
{
    public enum EngineType
    {
        Petrol,
        Diesel,
        Electric
    }
    public enum FuelType
    {
        Petrol,
        Diesel,
        Electricity
    }
    static class FuelTypeMethods
    {
        public static bool ValidForEngine(this FuelType fuel, EngineType type)
        {
            switch (type)
            {
                case EngineType.Diesel:
                    if (fuel == FuelType.Diesel) return true;
                    else return false;
                case EngineType.Petrol:
                    if (fuel == FuelType.Petrol) return true;
                    else return false;
            }
            return false;
        }
        public static string Name(this FuelType fuel)
        {
            switch (fuel)
            {
                case FuelType.Petrol:
                    return "Benzinas";
                case FuelType.Diesel:
                    return "Dyzelinas";
                case FuelType.Electricity:
                    return "Elektra";
                default:
                    return "Nenustatyta";
            }
        }
    }
}
