﻿using RLS.Display.Builder;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using System;

namespace RLS.Display
{
    class TutorialUI : IUI
    {
        private UI title;
        private UI title_bg;
        private UI body;
        private UI body_bg;
        private float posX = 557;
        private bool isDisposed = true;

        public TutorialUI(Player owner)
        {
            title = new UI(owner)
            {
                Position = new Vector2(posX, 336.000000),
                Text = "Title",
                BackColor = 255,
                Font = TextDrawFont.Slim,
                LetterSize = new Vector2(0.270000, 1.899999),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 1,
                Selectable = false,
                AutoDestroy = true
            };
            title_bg = new UI(owner)
            {
                Position = new Vector2(posX, 337.000000),
                Height = 0f,
                Width = 636f,
                Text = "_",
                BackColor = 255,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(0.500000, 1.799999),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 1,
                UseBox = true,
                BoxColor = 108396187,
                Selectable = false,
                AutoDestroy = true
            };
            title_bg.SetColor(108396187, 255);
            body = new UI(owner)
            {
                Position = new Vector2(posX + 2, 356.000000),
                Text = "_",
                BackColor = 255,
                Font = TextDrawFont.Slim,
                LetterSize = new Vector2(0.150000, 1.199999),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 1,
                Selectable = false,
                AutoDestroy = true
            };
            body_bg = new UI(owner)
            {
                Position = new Vector2(posX, 356.000000),
                Height = -15f,
                Width = 636f,
                Text = "_",
                BackColor = 255,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(0.529999, 6.799999),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 1,
                UseBox = true,
                BoxColor = 155,
                Selectable = false,
                AutoDestroy = true
            };
            body_bg.SetColor(155, 150);
            isDisposed = false;
        }

        public TutorialUI SetText(string text)
        {
            if (isDisposed) return this;
            if (text == null) return this;
            body.Text = UI.FixText(text);
            
            float offset = LongestLine(text)*3.5f;
            if (636f - offset < posX)
            {
                Vector2 tempPos = title.Position;
                if(tempPos.X!= 636f - offset + 2) title.Position = new Vector2(636f - offset + 2, tempPos.Y);
                tempPos = title_bg.Position;
                if (tempPos.X != 636f - offset) title_bg.Position = new Vector2(636f - offset, tempPos.Y);
                tempPos = body.Position;
                if (tempPos.X != 636f - offset + 2) body.Position = new Vector2(636f - offset + 2, tempPos.Y);
                tempPos = body_bg.Position;
                if (tempPos.X != 636f - offset) body_bg.Position = new Vector2(636f - offset, tempPos.Y);
            }
            else
            {
                Vector2 tempPos = title.Position;
                if (tempPos.X != posX + 2) title.Position = new Vector2(posX + 2, title.Position.Y);
                tempPos = title_bg.Position;
                if (tempPos.X != posX) title_bg.Position = new Vector2(posX, title_bg.Position.Y);
                tempPos = body.Position;
                if (tempPos.X != posX + 2) body.Position = new Vector2(posX + 2, 356.000000);
                tempPos = body_bg.Position;
                if (tempPos.X != posX) body_bg.Position = new Vector2(posX, 356.000000);
            }
            return this;
        }

        private int LongestLine(string text)
        {
            string[] splits = text.Split(new string[] { "\n" }, StringSplitOptions.None);
            int longest = 0;
            foreach (string part in splits)
            {
                if (longest < part.Length) longest = part.Length;
            }
            return longest;
        }

        public TutorialUI SetTitle(string text)
        {
            if (isDisposed) return this;
            if (text == null) return this;
            title.Text = UI.FixText(text);
            return this;
        }

        public void Dispose()
        {
            if (isDisposed) return;
            isDisposed = true;
            title.Dispose();
            title_bg.Dispose();
            body.Dispose();
            body_bg.Dispose();
        }

        public void Hide()
        {
            if (isDisposed) return;
            title.Hide();
            title_bg.Hide();
            body.Hide();
            body_bg.Hide();
        }

        public void Show(int hideAfter = -1)
        {
            if (isDisposed) return;
            title.Show(hideAfter);
            title_bg.Show(hideAfter);
            body_bg.Show(hideAfter);
            body.Show(hideAfter);
        }
    }
}
