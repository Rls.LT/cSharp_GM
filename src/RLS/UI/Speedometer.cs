﻿using RLS.Display.Builder;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;

namespace RLS.Display
{
    class Speedometer : IUI
    {
        private UI body_bg;
        private UI car_name;
        private UI info;

        private bool isDisposed = true;

        public Speedometer(Player owner)
        {
            body_bg = new UI(owner)
            {
                Position = new Vector2(388.000000, 371.000000),
                Height = 5f,
                Width = 240f,
                Text = "_",
                BackColor = 255,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(0.500000, 7.000000),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 1,
                UseBox = true,
                Selectable = false,
                AutoDestroy = true
            };
            body_bg.SetColor(707406536, 155);
            car_name = new UI(owner)
            {
                Position = new Vector2(242.000000, 374.000000),
                Text = "Pavadinimas",
                BackColor = 255,
                Size = new Vector2(330.000000, 30.000000),
                Font = TextDrawFont.Slim,
                LetterSize = new Vector2(0.189999, 1.000000),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                UseBox = true,
                Shadow = 0,
                Selectable = false,
                AutoDestroy = true,
            };
            car_name.SetColor(447610111, 255);
            info = new UI(owner)
            {
                Position = new Vector2(246.000000, 388.000000),
                Text = "info",
                BackColor = 255,
                Font = TextDrawFont.Slim,
                LetterSize = new Vector2(0.159998, 1.000000),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 0,
                Selectable = false,
                AutoDestroy = true,
            };
            isDisposed = false;
        }
        public Speedometer Update(string carName, string text)
        {
            if (isDisposed) return this;
            info.Text = UI.FixText(text);
            car_name.Text = UI.FixText(carName);
            return this;
        }
        public void Dispose()
        {
            if (isDisposed) return;
            isDisposed = true;
            body_bg.Dispose();
            info.Dispose();
            car_name.Dispose();
        }

        public void Hide()
        {
            if (isDisposed) return;
            body_bg.Hide();
            info.Hide();
            car_name.Hide();
        }

        public void Show(int hideAfter = -1)
        {
            if (isDisposed) return;
            body_bg.Show(hideAfter);
            info.Show(hideAfter);
            car_name.Show(hideAfter);
        }
    }
}
