﻿using RLS.Definitions;
using RLS.World.Players;

namespace RLS.Display.Controllers
{
    class InfoLines : Pooling<InfoLine>
    {
        private Player owner;
        private double step = 21; // distance between 2 info lines
        public InfoLines(Player player)
        {
            owner = player;
        }
        public void Dispose()
        {
            foreach (InfoLine line in All)
            {
                Remove(line);
                line.Dispose();
            }
        }
        public override void Add(InfoLine item)
        {
            item.Hidden += (sender, e) =>
            {
                Resort();
            };
            base.Add(item);
        }

        public void Show(InfoLine line, int hideAfter = -1)
        {
            if (line == null) return;
            if (!Contains(line)) return;
            if (line.IsShown)
            {
                line.Show(hideAfter);
                return;
            }
            int showing = 0;
            foreach(InfoLine _line in All)
            {
                if (_line.IsShown)
                {
                    _line.PositionOnScreen = showing++;
                }
            }
            line.PositionOnScreen = showing;
            line.ChangePosition(-(step * showing));
            line.Show(hideAfter);
        }

        public void Hide(InfoLine line)
        {
            if (!Contains(line)) return;
            line.Hide();
            Resort();
        }

        private void Resort()
        {
            int showing = 0;
            foreach (InfoLine _line in All)
            {
                if (_line.IsShown)
                {
                    if(_line.PositionOnScreen != showing)
                    {
                        double offset = step * showing;
                        if (offset == 0)
                            _line.ChangePosition(0);
                        else
                            _line.ChangePosition(-offset);
                        _line.ForceShow();
                    }
                    showing++;
                }
            }
        }

        public InfoLine GetLineByName(string nameToFind)
        {
            foreach (InfoLine line in All)
            {
                if (line.Name.Equals(nameToFind))
                    return line;
            }
            return null;
        }
    }
}
