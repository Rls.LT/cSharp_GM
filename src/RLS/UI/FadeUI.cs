﻿using RLS.Display.Builder;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;

namespace RLS.Display
{
    public class FadeUI : IUI
    {
        private UI ui;
        private bool isDisposed = true;
        public FadeUI(Player player)
        {
            ui = new UI(player)
            {
                Position = new Vector2(651.000000, 0.000000),
                Height = 0f,
                Width = -2f,
                Text = "_",
                BackColor = 255,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(0.500000, 50.000000),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 0,
                UseBox = true,
                BoxColor = 255,
                Selectable = false,
                AutoDestroy = true
            };
            ui.SetColor(255, 255);
            isDisposed = false;
        }

        public void Dispose()
        {
            if (isDisposed) return;
            isDisposed = true;
            ui.Dispose();
        }

        public void FadeIn(float timer)
        {
            if (isDisposed) return;
            ui.FadeIn(timer);
        }

        public void FadeOut(float timer)
        {
            if (isDisposed) return;
            ui.FadeOut(timer);
        }

        public void Hide()
        {
            throw new System.NotImplementedException();
        }

        public void Show(int hideAfter)
        {
            throw new System.NotImplementedException();
        }
    }
}
