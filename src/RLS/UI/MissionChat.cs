﻿
using RLS.Display.Builder;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;

namespace RLS.Display
{
    class MissionChat : IUI
    {
        private bool isDisposed = true;
        private UI ui;

        public MissionChat(Player player)
        {
            ui = new UI(player)
            {
                Position = new Vector2(339.000000, 364.000000),
                Text = "_",
                BackColor = 255,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(0.310000, 1.200000),
                ForeColor = -1,
                Outline = 1,
                Proportional = true,
                Alignment = TextDrawAlignment.Center,
                Shadow = 1,
                Selectable = false,
                AutoDestroy = true
            };
            isDisposed = false;
        }

        public MissionChat SetText(string text)
        {
            if (isDisposed) return this;
            if (text == null) return this;
            ui.Text = UI.FixText(text);
            return this;
        }

        public void Dispose()
        {
            if (isDisposed) return;
            isDisposed = true;
            ui.Dispose();
        }

        public void Hide()
        {
            if (isDisposed) return;
            ui.Hide();
        }

        public void Show(int hideAfter = -1)
        {
            if (isDisposed) return;
            ui.Show(hideAfter);
        }
    }
}
