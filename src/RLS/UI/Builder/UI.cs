﻿using RLS.Definitions;
using RLS.Events.Args;
using RLS.World.Players;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP;
using System;

namespace RLS.Display.Builder
{
    public partial class UI : PlayerTextDraw
    {
        Timer timer;
        private bool showing = false;
        private string boxColor = "000000";
        private int opacity = 0;

        public bool IsShown { get => showing; }

        public event EventHandler<UIArgs> Shown;
        public event EventHandler<UIArgs> Hidden;

        public UI(Player owner) : base(owner)
        {

        }

        public void SetColor(Color color, int opacity)
        {
            boxColor = Colors.GetHex(color, false);
            this.opacity = opacity;
        }
        public void ForceShow()
        {
            base.Show();
        }
        public void Show(int hideAfter = -1)
        {
            BoxColor = FromHex(boxColor, opacity);
            if (!showing)
                base.Show();
            showing = true;
            Shown?.Invoke(null, new UIArgs(this, Owner as Player));
            if (hideAfter == -1) return;
            if (timer != null)
                timer.Dispose();
            timer = new Timer(hideAfter, false);
            timer.Tick += (sender, e) =>
            {
                Hide();
                timer = null;
            };
        }

        public new void Hide()
        {
            if (!showing) return;
            showing = false;
            if(!IsDisposed)
                base.Hide();
            if (timer != null)
                timer.Dispose();
            Hidden?.Invoke(null, new UIArgs(this, Owner as Player));
        }

    }
}
