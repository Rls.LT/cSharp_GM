﻿using SampSharp.GameMode;
using SampSharp.GameMode.SAMP;

namespace RLS.Display.Builder
{
    partial class UI
    {
        public Vector2 Size { get => new Vector2(Width, Height); set { Width = value.X; Height = value.Y; } }

        public static string FixText(string text)
        {
            //encoding fix
            text = text.Replace("ą", "a").Replace("Ą", "A")
                .Replace("č", "c").Replace("Č", "C")
                .Replace("ę", "e").Replace("Ę", "E")
                .Replace("ė", "e").Replace("Ė", "E")
                .Replace("į", "i").Replace("Į", "I")
                .Replace("š", "s").Replace("Š", "S")
                .Replace("ų", "u").Replace("Ų", "U")
                .Replace("ū", "u").Replace("Ū", "U")
                .Replace("ž", "z").Replace("Ž", "Z");
            //new line fix
            text = text.Replace("\n", "~n~");
            return text;
        }

        public static Color FromRgb(int red, int green, int blue, int alpha)
        {
            return (red * 16777216) + (green * 65536) + (blue * 256) + alpha;
        }
        public static Color FromHex(string color, int opacity)
        {
            try
            {
                return FromRgb(
                    System.Convert.ToInt32(color.Substring(0,2), 16),
                    System.Convert.ToInt32(color.Substring(2, 2), 16),
                    System.Convert.ToInt32(color.Substring(4, 2), 16), opacity);
            }
            catch
            {
                return FromRgb(0, 0, 0, opacity);
            }
        }
    }
}
