﻿using SampSharp.GameMode.SAMP;
using System;

namespace RLS.Display.Builder
{
    partial class UI
    {
        private bool fading = false;

        public bool IsFading { get => fading; }

        public void FadeIn(float time = 500)
        {
            if (showing) return;
            fading = true;
            BoxColor = FromHex(boxColor, 0);
            showing = true;
            base.Show();
            int updateTimes = (int)Math.Round(time / 50);
            int currentOpacity = 0;
            Timer timer = new Timer(50, true);
            timer.Tick += (sender, e) =>
            {
                currentOpacity += (int)Math.Round(opacity * 1.0 / updateTimes);
                updateTimes--;
                if (currentOpacity > opacity || updateTimes < 1)
                {
                    currentOpacity = opacity;
                    timer.IsRepeating = false;
                    fading = false;
                }
                BoxColor = FromHex(boxColor, currentOpacity);
                base.Show();
            };
        }

        public void FadeOut(float time = 500)
        {
            if (!showing) return;
            fading = true;
            BoxColor = FromHex(boxColor, opacity);
            base.Show();
            int updateTimes = (int)Math.Round(time / 50);
            int currentOpacity = opacity;
            Timer timer = new Timer(50, true);
            timer.Tick += (sender, e) =>
            {
                currentOpacity -= (int)Math.Round(opacity * 1.0 / updateTimes);
                updateTimes--;
                if (currentOpacity < 1 || updateTimes < 1)
                {
                    timer.IsRepeating = false;
                    BoxColor = FromHex(boxColor, 0);
                    Hide();
                    fading = false;
                    return;
                }
                BoxColor = FromHex(boxColor, currentOpacity);
                base.Show();
            };
        }
    }
}
