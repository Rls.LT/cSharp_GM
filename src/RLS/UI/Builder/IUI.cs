﻿namespace RLS.Display.Builder
{
    interface IUI
    {
        void Show(int hideAfter);
        void Hide();
        void Dispose();
    }
}
