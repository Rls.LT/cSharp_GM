﻿
using RLS.Display.Builder;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;

namespace RLS.Display
{
    class Objective : IUI
    {
        private UI text = null;
        private UI line_top = null;
        private UI background = null;
        private UI line_bottom = null;
        private bool isDisposed = true;

        public Objective(Player player)
        {
            text = new UI(player)
            {
                Position = new Vector2(322.000000, 107.000000),
                Text = "Title",
                BackColor = 255,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(0.200000, 1.200000),
                ForeColor = -1,
                Outline = 0,
                Alignment = TextDrawAlignment.Center,
                Proportional = true,
                Shadow = 0,
                Selectable = false,
                AutoDestroy = true
            };
            line_top = new UI(player)
            {
                Position = new Vector2(213.000000, 102.000000),
                Text = ".",
                BackColor = 255,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(21.520046, 0.299999),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 0,
                Selectable = false,
                AutoDestroy = true
            };
            line_bottom = new UI(player)
            {
                Position = new Vector2(211.000000, 121.000000),
                Text = ".",
                BackColor = 255,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(21.520046, 0.299999),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 0,
                Selectable = false,
                AutoDestroy = true
            };
            background = new UI(player)
            {
                Position = new Vector2(155.000000, 130.000000),
                Text = "I",
                BackColor = 255,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(27.990070, -3.399999),
                ForeColor = 255,
                Outline = 0,
                Proportional = true,
                Shadow = 0,
                Selectable = false,
                AutoDestroy = true
            };
            isDisposed = false;
        }

        public Objective SetText(string text)
        {
            if (isDisposed) return this;
            if (text == null) return this;
            this.text.Text = UI.FixText(text);
            return this;
        }

        public void Dispose()
        {
            if (isDisposed) return;
            isDisposed = true;
            line_bottom.Dispose();
            line_top.Dispose();
            background.Dispose();
            text.Dispose();
        }

        public void Hide()
        {
            if (isDisposed) return;
            line_bottom.Hide();
            line_top.Hide();
            background.Hide();
            text.Hide();
        }

        public void Show(int hideAfter = -1)
        {
            if (isDisposed) return;
            line_bottom.Show(hideAfter);
            line_top.Show(hideAfter);
            background.Show(hideAfter);
            text.Show(hideAfter);
        }
    }
}
