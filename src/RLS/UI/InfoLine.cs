﻿using RLS.Display.Builder;
using RLS.Events.Args;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.SAMP;
using System;

namespace RLS.Display
{
    public class InfoLine : IUI
    {
        private UI background;
        private UI text;
        private UI icon;
        private UI additional;
        private bool isDisposed = true;
        private string name = "unset";
        private Player owner;
        public int PositionOnScreen { get; set; }

        public event EventHandler<InfolineArgs> Shown;
        public event EventHandler<InfolineArgs> Hidden;

        public string Name { get => name; }
        public bool IsShown { get => background.IsShown; }

        public InfoLine(Player player, string name)
        {
            this.owner = player;
            this.name = name;
            background = new UI(player)
            {
                Text = "_",
                Position = new Vector2(538.000000, 428.000000),
                BackColor = 255,
                BoxColor = -13434625,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(0.500000, 1.600000),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 1,
                UseBox = true,
                AutoDestroy = true,
                Selectable = false,
                Width = 638,
                Height = 0
            };
            background.SetColor(-13434625, 255);
            background.Hidden += (sender, e) => { Hide(); };
            text = new UI(player)
            {
                Text = "text",
                Position = new Vector2(553.000000, 429.000000),
                BackColor = 255,
                BoxColor = -13434625,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(0.200000, 1.200000),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 0,
                Selectable = false
            };
            additional = new UI(player)
            {
                Text = "00:00",
                Position = new Vector2(635.000000, 429.000000),
                Alignment = TextDrawAlignment.Right,
                BackColor = 255,
                BoxColor = -13434625,
                Font = TextDrawFont.Normal,
                LetterSize = new Vector2(0.200000, 1.200000),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 0,
                Selectable = false
            };
            icon = new UI(player)
            {
                Text = "hud:radar_hostpital",
                Position = new Vector2(539.000000, 430.000000),
                BackColor = 255,
                BoxColor = 120,
                Font = TextDrawFont.DrawSprite,
                LetterSize = new Vector2(0.500000, 1.000000),
                ForeColor = -1,
                Outline = 0,
                Proportional = true,
                Shadow = 0,
                Selectable = false,
                UseBox = true,
                Width = 11,
                Height = 11 
            };
            isDisposed = false;
        }
        public void ChangePosition(double verticalOffset)
        {
            background.Position = new Vector2(538.000000, 428 + verticalOffset);
            text.Position = new Vector2(553.000000, 429 + verticalOffset);
            additional.Position = new Vector2(635.000000, 429 + verticalOffset);
            icon.Position = new Vector2(539.000000, 430 + verticalOffset);
        }
        public InfoLine SetText(string text)
        {
            if (isDisposed) return this;
            if (text == null) return this;
            this.text.Text = UI.FixText(text);
            return this;
        }
        public InfoLine SetBackgroundColor(Color color)
        {
            if (isDisposed) return this;
            background.SetColor(color, 255);
            return this;
        }
        public InfoLine SetAdditionalText(string text)
        {
            if (isDisposed) return this;
            if (text == null) return this;
            additional.Text = UI.FixText(text);
            return this;
        }
        public InfoLine SetIcon(string text)
        {
            if (isDisposed) return this;
            if (text == null) return this;
            icon.Text = text;
            return this;
        }
        public void ForceShow()
        {
            background.ForceShow();
            icon.ForceShow();
            text.ForceShow();
            additional.ForceShow();
        }
        public void Show(int hideAfter = -1)
        {
            if (isDisposed) return;
            if(IsShown) ForceShow();
            background.Show(hideAfter);
            icon.Show(hideAfter);
            text.Show(hideAfter);
            additional.Show(hideAfter);
            Shown?.Invoke(null, new InfolineArgs(this, owner));
        }
        public void Hide()
        {
            if (isDisposed) return;
            background.Hide();
            icon.Hide();
            text.Hide();
            additional.Hide();
            Hidden?.Invoke(null, new InfolineArgs(this, owner));
        }
        public void Dispose()
        {
            if (isDisposed) return;
            isDisposed = true;
            background.Dispose();
            icon.Dispose();
            text.Dispose();
            additional.Dispose();
        }
    }
}
