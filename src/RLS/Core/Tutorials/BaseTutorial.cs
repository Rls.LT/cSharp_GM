﻿using RLS.Events.Args;
using RLS.Missions;
using RLS.Missions.Creator;
using RLS.World.Players;
using SampSharp.GameMode.SAMP;
using System;

namespace RLS.Core.Tutorials
{
    public abstract partial class BaseTutorial : Mission
    {
        public const int MaxTutorials = 20;
        /// <summary>
        /// Id of tutorial. Has to be UNIQUE
        /// </summary>
        public int Id { get; protected set; }
        /// <summary>
        /// Times to play this tutorial
        /// </summary>
        public int TimesToPlay { get => timesToPlay;
            protected set
            {
                if (value > 9 || value < 1) return;
                timesToPlay = value;
            }
        }
        private int timesToPlay = 1;
        /// <summary>
        /// Called when tutorial action is ended
        /// </summary>
        public new static event EventHandler<MissionArgs> ActionEnded;
        /// <summary>
        /// called after every tutorial step
        /// </summary>
        public event EventHandler<MissionArgs> TutorialStep;

        public override void SceneTimer(Step step)
        {
            timer = new Timer(step.Time, false);
            timer.Tick += (sender, e) =>
            {
                CurrentStep++;
                if (!step.IsLast)
                    ProcessStep();
                else
                    ActionEnded?.Invoke(null, new MissionArgs(this, Owner));
                TutorialStep?.Invoke(null, new MissionArgs(this, Owner));
            };
        }
        public BaseTutorial(Player player, string tutName) : base(player, tutName)
        {
        }
    }
}
