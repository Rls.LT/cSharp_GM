﻿using RLS.Missions.Creator;
using RLS.World.Players;
using SampSharp.GameMode;

namespace RLS.Core.Tutorials.Scripts
{
    class Entrance : BaseTutorial
    {
        public Entrance(Player player) : base(player, "Entrance")
        {
            Id = 1;
        }
        protected override void Create()
        {
            RLS.World.Entrances.Entrance entranceUsed = Owner.LastEntranceUsed;
            Step(new Control(false));
            Step(new Camera(entranceUsed.OutsidePos + Functions.PointFarFromPoint(12, entranceUsed.Exitangle+90) + new Vector3(0, 0, 1), entranceUsed.OutsidePos, 100));
            Step(new Tutorial("Apmokymai", "Pastatų įėjimai/išėjimai yra pažymėti geltonom rodyklėm ir užrašu.", 4000));
            Step(new Screen(1000, false));
            Step(new Camera(100));
            Step(new Control(true));
            Step(new Screen(1000, true).LastStepInScene());
        }
    }
}
