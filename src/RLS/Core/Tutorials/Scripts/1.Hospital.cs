﻿using SampSharp.GameMode;
using RLS.World.Players;
using RLS.Missions.Creator;

namespace RLS.Core.Tutorials.Scripts
{
    class Hospital : BaseTutorial
    {
        public Hospital(Player player) : base(player, "Hospital")
        {
            Id = 0;
        }
        protected override void Create()
        {
            Step(new Control(false));
            Step(new Camera(new Vector3(-2504.1860, 563.6412, 69.3339), new Vector3(-2505.1055, 564.0457, 69.2637), 100));
            Step(new Tutorial("Apmokymai", "Tai San Fierro ligoninė. Joje atsirasi kiekvieną kartą po sunkaus sužeidimo.", 4000));
            Step(new Camera(new Vector3(-2652.9246, 628.0325, 17.9172), new Vector3(-2652.3333, 628.8388, 17.6813), 100));
            Step(new Tutorial("Apmokymai", "Į ligoninę, kaip ir į kitus pastatus gali įeiti kada nori.", 4000));
            Step(new Screen(1000, false));
            Step(new Camera(100));
            Step(new Control(true));
            Step(new Screen(1000, true).LastStepInScene());
        }
    }
}
