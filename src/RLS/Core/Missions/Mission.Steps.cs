﻿using RLS.Events.Args;
using RLS.Missions.Creator;
using SampSharp.GameMode.SAMP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RLS.Missions
{
    partial class Mission
    {
        /// <summary>
        /// Called when mission action is ended
        /// </summary>
        public static event EventHandler<MissionArgs> ActionEnded;
        /// <summary>
        /// called after every mission step
        /// </summary>
        public event EventHandler<MissionArgs> MissionStep;

        private List<Step> steps = new List<Step>();

        /// <summary>
        /// Total steps in mission
        /// </summary>
        public int StepCount { get => steps.Count; }

        /// <summary>
        /// Current step in mission
        /// </summary>
        public int CurrentStep { get; protected set; }

        /// <summary>
        /// Add step to Mission steps list
        /// </summary>
        /// <param name="step">Step to add</param>
        public void Step(Step step)
        {
            step.Mission(this);
            steps.Add(step);
        }

        /// <summary>
        /// Clear all steps from list
        /// </summary>
        public void ClearSteps()
        {
            foreach (Step step in steps)
                step.Dispose();
            steps.Clear();
        }

        /// <summary>
        /// Get step by Id
        /// </summary>
        /// <param name="stepId">Step id to get [default: 0]</param>
        /// <returns></returns>
        public Step GetStep(int stepId = 0)
        {
            if (StepCount > stepId)
                return steps.ElementAt(stepId);
            else return null;
        }

        /// <summary>
        /// Remove step from step list
        /// </summary>
        /// <param name="stepId">Id to remove[default: 0]</param>
        public void RemoveStep(int stepId = 0)
        {
            if (StepCount > stepId)
                steps.RemoveAt(stepId);
        }
        /// <summary>
        /// Process step in mission
        /// </summary>
        protected void ProcessStep()
        {
            if (StepCount < 1) return;
            Step step = GetStep(CurrentStep);
            step.Proceed(Owner);
            SceneTimer(step);
        }

        public virtual void SceneTimer(Step step)
        {
            timer = new Timer(step.Time, false);
            timer.Tick += (sender, e) =>
            {
                CurrentStep++;
                if (!step.IsLast)
                    ProcessStep();
                else
                    ActionEnded?.Invoke(null, new MissionArgs(this, Owner));
                MissionStep?.Invoke(null, new MissionArgs(this, Owner));
            };
        }
    }
}
