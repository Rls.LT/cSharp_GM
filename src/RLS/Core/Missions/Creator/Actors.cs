﻿using RLS.World.Players;
using SampSharp.GameMode;

namespace RLS.Missions.Creator
{
    class Actors : Step
    {
        enum Type
        {
            CreateActor,
            DisposeActor
        }
        private Type type;
        private int model;
        private Vector3 position;
        private float facing;
        private string name;
        /// <summary>
        /// Create actor
        /// </summary>
        /// <param name="name">Actor name</param>
        /// <param name="model">Actor skin model</param>
        /// <param name="position">Position to create actor at</param>
        /// <param name="facing">Actor facing angle</param>
        public Actors(string name, int model, Vector3 position, float facing)
        {
            this.name = name;
            this.model = model;
            this.position = position;
            this.facing = facing;
            type = Type.CreateActor;
            time = 50;
        }
        /// <summary>
        /// Dispose actor
        /// </summary>
        /// <param name="actor">Actor to dispose</param>
        public Actors(string name)
        {
            this.name = name;
            type = Type.DisposeActor;

        }

        public override void Proceed(Player player)
        {
            RLS.World.Actor actor;
            switch (type)
            {
                case Type.CreateActor:
                    actor = World.Actor.Create(model, position, facing) as RLS.World.Actor;
                    if (actor == null) Log.Write("failed to create actor");
                    actor.VirtualWorld = player.VirtualWorld;
                    actor.SetName(name);
                    mission.AddActor(actor);
                    break;
                case Type.DisposeActor:
                    actor = mission.GetActorByName(name);
                    mission.RemoveActor(actor);
                    actor.Dispose();
                    break;
            }
        }
    }
}
