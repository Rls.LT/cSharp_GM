﻿using RLS.World.Players;

namespace RLS.Missions.Creator
{
    class Screen : Step
    {
        private bool fadeIn;
        /// <summary>
        /// Fade in or fade out scene
        /// </summary>
        /// <param name="stepTime">Time for effect</param>
        /// <param name="fadein">Fade in?</param>
        public Screen(int stepTime, bool fadeIn = false)
        {
            this.fadeIn = fadeIn;
            this.time = stepTime;
        }

        public override void Proceed(Player player)
        {
            if (fadeIn)
                player.Screen?.FadeOut(time);
            else
                player.Screen?.FadeIn(time);
        }
    }
}
