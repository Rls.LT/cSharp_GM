﻿using RLS.Streamer;
using RLS.World.Players;
using SampSharp.GameMode;

namespace RLS.Missions.Creator
{
    class Checkpoint : Step
    {
        private Vector3 pos;
        private float size;
        DynamicCheckpoint cp;
        // TODO: Change to dynamic cp when I can
        /// <summary>
        /// Create checkpoint for player
        /// </summary>
        /// <param name="position">Checkpoint position</param>
        /// <param name="size">Checkpoint size</param>
        public Checkpoint(Vector3 position, float size)
        {
            this.size = size;
            pos = position;
            time = 50;
        }
        public override void Proceed(Player player)
        {
            cp = new DynamicCheckpoint(pos, size, -1, -1, player);
            cp.Enter += (sender, e) =>
            {
                mission.Start();
                cp.Dispose();
            };
        }
        public override void Dispose()
        {
            cp?.Dispose();
            base.Dispose();
        }
    }
}
