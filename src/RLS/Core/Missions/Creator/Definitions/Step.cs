﻿using RLS.World.Players;
namespace RLS.Missions.Creator
{
    public abstract class Step
    {
        /// <summary>
        /// Step actor name
        /// </summary>
        protected string actorName;
        /// <summary>
        /// What should mission controller do after this step(end scene or not)
        /// </summary>
        private bool last;
        public bool IsLast{ get => last; }
        /// <summary>
        /// Time till step will end
        /// </summary>
        protected int time;
        public int Time { get => time; }
        /// <summary>
        /// Step will be last before pause/end of mission
        /// </summary>
        /// <returns></returns>
        public Step LastStepInScene()
        {
            last = true;
            return this;
        }
        protected Mission mission;
        public void Mission(Mission mission)
        {
            this.mission = mission;
        }
        /// <summary>
        /// Proceed step action
        /// </summary>
        /// <param name="player"></param>
        public abstract void Proceed(Player player);

        public virtual void Dispose()
        {}
    }
}
