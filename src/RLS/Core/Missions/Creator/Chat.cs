﻿using RLS.World.Players;

namespace RLS.Missions.Creator
{
    class Chat : Step
    {
        private string chatText;
        private bool useAnim;
        /// <summary>
        /// Actor speak step
        /// </summary>
        /// <param name="actor">Actor instance who will speak</param>
        /// <param name="textToSay">Text which actor has to say</param>
        /// <param name="stepTime">Time until next step</param>
        public Chat(string actorName, string textToSay, int stepTime = 2000)
        {
            this.actorName = actorName;
            SetVariables(textToSay, stepTime);
        }
        /// <summary>
        /// Player speak step
        /// </summary>
        /// <param name="textToSay">Test to display</param>
        /// <param name="stepTime">Time</param>
        /// <param name="useanim">Use speaking animation</param>
        public Chat(string textToSay, int stepTime = 2000, bool useanim = true)
        {
            SetVariables(textToSay, stepTime);
            useAnim = useanim;
        }
        private void SetVariables(string textToSay, int stepTime = 2000)
        {
            chatText = textToSay;
            time = stepTime;
        }
        public override void Proceed(Player player)
        {
            if (actorName != null)
            {
                player.ShowMissionChat("~b~" + mission?.GetActorByName(actorName)?.GetName() + "~w~: " + chatText, time);
                mission?.GetActorByName(actorName)?.TalkingStanding(time);
            }
            else
            {
                player.ShowMissionChat("~r~Tu~w~: " + chatText, time);
                if(useAnim)
                    player.ApplyAnimation("ped", "IDLE_chat", 4.1f, false, true, true, false, time);
            }
        }
    }
}
