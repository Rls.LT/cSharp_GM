﻿using RLS.World.Players;

namespace RLS.Missions.Creator
{
    class Objective : Step
    {
        private string text;
        public Objective(string text, int timeToShow)
        {
            this.text = text;
            time = timeToShow;
        }
        public override void Proceed(Player player)
        {
            player.ShowInfoBar(text, time);
            player.PlaySound(1137);
        }
    }
}
