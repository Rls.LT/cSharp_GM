﻿using RLS.World.Players;

namespace RLS.Missions.Creator
{
    class Tutorial : Step
    {

        private string title;
        private string text;
        /// <summary>
        /// Show tutorial UI for player
        /// </summary>
        /// <param name="title">UI title</param>
        /// <param name="text">Text to show</param>
        /// <param name="stepTime">Time to show</param>
        public Tutorial(string title, string text, int stepTime = 2000)
        {
            this.title = title;
            this.text = text;
            time = stepTime;
        }

        public override void Proceed(Player player)
        {
            player.ShowTutorial(title, text, time);
        }
    }
}
