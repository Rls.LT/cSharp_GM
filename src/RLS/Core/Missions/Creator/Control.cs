﻿using RLS.World.Players;
using SampSharp.GameMode;
namespace RLS.Missions.Creator
{
    class Control : Step
    {
        enum Type
        {
            ToggleControls,
            Teleport,
            PlaySound
        }
        private Type type;
        private bool toggle;
        private Vector3 teleportTo;
        private float facing;
        private int ID = -1;
        private int vWID = -1;
        private int interiorID = -1;

        /// <summary>
        /// Play sound to player
        /// </summary>
        /// <param name="soundID">Sound id</param>
        public Control(int soundID)
        {
            ID = soundID;
            type = Type.PlaySound;
        }
        /// <summary>
        /// Set player skin to
        /// </summary>
        /// <param name="skinId">Skin id</param>
        /// <param name="skin">unused</param>
        public Control Skin(int skinId)
        {
            ID = skinId;
            return this;
        }
        /// <summary>
        /// Teleport player to position.
        /// </summary>
        /// <param name="newPos">Position to teleport at</param>
        /// <param name="facing">Player facing angle</param>
        public Control(Vector3 newPos, float facing)
        {
            this.teleportTo = newPos;
            this.facing = facing;
            type = Type.Teleport;
        }
        /// <summary>
        /// Teleport actor to position
        /// </summary>
        /// <param name="actorName">Actor to teleport</param>
        /// <param name="newPos">Position</param>
        /// <param name="facing">Facing angle</param>
        public Control(string actorName, Vector3 newPos, float facing)
        {
            this.actorName = actorName;
            this.teleportTo = newPos;
            this.facing = facing;
            type = Type.Teleport;
        }
        /// <summary>
        /// Change virtual world for player
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Control ChangeVirtualWorld(int id)
        {
            vWID = id;
            return this;
        }
        /// <summary>
        /// Change interior for player
        /// </summary>
        /// <param name="id">interior id</param>
        /// <returns></returns>
        public Control ChangeInterior(int id)
        {
            interiorID = id;
            return this;
        }
        /// <summary>
        /// Toogle player controls
        /// </summary>
        /// <param name="toggle">toggle</param>
        public Control(bool toggle)
        {
            this.toggle = toggle;
            type = Type.ToggleControls;
        }

        public override void Proceed(Player player)
        {

            if (vWID != -1)
                player.VirtualWorld = vWID;
            if (interiorID != -1)
                player.Interior = interiorID;
            if (ID != -1)
                player.Skin = ID;
            switch (type)
            {
                case Type.ToggleControls:
                    player.ToggleControllable(toggle);
                    break;
                case Type.Teleport:
                    if (actorName == null)
                    {
                        player.Position = teleportTo;
                        player.Angle = facing;
                    }
                    else
                    {
                        World.Actor actor = mission?.GetActorByName(actorName);
                        if (actor == null) break;
                        actor.Position = teleportTo;
                        actor.FacingAngle = facing;
                    }
                    break;
                case Type.PlaySound:
                    player.PlaySound(ID);
                    break;
            }
        }
    }
}
