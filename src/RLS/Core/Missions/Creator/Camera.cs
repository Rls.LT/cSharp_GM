﻿using RLS.World.Players;
using SampSharp.GameMode;
namespace RLS.Missions.Creator
{
    class Camera : Step
    {
        enum CameraAction
        {
            SetBehind,
            Teleport,
            Move
        }
        private CameraAction action;
        private Vector3 cameraPos;
        private Vector3 cameraLookAt;
        private Vector3 fromCameraPos;
        private Vector3 fromCameraLookAt;
        /// <summary>
        /// Step to set camera behind player
        /// <param name="stepTime">Step end after</param>
        /// </summary>
        public Camera(int stepTime = 2000)
        {
            action = CameraAction.SetBehind;
            time = stepTime;
        }
        /// <summary>
        /// Teleport camera at pos
        /// </summary>
        /// <param name="newCameraPos">New camera pos</param>
        /// <param name="newCameraLookAt">New camera look at pos</param>
        /// <param name="stepTime">Time till next step</param>
        public Camera(Vector3 newCameraPos, Vector3 newCameraLookAt, int stepTime = 2000)
        {
            cameraPos = newCameraPos;
            cameraLookAt = newCameraLookAt;
            time = stepTime;
            action = CameraAction.Teleport;
        }
        /// <summary>
        /// Move camera smooth from one point to another
        /// </summary>
        /// <param name="oldCameraPos">Point from where camera should move</param>
        /// <param name="oldCameraLookAt">Old camera pointer pos</param>
        /// <param name="newCameraPos">Point to which camera has to move</param>
        /// <param name="newCameraLookAt">New camera pointer pos</param>
        /// <param name="stepTime">Camera move time</param>
        public Camera(Vector3 oldCameraPos, Vector3 oldCameraLookAt, Vector3 newCameraPos, Vector3 newCameraLookAt, int stepTime = 2000)
        {
            fromCameraPos = oldCameraPos;
            fromCameraLookAt = oldCameraLookAt;
            cameraPos = newCameraPos;
            cameraLookAt = newCameraLookAt;
            time = stepTime;
            action = CameraAction.Move;
        }

        public override void Proceed(Player player)
        {
            switch (action)
            {
                case CameraAction.SetBehind:
                    player.PutCameraBehindPlayer();
                    break;
                case CameraAction.Teleport:
                    player.CameraPosition = cameraPos;
                    player.SetCameraLookAt(cameraLookAt);
                    break;
                case CameraAction.Move:
                    player.InterpolateCameraPosition(fromCameraPos, cameraPos, time, SampSharp.GameMode.Definitions.CameraCut.Move);
                    player.InterpolateCameraLookAt(fromCameraLookAt, cameraLookAt, time, SampSharp.GameMode.Definitions.CameraCut.Move);
                    break;
            }
        }
    }
}
