﻿using System;
using RLS.World.Players;

namespace RLS.Missions.Creator
{
    class Anim : Step
    {
        private string animLib;
        private string animName;
        private bool loop;
        private bool freeze;
        /// <summary>
        /// Change player animation
        /// </summary>
        /// <param name="animLib">Animation library</param>
        /// <param name="freeze">Freeze player after animation ended</param>
        /// <param name="anim">Animation name</param>
        public Anim(String animLib, String anim, bool loop, bool freeze = false, int stepTime = 2000)
        {
            this.animLib = animLib;
            animName = anim;
            time = stepTime;
            this.freeze = freeze;
            this.loop = loop;
        }
        /// <summary>
        /// Change actor anim.
        /// </summary>
        /// <param name="actorName">Actor to effect</param>
        /// <param name="animLib">Animation library</param>
        /// <param name="anim">Animation name</param>
        /// <param name="loop">Loop animation?</param>
        /// <param name="freeze">Freeze actor after animation ended</param>
        /// <param name="stepTime"></param>
        public Anim(string actorName, String animLib, String anim, bool loop, bool freeze = false, int stepTime = 2000)
        {
            this.actorName = actorName;
            this.animLib = animLib;
            animName = anim;
            time = stepTime;
            this.freeze = freeze;
            this.loop = loop;
        }
        private void ClearAnim(Player player)
        {
            if (actorName == null)
                player.ApplyAnimation(animLib, "null", 0.0f, false, false, false, false, 0);
            else
                mission?.GetActorByName(actorName)?.ApplyAnimation(animLib, "null", 0.0f, false, false, false, false, 0);
        }
        public override void Proceed(Player player)
        {
            ClearAnim(player);
            if (actorName == null)
                player.ApplyAnimation(animLib, animName, 4f, loop, false, false, freeze, 0);
            else
                mission?.GetActorByName(actorName)?.ApplyAnimation(animLib, animName, 4f, loop, false, false, freeze, 0);
        }
    }
}
