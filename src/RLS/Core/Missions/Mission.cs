﻿using RLS.World.Players;
using SampSharp.GameMode.SAMP;
namespace RLS.Missions
{
    public abstract partial class Mission
    {
        static Mission()
        {
            Player.FullyLoaded += (sender, e) =>
            {
                if (e.Player is Player player)
                {
                    player.UseEntrance += (_sender, _e) =>
                    {
                        if (player.currentMission != null && player.currentMission.IsActionBlocked())
                        {
                            player.Error("Kol nepabaigei misijos, tol negali naudotis įėjimais/išėjimais");
                            _e.Cancel = true;
                        }
                    };
                }
            };
        }

        private bool created = false;
        protected string missionCode;
        public string MissionCode { get => missionCode; }

        protected Timer timer = null;
        protected bool blockActions;
        public bool IsActionBlocked()
        {
            return blockActions;
        }
        /// <summary>
        /// Mission constructor
        /// </summary>
        /// <param name="player"></param>
        /// <param name="missionCode">Mission code. Example: for first story mission - s1</param>
        public Mission(Player player, string missionCode)
        {
            Owner = player;
            this.missionCode = missionCode;
        }
        public Player Owner { get; set; }

        public int timeBeforeStart = 1000;

        #region mission controlls
        protected abstract void Create();

        public void Start()
        {
            if (!created)
            {
                created = true;
                Create();
            }
            if(CurrentStep!=0)
            {
                ProcessStep();
                return;
            }
            timer = new Timer(timeBeforeStart, false);
            timer.Tick += (sender, e) =>
            {
                ProcessStep();
            };
        }
        public void Dispose()
        {
            timer?.Dispose();
            End();
        }
        public void End()
        {
            ClearActorList();
            ClearSteps();
        }
        #endregion
    }
}
