﻿using System.Collections.Generic;
namespace RLS.Missions
{
    partial class Mission
    {
        private List<World.Actor> actors = new List<World.Actor>();
        public World.Actor[] AllActors { get => actors.ToArray(); }
        public void AddActor(World.Actor actor) => actors.Add(actor);
        public void ClearActorList()
        {
            foreach (World.Actor actor in AllActors)
                actor.Dispose();
            actors.Clear();
        }
        public void RemoveActor(World.Actor actor) => actors.Remove(actor);
        public World.Actor GetActorByName(string name)
        {
            foreach (World.Actor actor in AllActors)
                if (actor.GetName().Equals(name))
                    return actor;
            return null;
        }
    }
}
