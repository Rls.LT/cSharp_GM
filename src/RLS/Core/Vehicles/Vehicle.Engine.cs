﻿namespace RLS.Core.Vehicles
{
    partial class Vehicle
    {
        /// <summary>
        /// Start vehicle engine
        /// </summary>
        /// <returns>If engine started returns true, if not - false</returns>
        public bool StartEngine()
        {
            if (!Created) return false;
            if (Engine)
                return true;
            if (fuelAmount <= 0.0f)
            {
                fuelAmount = 0f;
                return false;
            }
            Engine = true;
            return true;
        }
        /// <summary>
        /// Stop vehicle engine
        /// </summary>
        /// <returns>If can't do that returns false</returns>
        public bool StopEngine()
        {
            if (!Created) return false;
            Engine = false;
            return true;
        }
    }
}
