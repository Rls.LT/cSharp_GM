﻿using SampSharp.GameMode;

namespace RLS.Core.Vehicles
{
    partial class Vehicle
    {
        private double traveledDistance = 0;
        private float lastTraveled = 0f;
        private int lastSynchedSpeed = 0;

        /// <summary>
        /// Total vehicle traveled distance
        /// </summary>
        public double TraveledDistance => traveledDistance;
        /// <summary>
        /// Current vehicle speed
        /// </summary>
        public int Speed
        {
            get
            {
                if (!Created) return 0;
               return (int)System.Math.Round(
                System.Math.Sqrt(
                    System.Math.Pow(System.Math.Abs(instance.Velocity.X), 2) +
                    System.Math.Pow(System.Math.Abs(instance.Velocity.Y), 2) +
                    System.Math.Pow(System.Math.Abs(instance.Velocity.Z), 2)
                ) * 190);
            }
        }
        /// <summary>
        /// Vehicle acceleration m/s
        /// </summary>
        public double Acceleration
        {
            //      3.6 = convert km/h     from ms to s
            get => ((Speed - lastSynchedSpeed) / 3.6) / (updateTime/1000.0);
        }
        /// <summary>
        /// Counts traveled distance from last check
        /// </summary>
        private void CountTraveledDistance()
        {
            Vector3 pos = new Vector3(Position.X, Position.Y, Position.Z);
            lastTraveled = lastSynchedPos.DistanceTo(pos) * 0.001f;
            if (lastTraveled < 1)
                traveledDistance += lastTraveled;//make distance to km
            else
                lastTraveled = 0;
            lastSynchedPos = pos;
            lastSynchedAngle = Angle;
        }
        /// <summary>
        /// Return distance from vehicle to point
        /// </summary>
        /// <param name="point"></param>
        /// <returns>Returns -1 if vehicle not created</returns>
        public float GetDistanceFromPoint(Vector3 point)
        {
            if (!Created) return -1f;
            return instance.GetDistanceFromPoint(point);
        }
    }
}
