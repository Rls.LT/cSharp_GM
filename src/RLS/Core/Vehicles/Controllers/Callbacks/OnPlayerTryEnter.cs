﻿
using RLS.World.Players;

namespace RLS.Core.Vehicles.Controllers
{
    partial class VehicleController
    {
        public void OnPlayerTryEnterVehicle(Player player, Vehicle vehicle, bool AsPassenger)
        {
            //job vehicles
            if (vehicle.Job != World.Jobs.JobTypes.NONE)
                OnPlayerTryEnterJobVehicle(player, vehicle, AsPassenger);
        }
        private void OnPlayerTryEnterJobVehicle(Player player, Vehicle vehicle, bool AsPassenger)
        {
            if(player.Job.Type != vehicle.Job && !AsPassenger)
            {
                player.Error("Tau negalima vairuoti šios transporto priemonės.");
                player.ClearAnimations();
                return;
            }
        }
    }
}
