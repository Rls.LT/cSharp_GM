﻿using RLS.Definitions;
using RLS.World.Players;
using RLS.World.Players.Definitions;
using SampSharp.GameMode.Events;

namespace RLS.Core.Vehicles.Controllers
{
    partial class VehicleController
    {
        public void OnPlayerPressKeyInVehicle(Player player, Vehicle vehicle, KeyStateChangedEventArgs e)
        {
            if (e.NewKeys == player.Settings.GetKey(KeyId.Engine).GtaKey(player))
            {
                if (!vehicle.Created)
                    return;
                if (vehicle.Engine)
                    vehicle.StopEngine();
                else
                    vehicle.StartEngine();
            }
        }
    }
}
