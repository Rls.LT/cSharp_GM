﻿using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Controllers;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.World;

namespace RLS.Core.Vehicles.Controllers
{
    partial class VehicleController : BaseVehicleController
    {

        public override void RegisterEvents(BaseMode gameMode)
        {
            base.RegisterEvents(gameMode);
            gameMode.PlayerKeyStateChanged += OnPlayerPressedKey;
            gameMode.VehicleDied += OnVehicleDied;
            gameMode.PlayerEnterVehicle += OnPlayerTryEnterVehicle;
        }

        private void OnPlayerTryEnterVehicle(object sender, EnterVehicleEventArgs e)
        {
            Vehicle vehicle = Vehicle.GetByBase(e.Vehicle);
            if (vehicle == null)
            {
                e.Player.ClearAnimations();
                return;
            }
            OnPlayerTryEnterVehicle(e.Player as Player, vehicle, e.IsPassenger);
        }

        private void OnPlayerPressedKey(object sender, KeyStateChangedEventArgs e)
        {
            Player player = (sender as Player);
            if (!player.InAnyVehicle)
                return;
            Vehicle vehicle = player.Vehicle;
            if (vehicle == null) return;
            OnPlayerPressKeyInVehicle(player, vehicle, e);
        }

        private void OnVehicleDied(object sender, SampSharp.GameMode.Events.PlayerEventArgs e)
        {
            Vehicle vehicle = Vehicle.GetByBase(sender as BaseVehicle);
            vehicle.Engine = false;
        }

        public override void RegisterTypes()
        {
            Vehicle.Register<Vehicle>();
            BaseVehicle.Register<BaseVehicle>();
        }
    }
}
