﻿using RLS.World.Jobs;

namespace RLS.Core.Vehicles
{
    public partial class Vehicle
    {
        /// <summary>
        /// Get Job id vehicle belongs to
        /// </summary>
        public JobTypes Job { get => belongsTo; }
        private JobTypes belongsTo = JobTypes.NONE;
        /// <summary>
        /// Set job id vehicle belongs to
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Vehicle ForJob(JobTypes type)
        {
            belongsTo = type;
            return this;
        }
    }
}
