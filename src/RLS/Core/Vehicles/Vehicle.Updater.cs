﻿using RLS.World.Players;
using System;

namespace RLS.Core.Vehicles
{
    partial class Vehicle
    {
        /// <summary>
        /// Nothing special just vehicle update timer. Use UpdaterTick instead
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void UptateTimer_Tick(object sender, EventArgs e) => UpdaterTick();
        /// <summary>
        /// OnVehicleUpdate
        /// </summary>
        public void OnUpdate()
        {
            if (Driver != null && Driver != LastDriver)
                LastDriver = Driver as Player;

            CountTraveledDistance();
            FuelUpdate();
            //after everything
            lastSynchedSpeed = Speed;
            (Driver as Player)?.ShowSpeedometer("nenustatytas",
                $"Kuras: {Fuel.ToString("F2")} l\nPravažiuota: {traveledDistance.ToString("F1")} km\nGreitis: {lastSynchedSpeed} km/h");
        }
        /// <summary>
        /// Called each tick
        /// </summary>
        private static void UpdaterTick()
        {
            foreach (Vehicle vehicle in All)
            {
                if (!vehicle.Created) continue;
                vehicle.OnUpdate();
                vehicle.VehicleUpdate?.Invoke(vehicle, new EventArgs());
            }
        }
    }
}
