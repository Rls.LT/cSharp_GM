﻿using Database;
using RLS.Controllers;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using System;

namespace RLS.Core.Vehicles
{
    partial class Vehicle
    {
        public void InsertInDatabase()
        {
            if (SqlId != 0 || owner == null) return;
            Result res = Database.MySql.Connection.Run(new Query(Configs.Database.vehicles).Insert(new Fields().
                Add("owner_id", owner.SqlId).
                Add("model", (int)Model), true));
            sqlId = res.InsertedRow;
            Log.Write($"[Personal vehicles] {owner.Name} vehicle inserted in database with Id: {sqlId}.", LogLevel.Debug);
            Save();
        }
        private void Save()
        {
            Vector3 pos = Position;
            Database.MySql.Connection.RunThreadedVoid(new Query(Configs.Database.vehicles).Update(new Fields().
                Add("run", TraveledDistance).
                Add("x", pos.X).
                Add("y", pos.Y).
                Add("z", pos.Z).
                Add("f", Angle).
                Add("fuel", Fuel).
                Add("engine", Convert.ToInt32(Engine)).
                Add("lights", Convert.ToInt32(Lights)).
                Add("locked", Convert.ToInt32(Lock))
            ).Where(new Field("id", "" + sqlId)));
            Log.Write($"[Personal vehicles] {Owner.Name} vehicle saved.", LogLevel.Debug);
        }
        public static void LoadForPlayer(Player player)
        {
            Result result = Database.MySql.Connection.Run(new Query(Configs.Database.vehicles).Select("*").Where(new Field("owner_id", player.SqlId)));
            if (result.NumRows < 1) return;
            for (int i = 0; i < result.NumRows; i++)
            {
                Fields row = result.GetRow(i);
                Vehicle veh = new Vehicle((VehicleModelType)row.GetFieldByName("model").Int(),
                    new Vector3(row.GetFieldByName("x").Float(), row.GetFieldByName("y").Float(), row.GetFieldByName("z").Float()), row.GetFieldByName("f").Float(), 1, 1)
                {
                    traveledDistance = row.GetFieldByName("run").Float(),
                    Fuel = row.GetFieldByName("fuel").Float()
                };
                veh.SetOwner(player);
                veh.sqlId = row.GetFieldByName("id").Int();
                //vehicle parameters
                veh.engine = row.GetFieldByName("engine").Bool();
                veh.lights = row.GetFieldByName("lights").Bool();
                veh.@lock = row.GetFieldByName("locked").Bool();
                veh.UpdateParameters();
                //~~~~~~~~~~~~~
                Log.Write($"[Personal vehicles] {player.Name} vehicle loaded.", LogLevel.Debug);
            }
        }
    }
}
