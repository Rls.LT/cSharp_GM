﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.Core.Vehicles
{
    partial class Vehicle
    {
        [Command("uzraktas")]
        public static void VehicleLock(Player player)
        {
            Vehicle owned = GetOwnedInRange(player);
            if (owned == null)
            {
                player.Error("Šalia tavęs nėra tavo transporto priemonių.");
                return;
            }
            if (owned.Lock)
            {
                owned.Lock = false;
                player.ShowUILineNamed("lock", Colors.Orange, "hud:radar_impound", "Atrakinta", "_", 3000);
            }
            else
            {
                owned.Lock = true;
                player.ShowUILineNamed("lock", Colors.Green, "hud:radar_impound", "Užrakinta", "_", 3000);
            }
        }
    }
}
