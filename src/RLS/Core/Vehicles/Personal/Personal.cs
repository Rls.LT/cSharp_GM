﻿using RLS.World.Players;
using System.Collections.Generic;

namespace RLS.Core.Vehicles
{
    partial class Vehicle
    {
        private int sqlId = 0;
        private Player owner = null;

        /// <summary>
        /// Vehicle database Id
        /// </summary>
        public int SqlId{ get => sqlId; }

        /// <summary>
        /// Get vehicle owner
        /// </summary>
        public Player Owner { get => owner; }

        /// <summary>
        /// Does vehicle have any owner
        /// </summary>
        public bool HasOwner { get => owner != null; }

        /// <summary>
        /// Just initialize personal vehicle controls for player
        /// </summary>
        public static void Personal()
        {
            Player.FullyLoaded += (sender, e) =>
                LoadForPlayer(e.Player as Player);
            Player.Disconnect += (sender, e) =>
                DestroyOwned(e.Player as Player);
        }

        /// <summary>
        /// Set vehicle owner
        /// </summary>
        /// <param name="player">Player who will own vehicle</param>
        public void SetOwner(Player player)
        {
            owner = player;
        }

        /// <summary>
        /// Destroy all vehicles owned by player
        /// </summary>
        /// <param name="player"></param>
        public static void DestroyOwned(Player player)
        {
            Vehicle[] array = AllOwnedByPlayer(player);
            if (array == null) return;
            foreach (Vehicle veh in array)
            {
                veh.Save();
                veh.instance?.Dispose();
                veh.Dispose();
            }
            Log.Write($"[Personal vehicles] {player.Name} vehicles disposed.", LogLevel.Debug);
        }

        /// <summary>
        /// All vehicles owned by player
        /// </summary>
        /// <param name="owner">Owner who owns vehicles</param>
        /// <returns>Returns array of vehicles owned by specified player</returns>
        public static Vehicle[] AllOwnedByPlayer(Player owner)
        {
            List<Vehicle> list = new List<Vehicle>();
            foreach (Vehicle vehicle in All)
            {
                if (vehicle.HasOwner && vehicle.Owner.SqlId.Equals(owner.SqlId))
                    list.Add(vehicle);
            }
            return list.ToArray();
        }

        /// <summary>
        /// Get one vehicle owned by player in range
        /// </summary>
        /// <param name="owner">Player who owns vehicle</param>
        /// <param name="range">Range to check in</param>
        /// <returns>If vehicle not found returns null</returns>
        public static Vehicle GetOwnedInRange(Player owner, float range = 5f)
        {
            foreach(Vehicle vehicle in All)
            {
                if (vehicle.HasOwner && owner.IsInRangeOfPoint(range, vehicle.Position))
                    if (vehicle.Owner.SqlId.Equals(owner.SqlId))
                        return vehicle;
            }
            return null;
        }
    }
}
