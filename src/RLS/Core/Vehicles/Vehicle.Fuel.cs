﻿using RLS.Definitions;
using SampSharp.GameMode.Definitions;

namespace RLS.Core.Vehicles
{
    partial class Vehicle
    {
        private float fuelAmount = 0f;
        private EngineType fuelType = EngineType.Petrol;
        /// <summary>
        /// Engine type etc: Diesel
        /// </summary>
        public EngineType EngineType { get => fuelType; }
        /// <summary>
        /// Amount of fuel in vehicle
        /// </summary>
        public float Fuel { get => fuelAmount; set => fuelAmount = value; }
        /// <summary>
        /// Vehicle fuel usage l/100km
        /// </summary>
        public float FuelUsage
        {
            get
            {
                if (!Engine) return 0f;
                float baseUsage = FuelUsageByModel(Model);
                float accBoost = (float)(Acceleration * baseUsage)/4;
                float speedBoost = baseUsage * Speed / 70f;
                float usage = baseUsage + accBoost + speedBoost;
                if (Speed == 0)
                    usage = baseUsage / 100;
                return (usage < 0) ? 0.1f : usage * (
                        (fuelType == EngineType.Diesel) ? 0.75f :  //diesel 0.75 x
                        (fuelType == EngineType.Electric) ? 1.5f : //electricity 1.5x
                        1f  // other 1x
                    );
            }
        }
        public void FuelUpdate()
        {
            Fuel -= (Speed > 0) ? FuelUsage * (lastTraveled / 100) : FuelUsage / 1000;
            if (Fuel < 0)
            {
                Engine = false;
                Fuel = 0;
            }
        }
        public static float FuelUsageByModel(VehicleModelType model)
        {
            return 10f;
        }
    }
}
