﻿using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Pools;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.World;
using System;
using System.Collections.Generic;

namespace RLS.Core.Vehicles
{
    public partial class Vehicle : IdentifiedPool<Vehicle>
    {
        private BaseVehicle instance;
        public static int updateTime = 500;//ms
        private static Timer uptateTimer;
        public bool Created { get => instance != null; }
        private Vector3 lastSynchedPos = Vector3.Zero;
        private float lastSynchedAngle = 0f;
        public event EventHandler VehicleUpdate;

        public Vector3 Position
        {
            get
            {
                if (!Created) return lastSynchedPos;
                return instance.Position;
            }
            set
            {
                lastSynchedPos = value;
                if (Created) instance.Position = value;
            }
        }

        public float Angle
        {
            get
            {
                if (!Created) return lastSynchedAngle;
                return instance.Angle;
            }
            set
            {
                lastSynchedAngle = value;
                if (Created) instance.Angle = value;
            }
        }

        private VehicleModelType model;
        public VehicleModelType Model => model;


        private bool engine;
        public bool Engine
        {
            get => engine;
            set
            {
                engine = value;
                UpdateParameters();
            }
        }

        private bool lights;
        public bool Lights
        {
            get => lights;
            set
            {
                lights = value;
                UpdateParameters();
            }
        }

        private bool alarm;
        public bool Alarm
        {
            get => alarm;
            set
            {
                alarm = value;
                UpdateParameters();
            }
        }

        private bool @lock;
        public bool Lock
        {
            get => @lock;
            set
            {
                @lock = value;
                UpdateParameters();
            }
        }

        private bool bonnet;
        public bool Bonnet
        {
            get => bonnet;
            set
            {
                bonnet = value;
                UpdateParameters();
            }
        }

        private bool boot;
        public bool Boot
        {
            get => boot;
            set
            {
                boot = value;
                UpdateParameters();
            }
        }

        /// <summary>
        /// LastVehicle driver
        /// </summary>
        public Player LastDriver { get; private set; }

        /// <summary>
        /// Get Vehicle driver. Null if no driver or vehicle not created
        /// </summary>
        public Player Driver
        {
            get
            {
                if (!Created) return null;
                return instance.Driver as Player;
            }
        }

        public Player[] Passengers
        {
            get
            {
                if (!Created) return null;
                List<Player> passengers = new List<Player>();
                foreach (BasePlayer passenger in instance.Passengers)
                    passengers.Add(passenger as Player);
                return passengers.ToArray();
            }
        }
        /// <summary>
        /// Put player in vehicle
        /// </summary>
        /// <param name="player"></param>
        /// <returns>If vehicle not exist or not created returns false</returns>
        public bool PutPlayerIn(Player player)
        {
            if (!Created) return false;
            player.PutInVehicle(instance);
            return true;
        }

        /// <summary>
        /// Get Vehicle instance by BaseVehicle
        /// </summary>
        /// <param name="veh"></param>
        /// <returns>Returns null if vehicle not created</returns>
        public static Vehicle GetByBase(BaseVehicle veh)
        {
            if (veh == null) return null;
            foreach(Vehicle inst in All)
            {
                if (inst.instance == null) continue;
                if (inst.instance.Id.Equals(veh.Id)) return inst;
            }
            return null;
        }

        /// <summary>
        /// Update vehicle params from variables
        /// </summary>
        public void UpdateParameters()
        {
            if (!Created) return;
            instance.SetParameters(Engine, Lights, Alarm, Lock, Bonnet, Boot, false);
        }

        /// <summary>
        /// Static Vehicle class constructor
        /// </summary>
        static Vehicle()
        {
            Personal();
            uptateTimer = new Timer(updateTime, true);
            uptateTimer.Tick += UptateTimer_Tick;
        }

        /// <summary>
        /// Create new Vehicle instance
        /// </summary>
        /// <param name="model">Vehicle model</param>
        /// <param name="position">Position to create vehicle in</param>
        /// <param name="roatation">Vehicle angle</param>
        /// <param name="color1">Primary vehicle color</param>
        /// <param name="color2">Secondary vehicle color</param>
        public Vehicle(VehicleModelType model, Vector3 position, float roatation, int color1, int color2)
        {
            this.model = model;
            instance = BaseVehicle.Create(model, position, roatation, color1, color2);
            lastSynchedPos = position;
        }
        /// <summary>
        /// Destroy BaseVehicle
        /// </summary>
        public void DestroyInstance()
        {
            instance?.Dispose();
        }
    }
}
