﻿using System;

namespace RLS.World.Entrances
{
    [Flags]
    public enum EntranceRestrictions
    {
        None,
        DisableForWanted
    }
}
