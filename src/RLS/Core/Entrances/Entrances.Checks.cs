﻿using RLS.Events.Args;
using RLS.World.Players;
using SampSharp.GameMode.SAMP;

namespace RLS.World.Entrances
{
    partial class Entrance
    {
        public void UseEnter(Player player, EntranceArgs args)
        {
            EntranceArgs enter = args;
            Enter?.Invoke(this, enter);
            if (enter.Cancel)
                return;
            player.PlaySound(1);
            player.Position = InsidePos;
            player.Angle = (float)enterAngle;
            player.Interior = toInterior;
            player.VirtualWorld = virtualWorld;
            player.PutCameraBehindPlayer();
            if(freezeFor != 0)
            {
                player.ToggleControllable(false);
                new Timer(freezeFor, false).Tick += (sender, e) =>
                {
                    player.ToggleControllable(true);
                };
            }
        }
        public void UseExit(Player player, EntranceArgs args)
        {
            EntranceArgs exit = args;
            Exit?.Invoke(this, exit);
            if (exit.Cancel)
                return;
            player.PlaySound(0);
            player.Position = OutsidePos;
            player.Angle = (float)exitAngle;
            player.Interior = fromInterior;
            player.VirtualWorld = fromVirtualWorld;
            player.PutCameraBehindPlayer();
            if (freezeFor != 0)
            {
                player.ToggleControllable(false);
                new Timer(freezeFor, false).Tick += (sender, e) =>
                {
                    player.ToggleControllable(true);
                };
            }
        }
        public bool IsPlayerCloseToEnter(Player player, float range = 3f)
        {
            if (player.IsInRangeOfPoint(range, OutsidePos) && player.Interior == fromInterior && player.VirtualWorld == fromVirtualWorld)
                return true;
            return false;
        }
        public bool IsPlayerCloseToExit(Player player, float range = 3f)
        {
            if (player.IsInRangeOfPoint(range, InsidePos) && player.Interior == Interior && player.VirtualWorld == virtualWorld)
                return true;
            return false;
        }
        public static Entrance PlayerCloseEnter(Player player, float range = 2f)
        {
            foreach(Entrance entrance in All)
            {
                if (entrance.IsPlayerCloseToEnter(player, range))
                    return entrance;
            }
            return null;
        }
        public static Entrance PlayerCloseExit(Player player, float range = 2f)
        {
            foreach (Entrance entrance in All)
            {
                if (entrance.IsPlayerCloseToExit(player, range))
                    return entrance;
            }
            return null;
        }
    }
}
