﻿using SampSharp.GameMode;
using System;
using System.Collections.Generic;
using RLS.Definitions;

namespace RLS.World.Entrances
{
    public partial class Entrance
    {
        private static List<Entrance> entrances = new List<Entrance>();
        public static Entrance[] All { get => entrances.ToArray(); }

        private Vector3 inside;
        private Vector3 outside;
        private Vector3 pickupOffset = Vector3.Zero;
        private Pickup enterPickup = null;
        private Pickup exitPickup = null;
        private EntranceGroups group = EntranceGroups.None;
        private EntranceRestrictions flags = EntranceRestrictions.None;
        private int toInterior = 0;
        private int virtualWorld = 0;
        private int fromInterior = 0;
        private int fromVirtualWorld = 0;
        private double enterAngle = 0;
        private double exitAngle = 0;
        private int freezeFor = 0;

        public double Exitangle { get => exitAngle; }
        public Vector3 OutsidePos { get => outside; }
        public Vector3 InsidePos { get => inside; }
        public int Interior { get => toInterior; }
        public EntranceGroups Group { get => group; }

        public event EventHandler<Events.Args.EntranceArgs> Enter;
        public event EventHandler<Events.Args.EntranceArgs> Exit;

        public Entrance(Vector3 outside, Vector3 inside)
        {
            this.inside = inside;
            this.outside = outside;
            group = EntranceGroups.None;
            entrances.Add(this);
        }
        public Entrance SetAngles(double inside, double outside)
        {
            enterAngle = inside;
            exitAngle = outside;
            return this;
        }
        public Entrance PickupOffset(Vector3 offset)
        {
            pickupOffset = offset;
            return this;
        }
        public Entrance FreezeFor(int _for)
        {
            freezeFor = _for;
            return this;
        }
        public Entrance SetInterior(int interior)
        {
            toInterior = interior;
            return this;
        }
        public Entrance SetVirtualWorld(int vW)
        {
            virtualWorld = vW;
            return this;
        }
        public Entrance SetFlags(EntranceRestrictions flags)
        {
            this.flags = flags;
            return this;
        }
        public bool HasFlag(EntranceRestrictions flag)
        {
            return flags.HasFlag(flag);
        }
        public Entrance SetGroup(EntranceGroups group)
        {
            this.group = group;
            return this;
        }
        public void ChangeText(string text)
        {
            if (enterPickup == null)
                return;
            enterPickup.ChangeText(text);
        }
        public void ChangeText(RichText text)
        {
            if (enterPickup == null)
                return;
            enterPickup.ChangeText(text.ToString());
        }
        public Entrance CreateVisual(string entranceTitle = "Įėjimas", int model = 19135)
        {
            if (model.Equals(19135) && pickupOffset.IsEmpty)
                pickupOffset = new Vector3(0, 0, -0.2);
            if (enterPickup == null)
            {
                enterPickup = new Pickup(model, 1, outside + pickupOffset, fromVirtualWorld);
                enterPickup.AddText($"{entranceTitle}\n[Enter]", Colors.White);
            }
            else
                enterPickup.ChangeText(entranceTitle);
            if (exitPickup == null)
            {
                exitPickup = new Pickup(model, 1, inside + pickupOffset, virtualWorld);
                exitPickup.AddText($"Išėjimas\n[Enter]", Colors.White);
            }
            return this;
        }
        public void Delete()
        {
            entrances.Remove(this);
            if (enterPickup != null)
                enterPickup.Dispose();
            if (exitPickup != null)
                exitPickup.Dispose();
        }
    }
}
