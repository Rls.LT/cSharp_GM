﻿using RLS.Commands;
using RLS.Definitions;
using RLS.World.Jobs;
using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;

namespace RLS.Other.Budget
{
    public class JobBudget : Budget
    {
        private Job job;
        public JobBudget(Job job, string name) : base(name)
        {
            this.job = job;
        }

        public override void Dialog(ListDialog dialog)
        {
            base.Dialog(dialog);
            dialog.AddItem("Išdalinti pinigų visiems darbuotojams");
            dialog.AddItem($"Išdalinti pinigų {Colors.Green}prisijungusiems{Colors.White} darbuotojams");
        }

        public override int DialogResponse(Player player, int listitem)
        {
            listitem = base.DialogResponse(player, listitem);
            if (listitem == 0)
                SpreadMoneyDialog(player);
            else if (listitem == 1)
                SpreadMoneyOnlineDialog(player);
            return listitem;
        }
        private void SpreadMoneyDialog(Player player, string ErrorText = "")
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            InputDialog dialog = new InputDialog($"{job.Id.GetName()} fondo valdymas", $"{Colors.Red}{ErrorText}{Colors.Hex.Dialog}\nKokią sumą nori išdalinti darbuotojams? [Max: {Functions.IntFormat(Money)}]", false, "Išdalinti", "Grįžti");
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    Menu(player);
                    return;
                }
                int.TryParse(e.InputText, out int money);
                if (money < 1 || money > int.MaxValue)
                {
                    SpreadMoneyDialog(player, "Tokios sumos išdalinti negali.");
                    return;
                }
                if (money > Money)
                {
                    SpreadMoneyDialog(player, "Tiek pinigų nėra fonde.");
                    return;
                }
                if (job.BudgetSpread(player, money))
                    Job.Message(player, $"Išdalinai @[{money}@] €. @[{job.WorkersCount()}@] darbuotojų gavo po @[{(money / job.WorkersCount())}@] €.");
                Menu(player);
            };
            dialog.Show(player);
        }
        private void SpreadMoneyOnlineDialog(Player player, string ErrorText = "")
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            InputDialog dialog = new InputDialog($"{job.Id.GetName()} fondo valdymas", $"{Colors.Red}{ErrorText}{Colors.Hex.Dialog}\nKokią sumą nori išdalinti prisijungusiems darbuotojams? [Max: {Functions.IntFormat(Money)}]", false, "Išdalinti", "Grįžti");
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    Menu(player);
                    return;
                }
                int.TryParse(e.InputText, out int money);
                if (money < 1 || money > int.MaxValue)
                {
                    SpreadMoneyOnlineDialog(player, "Tokios sumos išdalinti negali.");
                    return;
                }
                if (money > Money)
                {
                    SpreadMoneyOnlineDialog(player, "Tiek pinigų nėra fonde.");
                    return;
                }
                if (player.Manager.Job.BudgetSpread(player, money, true))
                    Job.Message(player, $"Išdalinai @[{money}@] €. @[{job.OnlineWorkersCount()}@] darbuotojų gavo po @[{(money / job.OnlineWorkersCount())}@] €.");
                Menu(player);
            };
            dialog.Show(player);
        }
    }
}
