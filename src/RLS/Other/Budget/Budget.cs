﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using System;

namespace RLS.Other.Budget
{
    public class Budget
    {
        private int amount = 0;
        private string name;

        public int Money { get => amount; }
        public event EventHandler BackButtonEvent;
        public event EventHandler MoneyOperationPerformed;

        public Budget(string name)
        {
            this.name = name;
        }

        public void UpdateName(string newName)
        {
            name = newName;
        }
        public bool Add(int amount)
        {
            if (amount < 0) return false;
            MoneyOperation(amount);
            return true;
        }
        public bool Remove(int amount)
        {
            if (amount < 0) return false;
            MoneyOperation(-amount);
            return true;
        }
        private void MoneyOperation(int amount)
        {
            this.amount += amount;
            MoneyOperationPerformed?.Invoke(null, new EventArgs());
        }

        public virtual void Dialog(ListDialog dialog)
        {
            dialog.AddItem($"Biudžete yra: {Colors.Green}{Functions.IntFormat(amount)}{Colors.White} €");
            dialog.AddItem("Išimti pinigų");
            dialog.AddItem("Padėti pinigų");
        }
        public virtual int DialogResponse(Player player, int listitem)
        {
            if (listitem == 0)
                Menu(player);
            else if (listitem == 1)
                GetMoneyDialog(player);
            else if (listitem == 2)
                PutMoneyDialog(player);
            return listitem - 3;
        }
        public void Menu(Player player)
        {
            ListDialog listDialog = new ListDialog($"{name} -> Biudžetas", "Vykdyti", "Grįžti");
            Dialog(listDialog);
            listDialog.Show(player);
            listDialog.Response += (sender, arg) =>
            {
                if (arg.DialogButton == DialogButton.Right)
                {
                    BackButtonEvent?.Invoke(player, new EventArgs());
                    return;
                }
                DialogResponse(player, arg.ListItem);
            };
        }
        private void PutMoneyDialog(Player player, string ErrorText = "")
        {
            InputDialog dialog = new InputDialog($"{name} -> Biudžetas", $"{Colors.Red}{ErrorText}{Colors.Hex.Dialog}\nKokią sumą nori įdėti? [Max: {Functions.IntFormat(player.Money)}]", false, "Įdėti", "Grįžti");
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    Menu(player);
                    return;
                }
                int.TryParse(e.InputText, out int money);
                if (money < 1 || money > int.MaxValue)
                {
                    PutMoneyDialog(player, "Tokios sumos įdėti negali.");
                    return;
                }
                if (money > player.Money)
                {
                    PutMoneyDialog(player, "Tiek pinigų neturi.");
                    return;
                }
                Add(money);
                player.GiveMoney(-money);
                Menu(player);
            };
            dialog.Show(player);
        }
        private void GetMoneyDialog(Player player, string ErrorText = "")
        {
            InputDialog dialog = new InputDialog($"{name} -> Biudžetas", $"{Colors.Red}{ErrorText}{Colors.Hex.Dialog}\nKokią sumą nori išimti? [Max: {Functions.IntFormat(amount)}]", false, "Išimti", "Grįžti");
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    Menu(player);
                    return;
                }
                int.TryParse(e.InputText, out int money);
                if (money < 1 || money > int.MaxValue)
                {
                    GetMoneyDialog(player, "Tokios sumos išimti negali.");
                    return;
                }
                if (money > amount)
                {
                    GetMoneyDialog(player, "Tiek pinigų fonde nėra.");
                    return;
                }
                Remove(money);
                player.GiveMoney(money);
                Menu(player);
            };
            dialog.Show(player);
        }
    }
}
