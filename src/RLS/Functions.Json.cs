﻿using Newtonsoft.Json;
using System;


namespace RLS
{
    class Json
    {
        public static T DecodeJson<T>(string value)
        {
            if(value==null || value == "")
            {
                Log.Write($"[JSon] Failed to decode json from string: was empty", LogLevel.Warning);
                return (T)Activator.CreateInstance(typeof(T));
            }
            try
            {
                T obj = JsonConvert.DeserializeObject<T>(value);
                return obj;
            }
            catch (JsonSerializationException i)
            {
                Log.Write($"[JSon] Failed to decode json from string: {value}\nError{i.Message}", LogLevel.Warning);
                return (T)Activator.CreateInstance(typeof(T));
            }
            catch (NullReferenceException i)
            {
                Log.Write($"[JSon] Failed to decode json from string: {value}\nError{i.Message}", LogLevel.Warning);
                return (T)Activator.CreateInstance(typeof(T));
            }
        }
        public static string EncodeJson(object toSerialize)
        {
            try
            {
                return JsonConvert.SerializeObject(toSerialize);
            }
            catch (JsonSerializationException i)
            {
                Log.Write($"[JSon] Failed to encode json string: {i.Message}", LogLevel.Warning);
                return null;
            }
        }
    }
}
