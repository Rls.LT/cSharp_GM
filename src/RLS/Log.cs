﻿namespace RLS
{
    enum LogLevel
    {
        All,
        Debug,
        Warning,
        Error
    }
    class Log
    {
        public static void Write(string text, LogLevel level = LogLevel.All)
        {
            if (level < Config.Logging) return;
            Print(text, level);
        }
        public static void Print(string text, LogLevel level = LogLevel.All)
        {
            if (level < Config.Logging) return;
            System.Console.WriteLine(text);
        }
    }
}
