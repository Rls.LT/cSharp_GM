﻿using System.Collections.Generic;

namespace RLS.World
{
    public abstract partial class Property<T>
    {
        private static List<T> properties = new List<T>();

        public static T[] All { get => properties.ToArray(); }

        protected void Add(T instance) => properties.Add(instance);
        protected void Remove(T instance) => properties.Remove(instance);
    }
}
