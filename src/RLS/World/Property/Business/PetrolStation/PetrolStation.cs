﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Display;

namespace RLS.World.Property
{
    partial class PetrolStation : Business
    {
        public PetrolStation(string name, Vector3 position, int price, float pumpRotation) : base(name, position, price)
        {
            DefaultPumpRotation = pumpRotation;
        }
        protected override void Config()
        {
            type = BusinessType.PetrolStation;
        }

        public void CreatePump(int pumpModel, FuelType type, Vector3 position, Vector3 rotation)
        {
            if (pumps[(int)type] != null) pumps[(int)type].Dispose();
            pumps[(int)type] = new Pump(this, pumpModel, type, position, rotation);
        }
        protected override int ControlDialogItemsResponse(Player player, int response)
        {
            response = base.ControlDialogItemsResponse(player, response);
            if (response == 0)
                FuelPriceControl(player);
            else if (response == 1)
                PumpAdd(player);
            else if (response == 2)
                PumpEdit(player);
            return response - 3;
        }
        protected override void ControlDialogItems(ListDialog dialog)
        {
            base.ControlDialogItems(dialog);
            dialog.AddItem("Kainų valdymas");
            dialog.AddItem("Kolonėlių atidarymas");
            dialog.AddItem("Kolonėlių redagavimas");
        }

        public override void Dispose()
        {
            foreach (Pump pump in pumps)
                pump?.Dispose();
            base.Dispose();
        }
    }
}
