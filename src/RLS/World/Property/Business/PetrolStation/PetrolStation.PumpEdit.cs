﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.World;

namespace RLS.World.Property
{
    partial class PetrolStation
    {
        private FuelType EditFuelPumpType = 0;
        protected override void UpdateEditableObject()
        {
            if(EditorObject == null || !EditorObject.IsValid)
            {
                Log.Write("[Petrol station] Editor object is not valid.");
                return;
            }
            Vector3 lastCameraPosition = EditorObject.Position + Functions.PointFarFromPoint(7, DefaultPumpRotation) + new Vector3(0, 0, 3);
            Vector3 lastCameraLookAt = EditorObject.Position + new Vector3(0, 0, 1);
            EditorObject.Position = pumpPositions[LastEditValue];
            Vector3 newCameraPos = pumpPositions[LastEditValue] + Functions.PointFarFromPoint(7, DefaultPumpRotation) + new Vector3(0, 0, 3);
            if (!lastCameraPosition.Equals(Vector3.Zero))
            {
                Editor.InterpolateCameraPosition(lastCameraPosition, newCameraPos, 500, CameraCut.Move);
                Editor.InterpolateCameraLookAt(lastCameraLookAt, EditorObject.Position + new Vector3(0, 0, 1), 100, CameraCut.Move);
            }
            else
            {
                Editor.CameraPosition = newCameraPos;
                Editor.SetCameraLookAt(EditorObject.Position + new Vector3(0, 0, 1));
            }
        }
        protected override void EndEdit()
        {
            EditorObject?.Dispose();
            EditorObject = null;
            if (Editor != null)
            {
                Editor.PutCameraBehindPlayer();
                Editor.ToggleControllable(true);
                Editor.SpecialState = SpecialState.NONE;
                Editor.HideTutorial();
                Editor.Disconnected -= OnDisconnectFromEditor;
                Editor = null;
            }
            LastEditValue = 0;
            LockedEditing = false;
        }
        protected override void StartEdit(Player player)
        {
            if(!FindEmptyPumpSlot(out int id))
            {
                player.Error("Nebėra laisvų vietų kolonėlėms.");
                return;
            }
            LastEditValue = id;
            EditorObject = new PlayerObject(Editor, 1686, pumpPositions[LastEditValue], new Vector3(0, 0, DefaultPumpRotation));
            player.ShowTutorial("Valdymas", "Vietos keitimui: ~r~4~w~ ir ~r~6~w~\nIšsaugojimui: ~r~Y~w~\nVeiksmo atšaukimui: ~r~N");
            base.StartEdit(player);
        }

        public override void OnEdit(Player player, Keys key)
        {
            if (key == Keys.AnalogLeft)
            {
                LastEditValue++;
                if (LastEditValue >= pumpPositions.Count)
                    LastEditValue -= pumpPositions.Count;
                if(!IsSlotEmpty(LastEditValue))
                {
                    OnEdit(player, key);
                    return;
                }
                UpdateEditableObject();
            }
            else if (key == Keys.AnalogRight)
            {
                LastEditValue--;
                if (LastEditValue < 0)
                    LastEditValue = pumpPositions.Count - 1;
                if (!IsSlotEmpty(LastEditValue))
                {
                    OnEdit(player, key);
                    return;
                }
                UpdateEditableObject();
            }
            else if (key == Keys.No)
            {
                player.Error("Atšaukei kolonėlės pirkimą.");
                EndEdit();
                ShowControllDialog(player);
            }
            else if(key == Keys.Yes)
            {
                if(pumps[(int)EditFuelPumpType] == null)
                {
                    if (budget.Money < RLS.Config.Business.PumpPrice)
                    {
                        EndEdit();
                        player.Error($"Veslo biudžete neužtenka pinigų kolonėlei(trūksta $s{(RLS.Config.Business.PumpPrice - budget.Money)}$m €).");
                        ShowControllDialog(player);
                        return;
                    }
                    budget.Remove(RLS.Config.Business.PumpPrice);
                    Message(player, $"Nusipirkai kolonėlę už $s{RLS.Config.Business.PumpPrice}$m €.");
                }
                else
                    Message(player, $"Pakeitei kolonėlės vietą.");
                CreatePump(1686, EditFuelPumpType, EditorObject.Position, EditorObject.Rotation);
                EndEdit();
                SaveData();
                ShowControllDialog(player);
            }
        }
    }
}
