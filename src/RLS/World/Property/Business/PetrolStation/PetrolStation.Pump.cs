﻿using RLS.Definitions;
using RLS.Streamer;
using RLS.Core.Vehicles;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP;
using System;
using System.Collections.Generic;

namespace RLS.World.Property
{
    partial class PetrolStation
    {
        private Pump[] pumps = new Pump[Enum.GetNames(typeof(FuelType)).Length];
        private List<Vector3> pumpPositions = new List<Vector3>();
        public float DefaultPumpRotation { get; set; }
        private bool FindEmptyPumpSlot(out int id)
        {
            for(int i = 0; i< pumpPositions.Count;i++)
            {
                if (IsSlotEmpty(i))
                {
                    id = i;
                    return true;
                }
            }
            id = 0;
            return false;
        }
        private int GetPumpSlot(Pump pump)
        {
            for (int i = 0; i < pumpPositions.Count; i++)
            {
                if (!IsSlotEmpty(i) && pumpPositions[i].Equals(pump.Position))
                {
                    return i;
                }
            }
            return 0;
        }
        private bool IsSlotEmpty(int pos)
        {
            foreach (Pump pump in pumps)
            {
                if (pump == null)
                    continue;
                if (pump.Position.Equals(pumpPositions[pos]))
                    return false;
            }
            return true;
        }
        public PetrolStation AddPumpPosition(Vector3 position)
        {
            pumpPositions.Add(position);
            return this;
        }

        private void FuelPriceControl(Player player)
        {
            if (pumps == null || pumps.Length < 1)
            {
                player.Error("Degalinėje nėra įrengtų kuro kolonėlių.");
                ShowControllDialog(player);
                return;
            }
            List<Pump> _pumps = new List<Pump>();
            foreach (Pump pump in pumps)
            {
                if (pump == null) continue;
                _pumps.Add(pump);
            }
            if (_pumps.Count < 1)
            {
                player.Error("Degalinėje nėra įrengtų kuro kolonėlių.");
                ShowControllDialog(player);
                return;
            }
            TablistDialog dialog = new TablistDialog("Kainų valdymas", new string[] { "Kuro tipas", "Kaina" }, "Keisti", "Uždaryti");

            foreach (Pump pump in _pumps)
            {
                dialog.Add(new string[] { "" + pump.Type.Name(), pump.Price + " €" });
            }
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    ShowControllDialog(player);
                    return;
                }
                if (e.ListItem > _pumps.Count || e.ListItem < 0)
                    return;
                ChangePumpPrice(player, _pumps[e.ListItem]);
            };
        }
        private void ChangePumpPrice(Player player, Pump pump, string error = null)
        {
            RichText text = new RichText(Colors.White, Colors.Lime);
            text.SetText($"{(error == null ? "" : "$e"+ error + "$m\n")}Kuro tipas: $s{pump.Type.Name()}$m\nDabartinė kaina: $s{pump.Price}$m\nĮvesk naują norimą kuro kainą:");
            InputDialog input = new InputDialog("Kainų valdymas", text.ToString(), false, "Keisti", "Uždaryti");
            input.Show(player);
            input.Response += (_sender, _e) =>
            {
                if (_e.DialogButton == DialogButton.Right)
                {
                    FuelPriceControl(player);
                    return;
                }
                if (!int.TryParse(_e.InputText, out int newPrice))
                {
                    ChangePumpPrice(player, pump, "Įvesta kaina nėra tinkama.");
                    return;
                }
                if (newPrice < 0 || newPrice > 20)
                {
                    ChangePumpPrice(player, pump, "Įvesta kaina nėra intervale [0-20].");
                    return;
                }
                pump.Price = newPrice;
                FuelPriceControl(player);
            };
        }
        private void PumpAdd(Player player)
        {
            List<int> list = new List<int>();
            for (int i = 0; i < Enum.GetNames(typeof(FuelType)).Length; i++)
            {
                if (pumps[i] == null)
                    list.Add(i);
            }
            if (list.Count == 0)
            {
                player.Error("Visos galimos kolonėlės jau atidarytos");
                ShowControllDialog(player);
                return;
            }
            ListDialog dialog = new ListDialog("Kolonėlių atidarymas", "Pirkti", "Grįžti");
            EditPumpFromList(player, dialog, list);
        }
        private void PumpEdit(Player player)
        {
            List<int> list = new List<int>();
            for (int i = 0; i < Enum.GetNames(typeof(FuelType)).Length; i++)
            {
                if (pumps[i] != null)
                    list.Add(i);
            }
            if (list.Count == 0)
            {
                player.Error("Šioje degalinėje dar nėra nupirktų kolonėlių.");
                ShowControllDialog(player);
                return;
            }
            ListDialog dialog = new ListDialog("Kolonėlių redagavimas", "Redaguoti", "Grįžti");
            EditPumpFromList(player, dialog, list);
        }
        private void EditPumpFromList(Player player, ListDialog dialog, List<int> list)
        {
            foreach (int pumpId in list)
            {
                dialog.AddItem(((FuelType)pumpId).Name() + "");
            }
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right || LockedEditing)
                {
                    ShowControllDialog(player);
                    return;
                }
                if (e.ListItem + 1 > list.Count)
                    return;
                if (budget.Money < RLS.Config.Business.PumpPrice && pumps[list[e.ListItem]] == null)
                {
                    player.Error($"Veslo biudžete neužtenka pinigų kolonėlei(trūksta $s{(RLS.Config.Business.PumpPrice - budget.Money)}$m €).");
                    ShowControllDialog(player);
                    return;
                }
                EditFuelPumpType = (FuelType)list[e.ListItem];
                StartEdit(player);
            };
        }
        class Pump
        {
            private DynamicObject pump;
            private Pickup pickup;
            private FuelType type;
            private float fuelInPump = 0f;
            private float pumpSpeed = 0.2f;
            private float maxDistance = 7f;
            private int pricePerLiter = 1;
            private Timer timer;
            private float totalFuel = 0;
            private PetrolStation station;
            private int model;
            private Player user;
            public Vector3 Position { get => pump.Position;}
            public float Rotation { get => pump.Rotation.Z; }

            public FuelType Type { get => type; }
            public int Price { get => pricePerLiter; set => pricePerLiter = value; }
            public int Model { get => model; }

            public Pump(PetrolStation station, int pumpModel, FuelType type, Vector3 position, Vector3 rotation)
            {
                model = pumpModel;
                this.station = station;
                this.type = type;
                if (pump != null) Dispose();
                pump = new DynamicObject(pumpModel, position, rotation);
                //pickup
                pickup = new Pickup(1650, 2, position + Functions.PointFarFromPoint(1.5, rotation.Z + (pumpModel == 1676 ? -90 : 0)) + new Vector3(0, 0, 0.7));
                pickup.PickedUp += (sender, e) =>
                {
                    Player player = e.Player as Player;
                    StartFilling(player, player.LastVehicle);
                };
                pickup.AddText(new RichText().SetText(type.Name()));
            }

            public float GetFuel() => fuelInPump;
            public void SetFuel(float amount) => fuelInPump = amount;

            public void StartFilling(Player player, Vehicle vehicle)
            {
                if(vehicle == null || !vehicle.Created || vehicle.GetDistanceFromPoint(player.Position) > maxDistance)
                {
                    player.Error("Šalia kolonėlės nėra tinkamos tavo transporto priemonės.");
                    return;
                }
                if (timer != null)
                {
                    player.Error("Šiuo metu kolonėlė užimta.");
                    return;
                }
                totalFuel = 0;
                user = player;
                user.ToggleControllable(false);
                user.KeyStateChanged += CheckKeys;
                timer = new Timer(500, true);
                timer.Tick += (sender, e) =>
                {
                    if (player.IsDeleted || vehicle.GetDistanceFromPoint(Position) > maxDistance || !FillFuel(player, vehicle))
                        StopFilling();
                };
            }

            private void CheckKeys(object sender, SampSharp.GameMode.Events.KeyStateChangedEventArgs e)
            {
                if (e.NewKeys == Keys.SecondaryAttack)
                    if(sender is Player player)
                        StopFilling();

            }

            private void StopFilling()
            {
                if(user != null)
                {
                    user.ToggleControllable(true);
                    if (totalFuel > 0)
                    {
                        int price = (int)Math.Ceiling(totalFuel * pricePerLiter);
                        user.GiveMoney(-price, "Kuras");
                        station.budget.Add(price);
                    }
                    if (totalFuel > 0)
                        user.LastVehicle.Fuel += totalFuel;
                    user.KeyStateChanged -= CheckKeys;
                }
                user = null;
                timer?.Dispose();
                timer = null;
            }
            private bool FillFuel(Player player, Vehicle vehicle)
            {
                if (timer == null) return false;
                if(!type.ValidForEngine(vehicle.EngineType))
                {
                    player.Error("Netinkama kuro rūšis tavo transporto priemonei.");
                    return false;
                }
                if (player.Money < Math.Ceiling((totalFuel + pumpSpeed) * pricePerLiter))
                {
                    player.Error("Tau neužtenka pinigų.");
                    return false;
                }
                if (pumpSpeed > fuelInPump)
                {
                    player.Error("Kolonėlėje baigėsi kuras.");
                    return false;
                }
                player.ShowTutorial("Degalinė",
                    $"{type.Name()}: ~r~{pricePerLiter}~w~ eur/l\nPripilta kuro: ~r~{totalFuel.ToString("N1")}~w~ l\nKaina: ~r~{Math.Ceiling(totalFuel * pricePerLiter)}~w~ eur\n\nBaigti su ~r~enter", 600);
                fuelInPump -= pumpSpeed;
                totalFuel += pumpSpeed;
                return true;
            }

            public void Dispose()
            {
                if (pump == null) return;
                StopFilling();
                pump?.Dispose();
                pickup?.Dispose();
            }
        }
    }
}
