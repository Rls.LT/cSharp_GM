﻿using RLS.Definitions;
using SampSharp.GameMode;
using System;

namespace RLS.World.Property
{
    partial class PetrolStation
    {
        class PumpDataToSave
        {
            public int PumpModel { get; set; }
            public int FuelType { get; set; }
            public float Fuel { get; set; }
            public int Price { get; set; }
            public int Pos { get; set; }
        }
        public override void SaveData()
        {
            PumpDataToSave[] save = new PumpDataToSave[Enum.GetNames(typeof(FuelType)).Length];
            for(int i=0;i< Enum.GetNames(typeof(FuelType)).Length;i++)
            {
                if(pumps[i] == null)
                    save[i] = null;
                else
                {
                    PumpDataToSave _save = new PumpDataToSave
                    {
                        PumpModel = pumps[i].Model,
                        FuelType = (int)pumps[i].Type,
                        Fuel = pumps[i].GetFuel(),
                        Price = pumps[i].Price,
                        Pos = GetPumpSlot(pumps[i])
                    };
                    save[i] = _save;
                }
            }
            data = Json.EncodeJson(save);
            base.SaveData();
        }
        public PetrolStation LoadPumps()
        {
            PumpDataToSave[] loaded = new PumpDataToSave[Enum.GetNames(typeof(FuelType)).Length];
            loaded = Json.DecodeJson<PumpDataToSave[]>(data);
            foreach (PumpDataToSave data in loaded)
            {
                if (data == null)
                    return this;
                else
                {
                    if (data.Pos + 1 > pumpPositions.Count) data.Pos = 0;
                    pumps[data.FuelType] = new Pump(this, data.PumpModel, (FuelType)data.FuelType, new Vector3(pumpPositions[data.Pos].X, pumpPositions[data.Pos].Y, pumpPositions[data.Pos].Z), new Vector3(0, 0, DefaultPumpRotation))
                    {
                        Price = data.Price
                    };
                    pumps[data.FuelType].SetFuel(data.Fuel);
                }
            }
            return this;
        }
    }
}
