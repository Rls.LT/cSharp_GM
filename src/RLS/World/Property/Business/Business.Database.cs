﻿using Database;
using RLS.Definitions;

namespace RLS.World.Property
{
    partial class Business
    {
        protected string data;
        public override void SaveData()
        {
            Query q = new Query(Configs.Database.business).Update(new Fields()
                    .Add("name", Name)
                    .Add("owner_id", OwnerId)
                    .Add("owner_name", OwnerName)
                    .Add("price", Price)
                    .Add("sell_lock", SellLockTill.ToString())
                    .Add("budget", budget.Money)
                    .Add("selling_for", SellingFor)
                    .Add("next_offer", NextServerOffer.ToString())
                    .Add("data", data)
                    ).Where(new Field("id", SqlId));
            Database.MySql.Connection.RunThreadedVoid(q);
            Log.Write($"[Business] Saving {SqlId} business data.");
        }
        protected override void LoadData()
        {
            Result result = Database.MySql.Connection.Run(new Query(Configs.Database.business).Select("*").Where(new Field("name", Name)));
            if (result.NumRows < 1)//not in database
            {
                Database.MySql.Connection.RunThreadedVoid(new Query(Configs.Database.business).Insert(new Fields()
                    .Add("id", sqlId)
                    .Add("name", Name)
                    ));
            }
            else if(result.NumRows == 1)
            {
                Fields fields = result.GetRow();
                ownerId = fields.GetFieldByName("owner_id").Int();
                ownerName = fields.GetFieldByName("owner_name").String;
                sellLock = new DateTime(fields.GetFieldByName("sell_lock").DateTime());
                budget.Add(fields.GetFieldByName("budget").Int());
                sellingFor = fields.GetFieldByName("selling_for").Int();
                nextServerOffer = new DateTime(fields.GetFieldByName("next_offer").DateTime());
                data = fields.GetFieldByName("data").String;
                if (OwnerId != 0)
                {
                    Result owner = Database.MySql.Connection.Run(new Query(Configs.Database.players).Select("name, last_played").Where(new Field("id", OwnerId)));
                    if (owner.NumRows == 1)
                    {
                        Fields ownerFields = owner.GetRow();
                        ownerName = ownerFields.GetFieldByName("name").String;
                    }
                }
                UpdateText();
            }
        }
    }
}
