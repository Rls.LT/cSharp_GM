﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.World.Property
{
    partial class Business
    {
        [Command("pirkti versla")]
        public static void BuyCommands(Player player)
        {
            Business business = GetInRange(player.Position, 4f);
            if (business == null || !business.IsOnSale())
            {
                player.Error("Prie tavęs nėra jokio verslo, kurį galėtum nusipirkti.");
                return;
            }
            MessageDialog dialog = business.FormatSellDialog("Verslo");
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                    return;
                if (!CanBuy(player, business)) return;
                if (business.OwnerId == 0)
                {
                    Message(player, "Sėkmingai nusipirkai verslą. Verslo valdymas: $s/verslas");
                    player.GiveMoney(-business.Price);
                    business.SetOwner(player);
                }
                else if (business.SellingFor != 0)// buy from player
                    business.BuyFromPlayer(player);
            };
        }
        [Command("pasiulymas verslas")]
        public static void OfferBuyCommands(Player player)
        {
            Business business = GetOffered(player);
            if (business == null || !business.IsOfferedToBuy())
            {
                player.Error("Tau nesiūlomas joks verslas.");
                return;
            }
            MessageDialog dialog = business.FormatSellDialog("Verslo");
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                Player owner = Player.GetByDbId(business.OwnerId);
                if (e.DialogButton == DialogButton.Right)
                {
                    if (owner != null)
                        Message(owner, $"$s{player.Name}$m atsisakė pirkti verslą.");
                    business.CancelSelling(false);
                    Message(player, "Atsisakei pirkti verslą.");
                    return;
                }
                if (!CanBuy(player, business))
                {
                    if (owner != null)
                        Message(owner, $"$s{player.Name}$m negalėjo pirkti verslo.");
                    business.CancelSelling(false);
                    return;
                }
                if (business.OwnerId == 0)
                {
                    Message(player, "Sėkmingai nusipirkai verslą. Verslo valdymas: $s/verslas");
                    player.GiveMoney(-business.Price);
                    business.SetOwner(player);
                }
                else if (business.SellingFor != 0)// buy from player
                    business.BuyFromPlayer(player);
            };
        }
        [Command("verslas")]
        public static void ControlCommands(Player player)
        {
            Business business = GetInRange(player.Position, 4f);
            if (business == null || !business.IsOwnedBy(player.SqlId))
            {
                player.Error("Prie tavęs nėra jokio verslo, kurį galėtum valdyti.");
                return;
            }
            if (player.InAnyVehicle)
            {
                player.Error("Verslo negalima valdyti iš transporto priemonės.");
                return;
            }
            if (business.SellingFor != 0)
            {
                MessageDialog messageDialog = new MessageDialog(business.TitleFormat(),
                    $"{Colors.Hex.White}Norėdamas valdyti verslą turi atšaukti dabartinį pardavimą.\n" +
                    "Ar nori tai padaryti?", "Taip", "Ne");
                messageDialog.Show(player);
                messageDialog.Response += (sender, e) =>
                {
                    if (e.DialogButton == DialogButton.Right)
                        return;
                    business.CancelSelling();
                    Message(player, "Atšaukei verslo pardavinėjimą.");
                    business.ShowControllDialog(player);
                };
                return;
            }
            business.ShowControllDialog(player);
        }

        protected override int ControlDialogItemsResponse(Player player, int response)
        {
            if (response == 0)
            {
                MessageDialog info = new MessageDialog(TitleFormat(), InfoText(), "Grįžti");
                info.Show(player);
                info.Response += (d, args) => { ShowControllDialog(player); };
            }
            else if (response == 1)
                budget.Menu(player);
            else if (response == 2)
                SellMenu(player, this, $"{Colors.Hex.Lime}{Name}{Colors.Hex.White} pardavimas");
            return response - 3;
        }

        protected override void ControlDialogItems(ListDialog dialog)
        {
            dialog.AddItem("Informacija");
            dialog.AddItem("Biudžetas");
            dialog.AddItem("Parduoti");
        }
    }
}
