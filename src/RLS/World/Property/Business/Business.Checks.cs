﻿using RLS.World.Players;

namespace RLS.World.Property
{
    partial class Business
    {
        public static bool CanBuy(Player player, Business business)
        {
            Business[] alreadyOwning = PlayerOwning(player);
            if (alreadyOwning.Length >= RLS.Config.Business.OwningLimit)
            {
                player.Error($"Tau jau priklauso $s{alreadyOwning.Length}/{RLS.Config.Business.OwningLimit}$m verslai, todėl daugiau pirkti negali.");
                return false;
            }
            if (business.IsOwnedBy(player.SqlId))
            {
                player.Error($"Iš savęs verslo pirkti negali.");
                return false;
            }
            foreach (Business bus in alreadyOwning)
            {
                if (bus.Type == business.Type)
                {
                    player.Error($"Tau jau priklauso tokio tipo verlsas, todėl antro tokio pačio pirkti nebegali.");
                    return false;
                }
            }
            if (player.TotalPlayed/60 < RLS.Config.Business.FromTotalPlayed)
            {
                player.Error($"Verslų sistema galima naudotis tik pražaidus: $s{RLS.Config.Business.FromTotalPlayed}$m min.");
                return false;
            }
            if (business.OwnerId == 0)
            {
                if (business.Price > player.Money)
                {
                    player.Error($"Tau trūksta $s{Functions.IntFormat(business.Price - player.Money)} €$m, kad nusipirktum šį verslą.");
                    return false;
                }
            }
            else if (business.SellingFor != 0)
            {
                if (business.Price > player.Money)
                {
                    player.Error($"Tau trūksta $s{Functions.IntFormat(business.SellingFor - player.Money)} €$m, kad nusipirktum šį verslą.");
                    return false;
                }
            }
            return true;
        }
    }
}
