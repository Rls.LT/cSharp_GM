﻿using System.Collections.Generic;

namespace RLS.World.Property
{
    partial class CarShop
    {
        public override void SaveData()
        {
            data = Json.EncodeJson(shopCars);
            Log.Write(data);
            base.SaveData();
        }
        protected override void LoadData()
        {
            base.LoadData();
            shopCars = Json.DecodeJson<List<int>>(data);
        }
    }
}
