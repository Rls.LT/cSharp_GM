﻿using RLS.Core.Vehicles;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.Events;
using System;
using System.Collections.Generic;

namespace RLS.World.Property
{
    partial class CarShop
    {
        public Vector3 ShopPreviewPos = new Vector3(-1953.5942, 265.6714, 35.3949);
        public Vehicle ShopCar;
        private List<int> AvaliableModels = new List<int>();
        public CarShop AddModel(int model)
        {
            AvaliableModels.Add(model);
            return this;
        }
        private List<int> shopCars = new List<int>();
        private bool IsModelUsed(int model)
        {
            foreach (int carModel in shopCars)
            {
                if (carModel < 400)
                    continue;
                if (carModel == model)
                    return true;
            }
            return false;
        }
        public void AddCar(Player player)
        {
            List<int> unusedModels = new List<int>();
            foreach (int model in AvaliableModels)
            {
                if (!IsModelUsed(model))
                    unusedModels.Add(model);
            }
            if(unusedModels.Count == 0)
            {
                player.Error("Visi galimi modeliai jau pardavinėjami");
                return;
            }
            ListDialog dialog = new ListDialog("Modelių pirkimas", "Pirkti", "Grįžti");
            ModelFromList(player, dialog, unusedModels,
                (sender, e) =>
                {
                    if (e.DialogButton == DialogButton.Right || LockedEditing)
                    {
                        ShowControllDialog(player);
                        return;
                    }
                    if (e.ListItem + 1 > unusedModels.Count)
                        return;
                    shopCars.Add(unusedModels[e.ListItem]);
                    Message(player, $"Nusipirkai $s{unusedModels[e.ListItem]}$m modelį.");
                    SaveData();
                });
        }
        public void RemoveCar(Player player)
        {
            if (shopCars.Count == 0)
            {
                player.Error("Parduotuvėje nepardavinėjamos jokios tr. priemonės.");
                return;
            }
            ListDialog dialog = new ListDialog("Modelių pardavimas", "Parduoti", "Grįžti");
            ModelFromList(player, dialog, shopCars,
                (sender, e) =>
                {
                    if (e.DialogButton == DialogButton.Right || LockedEditing)
                    {
                        ShowControllDialog(player);
                        return;
                    }
                    if (e.ListItem + 1 > shopCars.Count)
                        return;
                    Message(player, $"Pardavei $s{shopCars[e.ListItem]}$m modelį.");
                    shopCars.RemoveAt(e.ListItem);
                    SaveData();
                });
        }
        private void ModelFromList(Player player, ListDialog dialog, List<int> list, EventHandler<DialogResponseEventArgs> handler)
        {
            foreach (int modelID in list)
            {
                dialog.AddItem(""+ modelID);
            }
            dialog.Show(player);
            dialog.Response += handler;
        }
    }
}
