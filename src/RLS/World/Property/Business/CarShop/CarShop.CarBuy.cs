﻿using RLS.Core.Vehicles;
using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;

namespace RLS.World.Property
{
    partial class CarShop
    {
        public void OnPickupBuyPickup(Player player)
        {
            if (shopCars.Count < 1)
            {
                player.Error("Šiame salone nėra parduodamų modelių.");
                return;
            }
            if(Editor != null)
            {
                player.Error("Šiuo metu aptarnaujamas kitas klientas.");
                return;
            }
            player.CameraPosition = ShopPreviewPos + new Vector3(0,0,3) + Functions.PointFarFromPoint(15, 115);
            player.SetCameraLookAt(ShopPreviewPos);
            ShopCar = new Core.Vehicles.Vehicle((VehicleModelType)shopCars[0], ShopPreviewPos, 0f, 1, 1);
            player.SpecialState = SpecialState.EditingObject;
            Editor = player;
            Editor.Disconnected += OnEditorDisconnect;
            LastEditValue = 0;
            player.ToggleControllable(false);
            player.ShowTutorial("Valdymas", "Transporto keitimui: ~r~4~w~ ir ~r~6~w~\nPasirinkimui: ~r~Y~w~\nVeiksmo atšaukimui: ~r~N");
        }

        private void OnEditorDisconnect(object sender, SampSharp.GameMode.Events.DisconnectEventArgs e)
        {
            StopEdit();
        }
        private void StopEdit()
        {
            if (Editor == null) return;
            ShopCar?.DestroyInstance();
            ShopCar?.Dispose();
            Editor.ToggleControllable(true);
            Editor.HideTutorial();
            Editor.SpecialState = SpecialState.NONE;
            Editor.Disconnected -= OnEditorDisconnect;
            Editor?.PutCameraBehindPlayer();
            Editor = null;
        }
        public override void OnEdit(Player player, Keys key)
        {
            if(key == Keys.AnalogLeft)
            {
                LastEditValue++;
                if (LastEditValue >= shopCars.Count)
                    LastEditValue -= shopCars.Count;
                ShopCar.DestroyInstance();
                ShopCar.Dispose();
                ShopCar = new Vehicle((VehicleModelType)shopCars[LastEditValue], ShopPreviewPos, 0f, 1, 1);
            }
            else if(key == Keys.AnalogRight)
            {
                LastEditValue--;
                if (LastEditValue < 0)
                    LastEditValue = shopCars.Count - 1;
                ShopCar.DestroyInstance();
                ShopCar.Dispose();
                ShopCar = new Vehicle((VehicleModelType)shopCars[LastEditValue], ShopPreviewPos, 0f, 1, 1);
            }
            else if(key == Keys.No)
            {
                StopEdit();
            }
            else if(key == Keys.Yes)
            {
                if(Vehicle.AllOwnedByPlayer(player).Length >= RLS.Config.Vehicle.MaxOwned)
                {
                    player.Error($"Tu jau turi maksimalų automobilių kiekį(Max $s{RLS.Config.Vehicle.MaxOwned}$m).");
                    return;
                }
                Vehicle veh = new Vehicle((VehicleModelType)shopCars[LastEditValue], new Vector3(-1990.7231, 255.8393, 34.8990), 250f, 1, 1);
                veh.SetOwner(player);
                veh.InsertInDatabase();
                veh.Fuel = 10f;
                //TODO: add car prices
                player.SuccessMessage($"Nusipirkai transporto priemonę {shopCars[LastEditValue]} už xx");
                StopEdit();
            }
        }
        
    }
}
