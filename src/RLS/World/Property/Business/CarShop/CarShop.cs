﻿using RLS.Definitions;
using RLS.Streamer;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Display;

namespace RLS.World.Property
{
    partial class CarShop : Business
    {
        private Pickup CarSellPickup; 
        public CarShop(string name, Vector3 position, int price) : base(name, position, price)
        {
        }

        protected override void Config()
        {
            type = Definitions.BusinessType.CarShop;
            CarSellPickup = new Pickup(2485, 2, new Vector3(-1955.3317, 283.1257, 35.3905));
            RichText text = new RichText(Colors.White, Colors.Lime);
            text.SetText($"$s{Name}$m\nUžsakymas\nIšsirink čia!");
            CarSellPickup.AddText(text);
            CarSellPickup.PickedUp += (sender, e) =>
            {
                OnPickupBuyPickup(e.Player as Player);
            };
        }
        protected override int ControlDialogItemsResponse(Player player, int response)
        {
            response = base.ControlDialogItemsResponse(player, response);
            if (response == 0)
                AddCar(player);
            else if (response == 1)
                RemoveCar(player);
              
            return response - 2;
        }
        protected override void ControlDialogItems(ListDialog dialog)
        {
            base.ControlDialogItems(dialog);
            dialog.AddItem("Nupirkti transporto tipą");
            dialog.AddItem("Parduoti transporto tipą");
        }
    }
}
