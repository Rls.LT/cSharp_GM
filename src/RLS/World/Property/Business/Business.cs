﻿using RLS.Definitions;
using RLS.Other.Budget;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using System.Linq;

namespace RLS.World.Property
{
    partial class Business : Property<Business>
    {
        protected Budget budget;
        protected string name;
        public string Name { get => name; }
        protected BusinessType type;
        public BusinessType Type { get => type; }
        private Pickup pickup;

        protected Vector3 position;
        public override Vector3 Position { get => position; }

        public Business(string name, Vector3 position, int price) : base(price)
        {
            this.name = name;
            this.position = position;
            Config();
            Pickups();
            Add(this);
            budget = new Budget("Verslas");
            budget.BackButtonEvent += (sender, e) =>{
                ShowControllDialog((sender as Player));
            };
            LoadData();
            budget.MoneyOperationPerformed += (sender, e) =>
            {
                SaveData();
            };
        }

        protected override string TitleFormat()
        {
            return $"Verslo {Colors.Hex.Lime}{Name}{Colors.Hex.White} valdymas";
        }

        public override void Dispose()
        {
            pickup.Dispose();
        }

        public override void OnEdit(Player player, Keys key)
        {
            Log.Write("Not implemented OnEdit");
        }

        protected virtual void Pickups()
        {
            pickup = new Pickup(19524, 1, Position);
            RichText text = new RichText();
            pickup.AddText(text);
        }

        protected override void UpdateText()
        {
            RichText text = new RichText(Colors.Green.Darken(0.2f), Colors.Green);
            text.SetText($"{Type.GetName()}$s\"{Name}\"$m\n");
            if (ownerId != 0) text.SetText(text.GetText() + $"Savininkas: $s{OwnerName}$m\n");
            if (ownerId == 0)
                text.SetText(text.GetText() + $"Parduodamas už: $s{Functions.IntFormat(Price)}$m €\n[ /pirkti versla ]");
            else if (sellingFor != 0)
                text.SetText(text.GetText() + $"Parduodamas už: $s{Functions.IntFormat(SellingFor)}$m €\n[ /pirkti versla ]");
            pickup.ChangeText(text);
        }

        protected override string InfoText()
        {
            return new RichText(Colors.White, Colors.Lime,
                $"$mPavadinimas:\t\t$s{Name}$m\n" +
                $"Tipas:\t\t\t$s{Type.GetName()}$m\n" +
                $"Biudžete:\t\t$s{Functions.IntFormat(budget.Money)} €$m"
                ).ToString();
        }

        protected virtual void Config()
        {
            type = BusinessType.Default;
        }

        protected override string Names(Case @case, bool firstUpper)
        {
            string name = "none";
            switch (@case)
            {
                case Property<Business>.Case.Type:
                    name = "verslas";
                    break;
                case Property<Business>.Case.What:
                    name = "verslą";
                    break;
                case Property<Business>.Case.Whos:
                    name = "verslo";
                    break;
            }
            if (firstUpper)
                return name.First().ToString().ToUpper() + name.Substring(1);
            else
                return name;
        }
    }
}
