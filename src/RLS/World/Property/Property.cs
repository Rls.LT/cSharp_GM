﻿using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.World;

namespace RLS.World
{
    public abstract partial class Property<T>
    {
        public bool LockedEditing { get; protected set; }
        public PlayerObject EditorObject;
        public Player Editor;
        public int LastEditValue;

        protected abstract string TitleFormat();

        public virtual Vector3 Position { get; set; }

        public Property(int price)
        {
            this.price = price;
            ownerId = 0;
            sqlId = counter++;
        }
        public abstract void Dispose();

        protected virtual string InfoText()
        {
            throw new System.Exception("UpdateText not implemented");
        }

        protected virtual void UpdateText()
        {
            throw new System.Exception("UpdateText not implemented");
        }

        public bool IsInRange(Vector3 position, float range)
        {
            if (Vector3.Distance(position, Position) <= range)
                return true;
            return false;
        }
        public static T GetInRange(Vector3 position, float range)
        {
            foreach (T property in All)
            {
                if ((property as Property<T>).IsInRange(position, range))
                    return property;
            }
            return default(T);
        }
        public enum Case
        {
            /// <summary>Kas</summary>
            Type,
            /// <summary>Ką</summary>
            What,
            /// <summary>Ko</summary>
            Whos
        }
        protected abstract string Names(Case @case, bool firstUpper = false);
    }
}
