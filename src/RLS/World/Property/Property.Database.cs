﻿using Database;
using System;

namespace RLS.World
{
    public abstract partial class Property<T>
    {
        private static int counter = 1;
        protected int sqlId = 0;

        public int SqlId { get => sqlId; }

        public virtual void SaveData()
        {
            throw new Exception("SaveData not implemented");
        }

        protected virtual void LoadData()
        {
            throw new Exception("LoadData not implemented");
        }

        protected void SendMoneyToOfflineOwner(int amount)
        {
            Result owner = Database.MySql.Connection.Run(new Query(Configs.Database.players).Update(new Fields().
                Add(new Field("money_in_pocket", amount, "=money_in_pocket+"))
                ).Where(new Field("id", ownerId)));
        }
    }
}
