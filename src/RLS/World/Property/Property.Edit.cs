﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Events;

namespace RLS.World
{
    partial class Property<T>
    {
        public abstract void OnEdit(Player player, Keys key);
        protected virtual void UpdateEditableObject()
        {

        }
        protected virtual void EndEdit()
        {

        }
        protected virtual void StartEdit(Player player)
        {
            LockedEditing = true;
            Editor = player;
            Editor.SpecialState = SpecialState.EditingObject;
            UpdateEditableObject();
            Editor.Disconnected += OnDisconnectFromEditor;
        }
        protected void OnDisconnectFromEditor(object sender, DisconnectEventArgs e)
        {
            EndEdit();
        }
    }
}
