﻿using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;

namespace RLS.World
{
    partial class Property<T>
    {
        public static bool CanBeManaged(Player player, Property<T> property)
        {
            if (property.Equals(default(T))) return false;
            if (property.IsOwnedBy(player.SqlId)) return true;
            return false;
        }

        protected abstract void ControlDialogItems(ListDialog dialog);
        protected abstract int ControlDialogItemsResponse(Player player, int dialogItem);
        protected void ShowControllDialog(Player player)
        {
            ListDialog dialog = new ListDialog(TitleFormat(), "Pasirinkti", "Uždaryti");
            ControlDialogItems(dialog);
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                    return;
                else
                    ControlDialogItemsResponse(player, e.ListItem);
            };
        }
    }
}
