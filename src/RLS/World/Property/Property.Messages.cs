﻿using RLS.Definitions;
using RLS.World.Players;

namespace RLS.World
{
    partial class Property<T>
    {
        public static void Message(Player player, string text)
        {
            RichText textFormat = new RichText(Colors.White, Colors.Lime);
            textFormat.SetText(text);
            player.SendClientMessage(textFormat.ToString());
        }
    }
}
