﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.World.Property
{
    public partial class House
    {
        [Command("pirkti nama")]
        public static void BuyHouse(Player player)
        {
            House house = GetInRange(player.Position, 4f);
            if (house == null || !house.IsOnSale())
            {
                player.Error("Prie tavęs nėra jokio namo, kurį galėtum nusipirkti.");
                return;
            }
            MessageDialog dialog = house.FormatSellDialog("Namo");
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                    return;
                if (!CanBuy(player, house)) return;
                if (house.OwnerId == 0)
                {
                    Message(player, "Sėkmingai nusipirkai namą. Namo valdymas: $s/namas");
                    player.GiveMoney(-house.Price);
                    house.SetOwner(player);
                }
                else if (house.SellingFor != 0)// buy from player
                    house.BuyFromPlayer(player);
            };
        }
        [Command("pasiulymas namas")]
        public static void OfferBuyCommands(Player player)
        {
            House house = GetOffered(player);
            if (house == null || !house.IsOfferedToBuy())
            {
                player.Error("Tau nesiūlomas joks namas.");
                return;
            }
            MessageDialog dialog = house.FormatSellDialog("Namo");
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                Player owner = Player.GetByDbId(house.OwnerId);
                if (e.DialogButton == DialogButton.Right)
                {
                    if (owner != null)
                        Message(owner, $"$s{player.Name}$m atsisakė pirkti namą.");
                    house.CancelSelling(false);
                    Message(player, "Atsisakei pirkti namą.");
                    return;
                }
                if (!CanBuy(player, house))
                {
                    if (owner != null)
                        Message(owner, $"$s{player.Name}$m negalėjo pirkti namo.");
                    house.CancelSelling(false);
                    return;
                }
                if (house.OwnerId == 0)
                {
                    Message(player, "Sėkmingai nusipirkai namą. Namo valdymas: $s/namas");
                    player.GiveMoney(-house.Price);
                    house.SetOwner(player);
                }
                else if (house.SellingFor != 0)// buy from player
                    house.BuyFromPlayer(player);
            };
        }
        [Command("unamas")]
        public static void LockHouse(Player player)
        {
            House house = NearDoors(player);
            if(house == null) player.Error("null");
            if (!CanBeManaged(player, house)) player.Error("false");
            if (!house.IsOwnedBy(player.SqlId)) player.Error("not owned");
            if (!CanBeManaged(player, house))
            {
                player.Error("Šalia tavęs nėra jokio namo, kurio spyną galėtum valdyti.");
                return;
            }
            if (house.Locked)
            {
                player.Error("Namas jau užrakintas.");
                return;
            }
            house.locked = true;
            player.ShowUILineNamed("lock", Colors.Green, "hud:radar_propertyG", "Užrakintas", "_", 3000);
        }
        [Command("anamas")]
        public static void UnlockHouse(Player player)
        {
            House house = NearDoors(player);
            if (!CanBeManaged(player, house))
            {
                player.Error("Šalia tavęs nėra jokio namo, kurio spyną galėtum valdyti.");
                return;
            }
            if (!house.Locked)
            {
                player.Error("Namas jau atrakintas.");
                return;
            }
            house.locked = false;
            player.ShowUILineNamed("lock", Colors.Orange, "hud:radar_propertyG", "Atrakintas", "_", 3000);
        }
        [Command("namas")]
        public static void ControlCommands(Player player)
        {
            House house = NearDoors(player, 4f);
            if (house == null || !house.IsOwnedBy(player.SqlId))
            {
                player.Error("Prie tavęs nėra jokio namo, kurį galėtum valdyti.");
                return;
            }
            if (house.SellingFor != 0)
            {
                MessageDialog messageDialog = new MessageDialog(house.TitleFormat(),
                    $"{Colors.Hex.White}Norėdamas valdyti namą turi atšaukti dabartinį pardavimą.\n" +
                    "Ar nori tai padaryti?", "Taip", "Ne");
                messageDialog.Show(player);
                messageDialog.Response += (sender, e) =>
                {
                    if (e.DialogButton == DialogButton.Right)
                        return;
                    house.CancelSelling();
                    Message(player, "Atšaukei namo pardavinėjimą.");
                    house.ShowControllDialog(player);
                };
                return;
            }
            house.ShowControllDialog(player);
        }

        protected override int ControlDialogItemsResponse(Player player, int response)
        {
            if (response == 0)
            {
                MessageDialog info = new MessageDialog(TitleFormat(), InfoText(), "Grįžti");
                info.Show(player);
                info.Response += (d, args) => { ShowControllDialog(player); };
            }
            else if (response == 1)
                SellMenu(player, this, $"{Colors.Hex.Lime}{SqlId}{Colors.Hex.White} namo valdymas");
            return response - 2;
        }

        protected override void ControlDialogItems(ListDialog dialog)
        {
            dialog.AddItem("Informacija");
            dialog.AddItem("Parduoti");
        }
    }
}
