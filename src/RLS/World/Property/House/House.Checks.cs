﻿using RLS.World.Players;

namespace RLS.World.Property
{
    partial class House
    {
        public static bool CanBuy(Player player, House house)
        {
            House[] alreadyOwning = PlayerOwning(player);
            if (alreadyOwning.Length >= Config.House.OwningLimit)
            {
                player.Error($"Tau jau priklauso $s{alreadyOwning.Length}/{Config.House.OwningLimit}$m namai, todėl daugiau pirkti negali.");
                return false;
            }
            if (house.IsOwnedBy(player.SqlId))
            {
                player.Error($"Iš savęs namo pirkti negali.");
                return false;
            }
            if (player.TotalPlayed > Config.House.FromTotalPlayed)
            {
                player.Error($"Namų sistema galima naudotis tik pražaidus: $s{Config.House.FromTotalPlayed}$m min.");
                return false;
            }
            if (house.OwnerId == 0)
            {
                if (house.Price > player.Money)
                {
                    player.Error($"Tau trūksta $s{Functions.IntFormat(house.Price - player.Money)} €$m, kad nusipirktum šį namą.");
                    return false;
                }
            }
            else if (house.SellingFor != 0)
            {
                if (house.Price > player.Money)
                {
                    player.Error($"Tau trūksta $s{Functions.IntFormat(house.SellingFor - player.Money)} €$m, kad nusipirktum šį namą.");
                    return false;
                }
            }
            return true;
        }
    }
}
