﻿using RLS.Definitions;
using RLS.World.Entrances;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using System.Linq;

namespace RLS.World.Property
{
    public partial class House : Property<House>
    {
        private Entrance entrance;
        private Interior interior;
        private bool locked;
        
        public bool Locked { get => locked; }
        public override Vector3 Position { get => entrance.OutsidePos; }

        public House(int price, Vector3 entrancePosition, float outsideFacing, Interior interior) : base(price)
        {
            this.interior = interior;
            entrance = new Entrance(entrancePosition, interior.GetData().Position)
                .SetInterior(interior.GetData().Interior)
                .SetAngles(interior.GetData().FacingAngle, outsideFacing)
                .SetVirtualWorld(SqlId)
                .SetGroup(EntranceGroups.House)
                .CreateVisual("Namas", 1273);
            entrance.Enter += (sender, e) =>
            {
                if (Locked)
                {
                    e.Cancel = true;
                    e.player.Error("Namas užrakintas.");
                    return;
                }
            };
            Add(this);
            LoadData();
        }

        public override void Dispose()
        {
            Remove(this);
        }

        public override void OnEdit(Player player, Keys key)
        {
            throw new System.NotImplementedException();
        }

        protected override string TitleFormat()
        {
            return $"{Colors.Hex.Lime}{SqlId}{Colors.Hex.White} namo valdymas";
        }

        public static House NearDoors(Player player, float range = 3f)
        {
            foreach (House house in All)
            {
                if ((house.entrance.IsPlayerCloseToEnter(player, range) || house.entrance.IsPlayerCloseToExit(player, range)))
                    return house;
            }
            return null;

        }

        protected override string InfoText()
        {
            return new RichText(Colors.White, Colors.Lime,
                $"$mNumeris:\t\t$s{SqlId}$m\n" +
                $"Interjeras:\t\t$s{((int)interior + 1)}$m"
                ).ToString();
        }

        protected override void UpdateText()
        {
            RichText text = new RichText(Colors.White, Colors.Lime);
            text.SetText($"Namas $s{SqlId}$m\n");
            if (ownerId != 0) text.SetText(text.GetText() + $"Savininkas: $s{OwnerName}$m\n");
            if (ownerId == 0)
                text.SetText(text.GetText() + $"Parduodamas už: $s{Functions.IntFormat(Price)}$m\n[ /pirkti nama ]");
            else if (sellingFor != 0)
                text.SetText(text.GetText() + $"Parduodamas už: $s{Functions.IntFormat(SellingFor)}$m\n[ /pirkti nama ]");
            entrance.ChangeText(text);
        }
        protected override string Names(Case @case, bool firstUpper)
        {
            string name = "none";
            switch (@case)
            {
                case Property<House>.Case.Type:
                    name = "namas";
                    break;
                case Property<House>.Case.What:
                    name = "namą";
                    break;
                case Property<House>.Case.Whos:
                    name = "namo";
                    break;
            }
            if (firstUpper)
                return name.First().ToString().ToUpper() + name.Substring(1);
            else
                return name;
        }
    }
}
