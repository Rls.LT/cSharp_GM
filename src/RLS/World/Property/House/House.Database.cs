﻿using Database;
using RLS.Controllers;
using RLS.Definitions;

namespace RLS.World.Property
{
    public partial class House
    {
        public override void SaveData()
        {
            Database.MySql.Connection.RunThreadedVoid(new Query(Configs.Database.houses).Update(new Fields().
                Add("interior", (int)interior).
                Add("owner_id", OwnerId).
                Add("owner_name", OwnerName).
                Add("locked", Locked ? 1 : 0).
                Add("price", Price).
                Add("sell_lock", SellLockTill.ToString()).
                Add("selling_for", SellingFor).
                Add("next_offer", NextServerOffer.ToString())
            ).Where(new Field("id", SqlId)));
        }

        protected override void LoadData()
        {
            Result result = Database.MySql.Connection.Run(new Query(Configs.Database.houses).Select("*").Where(new Field("id", SqlId)));
            if (result.NumRows < 1)//not in database
            {
                Database.MySql.Connection.RunThreadedVoid(new Query(Configs.Database.houses).Insert(new Fields().
                    Add("id", SqlId).
                    Add("interior", (int)interior)
                ));
            }
            else if (result.NumRows == 1)
            {
                Fields fields = result.GetRow();
                ownerId = fields.GetFieldByName("owner_id").Int();
                ownerName = fields.GetFieldByName("owner_name").String;
                locked = fields.GetFieldByName("locked").Bool();
                sellLock = new DateTime(fields.GetFieldByName("sell_lock").DateTime());
                sellingFor = fields.GetFieldByName("selling_for").Int();
                nextServerOffer = new DateTime(fields.GetFieldByName("next_offer").DateTime());
                if (OwnerId != 0)
                {
                    Result owner = Database.MySql.Connection.Run(new Query(Configs.Database.players).Select("name, last_played").Where(new Field("id", OwnerId)));
                    if (owner.NumRows == 1)
                    {
                        Fields ownerFields = owner.GetRow();
                        ownerName = ownerFields.GetFieldByName("name").String;
                    }
                }
                UpdateText();
            }
        }
    }
}
