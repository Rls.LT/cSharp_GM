﻿using SampSharp.GameMode;

namespace RLS.World.Property
{
    public enum Interior
    {
        Worst
    }
    static class InteriorMethods
    {
        public static InteriorData GetData(this Interior interior)
        {
            switch (interior)
            {
                case Interior.Worst:
                    return new InteriorData(new Vector3(2259.66431f, -1135.99658f, 1050.63281f),
                        280f, 10);

                default:
                    return new InteriorData();
            }
        }
    }
    public class InteriorData
    {
        public Vector3 Position { get; set; }
        public float FacingAngle { get; set; }
        public int Interior { get; set; }
        public InteriorData()
        {
            Position = Vector3.Zero;
            FacingAngle = 0f;
            Interior = 0;
        }
        public InteriorData(Vector3 position, float facingAngle, int interior)
        {
            Position = position;
            FacingAngle = facingAngle;
            Interior = interior;
        }
    }
}
