﻿using RLS.Definitions;
using RLS.World.Players;
using System.Collections.Generic;

namespace RLS.World
{
    public abstract partial class Property<T>
    {
        protected int ownerId;
        public int OwnerId { get => ownerId; }
        protected string ownerName;
        public string OwnerName { get => ownerName; }

        protected virtual void RemoveOwner()
        {
            ownerId = 0;
            ownerName = "none";
            UpdateText();
            SaveData();
        }

        protected virtual void SetOwner(Player player)
        {
            sellingFor = 0;
            ownerId = player.SqlId;
            ownerName = player.Name;
            sellLock = new DateTime(System.DateTime.Now.AddDays(RLS.Config.Property.ResellLock));
            UpdateText();
            SaveData();
        }

        public bool IsOwnedBy(int ownerSqlId)
        {
            if (OwnerId == ownerSqlId)
                return true;
            else return false;
        }

        public static T[] PlayerOwning(Player player)
        {
            List<T> list = new List<T>();
            foreach (T property in All)
            {
                if ((property as Property<T>).IsOwnedBy(player.SqlId))
                    list.Add(property);
            }
            return list.ToArray();
        }
    }
}
