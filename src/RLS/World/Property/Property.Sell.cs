﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using System.Linq;

namespace RLS.World
{
    public abstract partial class Property<T>
    {
        protected int price;
        public int Price { get => price; }

        protected DateTime sellLock;
        public DateTime SellLockTill { get => sellLock; }

        protected int sellingFor = 0;
        public int SellingFor { get => sellingFor; }
        public int SellOfferTo { get; set; } = 0;

        protected DateTime nextServerOffer = new DateTime(DateTime.Now);
        public DateTime NextServerOffer { get => nextServerOffer; }

        public static T GetOffered(Player player)
        {
            foreach (T property in All)
            {
                if ((property as Property<T>).SellOfferTo.Equals(player.SqlId))
                    return property;
            }
            return default(T);
        }

        public virtual void StartSelling(int price)
        {
            if (sellingFor != 0) return;
            sellingFor = price;
            UpdateText();
            SaveData();
        }

        public virtual void CancelSelling(bool showMessage = false)
        {
            if (sellingFor == 0) return;
            if (SellOfferTo != 0)
            {
                Player player = Player.GetByDbId(SellOfferTo);
                if (player != null && showMessage)
                    Message(player, $"$s{OwnerName}$m atšaukė {Names(Case.Whos)} pardavimą.");
            }
            sellingFor = 0;
            SellOfferTo = 0;
            UpdateText();
            SaveData();
        }

        protected static void SellMenu(Player player, Property<T> property, string title)
        {
            ListDialog dialog = new ListDialog(title, "Pasirinkti", "Grįžti");
            string lockString = property.SellLockTill.SystemDate > DateTime.Now ? $"\t\t{Colors.Hex.Red}[Užrakinta iki: {property.SellLockTill.ToString()}]" : "";
            dialog.AddItem("Parduoti žaidėjui" + lockString);
            dialog.AddItem("Parduoti miestui" + (property.NextServerOffer.SystemDate > DateTime.Now ? $"\t\t{Colors.Hex.Red}[Kitas pasiūlymas: {property.NextServerOffer.ToString()}]" : lockString));
            dialog.AddItem("Pradėti pardavinėti" + lockString);
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    property.ShowControllDialog(player);
                    return;
                }
                if (!property.CanSell(player)) return;
                if (e.ListItem == 0)//to player
                    SellToPlayer(player, property, title);
                else if (e.ListItem == 1)//sell to town
                {
                    if (property.NextServerOffer.SystemDate > DateTime.Now)
                    {
                        SellMenu(player, property, title);
                        player.Error($"Kitą pasiūlymą gausi ne anksčiau, kaip $s{property.NextServerOffer.ToString()}$m.");
                        return;
                    }
                    SellToServer(player, property, title);
                }
                else if (e.ListItem == 2)//to 
                    StartSelling(player, property, title);

            };
        }

        private static void StartSelling(Player player, Property<T> property, string title, string error = "")
        {
            InputDialog dialog = new InputDialog(title,
                $"\t\t\t\t{Colors.Hex.Lime}Nekilnojamojo turto pardavinėjimas{Colors.Hex.White}\n" +
                "Šiuo būdu parduodamas turtą jį įdėsi į parduodamų nekilnojamojo turto sąrašą.\n" +
                "Namas pardavime liks net tau atsijungus.\n" +
                "Pardavimą atšaukti galima bet kada, bet už kiekvieną paskelbimą turi sumokėti tam tikrą sumą.\n\n" +
                $"Šiuo metu paskelbimas kainuos: {Colors.Hex.Lime}{Config.Property.SellingPrice}{Colors.Hex.White} €.\n" +
                $"Įvesk kainą, už kurią nori pradėti pardavinėti: "
                + (error.Length > 2 ? $"\n{Colors.Hex.Red}{error}" : ""),
                false, "Pardavinėti", "Grįžti");
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    SellMenu(player, property, title);
                    return;
                }
                if (player.Money < Config.Property.SellingPrice)
                {
                    StartSelling(player, property, title, $"Tau trūksta {(Config.Property.SellingPrice - player.Money)} €, kad pradėtum pardavinėjimą.");
                    return;
                }
                int.TryParse(e.InputText, out int price);
                if (price < 1)
                {
                    StartSelling(player, property, title, "Kaina netinkama. Minimali kaina yra 1 €.");
                    return;
                }
                if (price > Config.Property.MaxSellPrice)
                {
                    StartSelling(player, property, title, $"Kaina netinkama. Maksimali kaina yra {Functions.IntFormat(Config.Property.MaxSellPrice)} €.");
                    return;
                }
                player.GiveMoney(-Config.Property.SellingPrice);
                property.StartSelling(price);
                Message(player, $"{property.Names(Case.Type, true)} sėkmingai pradėtas pardavinėti už $s{Functions.IntFormat(price)}$m €.");
            };
        }

        private static void SellToServer(Player player, Property<T> property, string title)
        {
            MessageDialog dialog = new MessageDialog(title,
                $"\t\t\t{Colors.Hex.Lime}Nekilnojamojo turto pardavimas miestui{Colors.Hex.White}\n" +
                "Šiuo būdu parduodamas nekilnojamąjį turtą gausi pasiūlymą iš miesto su tam tikra kaina.\n" +
                "Kainos gali svyruoti nuo labai mažos iki didesnės, nei vertas pats turtas.\n" +
                "Atsisakius parduoti už tokią kainą, kitą pasiūlymą gausi tik po 24h.",
                "Pasiūlymas", "Grįžti");
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    SellMenu(player, property, title);
                    return;
                }
                property.GetOfferFromServer(player, title);
            };
        }

        private void GetOfferFromServer(Player player, string title)
        {
            nextServerOffer = new DateTime(DateTime.Now.AddDays(1));
            int offer = new System.Random().Next((int)System.Math.Round(Price * 0.3), (int)System.Math.Round(Price * 1.05));
            MessageDialog dialog = new MessageDialog(title,
               $"\t\t\t{Colors.Hex.Lime}Nekilnojamojo turto pardavimas miestui{Colors.Hex.White}\n\n" +
               $"Miestas už tavo {Names(Case.What)} siūlo: {Colors.Hex.Lime}{offer}{Colors.Hex.White} €\n\n" +
               "Atsisakius parduoti už tokią kainą, kitą pasiūlymą gausi tik po 24h.",
               "Parduoti", "Atsisakyti");
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    SellMenu(player, this, title);
                    Message(player, $"Atsisakei $s{offer}$m € pasiūlymo.");
                    return;
                }
                player.GiveMoney(offer);
                Message(player, $"Pardavei $s{SqlId}$m {Names(Case.What)} už $s{offer}$m € miestui.");
                RemoveOwner();
            };
        }

        private static void SellToPlayer(Player player, Property<T> property, string title, string error = "")
        {
            InputDialog input = new InputDialog(title,
                $"\t\t\t\t{Colors.Hex.Lime}{property.Names(Case.Whos, true)} pardavimas žaidėjui{Colors.Hex.White}\n" +
                $"Šis pardavimo būdas tinkamas, kai jau rastas pirkėjas, kuris nupirks nekilnojamąjį turtą.\n" +
                $"Norėdamas parduoti įvesk pirkėjo pilną Vardą_Pavardę ir sumą, už kurią nori parduoti.\n\n" +
                $"Tinkamas formatas būtų: {Colors.Hex.Lime}Vardas_Pavardė 50000"
                + (error.Length > 2 ? $"\n{Colors.Hex.Red}{error}" : ""),
                false, "Parduoti", "Grįžti");
            input.Show(player);
            input.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    SellMenu(player, property, title);
                    return;
                }
                if (e.InputText.Split(' ').Length != 2)
                {
                    SellToPlayer(player, property, "Įvestas fomatas netinkamas.");
                    return;
                }
                string[] strings = e.InputText.Split(' ');
                Player otherPlayer = Player.GetByFullName(strings[0]);
                if (otherPlayer == null)
                {
                    SellToPlayer(player, property, "Tokio žaidėjo nėra.");
                    return;
                }
                int.TryParse(strings[1], out int price);
                if (price < 1)
                {
                    SellToPlayer(player, property, "Kaina per maža. Minimali kaina 1 €.");
                    return;
                }
                if (price > Config.Property.MaxSellPrice)
                {
                    SellToPlayer(player, property, $"Kaina per didelė. Maksimali kaina {Functions.IntFormat(Config.Property.MaxSellPrice)} €.");
                    return;
                }
                property.sellingFor = price;
                property.SellOfferTo = otherPlayer.SqlId;
                //info to other player
                Message(otherPlayer, $"$s{player.Name}$m tau siūlo pirkti {property.Names(Case.What)} $s{property.SqlId}$m už {Functions.IntFormat(property.sellingFor)} €.");
                Message(otherPlayer, $"Norėdamas sutikti/atšaukti pasiūlymą rašyk: [ $s/pasiulymas {property.Names(Case.Type)}$m ].");
                //info to seller
                Message(player, $"Žaidėjui $s{otherPlayer.Name}$m pasiūlei pirkti {property.Names(Case.What)} už $s{Functions.IntFormat(property.sellingFor)}$m €.");
            };
        }

        protected void BuyFromPlayer(Player player)
        {
            Player owner = Player.GetByFullName(OwnerName);
            if (owner == null) //offline
            {
                SendMoneyToOfflineOwner(SellingFor);
            }
            else
            {
                owner.SuccessMessage($"{Names(Case.What, true)} sėkmingai parduotas. Jį nupirko $s{player.Name}.");
                owner.SuccessMessage($"Tu gavai $s{SellingFor}$m €.");
                owner.GiveMoney(SellingFor);
            }
            player.GiveMoney(-SellingFor);
            SetOwner(player);
        }
        protected virtual MessageDialog FormatSellDialog(string propertyType="Turto")
        {
            return new MessageDialog($"{propertyType} pirkimas",
                InfoText() + "\n" +
                $"Pardavėjas:\t\t{Colors.Hex.Lime}{(OwnerId == 0 ? "Serveris" : OwnerName)}{Colors.Hex.White}\n" +
                $"Kaina:\t\t\t{Colors.Hex.Lime}{Functions.IntFormat((SellingFor == 0? Price : SellingFor))} €\n",
                "Pirkti", "Atsisakyti");
        }
        protected bool IsOnSale()
        {
            return !(OwnerId != 0 && SellingFor == 0 || SellOfferTo != 0);
        }
        protected bool IsOfferedToBuy()
        {
            return !(OwnerId == 0 || SellingFor == 0 || SellOfferTo == 0);
        }
        public bool CanSell(Player player)
        {
            if (SellLockTill.SystemDate > DateTime.Now)
            {
                player.Error($"Šio turto negali parduoti. Jo pardavimas užrakintas iki $s{SellLockTill.ToString()}$m.");
                return false;
            }
            if (!IsOwnedBy(player.SqlId))
            {
                player.Error($"Šis turtas tau nebepriklauso.");
                return false;
            }
            return true;
        }
    }
}
