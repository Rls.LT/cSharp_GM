﻿using RLS.Core.Tutorials;
using System;
using System.Reflection;

namespace RLS.World.Players
{
    partial class Player
    {
        public BaseTutorial currentTutorial;
        private int[] tutorials = new int[BaseTutorial.MaxTutorials];
       
        private void LoadTutorials(int[] tutorials)
        {
            for (int i = 0; i < tutorials.Length; i++)
                this.tutorials[i] = tutorials[i];
        }

        public void OnPassedTutorial(BaseTutorial tutorial)
        {
            UpdateTutorialStatus(tutorial.Id, true);
            Log.Write($"[Tutorial] {Name} passed {tutorial.MissionCode} mission.", LogLevel.Debug);
            currentTutorial = null;
        }

        public void StartTutorial(Type tutType)
        {
            if (currentTutorial != null) return;
            if (!tutType.IsSubclassOf(typeof(BaseTutorial)))
            {
                Log.Write($"[Tutorials] StartTutorial({tutType.ToString()}) bad type for function.", LogLevel.Error);
                return;
            }
            currentTutorial = Activator.CreateInstance(tutType, this) as BaseTutorial;
            object value = currentTutorial.GetType().GetProperty("Id")?.GetValue(currentTutorial, null);
            int id = (value == null ? -1 : (int)value);
            if (IsTutorialPassed(id, currentTutorial.TimesToPlay))
            {
                currentTutorial = null;
                return;
            }
            currentTutorial?.Start();
        }

        public void UpdateTutorialStatus(int id, bool status)
        {
            if (id < 0 || id >= BaseTutorial.MaxTutorials)
            {
                Log.Write($"[Error] UpdateTutorialStatus({id}, {status}) received bad id.");
                return;
            }
            tutorials[id] = (status) ? 1 : 0;
        }

        public bool IsTutorialPassed(int id, int timesToPlay)
        {
            if (id < 0 || tutorials.Length<=id || id >= BaseTutorial.MaxTutorials)
            {
                Log.Write($"[Error] IsTutorialPassed({id}) received bad id.", LogLevel.Error);
                return true;
            }
            if(tutorials[id] == timesToPlay) Log.Write($"[Debug] Totorial with id: {id} already passed.", LogLevel.Debug);
            return tutorials[id] == timesToPlay;
        }
    }
}
