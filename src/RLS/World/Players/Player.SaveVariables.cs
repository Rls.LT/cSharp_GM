﻿using RLS.World.Jobs;
using RLS.World.Jobs.Definitions;
using System;
namespace RLS.World.Players
{
    partial class Player
    {
        private int score = 0;
        private int totalTimePlayed = 0;
        private int todayPlayed = 0;
        private int lastPlayed;
        public int TotalPlayed { get => totalTimePlayed + SessionTime; }
        public int TodayPlayed
        {
            get
            {
                if (DayUpdated.Date != DateTime.Today.Date)
                {
                    DayUpdated = DateTime.Today.Date;
                    todayPlayed = lastPlayed - new RLS.Definitions.DateTime(DateTime.Today.Date).Timestamp;
                }
                return todayPlayed + SessionTime;
            }
        }
        private int wantedLevel;
        public new int WantedLevel { get => wantedLevel;
            set
            {
                wantedLevel = value;
                if (wantedLevel > Config.MaxWantedLevel) wantedLevel = Config.MaxWantedLevel;
                base.WantedLevel = (int)Math.Ceiling(wantedLevel / 100.0);
            }
        }
        private int money;
        public int InHospitalFor { get; set; } = 0;

        private JobInfo job;
        private Manager manager;
        public Manager Manager { get => manager; set => manager = value; }
        public JobInfo Job { get => job; set => job = value; }
        public string Email { get; set; }
        internal int Experience { get => score; set { score = value; Score = value; } }
        public new int Money { get => money; }
        public new void ResetMoney()
        {
            base.ResetMoney();
            base.GiveMoney(Money);
        }
        public new void GiveMoney(int amount)
        {
            GiveMoney(amount, "Nenustatyta");
        }
        public void GiveMoney(int amount, string reason)
        {
            money += amount;
            base.ResetMoney();
            if (Math.Abs(amount) > 10000) ForcedDataSave();
            base.GiveMoney(money);
        }
    }
}
