﻿using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.SAMP;
using System.Collections.Generic;

namespace RLS.World.Players
{
    public class Attachment
    {
        public int Index { get; private set; }
        public int Model { get; private set; }
        public Player Owner { get; private set; }
        public Bone Bone { get; private set; }
        public Vector3 Offset { get; private set; }
        public Vector3 Scale { get; private set; } = new Vector3(1, 1, 1);
        public Vector3 Rotation { get; private set; }
        private bool attached = false;
        public Color Color1 { get; private set; } = 0;
        public Color Color2 { get; private set; } = 0;
        public Attachment(Player player, int index, int model, Bone bone)
        {
            Index = index;
            Model = model;
            Owner = player;
            Bone = bone;
        }
        public Attachment SetOffset(Vector3 offset)
        {
            Offset = offset;
            return this;
        }
        public Attachment SetRotation(Vector3 rotation)
        {
            Rotation = rotation;
            return this;
        }
        public Attachment SetScale(Vector3 scale)
        {
            Scale = scale;
            return this;
        }
        public Attachment SetColors(Color color1, Color color2)
        {
            Color1 = color1;
            Color2 = color2;
            return this;
        }
        public void Attach()
        {
            if (attached)
                Owner.RemoveAttachedObject(Index);
            attached = true;
            Owner.SetAttachedObject(Index, Model, Bone, Offset, Rotation, Scale, Color1, Color2);
        }
        public void Dispose()
        {
            Owner.RemoveAttachedObject(Index);
        }
    }
    public class Attachments
    {
        private List<Attachment> attachments = new List<Attachment>();
        private Player player;
        public Attachments(Player player) => this.player = player;

        public Attachment Add(int model, Bone bone)
        {
            if(attachments.Count >= Player.MaxAttachedObjects)
            {
                Log.Write("[Player attachments] Max attached object count reached.", LogLevel.Error);
                return null;
            }
            Attachment attachment = new Attachment(player, attachments.Count, model, bone);
            attachments.Add(attachment);
            return attachment;
        }
        public void Remove(int model)
        {
            foreach(Attachment attachment in attachments.ToArray())
            {
                if(attachment.Model == model)
                {
                    attachment.Dispose();
                    attachments.Remove(attachment);
                    return;
                }
            }
        }
    }
}
