﻿using RLS.Missions;
using RLS.World.Players.Stories;
using System.Collections.Generic;

namespace RLS.World.Players
{
    partial class Player
    {
        private Story story = new Story();
        public Mission currentMission;
        public void SetStory(Story story) => Story = story;
        /// <summary>
        /// Player story
        /// </summary>
        public Story Story { get => story; private set => story = value; }

        /// <summary>
        /// Temporary variable for player story selection
        /// </summary>
        public Story StorySelection { get; set; }

        private List<string> PassedMissions = new List<string>();

        public void StartStoryMission(string code)
        {
            if (!IsMissionPassed(code))//first mission
            {
                int.TryParse(code.Substring(1), out int missionId);
                if (missionId == 0) return;
                currentMission = Story.GetMission((missionId-1), this);
                currentMission?.Start();
            }
        }
        public void PassedMission(Mission mission)
        {
            PassedMissions.Add(mission.MissionCode);
            Log.Write($"[Missions] {Name} passed {mission.MissionCode} mission.", LogLevel.Debug);
            currentMission = null;
        }

        public bool IsMissionPassed(string code)
        {
            return PassedMissions.Contains(code);
        }
    }
}
