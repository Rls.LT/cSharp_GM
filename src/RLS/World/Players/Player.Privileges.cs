﻿using RLS.World.Jobs;

namespace RLS.World.Players
{
    partial class Player
    {
        private bool developer = false;
        private bool tester = false;
        private string Privileges
        {
            get => (developer?1:0) + "" + (tester?1:0);
            set
            {
                developer = (value.Length < 1 ? false : value[0] == '1');
                tester = (value.Length < 2 ? false : value[1] == '1');
            }
        }
        public bool SetTesterState { set => tester = value; }
        public bool IsDeveloper => developer;
        public bool IsTester => tester;
        public bool IsManager => Manager.JobId != JobTypes.NONE;
    }
}
