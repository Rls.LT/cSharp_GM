﻿using RLS.Definitions;
using RLS.World.Players.Definitions;
using SampSharp.GameMode.SAMP;

namespace RLS.World.Players
{
    partial class Player
    {
        public Attachments Attachments;
        public double TaxiOdoWhenEntered { get; set; } = -1;
        private int skin = 98;
        public int Clothes { get; set; } = 98;
        public new int Skin
        {
            get => skin;
            set
            {
                skin = value;
                base.Skin = skin;
            }
        }
        public new int Score
        {
            get => score;
            set
            {
                score = value;
                base.Score = score;
            }
        }
        public Color DefaultColor { get; set; } = 0xcccccc00;


        private ConnectionStates connection_state;
        private Gender gender = 0;
        private SpecialState specialState = SpecialState.NONE;
        internal SpecialState SpecialState { get => specialState; set => specialState = value; }
        public void SetConnectionState(ConnectionStates state) => connection_state = state;
        public ConnectionStates GetConnectionState() => connection_state;
        public void SetGender(Gender gender) => this.gender = gender;
        public Gender GetGender() => gender;

        private int joinedServerStamp = 0;

        public int JoinedServerOn
        {
            get => joinedServerStamp;
            set
            {
                if(joinedServerStamp == 0)
                    joinedServerStamp = value;
            }
        }
        public System.DateTime DayUpdated = System.DateTime.Today.Date;
        public int SessionTime
        {
            get => DateTime.CurrentTimestamp - JoinedServerOn;
        }
    }
}
