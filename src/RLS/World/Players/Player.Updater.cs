﻿using RLS.Events.Args;
using SampSharp.GameMode.SAMP;
using System;

namespace RLS.World.Players
{
    partial class Player
    {
        /// <summary>
        /// Player update event
        /// </summary>
        public event EventHandler<PlayerArgs> Updated;
        /// <summary>
        /// Player updater
        /// </summary>
        private Timer updater;
        private void OnPlayerUpdates(object sender, EventArgs e)
        {
            Updated?.Invoke(sender, new PlayerArgs(this));
        }
    }
}
