﻿using RLS.Display;
using RLS.Display.Controllers;
using SampSharp.GameMode.SAMP;

namespace RLS.World.Players
{
    partial class Player
    {
        private InfoLines infoLines;
        private TutorialUI tutorialUI;
        private MissionChat missionChat;
        private FadeUI fadeUI;
        private Objective infoBar;
        private Speedometer speedometer;


        private void InitializeUI()
        {
            infoLines = new InfoLines(this);
            infoLines.Add(new InfoLine(this, "hospital"));
            infoLines.Add(new InfoLine(this, "lock"));
            infoLines.Add(new InfoLine(this, "action"));

            tutorialUI = new TutorialUI(this);
            missionChat = new MissionChat(this);
            fadeUI = new FadeUI(this);
            infoBar = new Objective(this);
            speedometer = new Speedometer(this);


            Disposed += (sender, e) => DestroyUI();
        }

        private void DestroyUI()
        {
            infoLines?.Dispose();
            tutorialUI?.Dispose();
            missionChat?.Dispose();
            fadeUI?.Dispose();
            infoBar?.Dispose();
            speedometer?.Dispose();
        }
        public FadeUI Screen { get => fadeUI; }

        public void ShowInfoBar(string text, int timeToShow = -1)
        {
            infoBar?.SetText(text).Show(timeToShow);
        }

        public void ShowTutorial(string title, string text, int timeToShow = -1)
        {
            tutorialUI?.SetTitle(title).SetText(text).Show(timeToShow);
        }
        public void HideTutorial()
        {
            tutorialUI?.Hide();
        }

        public void ShowMissionChat(string text, int timeToShow = -1)
        {
            missionChat?.SetText(text).Show(timeToShow);
        }
        /// <summary>
        /// Shows ui line with specified name for player.
        /// </summary>
        /// <param name="name">UI name to search for</param>
        /// <param name="icon">Txd icon. TXD lib:txd name [hud:radar_hostpital]</param>
        /// <param name="updateText">Text to update or null</param>
        /// <param name="additional">Text to update or null</param>
        /// <param name="timeToShow">Time to show line for player or -1</param>
        public void ShowUILineNamed(string name, Color backColor, string icon = null, string updateText = null, string additional = null, int timeToShow = -1)
        {
            InfoLine line = infoLines?.GetLineByName(name);
            line?.SetBackgroundColor(backColor).SetIcon(icon).SetText(updateText).SetAdditionalText(additional);
            infoLines?.Show(line, timeToShow);
        }
        public void HideUILineNamed(string name)
        {
            infoLines?.GetLineByName(name)?.Hide();
        }

        public void ShowSpeedometer(string name, string info, int showFor = 1000)
        {
            speedometer?.Update(name, info).Show(showFor);
        }
    }
}
