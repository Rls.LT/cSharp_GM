﻿using RLS.Definitions;
using RLS.World.Players.Definitions;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP.Commands;
using System;
using RLS.World.Jobs;
namespace RLS.World.Players
{
    partial class Player
    {
        [Command("info")]
        public void InfoCommand(string otherPlayer = "")
        {
            Player player = null;
            if (!otherPlayer.Equals(""))
            {
                player = Get(otherPlayer);
                if (player == null)
                {
                    SendClientMessage(Colors.Red, "* Toks žaidėjas nerastas.");
                    return;
                }
            }
            if (player == null) player = this;
            string privileges = "";
            if (player.IsTester) privileges = "Testuotojas";
            if (player.IsDeveloper) privileges = "Kūrėjas";
            RichText text = new RichText(Colors.White, Colors.Blue);
            text.SetText(
                $"Žaidėjo vardas: $s{player.Name}$m [$s{player.Id}$m]\n" +
                $"Privilegijos: $s{privileges}$m\n" +
                $"Darbas: $s{player.Job.Type.GetWorkerName()}$m\n" +
                $"Direktorius: $s{player.Manager.JobId.GetName()}$m\n" +
                $"Ieškomas: $s{player.WantedLevel}$m lygiu\n" +
                $"Šiandien žaidė: $s{RLS.Definitions.DateTime.ReadableSeconds(player.TodayPlayed)}$m\n" +
                $"Šios sesijos laikas: $s{RLS.Definitions.DateTime.ReadableSeconds(player.SessionTime, ReadableFormat.Short)}$m\n" +
                $"Visas laikas serveryje: $s{RLS.Definitions.DateTime.ReadableSeconds(player.TotalPlayed)}$m"
                );
            MessageDialog dialog = new MessageDialog($"{Colors.Hex.Lime}{player.Name}{Colors.Hex.White} informacija",
                text.ToString(), "Uždaryti");
            dialog.Show(this);
        }

        [Command("nustatymai")]
        public void SettingsCommand()
        {
            Array keys = Enum.GetValues(typeof(KeyId));
            TablistDialog dialog = new TablistDialog("Žaidėjo nustatymai", new[] { "Nustatymas", "Reikšmė" }, "Keisti", "Uždaryti");
            //key settings
            foreach (KeyId key in keys)
            {
                dialog.Add(new string[]{ $"{key.GetString()}", $"{Settings.GetKey(key).ToString()}" });
            }
            dialog.Show(this);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                    return;
                if (keys.Length > e.ListItem && e.ListItem >= 0)
                    SelectKey((KeyId)e.ListItem);
            };
        }
        private void SelectKey(KeyId itemId)
        {
            Array keys = Enum.GetValues(typeof(Key));
            if (keys.Length < 1)
            {
                SendClientMessage(Colors.Red, "* Šiuo metu nėra galimų mygtukų.");
                SettingsCommand();
                return;
            }
            ListDialog dialog = new ListDialog("Pasirink mygtuką", "Pasirinkti", "Grįžti");
            foreach (Key key in keys)
            {
                dialog.AddItem($"{key.ToString()}");
            }
            dialog.Show(this);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                    return;
                settings.AddKey(itemId, (Key)e.ListItem);
                SettingsCommand();
            };
        }

    }
}
