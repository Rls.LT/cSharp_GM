﻿using RLS.Definitions;
using SampSharp.GameMode.SAMP;

namespace RLS.World.Players
{
    partial class Player
    {
        internal void SendClientMessage(Color color, RichText richText)
        {
            SendClientMessage(color, richText.ToString());
        }
        internal void SuccessMessage(string message)
        {
            RichText textFormat = new RichText(Colors.Green, Colors.Yellow);
            textFormat.SetText("* " + message);
            SendClientMessage(Colors.Green, textFormat.ToString());
        }
        internal void Error(string text)
        {
            RichText textFormat = new RichText(Colors.Red, Colors.Red.Darken(0.2f));
            textFormat.SetText("* " + text);
            SendClientMessage(Colors.Red, textFormat.ToString());
        }
        internal void Command(string text, string usage = null)
        {
            SendClientMessage(Colors.CommandText, $"* {(usage ?? "Naudojimas")}: " + "{59C4C5}" + $"{text}");
        }
    }
}
