﻿namespace RLS.World.Players
{
    partial class Player
    {
        private bool cuffed = false;
        public bool IsWithHandcuff { get => cuffed; }
        public void Handcuff(bool cuff)
        {
            cuffed = cuff;
            if(cuffed) SpecialAction = SampSharp.GameMode.Definitions.SpecialAction.Cuffed;
            else SpecialAction = SampSharp.GameMode.Definitions.SpecialAction.None;
            ToggleControllable(!cuff);
        }

        public override void ToggleControllable(bool toggle)
        {
            if (toggle && IsWithHandcuff) return;
            base.ToggleControllable(toggle);
        }
    }
}
