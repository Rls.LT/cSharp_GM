﻿using RLS.Definitions;
using System.Collections.Generic;

namespace RLS.World.Players.Definitions
{
    public class Settings
    {
        private List<Key> keys = new List<Key>();
        public Key[] Keys { get => keys.ToArray(); set => keys = new List<Key>(value); }
        public void AddKey(KeyId keyType, Key key)
        {
            if (keys.Count <= (int)keyType)
                keys.Add(key);
            else
                keys[(int)keyType] = key;
        }
        public Key GetKey(KeyId keyType)
        {
            if (keys.Count <= (int)keyType)
                return Key.N;
            else
                return keys[(int)keyType];
        }
        public Settings()//default
        {
            AddKey(KeyId.Engine, Key.Y);
            AddKey(KeyId.Gates, Key.H);
        }
    }
}
