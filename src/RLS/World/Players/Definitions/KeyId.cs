﻿namespace RLS.World.Players.Definitions
{
    public enum KeyId
    {
        Engine,
        Gates
    }
    static class KeyIdMethods
    {
        public static string GetString(this KeyId id)
        {
            switch (id)
            {
                case KeyId.Engine:
                    return "Variklio valdymas";
                case KeyId.Gates:
                    return "Vartų valdymas";
                default:
                    return "nežinomas";
            }
        }
    }
}
