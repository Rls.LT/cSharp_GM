﻿namespace RLS.World.Players.Definitions
{
    public enum ConnectionStates
    {
        NONE,
        CONNECTING,
        LOGGED,
        LOADED
    }
}
