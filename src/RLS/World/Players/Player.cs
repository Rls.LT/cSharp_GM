﻿using System;
using SampSharp.GameMode.World;
using SampSharp.GameMode;
using SampSharp.GameMode.Events;
using RLS.World.Players.Definitions;
using RLS.Core.Vehicles;
using RLS.Events.Args;
using SampSharp.GameMode.Definitions;
using RLS.World.Entrances;
using SampSharp.GameMode.SAMP;
using Database;

namespace RLS.World.Players
{
    public partial class Player : BasePlayer
    {
        public static event EventHandler<PlayerEventArgs> FullyLoaded;
        public static event EventHandler<PlayerEventArgs> Disconnect;
        public event EventHandler<EventArgs> Created;
        public event EventHandler<EntranceArgs> UseEntrance;
        public Entrance LastEntranceUsed;
    
        public int failedLogins = 0;
        private Settings settings;
        public Settings Settings { get => settings; }
        public Vector3 SpawnLocation { get; set; }
        private bool disposed = false;
        public bool IsDeleted { get => disposed; }
        public bool SpawnInHospital { get; set; } = false;

        public void OnUseEntrance(EntranceArgs entranceArgs)
        {
            UseEntrance?.Invoke(null, entranceArgs);
        }
        
        public Vehicle LastVehicle { get; private set; }

        public new Vehicle Vehicle => Vehicle.GetByBase(base.Vehicle);

        public Player()
        {
            Attachments = new Attachments(this);
            StateChanged += (sender, e) => 
            {
                if (e.NewState == PlayerState.Driving || e.NewState == PlayerState.Passenger)
                {
                    LastVehicle = Vehicle;
                }
            };
            Created += (sender, e) => InitializeUI();
            Disposed += (sender, e) => OnDisposed();
            OnCreated();
        }
        
        private void OnCreated()
        {
            antiRaceNumber = antiRaceCounter++;
            cache = new Cache();
            Email = "";
            Created?.Invoke(null, new PlayerEventArgs(this));
        }
        /// <summary>
        /// On player fully loaded
        /// </summary>
        public void OnLoaded()
        {
            updater = new Timer(500, true);
            updater.Tick += OnPlayerUpdates;
            SetConnectionState(ConnectionStates.LOADED);
            StartGame();
            ForcedDataSave();
            FullyLoaded?.Invoke(null, new PlayerEventArgs(this));
        }

        public override void OnDisconnected(DisconnectEventArgs e)
        {
            Disconnect?.Invoke(null, new PlayerEventArgs(this));
            base.OnDisconnected(e);
        }

        private void OnDisposed()
        {
            updater?.Dispose();
            disposed = true;
        }

        public void Kick(String kickMessage)
        {
            Console.WriteLine("[kick] Player " + Name + " kicked for: " + kickMessage);
            base.Kick();
        }

        public void Spawn(Vector3 newPosition)
        {
            base.Spawn();
            Position = newPosition;
        }

        public void StartGame()
        {
            SetSpawnInfo(NoTeam, skin, Position, 0f);
            Spawn();
            ResetMoney();
            ToggleControllable(true);
            PutCameraBehindPlayer();
        }

        public void ClearAnimsInLib(string lib) => ApplyAnimation(lib, "null", 0.0f, false, false, false, false, 0);
        #region overrides
        public void Kick(int time, string messageToPlayer)
        {
            System.Timers.Timer timer = new System.Timers.Timer(time);
            timer.Elapsed += (sender, e) => { Kick(); };
            timer.Start();
            SendClientMessage("Tu išmestas iš serverio dėl: " + messageToPlayer);
        }
        public override void Spawn()
        {
            SetSpawnInfo(NoTeam, skin, SpawnLocation, 0.0f);
            base.Spawn();
        }
        #endregion
        /// <summary>
        /// Get player by Id or name
        /// </summary>
        /// <param name="nameOrId">Name or id</param>
        /// <returns></returns>
        public static Player Get(string nameOrId)
        {
            bool numeric = int.TryParse(nameOrId, out int n);
            foreach(Player player in All)
            {
                if (numeric)
                {
                    if (player.Id.Equals(n))
                        return player;
                }
                else
                {
                    if (player.Name.Contains(nameOrId))
                        return player;
                }
            }
            return null;
        }
        public static Player GetByFullName(string name)
        {
            foreach (Player player in All)
            {
                if (player.Name.Equals(name))
                    return player;
            }
            return null;
        }
        public static Player GetByDbId(int id)
        {
            foreach (Player player in All)
            {
                if (player.SqlId.Equals(id))
                    return player;
            }
            return null;
        }

    }
}
