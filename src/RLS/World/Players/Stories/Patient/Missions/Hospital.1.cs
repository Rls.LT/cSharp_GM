﻿using RLS.Missions;
using RLS.Missions.Creator;
using SampSharp.GameMode;

namespace RLS.World.Players.Stories.Missions
{
    class Hospital : Mission
    {
        public Hospital(Player player): base(player, "s1")
        {
            blockActions = true;
        }
        protected override void Create()
        {
            Step(new Control(1).ChangeVirtualWorld(Owner.Id+1));//disable weather sounds
            Step(new Control(false));
            Step(new Camera(new Vector3(-2504.1860, 563.6412, 69.3339), new Vector3(-2505.1055, 564.0457, 69.2637), 2000));
            Step(new Screen(1000, false));
            Step(new Actors("Daktaras", 70, new Vector3(-2726.7910, 738.3301, 1381.7070), 90f));
            Step(new Control(new Vector3(-2729.5410, 737.6012, 1381.6714), 270f).Skin(177));
            Step(new Anim("INT_HOUSE", "BED_Loop_L", true, true, 50));
            Step(new Camera(new Vector3(-2730.6343, 741.3354, 1385.3784), new Vector3(-2729.9087, 740.6498, 1384.6162), 100));
            Step(new Screen(1000, true));
            Step(new Chat("Daktaras", "Atlikome visus reikiamus tyrimus ir panašu, kad jus sveikas.\nGalite ruoštis namo.", 5000));
            Step(new Chat("Bet, daktare, ką man daryti jei aš praradau atmintį?", 5000, false));
            Step(new Chat("Daktaras", "Manau, kad su laiku atmintis sugris.", 5000));
            Step(new Chat("Jei jau taip sakote..", 3000, false));
            Step(new Chat("Daktaras", "Prieš išvykstant namo užsukite pas seselę, ji atiduos jūsų daiktus.", 5000));
            Step(new Chat("Supratau, ačiū!", 3000, false));
            Step(new Screen(1000, false));
            Step(new Camera(1000));
            Step(new Control(new Vector3(-2727.5425, 737.9746, 1381.7000), 264f));
            Step(new Anim("INT_HOUSE", "null", true, false, 50));
            Step(new Actors("Daktaras"));
            Step(new Control(true));
            Step(new Screen(1000, true));
            Step(new Actors("Seselė", 308, new Vector3(-2726.2964, 727.8221, 1381.7078), 0f));
            Step(new Objective("Pakalbėk su sesele", 3000));
            Step(new Checkpoint(new Vector3(-2725.9536, 730.7949, 1381.7078), 2f).LastStepInScene());
            Step(new Control(false));
            Step(new Control(new Vector3(-2726.1394, 730.6536, 1381.7000), 180f));
            Step(new Camera(new Vector3(-2722.1394, 732.4736, 1382.7980), new Vector3(-2723.8413, 731.3604, 1382.7858), 2000));
            Step(new Chat("Seselė", "Kuo galėčiau padėti?", 3500));
            Step(new Chat("Mane išleidžia iš ligoninės. Norėčiau atsiimti savo daiktus.", 5000));
            Step(new Chat("Seselė", "Luktėlkite..", 3000));
            Step(new Anim("Seselė", "BAR", "Barserve_bottle", false, false, 2000));
            Step(new Chat("Seselė", "Štai imkite. Persirengti galite rūbinėje.", 2000));
            Step(new Screen(1000, false));
            Step(new Actors("Seselė"));
            Step(new Control(true));
            Step(new Camera(100));
            Step(new Checkpoint(new Vector3(-2700.6877, 727.8078, 1381.7000), 2f));
            Step(new Screen(1000, true));
            Step(new Objective("Nueik į rūbinę", 1500).LastStepInScene());
            Step(new Control(false));
            Step(new Screen(1000, false));
            Step(new Control(true).Skin(Owner.Story.Skin(Owner.GetGender())));
            Step(new Screen(1000, true));
            Step(new Objective("Išeik iš ligoninės", 1500));
            Step(new Tutorial("Apmokymai", "Norint išeiti iš ligoninės reikia prieiti prie durų, kurios pažymėtos\nišėjimu ir spausti 'Enter'.", 5000));
            Step(new Control(true).ChangeVirtualWorld(0).LastStepInScene());
        }
    }
}
