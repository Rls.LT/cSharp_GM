﻿using RLS.World.Players.Stories.Missions;
using SampSharp.GameMode;

namespace RLS.World.Players.Stories
{
    class Patient : Story
    {
        protected override void RegisterMissions()
        {
            RegisterMission(typeof(Hospital));
        }
        protected override void StorySet()
        {
            storyTitle = "Pabudęs iš komos";
            storyLine = "Tu ką tik pabudai iš komos, bet ne be pasėkmių. Tu praradai atmintį. Todėl, kai\n" +
                        "tave paleis bandysi ieškoti informacijos apie save.\n" +
                        " ";
            maleSkin = 98;
            femaleSkin = 148;
            SpawnPoint = new Vector3(-2727.6045, 740.6653, 1381.7000);
        }
    }
}
