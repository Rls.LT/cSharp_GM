﻿using RLS.Definitions;
using SampSharp.GameMode;

namespace RLS.World.Players.Stories
{
    public partial class Story
    {
        public int Id { get; private set; } = 0;
        protected string storyLine = "Story unset";
        protected string storyTitle = "Title unset";

        public Story()
        {
            StorySet();
            RegisterMissions();
        }

        protected virtual void StorySet()
        {
        }

        protected int maleSkin = 1;
        protected int femaleSkin = 1;

        public Vector3 SpawnPoint { get; protected set; } = Config.historySelection_Pos;

        public int MaleSkin { get => maleSkin; }
        public int FemaleSkin { get => femaleSkin; }
        public string Line { get => storyLine; }
        public string Title { get => storyTitle; }

        public int Skin(Gender gender)
        {
            if (gender == Gender.MALE)
                return MaleSkin;
            else
                return FemaleSkin;
        }

        public bool Selected { get => Id > 0; }


        public Story Next
        {
            get
            {
                if (All.Length <= Id)
                    return GetById();
                else
                    return GetById(Id + 1);
            }
        }

        public Story Prev
        {
            get
            {
                if (Id <= 1)
                    return GetById(All.Length);
                else
                    return GetById(Id-1);
            }
        }
    }
}
