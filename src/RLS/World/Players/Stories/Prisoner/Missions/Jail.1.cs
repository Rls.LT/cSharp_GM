﻿using RLS.Missions;
using RLS.Missions.Creator;
using SampSharp.GameMode;

namespace RLS.World.Players.Stories.Missions
{
    class Jail : Mission
    {
        public Jail(Player player): base(player, "s1")
        {
            blockActions = true;
            timeBeforeStart = 100;
        }
        protected override void Create()
        {
            Step(new Control(false).Skin(206));
            Step(new Camera(new Vector3(-1538.7192, 769.3051, 42.4498), new Vector3(-1539.3887, 768.5557, 42.2399), 2000));
            Step(new Screen(1000, false));
            Step(new Actors("Policininkas", 310, new Vector3(266.9284, 83.3417, 1001.1175), 90f));
            Step(new Control(new Vector3(265.0616, 83.0804, 1001.1175), 270f).ChangeInterior(6).ChangeVirtualWorld(Owner.Id + 1));
            Step(new Camera(new Vector3(263.4360, 80.2512, 1002.6875), new Vector3(264.1033, 80.9941, 1002.3088), 100));
            Step(new Screen(1000, true));
            Step(new Chat("Policininkas", "Žinai, kokia šiandien diena?", 3000));
            Step(new Chat("O taip, šiandien aš paleidžiamas į laisvę!", 4000));
            Step(new Chat("Policininkas", "Teisingai. Linkiu elgtis gerai ir nebegrįžti čia.", 4000));
            Step(new Chat("Pagaliau...", 2000));
            Step(new Chat("Policininkas", "Išeidamas daiktus gali pasiimti iš konsultantės.", 4000));
            Step(new Screen(1000, false));
            Step(new Actors("Policininkas"));
            Step(new Actors("Konsultantė", 307, new Vector3(251.2395, 68.0829, 1003.6469), 90f));
            Step(new Camera());
            Step(new Control(true));
            Step(new Screen(1000, true));
            Step(new Objective("Pakalbėk su konsultante", 3000));
            Step(new Checkpoint(new Vector3(249.9112, 68.0414, 1003.6406), 2f).LastStepInScene());
            Step(new Control(false));
            Step(new Control(new Vector3(250.0098, 68.1248, 1003.6469), 270f));
            Step(new Camera(new Vector3(247.9105, 68.2977, 1004.6367), new Vector3(248.9063, 68.2216, 1004.4733), 
                new Vector3(247.7538, 71.8528, 1004.9711), new Vector3(248.4202, 71.1091, 1004.8077), 2000));
            Step(new Chat("Konsultantė", "Kuo galėčiau padėti?", 3000));
            Step(new Chat("Šiandien mane išleidžia į laisvę, norėčiau pasiimti savo daiktus.\nTaip pat pagaliau pasikeisti drabužius..", 5000));
            Step(new Chat("Konsultantė", "Priminkite, kuo jūs vardu?", 3000));
            Step(new Chat($"Hmm.. {Owner.Name}"));
            Step(new Chat("Konsultantė", "Radau", 3000));
            Step(new Anim("Konsultantė", "BAR", "Barserve_bottle", false, false, 2000));
            Step(new Chat("Konsultantė", "Štai raktas nuo spintelės rūbinėje.", 2000));
            Step(new Screen(1000, false));
            Step(new Actors("Konsultantė"));
            Step(new Camera());
            Step(new Control(true));
            Step(new Screen(1000, true));
            Step(new Objective("Nueik į rūbinę", 3000));
            Step(new Checkpoint(new Vector3(253.5289, 77.0588, 1003.6406), 2f).LastStepInScene());
            Step(new Control(false));
            Step(new Screen(1000, false));
            Step(new Control(true).Skin(Owner.Story.Skin(Owner.GetGender())).ChangeVirtualWorld(0));
            Step(new Screen(1000, true));
            Step(new Objective("Išeik iš policijos departamento", 1500));
            Step(new Tutorial("Apmokymai", "Norint išeiti iš pastato reikia prieiti prie durų, kurios pažymėtos\nišėjimu ir spausti 'Enter'.", 5000));
            Step(new Control(true).LastStepInScene());

        }
    }
}
