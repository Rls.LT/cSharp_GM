﻿using RLS.World.Players.Stories.Missions;

namespace RLS.World.Players.Stories
{
    class Prisoner : Story
    {
        protected override void RegisterMissions()
        {
            RegisterMission(typeof(Jail));
        }
        protected override void StorySet()
        {
            storyTitle = "Buvęs kalinys";
            storyLine = "Tave šiandien paleidžia iš kalėjimo. Atsėdėjai bausmę, kurios nenusipelnei,\n" +
                        "nes tave pakišo. Išėjęs į laisvę trokšti atkeršyti kaltininkams. Su savimi\n" +
                        "pinigų neturi, bet jų prieš bausmės atlikimą pasislėpei tam tikroje vietoje.";
            maleSkin = 7;
            femaleSkin = 55;
        }
    }
}
