﻿using RLS.World.Players.Stories.Missions;

namespace RLS.World.Players.Stories
{
    class Tourist : Story
    {
        protected override void RegisterMissions()
        {
            RegisterMission(typeof(Plane));
        }
        protected override void StorySet()
        {
            storyTitle = "Emigrantas";
            storyLine = "Tu atvykai čia iš kitos šalies, todėl dabar tau reikia apsiprasti ir susirasti\n" +
                        "draugų. Savo gimtinėje patyrei nepriteklių, todėl pinigais pasigirti\n" +
                        "negali, bet patirties tau netrūkta.";
            maleSkin = 26;
            femaleSkin = 211;
        }
    }
}
