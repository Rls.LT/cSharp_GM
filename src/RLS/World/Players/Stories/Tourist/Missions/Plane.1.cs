﻿using RLS.Missions;
using RLS.Missions.Creator;
using SampSharp.GameMode;

namespace RLS.World.Players.Stories.Missions
{
    class Plane : Mission
    {
        public Plane(Player player): base(player, "s1")
        {
            blockActions = true;
            timeBeforeStart = 0;
        }
        protected override void Create()
        {
            Step(new Control(1).ChangeVirtualWorld(Owner.Id+1));//disable weather sounds
            Step(new Control(false).Skin(Owner.Story.Skin(Owner.GetGender())));
            Step(new Camera(new Vector3(-2797.4556, -689.6057, 1008.4626), new Vector3(-2798.4519, -689.6428, 1008.0748), 1000));
            Step(new Screen(1000, false)); 
            Step(new Control(new Vector3(0.6070, 25.6029, 1199.5985), 180f).ChangeInterior(1).ChangeVirtualWorld(Owner.Id + 1));
            Step(new Anim("INT_HOUSE", "LOU_In", false, true, 50));
            Step(new Camera(new Vector3(2.8009, 26.0744, 1200.1956), new Vector3(1.9554, 25.5431, 1200.0062), 1000));
            Step(new Screen(1000, true));
            //passengers
            Step(new Actors("Keleivis1", 37, new Vector3(0.5676, 28.3740, 1199.5985), 0f));
            Step(new Anim("Keleivis1", "INT_HOUSE", "LOU_In", false, true, 50));
            Step(new Actors("Keleivis2", 40, new Vector3(2.8363, 29.2004, 1199.5985), 0f));
            Step(new Anim("Keleivis2", "INT_HOUSE", "LOU_In", false, true, 50));
            //~~~~~~~~~~~~~~
            Step(new Actors("Stiuardesė", 11, new Vector3(1.6864, 31.5324, 1199.5985), 180f));
            Step(new Chat("Tikiuosi čia viskas bus kitaip..", 3000, false));
            Step(new Camera(new Vector3(2.8009, 26.0744, 1200.1956), new Vector3(1.9554, 25.5431, 1200.0062),
                new Vector3(2.8009, 26.0744, 1200.1956), new Vector3(1.6864, 31.5324, 1199.5985), 2000));
            Step(new Chat("Stiuardesė", "Leisimės po 10 minučių, prašome prisisegti diržus.", 3000));
            Step(new Screen(1000, false));
            Step(new Actors("Keleivis1"));
            Step(new Actors("Keleivis2"));
            Step(new Actors("Stiuardesė"));
            Step(new Control(new Vector3(-1434.2560, -148.9416, 18.2160), 252f).ChangeInterior(0));
            Step(new Camera());
            Step(new Control(true).ChangeVirtualWorld(0));
            Step(new Screen(1000, true));
            Step(new Checkpoint(new Vector3(-1433.1837, -164.9889, 18.2009), 2f));
            Step(new Objective("Atsiimk savo daiktus", 3000).LastStepInScene());

            Step(new Objective("Išeik iš oro uosto", 1500));
            Step(new Control(true).LastStepInScene());
        }
    }
}
