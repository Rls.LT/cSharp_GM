﻿using System.Collections.Generic;

namespace RLS.World.Players.Stories
{
    partial class Story
    {
        private static List<Story> stories = new List<Story>();

        public static Story[] All { get => stories.ToArray(); }
        public static void RegisterStory(Story story)
        {
            story.Id = All.Length+1;
            stories.Add(story);
        }
        public static Story GetById(int id = 1)
        {
            if (id == 0) return new Story();
            if (id > All.Length || id < 1) return null;
            return All[id-1];
        }
    }
}
