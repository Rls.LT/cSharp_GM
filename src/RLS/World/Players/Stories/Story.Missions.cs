﻿using RLS.Missions;
using System;
using System.Collections.Generic;

namespace RLS.World.Players.Stories
{
    partial class Story
    {
        public List<Type> missions = new List<Type>();
        public Type[] AllMissions { get => missions.ToArray(); }
        public void RegisterMission(Type type)
        {
            if (!type.IsSubclassOf(typeof(Mission)))
            {
                Log.Write("Failed to register mission. " + type + " is not subclass of " + typeof(Mission), LogLevel.Error);
                return;
            }
            missions.Add(type);
            Log.Write(type + " registered to story: " + Title, LogLevel.Debug);
        }
        public bool IsStoryMission(Type type)
        {
            return missions.Contains(type);
        }
        public Mission GetMission(int id, Player forPlayer)
        {
            if (id >= AllMissions.Length || id < 0) return null;
            return Activator.CreateInstance(AllMissions[id], forPlayer) as Mission;
        }

        protected virtual void RegisterMissions()
        {
        }
    }
}
