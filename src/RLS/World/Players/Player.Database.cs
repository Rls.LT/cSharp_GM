﻿using RLS.Definitions;
using RLS.World.Jobs;
using RLS.World.Jobs.Definitions;
using RLS.World.Players.Definitions;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using RLS.World.Players.Stories;
using System.Collections.Generic;
using Database;

namespace RLS.World.Players
{
    partial class Player
    {
        private static int antiRaceCounter=1;
        private int antiRaceNumber = 0;
        public int AntiRace { get => antiRaceNumber; }
        private int sqlId = 0;
        public int SqlId { get => sqlId; }
        public Cache cache;
        // TODO: implement antirace
        public void LoadFromCache()
        {
            if (cache.IsNull)
            {
                Kick();
                return;
            }
            cache.FindFieldByName("id").Int(out sqlId);
            Email = cache.FindFieldByName("email").String;
            cache.FindFieldByName("money_in_pocket").Int(out money);
            cache.FindFieldByName("time_played_total").Int(out totalTimePlayed);
            cache.FindFieldByName("time_played_today").Int(out todayPlayed);
            Score = cache.FindFieldByName("experience_total").Int();
            cache.FindFieldByName("last_played").Int(out lastPlayed);
            SpawnLocation = new Vector3(cache.FindFieldByName("x").Float(), cache.FindFieldByName("y").Float(), cache.FindFieldByName("z").Float());
            Interior = cache.FindFieldByName("interior").Int();
            VirtualWorld = cache.FindFieldByName("virtual_world").Int();
            gender = (Gender)cache.FindFieldByName("gender").Int();
            story = Story.GetById(cache.FindFieldByName("history").Int());
            job = Json.DecodeJson<JobInfo>(cache.FindFieldByName("job").String).Player(this);
            manager = new Manager((JobTypes)cache.FindFieldByName("manager").Int());
            settings = Json.DecodeJson<Settings>(cache.FindFieldByName("settings").String);
            PassedMissions = Json.DecodeJson<List<string>>(cache.FindFieldByName("missions").String);
            LoadTutorials(Json.DecodeJson<int[]>(cache.FindFieldByName("tutorials").String));
            WantedLevel = cache.FindFieldByName("wanted_level").Int();
            Skin = cache.FindFieldByName("current_skin").Int();
            Clothes = cache.FindFieldByName("skin").Int();

            //privileges
            Privileges = cache.FindFieldByName("privileges").String;
            //~~~~~~~~~~~~~~~~~~~~~~
            //reset today minutes if date is different
            if (DateTime.FromTimestamp(lastPlayed).Date != System.DateTime.Today)
                todayPlayed = 0;
            lastPlayed = DateTime.CurrentTimestamp;
        }
        public void ForcedDataSave()
        {
            if (connection_state != ConnectionStates.LOADED) return;
            Log.Write($"[Player] Forced saving player {Name}", LogLevel.Debug);
            Database.MySql.Connection.RunThreadedVoid(new Query(Configs.Database.players).Update(new Fields().
                Add("money_in_pocket", Money).
                Add("time_played_total", totalTimePlayed).
                Add("time_played_today", todayPlayed).
                Add("last_played", DateTime.CurrentTimestamp).
                Add("experience_total", score).
                Add("manager", (int)Manager.JobId).
                Add("missions", Json.EncodeJson(PassedMissions)).
                Add("job", Json.EncodeJson(job)).
                Add("tutorials", Json.EncodeJson(tutorials)).
                Add("wanted_level", WantedLevel).
                Add("current_skin", Skin)
            ).Where(new Field("id", "" + sqlId)));
        }
        public void SaveData(DisconnectReason reason)
        {
            if (connection_state != ConnectionStates.LOADED) return;
            Log.Write($"[Player] Saving player {Name}", LogLevel.Debug);
            Database.MySql.Connection.RunThreadedVoid(new Query(Configs.Database.players).Update(new Fields().
                Add("email", Email).
                Add("money_in_pocket", Money).
                Add("time_played_total", TotalPlayed).
                Add("time_played_today", TodayPlayed).
                Add("last_played", DateTime.CurrentTimestamp).
                Add("experience_total", score).
                Add("current_skin", Skin).
                Add("skin", Clothes).
                Add("x", Position.X).
                Add("y", Position.Y).
                Add("z", Position.Z).
                Add("interior", Interior).
                Add("virtual_world", VirtualWorld).
                Add("gender", (int)gender).
                Add("history", story.Id).
                Add("missions", Json.EncodeJson(PassedMissions)).
                Add("tutorials", Json.EncodeJson(tutorials)).
                Add("job", Json.EncodeJson(job)).
                Add("manager", (int)Manager.JobId).
                Add("privileges", Privileges).
                Add("settings", Json.EncodeJson(settings)).
                Add("wanted_level", WantedLevel)

            ).Where(new Field("id", "" + sqlId)));
        }
    }
}
