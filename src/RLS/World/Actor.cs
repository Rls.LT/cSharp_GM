﻿using System;
namespace RLS.World
{
    public class Actor : SampSharp.GameMode.World.Actor
    {
        private string name = "";
        public void SetName(String name) => this.name = name;
        public String GetName() => name;

        public void TalkingStanding(int time = 1000)
        {
            ClearAnimsInLib("ped");
            ApplyAnimation("ped", "IDLE_chat", 4.1f, false, true, true, false, time);
        }
        public void ClearAnimsInLib(string lib) => ApplyAnimation(lib, "null", 0.0f, false, false, false, false, 0);
    }
}
