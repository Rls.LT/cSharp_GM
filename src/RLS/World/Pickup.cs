﻿using RLS.Definitions;
using RLS.Streamer;
using SampSharp.GameMode;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.World;
using System;

namespace RLS.World
{
    class Pickup
    {
        private DynamicPickup realPickup;
        private TextLabel label = null;
        public event EventHandler<PlayerEventArgs> PickedUp;
        private int virtualWorld = 0;

        public Pickup(int model, int type, Vector3 position, int vw = 0)
        {
            virtualWorld = vw;
            realPickup = new DynamicPickup(model, type, position, vw);
            realPickup.PickedUp += ExecuteEvent;
        }
        public void Dispose()
        {
            realPickup.Dispose();
            if (label != null)
            {
                label.Dispose();
            }
        }
        private void ExecuteEvent(object sender, PlayerEventArgs e)
        {
            PickedUp?.Invoke(this, e);
        }
        public Pickup AddText(RichText text, float drawDistance=25f)
        {
            if(label != null) return this;
            label = new TextLabel(text.ToString(), text.GetMainColor(), realPickup.Position, drawDistance, virtualWorld);
            return this;
        }
        public Pickup AddText(string text, Color color, float drawDistance = 25f)
        {
            if (label != null) return this;
            label = new TextLabel(text, color, realPickup.Position, drawDistance, virtualWorld);
            return this;
        }
        public Pickup ChangeText(RichText text)
        {
            if (label != null)
            {
                label.Text = text.ToString();
            }
            return this;
        }
        public Pickup ChangeText(string text)
        {
            if (label != null)
            {
                label.Text = text.ToString();
            }
            return this;
        }
    }
}
