﻿using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.World;

namespace RLS.World.Maps
{
    class PetrolStations : Map
    {
        public override void RemoveObjects(Player player)
        {
            GlobalObject.Remove(player, 1686, new Vector3(-1675.2188, 407.1953, 6.3828), 20f);
        }
        public override void CreateMap()
        {
            
        }
    }
}
