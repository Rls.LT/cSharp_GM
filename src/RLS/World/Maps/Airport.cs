﻿using RLS.Streamer;
using RLS.World.Entrances;
using SampSharp.GameMode;

namespace RLS.World.Maps
{
    class Airport : Map
    {
        public override void Entrances()
        {
            new Entrance(new Vector3(-1413.7119, -294.2181, 14.1484), new Vector3(-1432.7340, -184.2137, 18.2009))
                .SetAngles(141, 345)
                .CreateVisual("Oro uostas");
        }
        public override void CreateMap()
        {
            new DynamicObject(19545, new Vector3(-1430.2199700f, -158.1197100f, 20.6920000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19545, new Vector3(-1415.9539800f, -162.7543900f, 20.6920000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19325, new Vector3(-1424.4940200f, -127.1319900f, 19.2675000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19325, new Vector3(-1418.2017800f, -129.1747000f, 19.2675000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19325, new Vector3(-1411.8857400f, -131.2256900f, 19.2675000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1897, new Vector3(-1427.5319800f, -126.1607000f, 20.5510000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1897, new Vector3(-1427.5319800f, -126.1607000f, 18.3151000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1897, new Vector3(-1426.5346700f, -126.4811000f, 19.3226000f), new Vector3(90.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1897, new Vector3(-1424.4068600f, -127.1683000f, 19.3226000f), new Vector3(90.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1897, new Vector3(-1423.2949200f, -127.5406600f, 18.3151000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1897, new Vector3(-1425.4005100f, -126.8619500f, 18.3151000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19325, new Vector3(-1405.5661600f, -133.2785800f, 19.2675000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19447, new Vector3(-1429.2133800f, -130.6821000f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19447, new Vector3(-1432.1909200f, -139.8439000f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19447, new Vector3(-1435.1678500f, -149.0065000f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19447, new Vector3(-1438.1451400f, -158.1633000f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19447, new Vector3(-1441.1219500f, -167.3271900f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19447, new Vector3(-1444.0982700f, -176.4883000f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19447, new Vector3(-1441.1204800f, -182.6026500f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19545, new Vector3(-1430.2180200f, -158.1197100f, 17.2009000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19545, new Vector3(-1415.9539800f, -162.7543900f, 17.2009000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(1892, new Vector3(-1423.8249500f, -180.6599600f, 17.1973000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(1572, new Vector3(-1437.1757800f, -183.1956000f, 17.8135000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1572, new Vector3(-1438.6798100f, -182.6868900f, 17.8135000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1572, new Vector3(-1440.3125000f, -182.1926100f, 17.8135000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1572, new Vector3(-1441.8138400f, -181.6951000f, 17.8135000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1572, new Vector3(-1443.5874000f, -181.1135600f, 17.8135000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1713, new Vector3(-1424.8547400f, -132.1492800f, 17.1983000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1713, new Vector3(-1425.3031000f, -130.2778900f, 17.1983000f), new Vector3(0.0000000f, 0.0000000f, 252.0000000f));
            new DynamicObject(1713, new Vector3(-1425.9897500f, -135.5700200f, 17.1983000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(2311, new Vector3(-1424.8969700f, -133.1789700f, 17.1991000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(1713, new Vector3(-1426.4492200f, -133.6835500f, 17.1983000f), new Vector3(0.0000000f, 0.0000000f, 252.0000000f));
            new DynamicObject(2311, new Vector3(-1425.9929200f, -136.6266600f, 17.1991000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(1713, new Vector3(-1427.5706800f, -137.1375700f, 17.1983000f), new Vector3(0.0000000f, 0.0000000f, 252.0000000f));
            new DynamicObject(1713, new Vector3(-1427.1024200f, -139.0378300f, 17.1983000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19447, new Vector3(-1429.5175800f, -147.4732100f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19387, new Vector3(-1423.4281000f, -149.4776000f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(2311, new Vector3(-1427.1687000f, -140.1044300f, 17.1991000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(1713, new Vector3(-1428.1350100f, -142.5330000f, 17.1983000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(1713, new Vector3(-1428.6012000f, -140.5771200f, 17.1983000f), new Vector3(0.0000000f, 0.0000000f, 252.0000000f));
            new DynamicObject(19447, new Vector3(-1417.3096900f, -151.4704000f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19447, new Vector3(-1408.1632100f, -154.3943900f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19859, new Vector3(-1434.4792500f, -147.1647000f, 18.3394000f), new Vector3(0.0000000f, 0.0000000f, 252.0000000f));
            new DynamicObject(19859, new Vector3(-1435.4069800f, -150.0129100f, 18.3394000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19447, new Vector3(-1431.9637500f, -185.5748300f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19447, new Vector3(-1422.8073700f, -188.5204200f, 18.9561000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19859, new Vector3(-1431.6983600f, -185.5696700f, 18.3394000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19859, new Vector3(-1434.5491900f, -184.6438900f, 18.3394000f), new Vector3(0.0000000f, 0.0000000f, 342.0000000f));
            new DynamicObject(19929, new Vector3(-1439.1658900f, -165.3840500f, 17.1980000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19929, new Vector3(-1436.4172400f, -166.2619900f, 17.1980000f), new Vector3(0.0000000f, 0.0000000f, 72.0000000f));
            new DynamicObject(19929, new Vector3(-1434.8681600f, -164.8018000f, 17.1980000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19929, new Vector3(-1433.9984100f, -162.1020800f, 17.1980000f), new Vector3(0.0000000f, 0.0000000f, 162.0000000f));
            new DynamicObject(19929, new Vector3(-1435.4765600f, -160.5969100f, 17.1980000f), new Vector3(0.0000000f, 0.0000000f, 252.0000000f));
            new DynamicObject(19929, new Vector3(-1438.2154500f, -159.7072400f, 17.1980000f), new Vector3(0.0000000f, 0.0000000f, 252.0000000f));
            new DynamicObject(19624, new Vector3(-1439.4338400f, -165.3118600f, 18.5261300f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19624, new Vector3(-1434.7999300f, -164.9407000f, 18.2141000f), new Vector3(90.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19624, new Vector3(-1434.0308800f, -160.8705600f, 18.2190500f), new Vector3(270.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(1210, new Vector3(-1434.4232200f, -163.3872400f, 18.2266800f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
        }
    }
}
