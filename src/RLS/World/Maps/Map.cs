﻿using RLS.World.Players;
using System.Collections.Generic;

namespace RLS.World.Maps
{
    public abstract class Map
    {
        public Map()
        {
            Add(this);
        }
        public abstract void CreateMap();
        public virtual void Entrances()
        {

        }
        public virtual void RemoveObjects(Player player)
        {
            
        }

        private static List<Map> maps = new List<Map>();
        public static Map[] All { get => maps.ToArray(); }
        public static void Add(Map map)
        {
            maps.Add(map);
        }
        public static void RemoveObjectsForPlayer(Player player)
        {
            foreach (Map map in All)
                map.RemoveObjects(player);
        }
        public static void CreateMaps()
        {
            foreach (Map map in All)
            {
                map.CreateMap();
                map.Entrances();
            }
        }
    }
}
