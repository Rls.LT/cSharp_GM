﻿using RLS.Streamer;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.World;
namespace RLS.World.Maps.JobMaps
{
    class Taxi : Map
    {
        public override void CreateMap()
        {
            new DynamicObject(19866, new Vector3(-1972.147705, 884.179687, 43.454700), new Vector3(0.000000, 0.000000, 90.000000))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1972.1477100f, 887.6796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1972.1477100f, 880.6796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1972.1477100f, 891.1796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1972.1477100f, 877.1796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1972.1477100f, 894.6796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1972.1477100f, 873.6796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1972.1477100f, 870.1796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1972.1477100f, 898.1796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1987.1477100f, 877.1796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1987.1477100f, 873.6796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1987.1477100f, 870.1796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1987.1477100f, 891.1796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1987.1477100f, 894.6796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(19866, new Vector3(-1987.1477100f, 898.1796900f, 43.4547000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f))
                .SetMaterial(0, 10765, "airportgnd_sfse", "white", 0x00000000);
            new DynamicObject(1569, new Vector3(-1989.451538, 806.698974, 44.426700), new Vector3(0.000000, 0.000000, 90.000000))
                .SetMaterial(0, 16093, "a51_ext", "des_backdoor1", 0x00000000);
            new DynamicObject(9915, new Vector3(-1951.710937, 884.179687, 49.335899), new Vector3(0.000000, 0.000000, 0.000000))
                .SetMaterial(3, 9514, "711_sfw", "ws_carpark2", 0x00000000)
                .SetMaterial(4, 9514, "711_sfw", "ws_carpark2", 0x00000000);

            new DynamicObject(19865, new Vector3(-1968.8814700f, 866.2055100f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1968.8814700f, 871.2055100f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1968.8814700f, 876.2055100f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1968.8814700f, 881.2055100f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1968.8814700f, 886.2055100f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1968.8814700f, 891.2055100f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1968.8814700f, 896.2055100f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1968.8814700f, 901.2055100f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1968.8840300f, 902.1854900f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1971.3950200f, 863.7224700f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
            new DynamicObject(19865, new Vector3(-1976.3950200f, 863.7224700f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
            new DynamicObject(19865, new Vector3(-1981.3950200f, 863.7224700f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
            new DynamicObject(19865, new Vector3(-1986.1550300f, 863.7199700f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
            new DynamicObject(19865, new Vector3(-1971.3950200f, 904.6768800f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
            new DynamicObject(19865, new Vector3(-1976.3950200f, 904.6768800f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
            new DynamicObject(19865, new Vector3(-1981.3950200f, 904.6768800f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
            new DynamicObject(19865, new Vector3(-1986.1550300f, 904.6749900f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
            new DynamicObject(1232, new Vector3(-1985.9062500f, 866.9375000f, 46.7813000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(1232, new Vector3(-1972.9062500f, 866.9375000f, 46.7813000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(1232, new Vector3(-1985.9062500f, 901.7031300f, 46.7813000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(1232, new Vector3(-1972.9062500f, 901.7031300f, 46.7813000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(1232, new Vector3(-1979.4062500f, 866.9375000f, 46.7813000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(1232, new Vector3(-1979.4062500f, 901.7031300f, 46.7813000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1990.4854700f, 869.8894000f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1990.4854700f, 874.8894000f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1990.4840100f, 877.8093900f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1990.4840100f, 890.7493900f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1990.4854700f, 898.4494000f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(19865, new Vector3(-1990.4854700f, 893.4494000f, 44.1950000f), new Vector3(0.0000000f, 0.0000000f, 0.0000000f));
            new DynamicObject(966, new Vector3(-1990.6080300f, 887.5814800f, 44.1571000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
            new DynamicObject(968, new Vector3(-1990.6080300f, 887.5814800f, 44.8771000f), new Vector3(0.0000000f, 0.0000000f, 90.0000000f));
        }

        public override void RemoveObjects(Player player)
        {
            GlobalObject.Remove(player, 1232, new Vector3(-1980.906, 886.937, 46.781), 22.250f);
            GlobalObject.Remove(player, 1375, new Vector3(-1996.164, 888.414, 46.148), 0.250f);
            GlobalObject.Remove(player, 10007, new Vector3(-1951.710, 884.179, 49.335), 0.250f);
            GlobalObject.Remove(player, 9915, new Vector3(-1951.710, 884.179, 49.335), 0.250f);
        }
    }
}
