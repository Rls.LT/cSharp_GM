﻿using SampSharp.GameMode;
using SampSharp.GameMode.World;

namespace RLS.World.Maps.Missions
{
    class Tourist : Map
    {
        public override void CreateMap()
        {
            new GlobalObject(1681, new Vector3(-2825.20923, -689.91467, 1000.25531), new Vector3(0.00000, 0.00000, 270.0));
        }
    }
}
