﻿using System.Collections.Generic;
using SampSharp.GameMode;
using RLS.World.Players;
using SampSharp.GameMode.Display;
using RLS.Definitions.Commands;
using RLS.World.Jobs.Definitions;
using RLS.Definitions;
using RLS.Other.Budget;

namespace RLS.World.Jobs
{
    public partial class Job
    {
        private static List<Job> jobs = new List<Job>();
        public static void Add(Job job) => jobs.Add(job);
        public static Job[] All { get => jobs.ToArray(); }

        protected JobTypes type;
        public JobTypes Id { get => type; }
        protected int requiredExperience;
        public int ExperienceRequired { get => requiredExperience; }
        private Pickup join;
        protected Vector3 joinLocation;
        public Vector3 gpsLocation;
        protected bool withManager = false;
        public bool IsInvitable { get; protected set; } = false;
        public bool HasManager { get => withManager; }
        protected int maxWorkers = Config.Job.MaxWorkers;
        public int MaxWorkers { get => maxWorkers; }
        private JobBudget budget;
        public JobBudget Budget { get => budget; }
        public void BudgetDialog(Player player)
        {
            budget.Menu(player);
        }
        public Job()
        {
            RegisterCommands();
            budget = new JobBudget(this, "Darbo");
            budget.MoneyOperationPerformed += (sender, e) => SaveData();
            JobConfig();
            budget.UpdateName(type.GetName());
            CreateVehicles();
            CreatePickups();
            JobSkins();
        }

        public bool BudgetSpread(Player manager, int money, bool online = false)
        {
            if (money > budget.Money || money < 1) return false;
            int splitInto;
            if (online) splitInto = OnlineWorkersCount();
            else splitInto = WorkersCount();
            if (splitInto == 0) return false;
            budget.Remove(money);
            int moneyToGive = money / splitInto;
            foreach (Worker worker in workers)
                worker.AddMoney(manager, moneyToGive, online);
            if (money > 10000)
                SaveData();
            return true;
        }

        protected virtual void JobConfig()
        {
            LoadData();
        }

        public virtual void CreatePickups()
        {
            join = new Pickup(1314, 2, joinLocation);
            join.PickedUp += OnPlayerPickupWorkPickup;
            join.AddText(new RichText(Colors.JobText, Colors.JobText.Darken(0.2f), $"{type.GetName()}\nNuo: {ExperienceRequired} xp"));
        }

        protected List<Command> commands = new List<Command>();
        public void AddCommand(string command, string description)
        {
            commands.Add(new Command(command, description));
        }
        public void Commands(Player player)
        {
            TablistDialog commandsDialog = new TablistDialog("Darbo komandos", new[] { "Komanda", "Paaiškinimas" }, "Uždaryti");
            foreach (Command command in commands)
            {
                commandsDialog.Add(command.ToDialogRow());
            }
            if (IsInvitable)
                commandsDialog.Add(new Command("vaziuoju", "Priimti iškvietimą").ToDialogRow());
            commandsDialog.Show(player);
        }

        public static Job GetById(JobTypes jobID)
        {
            foreach (Job job in All)
            {
                if (job.Id == jobID)
                    return job;
            }
            return null;
        }
    }
}
