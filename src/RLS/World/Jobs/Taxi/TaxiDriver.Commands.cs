﻿using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.SAMP.Commands;
using RLS.Core.Vehicles;
using SampSharp.GameMode.Display;
using RLS.Definitions;

namespace RLS.World.Jobs.Taxi
{
    partial class TaxiDriver
    {
        protected override void RegisterCommands()
        {
            AddCommand("imti", "Pradėti arba atšaukti pinigų ėmimą iš žadėjo pinigus");
            base.RegisterCommands();
        }
        [Command("imti")]
        public static void TaxiCustomer(Player player)
        {
            if (!CanPlayerUseCommand(player, JobTypes.TAXI))
                return;
            if (!player.InAnyVehicle || player.VehicleSeat != 0 || player.Vehicle == null || player.Vehicle.Job != JobTypes.TAXI)
            {
                player.Error($"Norint panaudoti šią komandą reikia sėdėti $staksi$m transporte, vairuotojo vietoje.");
                return;
            }
            Player[] passengers = player.Vehicle.Passengers;
            if (passengers == null || passengers.Length < 1)
            {
                player.Error($"Keleivių transporte nėra.");
                return;
            }
            TablistDialog dialog = new TablistDialog("Imti pinigus iš žaidėjų", new[] { "Keleivis", "Imami pinigai" }, "Keisti", "Uždaryti");
            foreach(Player passenger in passengers)
            {
                dialog.Add(new[] { passenger.Name, (passenger.TaxiOdoWhenEntered == -1? $"{Colors.Hex.Red}Ne":$"{Colors.Hex.Green}Taip") });
            }
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                    return;
                if (player.Vehicle == null)
                    return;
                if(e.ListItem+1 > player.Vehicle.Passengers.Length)
                {
                    player.Error("Paskutinis keleivis išlipo.");
                    TaxiCustomer(player);
                    return;
                }
                if(passengers[e.ListItem].Vehicle != player.Vehicle)
                {
                    player.Error("Keleivis nebesėdi transporte");
                    TaxiCustomer(player);
                    return;
                }
                if (player.Vehicle.Passengers[e.ListItem].TaxiOdoWhenEntered == -1)
                {
                    player.Vehicle.Passengers[e.ListItem].StateChanged += OtherPlayer_StateChanged;
                    player.Vehicle.Passengers[e.ListItem].TaxiOdoWhenEntered = player.Vehicle.TraveledDistance;
                }
                else
                {
                    player.Vehicle.Passengers[e.ListItem].StateChanged -= OtherPlayer_StateChanged;
                    player.Vehicle.Passengers[e.ListItem].TaxiOdoWhenEntered = -1;
                }
                TaxiCustomer(player);
            };
        }

        public static void OnPlayerExitTaxi(Player player, Vehicle taxi)
        {
            if (taxi.LastDriver == null) return;
            int price = PayForDistance(taxi.TraveledDistance - player.TaxiOdoWhenEntered);
            if (player.Money < price)
            {
                taxi.LastDriver.Error($"$s{player.Name}$m neturėjo pakankamai pinigų susimokėti.");
                player.Error("Tu neturėjai pakankamai pinigų susimokėti už taksi.");
                return;
            }
            player.GiveMoney(-price);
            taxi.LastDriver.GiveMoney(price);
            taxi.LastDriver.SuccessMessage($"$s{player.Name}$m sumokėjo $s{price}$m €.");
            player.SuccessMessage($"Sumokėjai $s{price}$m € už taksi.");
        }

        private static void OtherPlayer_StateChanged(object sender, SampSharp.GameMode.Events.StateEventArgs e)
        {
            if(e.NewState == PlayerState.OnFoot)
            {
                (sender as Player).StateChanged -= OtherPlayer_StateChanged;
                if ((sender as Player).TaxiOdoWhenEntered != -1)
                {
                    OnPlayerExitTaxi((sender as Player), (sender as Player).LastVehicle);
                    (sender as Player).TaxiOdoWhenEntered = -1;
                }
            }
        }
    }
}
