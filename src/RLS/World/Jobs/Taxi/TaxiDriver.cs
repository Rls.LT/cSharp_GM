﻿using SampSharp.GameMode;

namespace RLS.World.Jobs.Taxi
{
    partial class TaxiDriver : Job
    {
        protected override void JobConfig()
        {
            type = JobTypes.TAXI;
            joinLocation = new Vector3(-1985.2814, 825.4117, 44.8451);
            gpsLocation = new Vector3(-1985.2814, 825.4117, 44.8451);
            requiredExperience = 5000;
            withManager = true;
            IsInvitable = true;
            base.JobConfig();
        }

        protected override void JobSkins()
        {
            skins.Add(59);
            base.JobSkins();
        }
        /// <summary>
        /// Count taxi pay check
        /// </summary>
        /// <param name="distance">Distance passenger travelled</param>
        /// <returns></returns>
        public static int PayForDistance(double distance)
        {
            return (int)System.Math.Round(distance * Config.Job.Taxi.PricePerKm);
        }
    }
}
