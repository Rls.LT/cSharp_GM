﻿using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using RLS.Core.Vehicles;

namespace RLS.World.Jobs.Taxi
{
    partial class TaxiDriver
    {
        public override void CreateVehicles()
        {
            vehicles.Add(new Vehicle(VehicleModelType.Taxi, new Vector3(-1972.0544, 878.8651, 44.9860), 91.5725f, 1, 234).ForJob(type));


        }
    }
}
