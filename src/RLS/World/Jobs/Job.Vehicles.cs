﻿using System.Collections.Generic;
using RLS.Core.Vehicles;
namespace RLS.World.Jobs
{
    public partial class Job
    {
        protected List<Vehicle> vehicles = new List<Vehicle>();
        public int GetVehicleCount() => vehicles.Count;
        public virtual void CreateVehicles()
        { }
    }
}
