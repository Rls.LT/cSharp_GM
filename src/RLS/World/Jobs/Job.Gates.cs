﻿using System.Collections.Generic;

namespace RLS.World.Jobs
{
    partial class Job
    {
        private List<Gates> gates = new List<Gates>();
        public Gates[] AllGates { get => gates.ToArray(); }
        public void AddGates(Gates gate)
        {
            gate.Opening += (sender, e) => 
            {
                if (e.player.Job.Type != type)
                    e.Cancel = true;
            };
            gate.Closing += (sender, e) =>
            {
                if (e.player.Job.Type != type)
                    e.Cancel = true;
            };
            gates.Add(gate);
        }
        public void RemoveGates(Gates gate)
        {
            gates.Remove(gate);
        }
    }
}
