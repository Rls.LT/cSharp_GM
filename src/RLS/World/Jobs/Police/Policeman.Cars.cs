﻿using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using RLS.Core.Vehicles;

namespace RLS.World.Jobs.Police
{
    partial class Policeman
    {
        public override void CreateVehicles()
        {
            vehicles.Add(new Vehicle(VehicleModelType.PoliceCarSFPD, new Vector3(-1576.6765, 673.1928, 6.9597), 0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.PoliceCarSFPD, new Vector3(-1588.3776, 673.1928, 6.9558), 0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.PoliceCarSFPD, new Vector3(-1600.1293, 673.1928, 6.9552), 0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.PoliceCarSFPD, new Vector3(-1612.2096, 673.1928, 6.9566), 0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.PoliceMaverick, new Vector3(-1681.7094, 705.8781, 30.7782), 90.0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.PoliceRanger, new Vector3(-1582.3710, 673.1928, 7.3857), 180.0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.PoliceRanger, new Vector3(-1594.3147, 673.1928, 7.3842), 180.0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.PoliceRanger, new Vector3(-1606.1887, 673.1928, 7.3802), 180.0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.HPV1000, new Vector3(-1616.7733, 672.3620, 6.7597), 90.0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.HPV1000, new Vector3(-1616.7733, 674.5485, 6.7506), 90.0f, 1, 234).ForJob(type));
            vehicles.Add(new Vehicle(VehicleModelType.HPV1000, new Vector3(-1616.7733, 676.6236, 6.7576), 90.0f, 1, 234).ForJob(type));

        }
    }
}
