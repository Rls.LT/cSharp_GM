﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.World.Jobs.Police
{
    partial class Policeman
    {
        protected override void RegisterCommands()
        {
            AddCommand("ant", "Surakinti žaidėją");
            AddCommand("ita", "Uždėti žaidėjui įtarimą");
            AddCommand("ist", "Išteisinti žaidėją");
            AddCommand("bauda", "Pasiūlyti mokėti baudą");
            AddCommand("ieskomi", "Žaidėjai su ieškomumo lygiu");
            AddCommand("tikrinti", "Patikrinti žaidėją");
            AddCommand("kliutis", "Padėti kliūtį prieš save");
            base.RegisterCommands();
        }

        [Command("kliutis")]
        public static void OfficerBarrier(Player player)
        {
            if (!CanPlayerUseCommand(player, JobTypes.POLICE))
                return;
            Barrier barrier = HasBarrier(player);
            if (barrier != null)
            {
                player.Error("Kliūtis sunaikinta.");
                barrier.Dispose();
            }
            else
                CreateBarrier(player);
        }

        [Command("tikrinti")]
        public static void SearchPlayer(Player player, Player otherPlayer = null)
        {
            if (!CanPlayerUseCommand(player, JobTypes.POLICE))
                return;
            if (otherPlayer == null)
            {
                player.Command("/tikrinti [žaidėjas]", "Patikrinti žaidėją");
                return;
            }
            if (!player.IsInRangeOfPoint(3f, otherPlayer.Position))
            {
                player.Error($"Žaidėjas per toli.");
                return;
            }
            Timer timer = new Timer(1000, true);
            int timeLeft = 5;
            timer.Tick += (sender, e) =>
            {
                if (!player.IsInRangeOfPoint(3f, otherPlayer.Position))
                {
                    player.Error($"Žaidėjas per toli.");
                    player.HideUILineNamed("action");
                    timer.IsRepeating = false;
                    otherPlayer.HideUILineNamed("action");
                    return;
                }
                if(--timeLeft<=0)
                {
                    CheckPlayer(otherPlayer, player);
                    timer.IsRepeating = false;
                }
                else
                {
                    player.ShowUILineNamed("action", Colors.Blue, "hud:radar_police", "Tikrinamas..", "" + timeLeft, 1100);
                    otherPlayer.ShowUILineNamed("action", Colors.Blue, "hud:radar_police", "Tave tikrina", "" + timeLeft, 1100);
                }
            };
            timer.OnTick(new System.EventArgs());

        }
        private static void CheckPlayer(Player player, Player cop)
        {
            if (!cop.IsInRangeOfPoint(3f, player.Position))
            {
                cop.Error($"Žaidėjas per toli.");
                return;
            }
            if (player.WantedLevel > 0)
                cop.Error($"Žaidėjas ieškomas $s{player.WantedLevel}$m lygiu.");
        }
        [Command("ieskomi")]
        public static void RemoveWantedLevel(Player player)
        {
            if (!CanPlayerUseCommand(player, JobTypes.POLICE))
                return;
            TablistDialog dialog = new TablistDialog($"Ieškomi žaidėjai", new[] { "Žaidėjas", "Darbas", "Lygis" }, "Uždaryti");
            foreach(Player pl in Player.All)
            {
                if (pl.WantedLevel > 200)
                    dialog.Add(new[] { Colors.Hex.Red + pl.Name, Colors.Hex.Red + pl.Job.Type.GetWorkerName(), Colors.Hex.Red + pl.WantedLevel + " lygiu" });
                else if (pl.WantedLevel > 100)
                    dialog.Add(new[] { Colors.Hex.Orange + pl.Name, Colors.Hex.Orange + pl.Job.Type.GetWorkerName(), Colors.Hex.Orange + pl.WantedLevel + " lygiu" });
                else if (pl.WantedLevel > 40)
                    dialog.Add(new[] { Colors.Hex.Yellow + pl.Name, Colors.Hex.Yellow + pl.Job.Type.GetWorkerName(), Colors.Hex.Yellow + pl.WantedLevel + " lygiu" });
                else
                    dialog.Add(new[] { Colors.Hex.Green + pl.Name, Colors.Hex.Green + pl.Job.Type.GetWorkerName(), Colors.Hex.Green + pl.WantedLevel + " lygiu" });
            }
            dialog.Show(player);
        }
        [Command("bauda")]
        public static void Fine(Players.Player player, Players.Player otherPlayer = null)
        {
            if (!CanPlayerUseCommand(player, JobTypes.POLICE))
                return;
            if (otherPlayer == null)
            {
                player.Command("/bauda [žaidėjas]", "Pasiūlyti mokėti baudą");
                return;
            }
            if (!player.IsInRangeOfPoint(3f, otherPlayer.Position))
            {
                player.Error($"Žaidėjas per toli.");
                return;
            }
            int fine = otherPlayer.WantedLevel * Config.MoneyPerStar * ((otherPlayer.WantedLevel / 10) > 0 ? (otherPlayer.WantedLevel / 10) : 1);
            Job.Ration(player, $"Pasiūliau $s{otherPlayer.Name}$m susimokėti baudą.");
            MessageDialog dialog = new MessageDialog("Bauda", $"Pareigūnas {player.Name} siūlo tau susimokėti {fine} € baudą. Ar nori tai padaryti?", "Sumokėti", "Atsisakyti");
            dialog.Show(otherPlayer);
            dialog.Response += (sender, e) =>
            {
                if(e.DialogButton == DialogButton.Right)
                {
                    player.Error("Žaidėjas atsisakė mokėti baudą.");
                    otherPlayer.Error("Atsisakei mokėti baudą.");
                    return;
                }
                if(fine > otherPlayer.Money)
                {
                    player.Error("Žaidėjui nepakako pinigų susimokėti baudą.");
                    otherPlayer.Error("Tau nepakanka pinigų kišenėje, kad susimokėtum baudą.");
                    return;
                }
                otherPlayer.GiveMoney(-fine, "Bauda");
                player.GiveMoney((int)System.Math.Round(fine * 0.2), "Bauda");
                otherPlayer.WantedLevel = 0;
                Job.Ration(player, $"$s{otherPlayer.Name}$m susimokėjo baudą.");
                Job.GetById(player.Job.Type)?.Budget.Add((int)System.Math.Round(fine * 0.8));
                otherPlayer.SuccessMessage($"Susimokėjai $s{fine}$m € baudą ir buvai išteisintas.");
            };
        }
        [Command("ist")]
        public static void RemoveWantedLevel(Players.Player player, Players.Player otherPlayer = null)
        {
            if (!CanPlayerUseCommand(player, JobTypes.POLICE))
                return;
            if (otherPlayer == null)
            {
                player.Command("/ist [žaidėjas]", "Išteisinti žaidėją");
                return;
            }
            if (!player.IsInRangeOfPoint(3f, otherPlayer.Position))
            {
                player.Error($"Žaidėjas per toli.");
                return;
            }
            otherPlayer.SuccessMessage($"Pareigūnas $s{player.Name}$m tave išteisino.");
            Job.Ration(player, $"Išteisinau $s{otherPlayer.Name}$m.");
            otherPlayer.WantedLevel = 0;
        }
        [Command("ita")]
        public static void WantedLevel(Players.Player player, Players.Player otherPlayer = null)
        {
            if (!CanPlayerUseCommand(player, JobTypes.POLICE))
                return;
            if (otherPlayer == null)
            {
                player.Command("/ita [žaidėjas]", "Uždėti žaidėjui įtarimą");
                return;
            }
            if (!player.IsInRangeOfPoint(3f, otherPlayer.Position))
            {
                player.Error($"Žaidėjas per toli.");
                return;
            }
            TablistDialog dialog = new TablistDialog($"Įtarti {Colors.Hex.Blue}{otherPlayer.Name}", new[] { "Priežastis", "Lygis" }, "Įtarti", "Atšaukti");
            AddWantedReasons(dialog);
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                    return;
                if (e.ListItem < 0 || e.ListItem >= AllWantedReasons.Length)
                    return;
                if(otherPlayer.WantedLevel == Config.MaxWantedLevel)
                {
                    player.Error($"Žaidėjas jau turi maksimalų ieškomumo lygį[{Config.MaxWantedLevel}].");
                    return;
                }
                otherPlayer.Error($"Pareigūnas $s{player.Name}$m uždėjo tau ieškomumą už: $s{AllWantedReasons[e.ListItem].Item1}$m.");
                Job.Ration(player, $"Uždėjo ieškomumo lygį $s{otherPlayer.Name}$m už: $s{AllWantedReasons[e.ListItem].Item1}$m.");
                otherPlayer.WantedLevel += AllWantedReasons[e.ListItem].Item2;
            };
        }

        [Command("ant")]
        public static void CuffHands(Players.Player player, Players.Player otherPlayer = null)
        {
            if (!CanPlayerUseCommand(player, JobTypes.POLICE))
                return;
            if(otherPlayer == null)
            {
                player.Command("/ant [žaidėjas]", "Surakinti žaidėją");
                return;
            }
            if (!player.IsInRangeOfPoint(3f, otherPlayer.Position))
            {
                player.Error($"Žaidėjas per toli.");
                return;
            }
            if (otherPlayer.IsWithHandcuff)
            {
                otherPlayer.Handcuff(false);
                otherPlayer.Attachments.Remove(19418);
                player.SendClientMessage(Colors.Green, $"Atrakinai {otherPlayer.Name}.");
                otherPlayer.SendClientMessage(Colors.Green, $"Tave atrakino {player.Name}.");
                Job.ActionMessage(player, $"Atrakinau {otherPlayer.Name}.");
            }
            else
            {
                otherPlayer.Handcuff(true);
                otherPlayer.Attachments.Add(19418, Bone.LeftHand)
                    .SetOffset(new Vector3(0.0109999, 0.04799998, -0.004000009))
                    .SetRotation(new Vector3(9.000013, -18.8, -76.69991))
                    .SetScale(new Vector3(1.009998, 0.9479995, 1.859997))
                    .Attach();
                player.SendClientMessage(Colors.Orange, $"Surakinai {otherPlayer.Name}.");
                otherPlayer.SendClientMessage(Colors.Orange, $"Tave surakino {player.Name}.");
                Job.ActionMessage(player, $"Surakinau {otherPlayer.Name}.");
            }
        }
    }
}
