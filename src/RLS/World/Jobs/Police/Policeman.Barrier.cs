﻿using RLS.Definitions;
using RLS.Streamer;
using RLS.World.Players;
using SampSharp.GameMode;
using SampSharp.GameMode.Events;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.World;
using System;
using System.Collections.Generic;

namespace RLS.World.Jobs.Police
{
    partial class Policeman
    {
        public class Barrier
        {
            private Player owner;
            public Player Owner{ get => owner; }
            private DynamicObject barrier;
            private TextLabel text;
            public Barrier(Player player)
            {
                owner = player;
                barrier=new DynamicObject(978, Functions.PointFarFromPoint(3, player.Angle + 90) + player.Position - new Vector3(0, 0, 0.2), new Vector3(0, 0, player.Angle));
                Color color = Colors.White.Darken(0.2f);
                color.A = 155;
                text = new TextLabel($"* {player.Name}: Kelias užtvertas", color, Functions.PointFarFromPoint(3.5, player.Angle + 90) + player.Position, 5f);
                owner.Disconnected += OnDisconnect;
            }
            public void Dispose()
            {
                barrier.Dispose();
                text.Dispose();
                barriers.Remove(this);
                owner.Disconnected -= OnDisconnect;
            }
            public void OnDisconnect(object sender, DisconnectEventArgs args)
            {
                Dispose();
            }

        }
        private static List<Barrier> barriers = new List<Barrier>();
        public static Barrier[] AllBarriers { get => barriers.ToArray(); }
        public static Barrier HasBarrier(Player player)
        {
            foreach(Barrier barrier in AllBarriers)
            {
                if (barrier.Owner.SqlId == player.SqlId)
                    return barrier;
            }
            return null;
        }
        public static void CreateBarrier(Player player)
        {
            HasBarrier(player)?.Dispose();
            barriers.Add(new Barrier(player));
        }
    }
}
