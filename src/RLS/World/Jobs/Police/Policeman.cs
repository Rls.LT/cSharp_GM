﻿using RLS.World.Entrances;
using SampSharp.GameMode;
using SampSharp.GameMode.Display;
using System;
using System.Collections.Generic;

namespace RLS.World.Jobs.Police
{
    partial class Policeman : Job
    {
        static Policeman()
        {
            wantedLevels.Add(Tuple.Create("Test", 1));
            wantedLevels.Add(Tuple.Create("Test 50", 50));
            wantedLevels.Add(Tuple.Create("Test 100", 100));
        }
        public static Tuple<string, int>[] AllWantedReasons { get => wantedLevels.ToArray(); }
        private static List<Tuple<string, int>> wantedLevels = new List<Tuple<string, int>>();
        public static TablistDialog AddWantedReasons(TablistDialog dialog)
        {
            foreach(Tuple<string, int> item in wantedLevels)
            {
                dialog.Add(new[] { ""+item.Item1, ""+item.Item2 });
            }
            return dialog;
        }
        protected override void JobConfig()
        {
            type = JobTypes.POLICE;
            joinLocation = new Vector3(255.2098, 67.2456, 1003.6406);
            gpsLocation = new Vector3(-1605.7096, 711.4132, 13.8672);
            requiredExperience = 10000;
            withManager = true;
            AddGates(new Gates(968, new Vector3(-1572.2004, 658.6830, 6.8981), new Vector3(0.0000, 90.0000, 90.0000))
                .Opened(new Vector3(-1572.2004, 658.6830, 6.8981), new Vector3(0.0000, 0.0000, 90.0000))
                .SetSpeed(0.005f));
            new Entrance(new Vector3(-1605.7096, 711.4132, 13.3672), new Vector3(246.6756, 62.6606, 1003.1406))
                .SetInterior(6).SetAngles(0, 0).CreateVisual("Policijos departamentas");
            base.JobConfig();
        }
        protected override void JobSkins()
        {
            skins.Add(280);
            skins.Add(281);
            base.JobSkins();
        }
    }
}
