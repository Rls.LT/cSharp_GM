﻿using System.Collections.Generic;

namespace RLS.World.Jobs
{
    public partial class Job
    {
        protected List<int> skins = new List<int>();
        public int[] AllSkins { get => skins.ToArray(); }
        protected virtual void JobSkins() { }
    }
}
