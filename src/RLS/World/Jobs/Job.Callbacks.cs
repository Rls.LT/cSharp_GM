﻿using RLS.World.Players;

namespace RLS.World.Jobs
{
    public partial class Job
    {
        protected virtual void OnPlayerPickupWorkPickup(object sender, SampSharp.GameMode.Events.PlayerEventArgs e)
        {
            Player player = e.Player as Player;
            Join(player);
        }
    }
}
