﻿using RLS.Definitions;
using RLS.Events.Args;
using RLS.World.Jobs.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.Events;
using System.Collections.Generic;

namespace RLS.World.Jobs
{
    partial class Job
    {
        /// <summary>Service requests list</summary>
        private List<Request> requests = new List<Request>();
        /// <summary>
        /// Add player invite to request list
        /// </summary>
        /// <param name="player"></param>
        public void AddRequest(Player player)
        {
            player.Disconnected += OnRequesterDisconnects;
            requests.Add(new Request(player));
            Ration($"$s{player.Name} [{player.Id}]$m kviečia jūsų tarnybą. Norėdamas jį priimti rašyk $s/vaziuoju$m.");
        }
        /// <summary>
        /// Check if player already requested service
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public bool HasRequest(Player player) => requests.Contains(new Request(player));
        /// <summary>
        /// Remove request from list
        /// </summary>
        /// <param name="player"></param>
        public void RemoveRequest(Player player, bool successfull = false)
        {
            OnRequestRemoved(GetPlayerRequest(player));
            if(!successfull) Ration($"$s{player.Name}$m atšaukė savo kvietimą.");
        }
        private void OnRequestRemoved(Request request)
        {
            if (request == null) return;
            requests.Remove(request);
            request.RequestingPlayer.Disconnected -= OnRequesterDisconnects;
            if (request.Responser != null)
            {
                request.Responser.SetPlayerMarker(request.RequestingPlayer, request.RequestingPlayer.DefaultColor);
                request.Responser.Updated -= OnResponserUpdates;
            }
        }

        public Request GetPlayerRequest(Player player)
        {
            foreach(Request request in requests.ToArray())
            {
                if (request.RequestingPlayer == player)
                    return request;
            }
            return null;
        }

        private void OnRequesterDisconnects(object sender, DisconnectEventArgs e)
        {
            Player player = sender as Player;
            OnRequestRemoved(GetPlayerRequest(player));
            Ration($"$s{player.Name}$m atsijungė.");
        }
        ///responser
        private void OnResponserDisconnects(object sender, DisconnectEventArgs e)
        {
            Player player = sender as Player;
            Request request = GetRequestByResponser(player);
            if (request != null)
            {
                request.RequestingPlayer?.Error("Tarnybos darbuotojas atsijungė. Išsikviesk tarnybą iš naujo.");
            }
            OnRequestRemoved(request);
            player.Disconnected -= OnResponserDisconnects;
        }
        /// <summary>
        /// On responsed player updates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnResponserUpdates(object sender, PlayerArgs e)
        {
            Request request = GetRequestByResponser(e.player);
            if (request != null)
            {
                if (request.RequestingPlayer == null) return;
                e.player.SetPlayerMarker(request.RequestingPlayer, Colors.Yellow);
                if(e.player.IsInRangeOfPoint(5f, request.RequestingPlayer.Position))
                {
                    Ration(e.player, $"Atvykau į iškvietimą pas $s{request.RequestingPlayer.Name}$m.");
                    request.RequestingPlayer.SuccessMessage($"Kviestos tarnybos darbuotojas $s{e.player.Name}$m atvyko.");
                    RemoveRequest(request.RequestingPlayer, true);
                    e.player.Disconnected -= OnResponserDisconnects;
                }
            }
        }
        /// <summary>
        /// Service worker responses to request
        /// </summary>
        /// <param name="request"></param>
        /// <param name="responser"></param>
        public void Response(Request request, Player responser)
        {
            request.Responser = responser;
            request.Responser.Disconnected += OnResponserDisconnects;
            request.Responser.Updated += OnResponserUpdates;
        }
        /// <summary>
        /// Get Request by responser
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public Request GetRequestByResponser(Player player)
        {
            foreach (Request request in requests.ToArray())
            {
                if (request.Responser == player)
                    return request;
            }
            return null;
        }
    }
}
