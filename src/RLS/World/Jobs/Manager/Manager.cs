﻿using RLS.World.Players;

namespace RLS.World.Jobs
{
    public partial class Manager
    {
        private Job job = null;
        public Job Job { get => job; }
        public JobTypes JobId { get => (job == null ? JobTypes.NONE : job.Id);}
        public Manager(JobTypes jobId)
        {
            Add(jobId);
        }
        public void Remove()
        {
            job = null;
        }
        public bool IsValid() => (job == null ? false : true);
        public void Add(JobTypes jobId)
        {
            Job job = Job.GetById(jobId);
            if (job == null) return;
            this.job = job;
        }
    }
}
