﻿using RLS.Commands;
using RLS.Definitions;
using RLS.Definitions.Commands;
using RLS.World.Jobs.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP;
using SampSharp.GameMode.SAMP.Commands;
using System;

namespace RLS.World.Jobs
{
    partial class Manager
    {
        #region ManagerControls
        [Command("direktorius", UsageMessage = "Direktoriaus paskyrimas")]
        public static void ManagersControl(Player player, string command = null, Player otherPlayer = null, int type = 0)
        {
            if (!Commands.Messages.CanAccess(player, player.IsDeveloper))
                return;
            if (command == null || otherPlayer == null)
            {
                player.SendClientMessage(Colors.CommandText, $"* Direktoriaus uždėjimas/nuėmimas: " + "{59C4C5} /direktorius [nuimti/uzdeti] [žaidėjas]");
                return;
            }
            if (command.Equals("uzdeti"))
                SetManager(player, otherPlayer, type);
            else if (command.Equals("nuimti"))
                RemoveManager(player, otherPlayer);
        }
        public static void SetManager(Player player, Player otherPlayer, int type)
        {
            if (otherPlayer.Manager.JobId != JobTypes.NONE)
            {
                player.Error($"@[{otherPlayer.Name}@] šiuo metu užima kito direktoriaus pareigas. Pirmiausia nuimk direktorių.");
                return;
            }
            if(type < 1 || type > Enum.GetValues(typeof(JobTypes)).Length || Job.GetById((JobTypes)type) != null && !Job.GetById((JobTypes)type).HasManager)
            {
                player.Error($"Blogas direktoriaus tipas.");
                return;
            }
            otherPlayer.Manager.Add((JobTypes)type);
            otherPlayer.ForcedDataSave();
            RichText textFormat = new RichText(Colors.JobText, $"Tave paskyrė @[\"{otherPlayer.Manager.JobId.GetName()}\"@] direktoriumi(-e).");
            otherPlayer.SendClientMessage(Colors.JobText, textFormat);
            player.SendClientMessage(Colors.JobText, textFormat.SetText($"Paskyrei @[{otherPlayer.Name}@] @[\"{otherPlayer.Manager.JobId.GetName()}\"@] direktoriumi(-e)."));
        }
        public static void RemoveManager(Player player, World.Players.Player otherPlayer)
        {
            if (otherPlayer.Manager.JobId == JobTypes.NONE)
            {
                player.Error($"@[{otherPlayer.Name}@] neturi direktoriaus privilegijų.");
                return;
            }
            otherPlayer.Manager.Remove();
            otherPlayer.ForcedDataSave();
            RichText textFormat = new RichText(Colors.JobText, "Tave nušalino nuo direktoriaus(-ės) pareigų.");
            otherPlayer.SendClientMessage(Colors.JobText, textFormat);
            player.SendClientMessage(Colors.JobText, textFormat.SetText($"Nušalintai @[{otherPlayer.Name}@] nuo direktoriaus(-ės) pareigų."));
        }
        #endregion
        [Command("pakviesti", UsageMessage = "Pakviesti žaidėją į darbą")]
        public static void InvitePlayerToJobCommand(Player player, Player otherPlayer)
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            Job job = player.Manager.Job;
            if (!Manager.IsInvitable(player, otherPlayer, job))
                return;
            job.CreateInvite(player.Name, otherPlayer.Name);
            Job.Message(otherPlayer, $"Pakvietimas žaidėjui @[{otherPlayer.Name}@] išsiųstas. Jei nori jį atšaukti: @[/pakvietimai@]");
            Job.Message(otherPlayer, $"@[{player.Manager.JobId.GetName()}@] kviečia tave į darbą. Norėdamas(-a) atšaukti jį rašyk: @[/anuliuoti pakvietima@].");

        }
        [Command("pakvietimai")]
        public static void InvitesToJobCommand(Player player)
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            Job job = player.Manager.Job;
            if (job == null)
                return;
            if (job.AllInvites.Length == 0)
            {
                player.Error("Pakviestų žaidėjų į darbą nėra.");
                return;
            }
            TablistDialog invitesDialog = new TablistDialog("Pakviesti žaidėjai į darbą", new[] { "Žaidėjas", "Pakvietė", "Pakviestas" }, "Ištrinti", "Uždaryti");
            foreach (Invite invite in job.AllInvites)
            {
                invitesDialog.Add(new[] { invite.InvitedPlayer, invite.Inviter, invite.DateString });
            }
            invitesDialog.Response += (sender, e) => {
                if (e.DialogButton == DialogButton.Right)
                    return;
                if (job.GetInviteById(e.ListItem) == null)
                    return;
                Player invitedPlayer = Player.GetByFullName(job.GetInviteById(e.ListItem).InvitedPlayer);
                job.DestroyInvite(job.GetInviteById(e.ListItem));
                if (invitedPlayer != null)
                    Job.Message(invitedPlayer, $"@[{player.Manager.JobId.GetName()}@] atšaukė kvietimą į darbą.");
                Job.Message(player, $"Pakvietimas žaidėjui @[{invitedPlayer.Name}@] atšauktas.");
                InvitesToJobCommand(e.Player as Player);
            };
            invitesDialog.Show(player);
        }
        [Command("darbuotojai")]
        public static void Manager_WorkersList(Player player)
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            Job job = player.Manager.Job;
            if (job == null) return;
            if (job.AllWorkers.Length == 0)
            {
                player.Error("Tavo firmoje nėra darbuotojų.");
                return;
            }
            TablistDialog workersDialog = new TablistDialog("Firmos darbuotojai",
                new[] {
                    "Vardas",
                    "Įspėjimai",
                    "Prisijungęs"
                }, "Valdyti", "Uždaryti");
            foreach (Worker worker in job.AllWorkers)
            {
                workersDialog.Add(new string[] {
                    worker.Name,
                    $"{worker.Warnings}/{Config.Job.WarningsToKick}",
                    (Player.GetByFullName(worker.Name)==null?Colors.GetHex(Color.Red)+"Atsijungęs":Colors.GetHex(Color.Green)+"Prisijungęs")
                });
            }
            workersDialog.Show(player);
            workersDialog.Response += (sender, e) => {
                if (!Messages.CanAccess(player, player.IsManager))
                    return;
                if (e.DialogButton == DialogButton.Right)
                    return;
                if (job.WorkersCount() <= e.ListItem)
                {
                    player.Error("Įvyko klaida, galimai darbuotojų sąrašas pasikeitė.");
                    return;
                }
                Worker worker = job.AllWorkers[e.ListItem];
                WorkersDialogResponse(player, worker);
            };
        }
        private static void WorkersDialogResponse(Player player, Worker worker)
        {
            ListDialog listDialog = new ListDialog($"{worker.Name} valdymas", "Vykdyti", "Grįžti");
            listDialog.AddItem("Išmesti iš darbo");
            listDialog.AddItem($"Įspėti\t\t\t\t\t\t[{worker.Warnings}/{Config.Job.WarningsToKick}]");
            listDialog.AddItem($"Nuimti įspėjimą");
            listDialog.Show(player);
            listDialog.Response += (_sender, arg) =>
            {
                if (!Messages.CanAccess(player, player.IsManager))
                    return;
                if (arg.DialogButton == DialogButton.Right)
                {
                    Manager_WorkersList(player);
                    return;
                }
                if (arg.ListItem == 0)//kick
                    KickFromJobDialog(player, worker);
                else if (arg.ListItem == 1)//warn
                    WarnWorkerDialog(player, worker);
                else if (arg.ListItem == 2)//unwarn
                {
                    if (worker.Warnings < 1)
                    {
                        player.Error("Žaidėjas neturi įspėjimų.");
                        WorkersDialogResponse(player, worker);
                        return;
                    }
                    RemoveWarningWorkerDialog(player, worker);
                }
            };
        }

        private static void RemoveWarningWorkerDialog(Player player, Worker worker)
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            MessageDialog dialog = new MessageDialog("Išmetimas iš darbo", $"Ar tikrai nori nuimti įspėjimą {worker.Name}?", "Taip", "Ne");
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Left)
                    worker.Unwarn(player);
                WorkersDialogResponse(player, worker);
            };
            dialog.Show(player);
        }

        private static void WarnWorkerDialog(Player player, Worker worker)
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            InputDialog inputDialog = new InputDialog("Įspėjimas", $"{worker.Name} įspėjimas\nĮvesk priežastį, dėl kurios dedi įspėjimą:", false, "Įspėti", "Grįžti");
            inputDialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    WorkersDialogResponse(player, worker);
                    return;
                }
                if (e.InputText.Length < 4)
                {
                    player.Error("Priežastis per trumpa. Minimalus priežasties ilgis 4 simboliai.");
                    WorkersDialogResponse(player, worker);
                    return;
                }
                if (e.InputText.Length > 64)
                {
                    player.Error("Priežastis per ilga. Maksimalus priežasties ilgis 64 simboliai.");
                    WorkersDialogResponse(player, worker);
                    return;
                }
                worker.Warn(player, e.InputText);
                Manager_WorkersList(player);
            };
            inputDialog.Show(player);
        }

        private static void KickFromJobDialog(Player player, Worker worker)
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            MessageDialog dialog = new MessageDialog("Išmetimas iš darbo", $"Ar tikrai nori išmesti {worker.Name} iš darbo?", "Taip", "Ne");
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    WorkersDialogResponse(player, worker);
                    return;
                }
                worker.Kick(player);
                Manager_WorkersList(player);
            };
            dialog.Show(player);
        }
    }
}
