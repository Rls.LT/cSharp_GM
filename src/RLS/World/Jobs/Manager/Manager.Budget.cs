﻿using RLS.Commands;
using RLS.World.Players;
using SampSharp.GameMode.SAMP.Commands;
namespace RLS.World.Jobs
{
    public partial class Manager
    {
        [Command("dfondas")]
        private static void BudgetDialog(Player player)
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            player.Manager.Job.BudgetDialog(player);
        }
    }
}
