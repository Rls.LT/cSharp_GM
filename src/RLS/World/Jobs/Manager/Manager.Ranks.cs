﻿using RLS.Commands;
using RLS.Definitions;
using RLS.World.Jobs.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP.Commands;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace RLS.World.Jobs
{
    partial class Manager
    {
        [Command("rangai")]
        public static void RanksList(Player player)
        {
            if (!Messages.CanAccess(player, player.IsManager))
                return;
            Job job = player.Manager.Job;
            TablistDialog ranksDialog = new TablistDialog("Rangų valdymas",
                new[] {
                    "#",
                    "Rangas",
                    "Aprangų skaičius"
                }, "Pasirinkti", "Uždaryti");
            foreach (JobRank rank in job.AllRanks)
            {
                ranksDialog.Add(new string[] {
                    ""+(job.GetRankId(rank)+1),
                    rank.Name,
                    (rank.AllSkins.Length == 0?$"{Colors.Red}dar nenustatyta":""+rank.AllSkins.Length)
                });
            }
            ranksDialog.Add(new string[] { " ", " ", " " });
            ranksDialog.Add(new string[] {
                    " ",$"{Colors.Green}Pridėti naują"," "
                });
            ranksDialog.Show(player);

            ranksDialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                    return;
                if (e.ListItem > job.AllRanks.Length)//create new
                {
                    if (job.AllRanks.Length >= Config.Job.MaxJobRanks)
                    {
                        player.Error($"Pasiektas maksimalus rangų skaičius[Max {Config.Job.MaxJobRanks}].");
                        RanksList(player);
                        return;
                    }
                    job.AddNewRank(new JobRank("Pavadinimas nenustatytas"));
                    RanksList(player);
                }
                else if (e.ListItem > job.AllRanks.Length || e.ListItem < 0 || e.ListItem == job.AllRanks.Length)
                {
                    RanksList(player);
                    return;
                }
                else
                {
                    EditRankMenu(player, job.GetRankInPos(e.ListItem));
                }

            };
        }
        public static void EditRankMenu(Player player, JobRank rank)
        {
            ListDialog editDialog = new ListDialog($"Rangai -> {Colors.Hex.Lime}{rank.Name}{Colors.Hex.Dialog} redagavimas", "Pasirinkti", "Grįžti");
            editDialog.AddItem("Pakeisti pavadinimą");
            editDialog.AddItem($"Keisti aprangą\t\t{(rank.AllSkins.Length == 0 ? $"{Colors.Red}dar nenustatytas" : rank.AllSkins.Length+" apranga(-os)")}");
            editDialog.AddItem("Ištrinti rangą");
            editDialog.Show(player);
            editDialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                {
                    RanksList(player);
                    return;
                }
                if (e.ListItem == 0)//change name
                {
                    ChangeRankName(player, rank);
                }
                else if (e.ListItem == 1)//change skins rank
                {
                    ChangeRankSkin(player, rank);
                }
                else if (e.ListItem == 2)//delete rank
                {
                    DeleteRank(player, rank);
                }
            };
        }
        public static void ChangeRankName(Player player, JobRank rank)
        {
            RichText richText = new RichText(Colors.White, Colors.Yellow);
            richText.SetText($"$mDabartinis rango pavadinimas: $y{rank.Name}$m\n\n" +
                "Reikalavimai naujam pavadinimui:\n" +
                "$s*$m Pavadinimas turi būti bent $s4$m simbolių ilgio;\n" +
                $"$s*$m Pavadinimas turi būti ne ilgesnis nei $s{Config.Job.MaxJobRankNameLength}$m simbolių;\n" +
                "$s*$m Pavadinimas gali būti sudarytas tik iš raidžių;\n" +
                "$s*$m Pavadinime negali būti necenzūrinęs leksikos;\n\n" +
                "Įvesk naują rango pavadinimą:");
            InputDialog dialog = new InputDialog($"Rangai -> {Colors.Hex.Lime}Pavadinimo keitimas", richText.ToString(), false, "Keisti", "Atšaukti");
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                System.Console.WriteLine($"{e.InputText}");
                if (e.DialogButton == DialogButton.Right)
                {
                    EditRankMenu(player, rank);
                    return;
                }
                if (e.InputText.Length < 4)
                {
                    player.Error("Rango pavadinimas turi būti bent 4 simbolių.");
                    ChangeRankName(player, rank);
                    return;
                }
                if (e.InputText.Length > Config.Job.MaxJobRankNameLength)
                {
                    player.Error($"Maksimalus rango pavadinimas {Config.Job.MaxJobRankNameLength}. Tavo įvestas viršyja limitą.");
                    ChangeRankName(player, rank);
                    return;
                }
                Regex rgx = new Regex("^[a-zA-Z ĄČĘĖĮŠŲŪąčęėįšųū]+$");
                if (!rgx.IsMatch(e.InputText))
                {
                    player.Error("Rango pavadinime negali būti simbolių, leistinos tik raidės.");
                    ChangeRankName(player, rank);
                    return;
                }
                Job.Message(player, $"Rango pavadinimas pakeistas iš $s{rank.Name}$m į $s{e.InputText}$m.");
                rank.Name = e.InputText;
                EditRankMenu(player, rank);
            };
        }
        public static void ChangeRankSkin(Player player, JobRank rank)
        {
            Job job = player.Manager.Job;
            ListDialog editDialog = new ListDialog($"Rangai -> {Colors.Hex.Lime}{rank.Name}{Colors.Hex.Dialog} aprangų nustatymai", "Pasirinkti", "Grįžti");
            List<int> list = new List<int>();
            list.AddRange(rank.AllSkins);
            list.AddRange(job.UnusedSkins().ToArray());
             
            foreach (int skin in list)
            {
                if(Array.IndexOf(rank.AllSkins, skin) > -1)
                    editDialog.AddItem($"ID: {skin}\t\t\t\t\t\t[ {Colors.Hex.Lime}•{Colors.Hex.White} ]");
                else
                    editDialog.AddItem($"ID: {skin}\t\t\t\t\t\t[   ]");
            }
            if (editDialog.Items.Count == 0)
            {
                player.Error("Visos galimos aprangos jau sudėtos ant kitų rangų.");
                EditRankMenu(player, rank);
                return;
            }
            if(rank.AllSkins.Length != 0) editDialog.AddItem($"{Colors.Hex.Red}Nuimti visas aprangas nuo šio rango");
            editDialog.Show(player);
            editDialog.Response += (sender, e) =>
            {
                if(e.DialogButton == DialogButton.Right || e.ListItem < 0)
                {
                    EditRankMenu(player, rank);
                    return;
                }
                if(e.ListItem >= list.Count)//remove skin
                {
                    rank.ClearSkins();
                    ChangeRankSkin(player, rank);
                    Job.Message(player, "Visos aprangos nuo rango nuimtos.");
                }
                else
                {
                    if (Array.IndexOf(rank.AllSkins, list[e.ListItem]) > -1)
                    {
                        rank.Remove(list[e.ListItem]);
                        Job.Message(player, $"$e-$m Apranga $sID: {list[e.ListItem]}$m pašalinta.", true);
                    }
                    else
                    {
                        rank.AllSkins = new int[] { list[e.ListItem] };
                        Job.Message(player, $"$g+$m Pridėta rango apranga $sID: {list[e.ListItem]}$m.", true);
                    }
                    ChangeRankSkin(player, rank);
                }
            };
        }
        public static void DeleteRank(Player player, JobRank rank)
        {
            Job job = player.Manager.Job;
            RichText richText = new RichText(Colors.White, Colors.Yellow);
            richText.SetText($"Pavadinimas: $s{rank.Name}$m\n" +
                "Ar tikrai nori ištrinti rangą?\n");
            MessageDialog dialog = new MessageDialog($"Rangai -> {Colors.Hex.Lime}Ištrinti rangą", richText.ToString(), "Ištrinti", "Atšaukti");
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if(e.DialogButton == DialogButton.Right)
                {
                    EditRankMenu(player, rank);
                }
                else
                {
                    job.RemoveRank(rank);
                    Job.Message(player, "Rangas sėkmingai ištrintas.");
                    RanksList(player);
                }
            };
        }
    }
}
