﻿using RLS.World.Players;
namespace RLS.World.Jobs
{
    public partial class Manager
    {
        public static bool IsInvitable(Player player, Player otherPlayer, Job job)
        {
            int countInDb = job.WorkersCount();
            int invitedCount = job.GetInvitesCount();
            if (countInDb + invitedCount > job.MaxWorkers)
            {
                player.Error($"Šiuo metų darbe yra pakankamai darbuotojų[@[{invitedCount}@] pakviestų, @[{countInDb}@] dirbančių].");
                return false;
            }
            if (job.HasInvite(otherPlayer.Name))
            {
                player.Error($"@[{otherPlayer.Name}@] jau turi pakvietimą į darbą.");
                return false;
            }
            if (job.HasInvite(otherPlayer.Name))
            {
                player.Error($"@[{otherPlayer.Name}@] jau turi pakvietimą į kitą darbą.");
                return false;
            }
            if (otherPlayer.Job.Type == job.Id)
            {
                player.Error($"@[{otherPlayer.Name}@] jau dirba čia.");
                return false;
            }
            return true;
        }
    }
}
