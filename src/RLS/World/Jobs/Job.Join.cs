﻿using RLS.Controllers;
using RLS.Definitions;
using RLS.World.Jobs.Definitions;
using RLS.World.Players;

namespace RLS.World.Jobs
{
    public partial class Job
    {
        private bool CanJoin(Player player)
        {
            if (WorkersCount() >= MaxWorkers)
            {
                player.SendClientMessage(Colors.JobText,
                    $"[{type.GetName()}] Šiuo metu visos darbuotojų vietos užimtos, sugrįžkite veliau.");
                return false;
            }
            if (skins.Count == 0)
            {
                player.SendClientMessage(Colors.JobText,
                    $"[{type.GetName()}] Darbe nėra pridėtų aprangų, todėl į jį įsidarbinti negalima.");
                return false;
            }
            if (player.Job.Type == type)
            {
                player.SendClientMessage(Colors.JobText,
                    $"[{type.GetName()}] Tu jau čia dirbi. Norėdamas palikti darbą /paliktidarba.");
                return false;
            }
            if (player.Experience < ExperienceRequired)
            {
                player.SendClientMessage(Colors.JobText,
                    $"[{type.GetName()}] Tu dar nepakankamai patyręs, šiame darbe galima dirbti tik nuo {Functions.IntFormat(ExperienceRequired)}(trūksta {Functions.IntFormat(ExperienceRequired - player.Experience)}).");
                return false;
            }
            if (withManager && !HasInvite(player.Name))
            {
                player.SendClientMessage(Colors.JobText,
                    $"[{type.GetName()}] Norint čia įsidarbinti reikalingas direktoriaus pakvietimas.");
                return false;
            }
            return true;
        }
        public void Join(Player player)
        {
            if (!CanJoin(player)) return;
            if (withManager)
            {
                Invite invite = GetPlayerInvite(player.Name);
                if (invite == null) return;
                DestroyInvite(invite);
            }
            AddWorker(new Worker(this, player.SqlId, player.Name, player.Job.WarningCount, 0));
            player.Job.SetNew(type);
            player.SendClientMessage(Colors.JobText,
                    $"[{type.GetName()}] Sveikiname įsidarbinus. Pagrindinės darbo komandos /darbas.");
            player.ForcedDataSave();
            JobsController.PlayerJoinedJob(this, player);
        }

        public void Leave(Player player)
        {
            player.Job.SetNew(JobTypes.NONE);
            if (player.Skin != player.Clothes)
                player.Skin = player.Clothes;
            RemoveWorker(GetWorkerByName(player.Name));
            player.SendClientMessage(Colors.JobText,
                    $"[{type.GetName()}] Sėkmingai palikai darbą.");
        }
        public void Kick(Player player)
        {
            player.Job.SetNew(JobTypes.NONE);
            RemoveWorker(GetWorkerByName(player.Name));
            player.SendClientMessage(Colors.JobText,
                    $"[{type.GetName()}] Buvai išmestas iš darbo.");
        }
    }
}
