﻿using RLS.Definitions;
using RLS.World.Players;
using SampSharp.GameMode;

namespace RLS.World.Jobs
{
    public partial class Job
    {
        public static void Message(Player player, string message, bool removeStar = false)
        {
            RichText text = new RichText(Colors.JobText, Colors.JobText.Darken(0.2f));
            if (removeStar)
                player.SendClientMessage(Colors.JobText, $"{text.SetText(message)}");
            else
                player.SendClientMessage(Colors.JobText, $"* {text.SetText(message)}");
        }
        public void Ration(string message)
        {
            foreach (Player pl in Player.All)
            {
                RichText text = new RichText(Colors.Yellow, Colors.Yellow.Darken(0.2f));
                if (pl.Job.Type == Id)
                    pl.SendClientMessage(Colors.Yellow, $"[Racija]: {text.SetText(message)}");
            }
        }
        public static void Ration(Player player, string message)
        {
            foreach (Player pl in Player.All)
            {
                RichText text = new RichText(Colors.Yellow, Colors.Yellow.Darken(0.2f));
                if (pl.Job.Type == player.Job.Type)
                    pl.SendClientMessage(Colors.Yellow, $"[Racija] {player.Name}: {text.SetText(message)}");
            }
        }
        public static void ActionMessage(Player player, string message)
        {
            RichText text = new RichText(Colors.JobText, Colors.JobText.Darken(0.2f));
            Vector3 actionPos = player.Position;
            foreach(Player pl in Player.All)
            {
                if(pl.IsInRangeOfPoint(Config.ActionMessageRange, actionPos) && pl.VirtualWorld == player.VirtualWorld && pl.Interior == player.Interior)
                    pl.SendClientMessage(Colors.Yellow, $"* {player.Name}: {text.SetText(message)}");
            }
        }

    }
}
