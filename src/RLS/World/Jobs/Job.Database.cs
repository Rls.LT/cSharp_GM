﻿using Database;
using RLS.World.Jobs.Definitions;
using System.Collections.Generic;

namespace RLS.World.Jobs
{
    public partial class Job
    {
        private void CreateInviteInDb(Invite invite)
        {
            Database.MySql.Connection.RunVoid(new Query(Configs.Database.job_invites).Insert(new Fields().
                        Add("id", invite.SqlID).
                        Add("job_id", (int)type).
                        Add("player_name", invite.InvitedPlayer).
                        Add("invited_by", invite.Inviter).
                        Add("date", invite.DateString)
                ));
        }
        public void DestroyInvite(Invite invite)
        {
            if (invite != null)
                invites.Remove(invite);
            Database.MySql.Connection.RunVoid(new Query(Configs.Database.job_invites).Delete().Where(new Field("id", invite.SqlID)));
        }
        public static void SaveInvites()
        {
            Database.MySql.Connection.RunVoid(new Query(Configs.Database.job_invites).Delete());
            foreach (Job job in All)
                job.ResaveInvites();

        }
        private void LoadInvites()
        {
            Result res = Database.MySql.Connection.Run(new Query(Configs.Database.job_invites).Select("*"));
            for (int i = 0; i < res.NumRows; i++)
            {
                Fields row = res.GetRow(i);
                CreateInvite(row.GetFieldByName("invited_by").String, row.GetFieldByName("player_name").String, row.GetFieldByName("date").String, false);
            }

        }
        public void ResaveInvites()
        {
            foreach (Invite invite in invites)
            {
                Database.MySql.Connection.RunVoid(new Query(Configs.Database.job_invites).Insert(new Fields().
                    Add("id", invite.SqlID).
                    Add("job_id", (int)type).
                    Add("player_name", invite.InvitedPlayer).
                    Add("invited_by", invite.Inviter).
                    Add("date", invite.DateString)
                ));
            }
        }
        public virtual void LoadData()
        {
            Result qResult = Database.MySql.Connection.Run(new Query(Configs.Database.jobs).Select("*").Where(new Field("id", "" + (int)type)));
            if (qResult.NumRows < 1)
                Database.MySql.Connection.RunVoid(new Query(Configs.Database.jobs).Insert(
                    new Fields().Add("id", (int)type).Add("name", type.GetName())
                    ));
            else //load job data
            {
                budget.Add(qResult.GetRow(0).GetFieldByName("budget").Int());
                System.Console.WriteLine("Loading ranks");
                ranks = Json.DecodeJson<List<JobRank>>(qResult.GetRow(0).GetFieldByName("ranks").String);
                System.Console.WriteLine("Ranks loaded");
            }
            LoadWorkers();
            LoadInvites();
        }
        private void LoadWorkers()
        {
            Result qResult = Database.MySql.Connection.Run(new Query(Configs.Database.players).Select("id, name, job").Where(new Field("job", "%\"Type\":" + (int)type + "%", " LIKE ")));
            for (int i = 0; i < qResult.NumRows; i++)
            {
                Fields row = qResult.GetRow(i);
                JobInfo playerJob = Json.DecodeJson<JobInfo>(row.GetFieldByName("job").String);
                Worker worker = new Worker(this, row.GetFieldByName("id").Int(), row.GetFieldByName("name").String, playerJob.WarningCount, playerJob.Rank);
                AddWorker(worker);
            }
        }
        public virtual void SaveData()
        {
            //main data
            Database.MySql.Connection.RunThreadedVoid(new Query(Configs.Database.jobs).Update(new Fields().
                Add("name", type.GetName()).
                Add("budget", budget.Money).
                Add("ranks", Json.EncodeJson(ranks))
            ).Where(new Field("id", "" + (int)type)));
            Log.Write($"[Jobs] Saved {(int)type} job data.");
        }
    }
}
