﻿using Newtonsoft.Json;
using RLS.World.Jobs.Definitions;
using System;
using System.Collections.Generic;


namespace RLS.World.Jobs
{
    public partial class Job
    {
        private List<JobRank> ranks = new List<JobRank>();
        
        public JobRank[] AllRanks { get => ranks.ToArray(); }

        public void AddNewRank(JobRank rank)
        {
            ranks.Add(rank);
        }
        public int GetRankId(JobRank rank)
        {
            return ranks.IndexOf(rank);
        }
        public JobRank GetRankInPos(int id)
        {
            if (id >= ranks.Count)
                return null;
            else
                return AllRanks[id];
        }
        public void RemoveRank(JobRank rank)
        {
            ranks.Remove(rank);
        }
        public bool IsSkinUsed(int skin)
        {
            foreach (JobRank rank in AllRanks)
            {
                if (Array.IndexOf(rank.AllSkins, skin) > -1)
                    return true;
            }
            return false;
        }
        public List<int> UnusedSkins()
        {
            List<int> list = new List<int>();
            foreach (int skin in AllSkins)
            {
                if (!IsSkinUsed(skin))
                    list.Add(skin);
            }
            return list;
        }
        public List<int> AvailableSkinsForRank(int rankId)
        {
            List<int> list = new List<int>();
            for(int i = 0; i <= rankId && i<AllRanks.Length; i++)
            {
                JobRank rank = AllRanks[i];
                if (rank.AllSkins.Length != 0)
                    list.AddRange(rank.AllSkins);
            }
            return list;
        }
    }
}
