﻿using RLS.Definitions;
using RLS.World.Jobs.Definitions;
using RLS.World.Players;
using SampSharp.GameMode.Definitions;
using SampSharp.GameMode.Display;
using SampSharp.GameMode.SAMP.Commands;
using System.Collections.Generic;

namespace RLS.World.Jobs
{
    public partial class Job
    {
        [Command("paliktidarba")]
        public static void LeaveJobCommand(Player player)
        {
            Job job = GetById(player.Job.Type);
            if (job == null)
            {
                player.Error("Tu niekur nedirbi.");
                return;
            }
            if (!CanPlayerUseCommand(player, job.type)) return;
            job.Leave(player);
        }

        [Command("kviesti")]
        public static void RequestWorker(Player player)
        {
            TablistDialog dialog = new TablistDialog("Tarnybos iškvietimas", new[] { "Tarnyba", "Prisijungusių darbuotojų skaičius"}, "Kviesti", "Uždaryti");
            List<Job> jobsInvitable = new List<Job>();
            foreach (Job job in All)
            {
                if (job.IsInvitable)
                {
                    jobsInvitable.Add(job);
                    dialog.Add(new[] { job.Id.GetName(), "" + job.OnlineWorkersCount() });
                }
            }
            if (dialog.Count<1)
            {
                player.Error("Nėra nei vienos tarnybos, kurią būtų galima iškviesti.");
                return;
            }
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right)
                    return;
                if (jobsInvitable.Count <= e.ListItem)
                    return;
                Job job = jobsInvitable[e.ListItem];
                if (job.HasRequest(player))
                {
                    player.Error("Atšaukei iškviestą tarnybą.");
                    job.RemoveRequest(player);
                    return;
                }
                if (job.OnlineWorkersCount()<1)
                {
                    player.Error($"$s{job.Id.GetName()}$m šiuo metu neturi aktyvių darbuotojų.");
                    return;
                }
                job.AddRequest(player);
                player.SuccessMessage($"$s{job.Id.GetName()}$m iškviestas. Gausi pranešimą, vos tik kas sureaguos į tavo iškvietimą.");
            };
        }
        [Command("vaziuoju")]
        public static void AnswerRequest(Player player, Player otherPlayer = null)
        {
            if (player.Job.Type == JobTypes.NONE)
                return;
            if(otherPlayer == null)
            {
                player.SendClientMessage(Colors.CommandText, "Priimti iškvietimą: {59C4C5}/vaziuoju [žaidėjas]");
                return;
            }
            if(player == otherPlayer)
            {
                player.Error("Važiuoti į savo iškvietimus negali.");
                return;
            }
            Job job = GetById(player.Job.Type);
            if (job == null)
                return;
            if(!job.IsInvitable)
            {
                player.Error("Tavo darbas negali priimti iškvietimų.");
                return;
            }
            if (!job.HasRequest(otherPlayer))
            {
                player.Error("Žaidėjas nekviečia tavo tarnybos");
                return;
            }
            Request request = job.GetPlayerRequest(otherPlayer);
            if (request == null || request.RequestState != RequestState.Requested)
            {
                player.Error("Šio iškvietimo priimti nebegali.");
                return;
            }
            request.RequestState = RequestState.Accepted;
            job.Response(request, player);
            player.SetPlayerMarker(otherPlayer, Colors.Yellow);
            Ration(player, $"Priėmiau $s{otherPlayer.Name}$m iškvietimą");
            otherPlayer.SuccessMessage($"$s{player.Name} [{player.Id}]$m priėmė tavo iškvietimą.");
        }
        [Command("darbas", PermissionChecker = typeof(IsPlayerInAnyJob))]
        public static void JobCommands(Player player)
        {
            Job job = GetById(player.Job.Type);
            if (!CanPlayerUseCommand(player, job.type)) return;
            job.Commands(player);
        }

        [Command("uniforma", PermissionChecker = typeof(IsPlayerInAnyJob))]
        public static void JobSkinsCommands(Player player)
        {
            Job job = GetById(player.Job.Type);
            if (!CanPlayerUseCommand(player, job.type)) return;
            List<int> skinList = job.AvailableSkinsForRank(player.Job.Rank);
            if(skinList.Count == 0)
            {
                player.Error("Tavo rangui negalima jokia apranga, pranešk direktoriui!");
                return;
            }
            ListDialog dialog = new ListDialog("Darbo uniformos pasirinkimas", "Pasirinkti", "Atšaukti");
            foreach (int skin in skinList)
                dialog.AddItem("ID: " + skin);
            dialog.Show(player);
            dialog.Response += (sender, e) =>
            {
                if (e.DialogButton == DialogButton.Right || e.ListItem >= skinList.Count || e.ListItem < 0)
                    return;
                player.Skin = skinList[e.ListItem];
                Job.Message(player, $"Apsirengei unformą ID: $s{skinList[e.ListItem]}$m");
            };
        }

        [Command("anuliuoti pakvietima")]
        public static void CancelInvite(Player player)
        {
            Job job = GetById(Job.HasInviteToJob(player.Name));
            if (job == null)
            {
                player.Error("Tu neturi pakvietimo į darbą.");
                return;
            }
            Definitions.Invite invite = job.GetPlayerInvite(player.Name);
            if (invite == null)
            {
                player.Error("Tu neturi pakvietimo į darbą.");
                return;
            }
            Job.Message(player, $"Atšaukei pakvietimą į \"@[{job.Id.GetName()}@]\"");
            job.DestroyInvite(invite);
        }

        [Command("ispejimai darbo")]
        public static void JobWarningShow(Player player)
        {
            player.Job.ShowWarningList(player);
        }

        protected virtual void RegisterCommands()
        {
            AddCommand("paliktidarba", "Darbo palikimas");
            AddCommand("darbas", "Visos darbo komandos");
            AddCommand("uniforma", "Darbo aprangos pasikeitimas");
        }
        /// <summary>
        /// Check if player have privileges to job
        /// </summary>
        /// <param name="player">Player to check</param>
        /// <param name="type">job type</param>
        /// <param name="print">Print error message</param>
        /// <returns></returns>
        public static bool CanPlayerUseCommand(Player player, JobTypes type, bool print = false)
        {
            if (type == player.Job.Type)
                return true;
            else
            {
                if (print) player.Error("Tu neturi privilegijų, kad panaudotum šią komandą.");
                return false;
            }
        }
    }
}
