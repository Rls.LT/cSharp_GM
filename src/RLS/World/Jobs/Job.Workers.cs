﻿using RLS.World.Jobs.Definitions;
using System.Collections.Generic;

namespace RLS.World.Jobs
{
    public partial class Job
    {
        private List<Worker> workers = new List<Worker>();
        public Worker[] AllWorkers { get => workers.ToArray(); }
        public Worker GetWorkerByName(string playerName)
        {
            foreach (Worker worker in AllWorkers)
            {
                if (worker.Name.Equals(playerName))
                    return worker;
            }
            return new Worker();
        }
        public void RemoveWorker(Worker worker)
        {
            workers.Remove(worker);
        }
        public void AddWorker(Worker worker)
        {
            workers.Add(worker);
        }
        public int WorkersCount()
        {
            return AllWorkers.Length;
        }
        public int OnlineWorkersCount()
        {
            int count = 0;
            foreach (Worker worker in AllWorkers)
            {
                if (worker.IsOnline())
                    count++;
            }
            return count;
        }
    }
}
