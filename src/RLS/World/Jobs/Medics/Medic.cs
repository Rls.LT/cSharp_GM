﻿using SampSharp.GameMode;

namespace RLS.World.Jobs.Medics
{
    partial class Medic : Job
    {
        protected override void JobConfig()
        {
            type = JobTypes.MEDIC;
            joinLocation = new Vector3(-2730.2681, 725.6486, 1381.7000);
            gpsLocation = new Vector3(-2730.2681, 725.6486, 11.7000);
            requiredExperience = 2000;
            withManager = true;
            base.JobConfig();
        }

        protected override void JobSkins()
        {
            skins.Add(274);
            skins.Add(275);
            skins.Add(276);
            skins.Add(308);
            base.JobSkins();
        }
    }
}
