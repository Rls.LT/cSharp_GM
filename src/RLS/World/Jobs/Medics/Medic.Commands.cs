﻿using RLS.World.Players;
using SampSharp.GameMode.SAMP.Commands;

namespace RLS.World.Jobs.Medics
{
    partial class Medic
    {
        protected override void RegisterCommands()
        {
            AddCommand("pagydyti", "Pagydyti žaidėją");
            base.RegisterCommands();
        }

        [Command("pagydyti")]
        public static void OfficerBarrier(Player player, Player otherPlayer = null)
        {
            if (!CanPlayerUseCommand(player, JobTypes.MEDIC))
                return;
            if (otherPlayer == null)
            {
                player.Command("/pagydyti [žaidėjas]", "Pagydyti žaidėją");
                return;
            }
            if (!player.IsInRangeOfPoint(3f, otherPlayer.Position))
            {
                player.Error($"Žaidėjas per toli.");
                return;
            }
            Job.Ration(player, $"Pagydžiau $s{otherPlayer.Name}$m.");
            otherPlayer.Health = 100f;
            otherPlayer.SuccessMessage($"Tave pagydė $s{player.Name}$m.");

        }
    }
}
