﻿using RLS.World.Players;

namespace RLS.World.Jobs.Definitions
{
    public partial class Worker
    {
        private string playerName = "empty";
        private int warnings;
        private int sqlId;
        private Player player;
        private int rank;
        private Job job;
        public string Name{ get => playerName; }
        public int Warnings { get => warnings; }
        public Worker() { }
        public bool IsEmpty { get => playerName.Equals("empty"); }
        public Worker(Job job, int sqlId, string playerName, int warnings, int rank)
        {
            this.job = job;
            this.sqlId = sqlId;
            this.playerName = playerName;
            this.warnings = warnings;
            this.rank = rank;
        }
        public bool IsOnline()
        {
            return Player.GetByFullName(playerName) != null;
        }
        public void AddMoney(Player by, int amount, bool online = false)
        {
            player = Player.GetByFullName(playerName);
            if (player == null)//offline
            {
                if(online==false) UpdateOfflinePlayer(job.Id, amount);
            }
            else//online
            {
                player.GiveMoney(amount);
                Job.Message(player, $"@[{Name}@] išdalino premijas, tu gavai @[{amount}@].");
            }
        }
        public void Unwarn(Player by)
        {
            if (warnings < 1) return;
            player = Player.GetByFullName(playerName);
            warnings--;
            if (player == null)//offline
            {
                UpdateOfflinePlayer(job.Id);
            }
            else//online
            {
                player.Job.Unwarn(by.Name);
            }
            Job.Message(by, $"Įspėjimas @[{Name}@] nuimtas.");
        }
        public void Warn(Player warnedBy, string reason)
        {
            player = Player.GetByFullName(playerName);
            warnings++;
            if (player==null)//offline
            {
                if(warnings>=Config.Job.WarningsToKick)
                    UpdateOfflinePlayer(JobTypes.NONE);
                else
                    UpdateOfflinePlayer(job.Id);
            }
            else//online
            {
                player.Job.Warn(reason, warnedBy.Name);
            }
            Job.Message(warnedBy, $"Įspėjimas @[{Name}@] uždėtas.");
            InsertWarning(warnedBy, reason);
        }
        public void Kick(Player by)
        {
            player = Player.GetByFullName(playerName);
            if (player == null)//offline
            {
                warnings = 0;
                UpdateOfflinePlayer(JobTypes.NONE);
                job.RemoveWorker(this);
            }
            else//online
            {
                job.Kick(player);
                player.ForcedDataSave();
            }
            Job.Message(by, $"@[{Name}@] išmestas iš darbo.");
        }
        public void CheckIfRankValid(Player by)
        {
            player = Player.GetByFullName(playerName);
            if (player == null)//offline
            {
                if (rank>=job.AllRanks.Length)
                    rank = 0;
            }
            else//online
            {
                if (player.Job.Rank >= job.AllRanks.Length)
                    player.Job.Rank = 0;
                Job.Message(player, $"@[{by.Name}@] Ištrynė rangą, todėl tu perkeliamas į pradinį.");
            }
        }
    }
}
