﻿using System.Collections.Generic;
using System.ComponentModel;

namespace RLS.World.Jobs.Definitions
{
    public class JobRank
    {
        public string Name { get; set; }
        [DefaultValue(0)]
        public int[] AllSkins { get => skins.ToArray(); set => skins.AddRange(value); }
        public void ClearSkins() => skins.Clear();
        public void Remove(int skin) => skins.Remove(skin);
        private List<int> skins = new List<int>();
        public JobRank(string name)
        {
            if(name.Length>Config.Job.MaxJobRankNameLength)
                Name = name.Substring(0, 30);
            else
                Name = name;
        }
    }
}
