﻿using RLS.World.Players;
using SampSharp.GameMode.Display;
using Newtonsoft.Json;
using Database;

namespace RLS.World.Jobs.Definitions
{

    public class JobInfo
    {
        public JobTypes Type { get; set; }
        public int WarningCount { get; set; }
        private int rank;
        public int Rank
        {
            get => rank;
            set {
                Job job = Job.GetById(Type);
                if (job == null)
                {
                    rank = 0;
                    return;
                }
                else if (value >= job.AllRanks.Length)
                {
                    rank = 0;
                    return;
                }
                rank = value;
            }
        }
        [JsonIgnore]
        private Player player;
        public JobInfo()
        {
            Type = JobTypes.NONE;
            WarningCount = 0;
            Rank = 0;
        }
        public JobInfo(Player player, JobTypes job, int warnings)
        {
            this.player = player;
            Type = job;
            WarningCount = warnings;
        }
        public JobInfo(Player player, int job, int warnings)
        {
            this.player = player;
            Type = (JobTypes)job;
            WarningCount = warnings;
        }
        public JobInfo Player(Player player)
        {
            this.player = player;
            return this;
        }
        public void Warn(string reason, string warned_by)
        {
            WarningCount++;
            Job.Message(player, $"@[{warned_by}@] tave įspėjo už: @[{reason}@].");
            if (WarningCount >= Config.Job.WarningsToKick)
            {
                Job job = Job.GetById(Type);
                if(job!=null)
                    job.Kick(player);
            }
            player.ForcedDataSave();
        }

        public void Unwarn(string name)
        {
            if (WarningCount < 1) return;
            WarningCount--;
            Job.Message(player, $"@[{name}@] tau nuėmė įspėjimą.");
            player.ForcedDataSave();
        }

        public void DeleteWarningLog(int warningCount=-1)
        {
            if (warningCount < 1)
            {
                Result res = Database.MySql.Connection.Run(new Query(Configs.Database.job_warnings).Select("id").Where(new Field("player_id", player.SqlId)));
                warningCount = res.NumRows;
            }
            if(warningCount > Config.Job.MaxWarningsInDb)
                Database.MySql.Connection.RunThreadedVoid(new Query(Configs.Database.job_warnings).Delete().Where(new Field("player_id", player.SqlId)).OrderBy("id").Limit(warningCount - Config.Job.MaxWarningsInDb));
        }
        public void ShowWarningList(Player forPlayer)
        {
            Result res = Database.MySql.Connection.Run(new Query(Configs.Database.job_warnings).Select("*").Where(new Field("player_id", player.SqlId)).OrderBy("id", false));
            DeleteWarningLog(res.NumRows);
            if (res.NumRows==0)
            {
                forPlayer.Error($"@[{player.Name}@] įspėjimų istorija tuščia.");
                return;
            }
            TablistDialog warningsDialog = new TablistDialog($"{player.Name} darbo įspėjimai", new[] { "Data", "Įspėjo", "Priežastis" }, "Uždaryti");
            for(int i = 0; i < res.NumRows; i++)
            {
                warningsDialog.Add(new[] 
                {
                    res.GetRow(i).GetFieldByName("date").String,
                    res.GetRow(i).GetFieldByName("warned_by").String,
                    res.GetRow(i).GetFieldByName("reason").String
                });
                if (i + 1 == Config.Job.MaxWarningsInDb)
                    break;
            }
            warningsDialog.Show(player);
        }
        public void SetNew(JobTypes job)
        {
            WarningCount = 0;
            Type = job;
        }
    }
}
