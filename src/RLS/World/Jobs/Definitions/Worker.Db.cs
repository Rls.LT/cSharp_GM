﻿using Database;
using RLS.Controllers;
using RLS.World.Players;

namespace RLS.World.Jobs.Definitions
{
    public partial class Worker
    {
        public void InsertWarning(Player warnedBy, string reason)
        {
            Database.MySql.Connection.RunVoid(new Query(Configs.Database.job_warnings).Insert(new Fields().
                Add("player_id", sqlId).
                Add("warned_by", warnedBy.Name).
                Add("reason", reason).
                Add("date", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
            ));
        }
        public void UpdateOfflinePlayer(JobTypes newJobType, int moneyGive=0)
        {
            if (moneyGive == 0)
            {
                JobInfo jobinfo = new JobInfo(null, newJobType, warnings)
                {
                    Rank = rank
                };
                Fields field = new Fields().Add("job", Json.EncodeJson(jobinfo));
                if (newJobType == JobTypes.NONE)
                    field.Add(new Field("current_skin", "skin", "="));
                Database.MySql.Connection.RunThreadedVoid(new Query(Configs.Database.players).Update(field).Where(new Field("id", sqlId)));
            }
            else
            {
                Query query = new Query(Configs.Database.players).Update(new Fields().
                    Add(new Field("money_in_pocket", moneyGive, "= money_in_pocket + "))
                ).Where(new Field("id", sqlId));
                System.Console.WriteLine(query.Format());
                Database.MySql.Connection.RunThreadedVoid(query);
            }
        }
    }
}
