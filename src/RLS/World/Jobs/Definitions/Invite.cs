﻿using RLS.Definitions;

namespace RLS.World.Jobs.Definitions
{
    public class Invite
    {
        private static int id = 1;
        private DateTime dateTime;
        private string by;
        private string to;
        private int sqlId = 0;
        public int SqlID { get => sqlId; }
        public Invite()
        {
            dateTime = new DateTime(System.DateTime.Now);
            by = "unknown";
        }

        public Invite(string by, string to)
        {
            this.by = by;
            dateTime = new DateTime(System.DateTime.Now);
            this.to = to;
            sqlId = id;
            id++;
        }
        public Invite(string by, string to, string date)
        {
            FromDatabase(by, date);
            this.to = to;
            sqlId = id;
            id++;
        }
        public string InvitedPlayer { get => to; }
        public string Inviter { get => by; set => by = value; }
        public DateTime DateTime { get => dateTime; }
        public string DateString { get => dateTime.ToString(); }
        public void FromDatabase(string by, string date)
        {
            if(date=="now") dateTime = new DateTime(System.DateTime.Now);
            else dateTime = new DateTime(date);
            this.by = by;
        }
    }
}
