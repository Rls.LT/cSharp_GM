﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLS.World.Jobs
{
    public enum JobTypes
    {
        NONE,
        POLICE,
        MEDIC,
        TAXI
    }
    static class WorkTypesMethods
    {

        public static String GetName(this JobTypes s1)
        {
            switch (s1)
            {
                case JobTypes.POLICE:
                    return "Policijos departamentas";
                case JobTypes.MEDIC:
                    return "Sveikatos priežiūros centras";
                case JobTypes.TAXI:
                    return "Taksi";
                default:
                    return "Neįvestas";
            }
        }

        public static String GetWorkerName(this JobTypes s1)
        {
            switch (s1)
            {
                case JobTypes.NONE:
                    return "Bedarbis(-ė)";
                case JobTypes.POLICE:
                    return "Policininkas(-ė)";
                case JobTypes.MEDIC:
                    return "Medikas(-ė)";
                case JobTypes.TAXI:
                    return "Taksistas(-ė)";
                default:
                    return "Bedarbis";
            }
        }
    }
}
