﻿using RLS.World.Players;
using System.Collections.Generic;

namespace RLS.World.Jobs.Definitions
{
    public enum RequestState
    {
        Requested,
        Accepted
    }
    public class Request
    {
        public Player RequestingPlayer { get; private set; }
        public RequestState RequestState { get; set; }
        public Player Responser { get; set; }
        public Request(Player requester)
        {
            RequestingPlayer = requester;
            RequestState = RequestState.Requested;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Request request = obj as Request;
            if (request == null) return false;
            else return RequestingPlayer.SqlId.Equals(request.RequestingPlayer.SqlId);
        }

        public override int GetHashCode()
        {
            return 927628175 + EqualityComparer<Player>.Default.GetHashCode(RequestingPlayer);
        }
    }
}
