﻿namespace RLS.World.Jobs.Definitions
{
    public class JobSkin
    {
        public int Id { get; set; }

        public JobSkin(int skinId)
        {
            Id = skinId;
        }
    }
}
