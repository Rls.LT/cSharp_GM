﻿using System;
using RLS.World.Players;
using SampSharp.GameMode.SAMP.Commands.PermissionCheckers;
using SampSharp.GameMode.World;

namespace RLS.World.Jobs.Definitions
{
    class IsPlayerInAnyJob : IPermissionChecker
    {
        #region Implementation of IPermissionChecker
        /// <summary>
        ///     Gets the message displayed when the player is denied permission.
        /// </summary>
        public string Message
        {
            get { return "Kad panaudotum šią komandą turi būti įsidarbinęs."; }
        }

        /// <summary>
        ///     Checks the permission for the specified player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>true if allowed; false if denied.</returns>
        public bool Check(BasePlayer _player)
        {
            Player player = _player as Player;
            if (player.Job.Type == JobTypes.NONE || Job.GetById(player.Job.Type) == null)
                return false;
            return true;
        }
        #endregion
    }
}
