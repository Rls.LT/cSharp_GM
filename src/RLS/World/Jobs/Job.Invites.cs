﻿using RLS.World.Jobs.Definitions;
using System.Collections.Generic;

namespace RLS.World.Jobs
{
    public partial class Job
    {
        private List<Invite> invites = new List<Invite>();
        public Invite[] AllInvites { get => invites.ToArray(); }
        public void CreateInvite(string player, string inviteTo, string date = "now", bool createDbRow = true)
        {
            Invite invite = new Invite(player, inviteTo, date);
            invites.Add(invite);
            if (createDbRow)
                CreateInviteInDb(invite);
        }
        public bool HasInvite(string playerName)
        {
            return GetPlayerInvite(playerName) != null;
        }
        public Invite GetInviteById(int id)
        {
            if (id >= AllInvites.Length)
                return null;
            return AllInvites[id];
        }
        public Invite GetPlayerInvite(string playerName)
        {
            foreach (Invite invite in AllInvites)
            {
                if (playerName.Equals(invite.InvitedPlayer))
                    return invite;
            }
            return null;
        }
        public int GetInvitesCount()
        {
            return AllInvites.Length;
        }
        public static JobTypes HasInviteToJob(string playerName)
        {
            foreach(Job job in All)
            {
                if (job.HasInvite(playerName))
                    return job.Id;
            }
            return JobTypes.NONE;
        }
    }
}
