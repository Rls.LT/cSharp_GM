﻿using SampSharp.GameMode;
using System.Collections.Generic;

namespace RLS.World
{
    partial class Gates
    {
        private static List<Gates> gates = new List<Gates>();
        public static Gates[] All { get => gates.ToArray(); }
        public static Gates FindClose(Vector3 postition, float range=4f)
        {
            foreach(Gates gate in All)
            {
                if (gate.closedPosition.DistanceTo(postition) < range || gate.openedPosition.DistanceTo(postition) < range)
                    return gate;
            }
            return null;
        }
    }
}
