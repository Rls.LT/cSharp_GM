﻿using RLS.Events.Args;
using RLS.Streamer;
using RLS.World.Players;
using SampSharp.GameMode;
using System;

namespace RLS.World
{
    public partial class Gates 
    {
        private DynamicObject gateObject;
        private Vector3 closedPosition;
        private Vector3 closedRotation;
        private Vector3 openedPosition;
        private Vector3 openedRotation;
        private int model;
        private bool closed = true;
        private float speed = 0.5f;

        public bool IsClosed { get => closed; }

        public event EventHandler<GatesArgs> Opening;
        public event EventHandler<GatesArgs> Closing;

        public Gates(int model, Vector3 closedPosition, Vector3 closedRotation)
        {
            Closed(closedPosition, closedRotation);
            this.model = model;
            gateObject = new DynamicObject(model, closedPosition, closedRotation);
            gates.Add(this);
        }

        public void Dispose()
        {
            if(gateObject!=null)
                gateObject.Dispose();
            gates.Remove(this);
        }

        public Gates SetSpeed(float speed)
        {
            this.speed = speed;
            return this;
        }

        public Gates Closed(Vector3 closedPosition, Vector3 closedRotation)
        {
            this.closedPosition = closedPosition;
            this.closedRotation = closedRotation;
            if (closed)
            {
                if(gateObject != null)
                    gateObject.Dispose();
                gateObject = new DynamicObject(model, closedPosition, closedRotation);
            };
            return this;
        }

        public Gates Opened(Vector3 openPosition, Vector3 openRotation)
        {
            this.openedPosition = openPosition;
            this.openedRotation = openRotation;
            if (!closed)
            {
                gateObject.Dispose();
                gateObject = new DynamicObject(model, openPosition, openRotation);
            };
            return this;
        }

        public void Close(Player player)
        {
            if (IsClosed) return;
            GatesArgs close = new GatesArgs(this, player, false);
            Closing?.Invoke(this, close);
            if (close.Cancel)
                return;
            closed = true;
            gateObject.Move(closedPosition, speed, closedRotation);
        }

        public void Open(Player player)
        {
            if (!IsClosed) return;
            GatesArgs open = new GatesArgs(this, player, true);
            Opening?.Invoke(this, open);
            if (open.Cancel)
                return;
            closed = false;
            gateObject.Move(openedPosition + new Vector3(0, 0, 0.01), speed, openedRotation);//+ another vector for moving slow
        }

    }
}
