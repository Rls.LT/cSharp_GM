﻿using SampSharp.GameMode;
using System;

namespace RLS
{
    partial class Functions
    {
        public static Vector3 PointFarFromPoint(double distance, double rotation)
        {
            return new Vector3(distance * Math.Cos(DegreesInRadians(rotation)), distance * Math.Sin(DegreesInRadians(rotation)));
        }
        public static double DegreesInRadians(double degrees)
        {
            return (degrees * (Math.PI / 180));
        }
    }
}
