﻿using SampSharp.GameMode;

namespace RLS
{
    abstract class Config
    {
        public static LogLevel Logging = LogLevel.All;

        public static string[] ValidMailEndings = { "gmail.com", "mail.ru", "yahoo.com", "inbox.lt" };
        public static Vector3 historySelection_Pos = new Vector3(-2482.2939, -284.4624, 40.5942);
        public static Vector3 historySelection_CameraPos = new Vector3(-2482.4824, -274.2534, 41.8549);
        public static Vector3 historySelection_CameraLookAt = new Vector3(-2482.4573, -275.2573, 41.7298);

        public static float ActionMessageRange = 15f;
        public static int MaxWantedLevel = 600;
        public static int MoneyPerStar = 10;
        public static int InHospitalAfterDeath = 30;//in seconds
        public abstract class Vehicle
        {
            public static int UpdateTimer = 500;//ms
            public static int MaxOwned = 2;
        }
        public abstract class Property
        {
            public static int MaxSellPrice = 10000000;
            public static int SellingPrice = 500;
            public static int ResellLock = 3; // in days
            public static int OwningLimit = 1;
        }
        public abstract class Business
        {
            public static int FromTotalPlayed = 5000;
            public static int PumpPrice = 50000;
            public static int OwningLimit = 1;
        }
        public abstract class House
        {
            public static int FromTotalPlayed = 5000;
            public static int OwningLimit = 1;
        }
        public abstract class Job
        {
            public static int MaxWorkers = 20;
            public static int MaxJobRanks = 8;
            public static int MaxJobRankNameLength = 30;
            public static int WarningsToKick = 3;
            public static int MaxWarningsInDb = 5;
            public abstract class Medics
            {
                
            }
            public abstract class Taxi
            {
                public static double PricePerKm = 10;
            }
        }
    }
}
